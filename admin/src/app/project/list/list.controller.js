export class ProjectListController {
  constructor($log, projectService, $timeout, sectorService, regionService, fundingService) {
    'ngInject';
    this.$log = $log.log
    this.$timeout = $timeout
    this.sectorService = sectorService
    this.regionService = regionService
    this.fundingService = fundingService
    this.$log('ArticleListController initialized')
    this.service = projectService
    this.filterSector = ''
    this.filterRegion = ''
    this.filterFunding = ''
    this.pageCount = 0
    this.search = ''
    this.allData = 0
    this.count = 0
    this.total = 0
    this.listData = []
    this.page = 1
    this.limit = 10
    this.isTableLoading = true
    this.order = 'id DESC'
    this.reqeust = {
      limit: this.limit
    }
    this.loadSectors()
    this.loadRegions()
    this.loadFundings()
  }

  doSearch() {
    this.$timeout(() => {
      if (this.search.length > 2 || this.search === '') {
        this.page = 1;
        this.load();
      }
    }, 500)
  }

  loadSectors() {
    this.sectorService.list().then(items => {
      this.sectors = items
    })
  }

  loadRegions() {
    this.regionService.list().then(items => {
      this.regions = items
    })
  }

  loadFundings() {
    this.fundingService.list().then(items => {
      this.fundings = items
    })
  }

  load() {
    if (this.page) this.reqeust.offset = this.page - 1
    if (this.search)
      this.reqeust.string = this.search
    else
      delete this.reqeust.string

    if (this.filterSector)
      this.reqeust.branch_id = this.filterSector
    else
      delete this.reqeust.branch_id

    if (this.filterRegion)
      this.reqeust.region_id = this.filterRegion
    else
      delete this.reqeust.region_id

    if (this.filterFunding)
      this.reqeust.source_id = this.filterFunding
    else
      delete this.reqeust.source_id

    this.service.list(this.reqeust).then(articles => {
      this.pageCount = Math.ceil(articles.total / this.limit);
      this.listData = articles.list
      this.total = articles.total
    })
  }
}
