export class LogsListController {
  constructor(logsService, $timeout) {
    'ngInject';
    this.service = logsService
    this.$timeout = $timeout
    this.pageCount = 0
    this.search = ''
    this.allData = 0
    this.count = 0
    this.listData = []
    this.pageCount = 0
    this.search = ''
    this.limit = 10
    this.reqeust = {
      limit: this.limit
    }
  }

  doSearch() {
    this.$timeout(() => {
      if (this.search.length > 2 || this.search === '') {
        this.page = 1;
        this.load();
      }
    }, 500)
  }

  load() {

    if (this.page) this.reqeust.offset = this.page - 1
    if (this.search)
      this.reqeust.string = this.search
    else
      delete this.reqeust.string

    this.service.list(this.reqeust).then(items => {
      this.listData = items.list
      this.total = items.total
      this.pageCount = Math.ceil(this.total / this.limit);
    })
  }

}
