'use strict'
import inc from './include'

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('page', {
    name: DataTypes.STRING,
    name_en: DataTypes.STRING,
    content: DataTypes.STRING,
    content_en: DataTypes.STRING,
    sidebar: DataTypes.ENUM('event', 'project', 'report')
  }, {
    classMethods: {
      findByIdsWIthMenu (ids) {
        const include = inc(sequelize.models)
        return this.findAll({
          where: {
            id: ids
          },
          include: include({
            menu: {
              parent: {
                parent: true
              }
            }
          })
        })
      }
    }
  })
}

