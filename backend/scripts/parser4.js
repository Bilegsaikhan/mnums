let fs = require('fs');
let request = require('request');
let fall = require('async-waterfall');
let count = 0;

fs.readFile('dd.json', (err, data) => {
  let jsonData = JSON.parse(data);
  let categories = [];

  request.get('http://43.231.113.151:5000/api/product_cat', (err, res, body) => {
    let jsonBody = JSON.parse(body);
    jsonBody.forEach((category) => {
      categories.push({id: category.id, name: category.name, type: 'cat'})
      category.productSubCats.forEach((subCategory) => {
        categories.push({id: subCategory.id, name: subCategory.name, type: 'subCat', product_cat_id: subCategory.product_cat_id})
      })
    })
    
    let tasks = [];

    jsonData.forEach((entry) => {
      let task = (callback) => {
        try {
          let tempCat = categories.find((found) => {
            return found.name === entry.category
          })

          let requestObject = {}
          if (tempCat.type === 'cat') {
            requestObject.product_cat_id = tempCat.id
          } else {
            requestObject.product_sub_cat_id = tempCat.id
            requestObject.product_cat_id = tempCat.product_cat_id
          }

          requestObject.name = entry.title;
          requestObject.name_en = entry.title;
          requestObject.mn = true;
          requestObject.en = true;
          requestObject.desc = entry.shortDetail;
          requestObject.desc_en = entry.shortDetail;
          requestObject.content = entry.longDetail;
          requestObject.content_en = entry.longDetail;
          requestObject.price = '';
          requestObject.provider_id = null;

          let images = [];

          entry.images.forEach((image, idx) => {
            let is_main = false;
            if (idx === 0) {
              is_main = true;
            }
            images.push({url: image, is_main: is_main});
          })

          requestObject.images = images;
          console.log('~~~~~~~~~~~~~~~~posting', count++);
          console.log('aa', requestObject);
          request.post({
            url: 'http://43.231.113.151:5000/api/product',
            form: requestObject
          }, (err, res, body) => {
            if (err) throw err;
            if (res.statusCode !== 200) {
              throw new Error(body);
            }
            callback();
          });
        } catch (error) {
          callback(error);
        }
      }

      tasks.push(task);
    })

    console.log('tasks', tasks.length);

    fall(tasks, (error) => {
      if (error) {
        console.log('error')
      }
      console.log('done');
    })
  })



//   requestObject = {
//     product_cat_id: '',
//     product_sub_cat_id: '',
//     mn: true,
//     en: true,
//     name: '',
//     name_en: '',
//     desc: '',
//     desc_en: '',
//     content: '',
//     content_en: '',
//     price: '',
//   }
})
