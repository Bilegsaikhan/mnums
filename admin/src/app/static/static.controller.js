export class StaticController {
  constructor($state) {
    'ngInject';
    this.$state = $state
    this.pageCount = 0
    this.search = ''
    this.allData = 0
    this.count = 0
    this.page = 1
    this.limit = 10
    this.isTableLoading = true
    this.order = 'id DESC'
    this.staticsObj = {}

    this.staticsList = {
        // job: [
        //   {
        //     key: 'job_dbm',
        //     name: 'МОНГОЛЫН ЭРҮҮЛ МЭНДИЙН ҮҮСГЭЛ СУДАЛГААНЫ ТӨВ-НД АЖИЛЛАХ',
        //     type: 'textarea'
        //   }, {
        //     key: 'job_open',
        //     name: 'Нээлттэй ажлын байр',
        //     type: 'textarea'
        //   }, {
        //     key: 'job_training',
        //     name: 'Сургалт & семинар',
        //     type: 'textarea'
        //   }
        // ],
        contact: [
          {
            key: 'address', //hayag
            name: 'Хаяг',
            type: 'textarea'
          }, {
            key: 'application',
            name: 'Цагийн хуваарь',
            type: 'textarea'
          }, {
            key: 'contact',
            name: 'Холбоо барих',
            type: 'textarea'
          }
        ],
        social: [
          {
            key: 'linkedin',
            name: 'Linkedin',
            type: 'text'
          }, {
            key: 'twitter',
            name: 'Twitter',
            type: 'text'
          }, {
            key: 'youtube',
            name: 'Youtube',
            type: 'text'
          }, {
            key: 'facebook',
            name: 'Facebook',
            type: 'text'
          }
        ]

    }


    this.statics = [
      {
        key: 'address', //hayag
        name: 'Хаяг',
        type: 'textarea'
      }, {
        key: 'application',
        name: 'Цагийн хуваарь',
        type: 'textarea'
      }, {
        key: 'contact',
        name: 'Холбоо барих( Ажилчдын нэрс )',
        type: 'textarea'
      }, {
        key: 'finance_bundle',
        name: 'Санхүүжилтийн багц (Их наядаар)',
        type: 'text'
      }, {
        key: 'finance_project',
        name: 'Санхүүжүүлсэн төсөл хөтөлбөрүүд',
        type: 'text'
      }, {
        key: 'job_dbm',
        name: 'МОНГОЛЫН ЭРҮҮЛ МЭНДИЙН ҮҮСГЭЛ СУДАЛГААНЫ ТӨВ-НД',
        type: 'textarea'
      }, {
        key: 'job_open',
        name: 'Нээлттэй ажлын байр',
        type: 'textarea'
      }, {
        key: 'job_training',
        name: 'Сургалт & семинар',
        type: 'textarea'
      }, {
        key: 'linkedin',
        name: 'Linkedin',
        type: 'text'
      }, {
        key: 'twitter',
        name: 'Twitter',
        type: 'text'
      }, {
        key: 'youtube',
        name: 'Youtube',
        type: 'text'
      }, {
        key: 'facebook',
        name: 'Facebook',
        type: 'text'
      }
    ]

    this.statics.map(item => {
      this.staticsObj[item.key] = item
    })

    // this.staticsObj = {
    //   address: {
    //     key: 'address', //hayag
    //     name: 'Хаяг'
    //   },
    //   application: {
    //     key: 'application',
    //     name: 'Бичгээр өргөдөл, гомдол хүлээн авах'
    //   },
    //   contact: {
    //     key: 'contact',
    //     name: 'Холбоо барих( Ажилчдын нэрс )'
    //   },
    //   finance_bundle: {
    //     key: 'finance_bundle',
    //     name: 'Санхүүжилтийн багц (Их наядаар)'
    //   },
    //   finance_project: {
    //     key: 'finance_project',
    //     name: 'Санхүүжүүлсэн төсөл хөтөлбөрүүд'
    //   },
    //   job_dbm: {
    //     key: 'job_dbm',
    //     name: 'Монгол улсын хөгжлийн банк-нд ажиллах'
    //   },
    //   job_open: {
    //     key: 'job_open',
    //     name: 'Нээлттэй ажлын байр'
    //   },
    //   job_training: {
    //     key: 'job_training',
    //     name: 'Сургалт & семинар'
    //   },
    //   linkedin: {
    //     key: 'linkedin',
    //     name: 'Linkedin'
    //   },
    //   twitter: {
    //     key: 'twitter',
    //     name: 'Twitter'
    //   },
    //   youtube: {
    //     key: 'youtube',
    //     name: 'Youtube'
    //   },
    //   facebook: {
    //     key: 'facebook',
    //     name: 'Facebook'
    //   }
    // }
  }
}
