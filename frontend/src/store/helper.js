// export const API_ROOT = "localhost:5000/api/";
export const API_ROOT = "104.225.219.96:5001/api/";

export function localUrl(url) {
  if (url.startsWith("//")) {
    return `https:${url}`;
  }

  if (url.startsWith("http")) {
    return url;
  }

  return `http://${API_ROOT}${url}`;
}

function createFetchKnowingCookie() {
  return (url, options = {}) => {
    return fetch(localUrl(url), options);
  };
}

export default function createHelpers() {
  const fetchKnowingCookie = createFetchKnowingCookie();

  return {
    fetch: fetchKnowingCookie
  };
}
