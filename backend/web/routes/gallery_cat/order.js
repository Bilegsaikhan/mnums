'use strict'
import { GalleryCat } from '../../../model'
import Err from '../../../modules/err'

export default async function updateCat (req, res) {
  if (!req.body.ids || !Array.isArray(req.body.ids)) return res.callback(Err.badRequest('bad ids'))
  const ids = req.body.ids
  await GalleryCat.updateOrder(ids)
  res.callback()
}
