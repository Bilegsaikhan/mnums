import {
    LOAD_TOP_PROJECT, LOAD_TOP_PROJECT_SUCCESS, LOAD_TOP_PROJECT_ERROR,
    LOAD_TOP_REPORT, LOAD_TOP_REPORT_SUCCESS, LOAD_TOP_REPORT_ERROR,
    LOAD_TOP_NEWS, LOAD_TOP_NEWS_SUCCESS, LOAD_TOP_NEWS_ERROR,
} from "../constants";

const defaultState = {
    topReports: [],
    topProjects: [],
    topNews: []
};

// reducer
export default function home(state = defaultState, action) {
    switch (action.type) {
        case LOAD_TOP_PROJECT: {
            return { ...state };
        }

        case LOAD_TOP_PROJECT_SUCCESS: {
            return { ...state, topProjects: action.payload.list };
        }

        case LOAD_TOP_PROJECT_ERROR: {
            return { ...state };
        }
        case LOAD_TOP_REPORT: {
            return { ...state };
        }

        case LOAD_TOP_REPORT_SUCCESS: {
            return { ...state, topReports: action.payload.list };
        }

        case LOAD_TOP_REPORT_ERROR: {
            return { ...state };
        }
        case LOAD_TOP_NEWS: {
            return { ...state };
        }

        case LOAD_TOP_NEWS_SUCCESS: {
            return { ...state, topNews: action.payload.list };
        }

        case LOAD_TOP_NEWS_ERROR: {
            return { ...state };
        }


        default: {
            return state;
        }
    }
}
