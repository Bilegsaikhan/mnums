import React, { Component } from 'react';
import {
  // IoIosSearch,
  IoIosMail,
  IoIosCall
} from 'react-icons/io';
import { FaFacebookF, FaTwitter, FaYoutube } from 'react-icons/fa';
import { NavLink } from 'react-router-dom';
import './style.scss';
import logo from '../../assets/logo.jpg';

export default class Header extends Component {
  render() {
    return (
      <header className="App-header">
        <div className="bg-grey border-bottom sub-header py-2">
          <div className="container">
            <div className="d-flex flex-column flex-md-row align-items-center">
              <nav className="my-0 mr-md-auto font-weight-normal contact-here">
                <a className="p-2 text-small" href="#">
                  <IoIosCall className="mr-2" />
                  (+976) 70287809
                </a>
                <a className="p-2 text-small" href="mailto:info@imsm.mn">
                  <IoIosMail className="mr-2" />
                  info@imsm.mn
                </a>
              </nav>
              <nav className="my-2 my-md-0 mr-md-3">
                <a
                  className="p-2"
                  target="_blank"
                  href="https://www.facebook.com/InstituteOfMedicalSciences"
                >
                  <FaFacebookF />
                </a>
                <a className="p-2" href="#">
                  <FaTwitter />
                </a>
                <a className="p-2" href="#">
                  <FaYoutube />
                </a>
              </nav>
            </div>
          </div>
        </div>
        <div className="bg-white border-bottom">
          <div className="container">
            <div className="d-flex flex-column flex-md-row align-items-center py-3">
              <NavLink
                className="my-0 mr-md-auto d-flex flex-column flex-md-row align-items-center"
                id="logo"
                to="/"
              >
                <img
                  src={logo}
                  width="100"
                  className="mr-3"
                  alt="Т.Шагдарсүрэнгийн Нэрэмжит Анагаах Ухааны Хүрээлэн"
                />
                <div className="d-flex flex-column">
                  <span className="logo-caption">
                    Т.Шагдарсүрэнгийн Нэрэмжит
                  </span>
                  <span className="logo-title1">Анагаах Ухааны</span>
                  <span className="logo-title2">Хүрээлэн</span>
                </div>
              </NavLink>
              <nav className="my-2 my-md-0 mr-md-3 main-nav d-flex flex-row">
                <div className="nav-item">
                  <NavLink
                    className="p-2 hover-1"
                    to="/page/1"
                    activeClassName="active"
                  >
                    Бидний тухай
                  </NavLink>
                  <div className="sub-nav py-2 px-3">
                    <NavLink to="/page/1" activeClassName="active">
                      Захирлын мэндчилгээ »
                    </NavLink>
                    <NavLink to="/page/2" activeClassName="active">
                      Танилцуулга »
                    </NavLink>
                    <NavLink to="/page/3" activeClassName="active">
                      Эрхэм зорилго »
                    </NavLink>
                    <NavLink to="/page/4" activeClassName="active">
                      Үйл ажиллагааны үндсэн чиг үүрэг »
                    </NavLink>
                    <NavLink to="/page/5" activeClassName="active">
                      Бахархалт хүмүүс »
                    </NavLink>
                    <NavLink to="/page/6" activeClassName="active">
                      Түүхэн замнал »
                    </NavLink>
                    <NavLink to="/page/7" activeClassName="active">
                      АУХ-ийн эрдэмтэн, судлаачид »
                    </NavLink>
                  </div>
                </div>
                <div className="nav-item">
                  <NavLink
                    className="p-2 hover-1"
                    to="/event"
                    activeClassName="active"
                  >
                    Бидний үйл ажиллагаа
                  </NavLink>
                  <div className="sub-nav py-2 px-3">
                    <NavLink to="/aboutus" activeClassName="active">
                      Төсөл »
                    </NavLink>
                    <NavLink to="/aboutus" activeClassName="active">
                      Үйл ажиллагааны үндсэн чиг үүрэг »
                    </NavLink>
                    <NavLink to="/aboutus" activeClassName="active">
                      Сектор »
                    </NavLink>
                  </div>
                </div>
                <div className="nav-item">
                  <NavLink
                    className="p-2 hover-1"
                    to="/shilendans"
                    activeClassName="active"
                  >
                    Шилэн Данс
                  </NavLink>
                </div>
                <div className="nav-item">
                  <NavLink
                    className="p-2 hover-1"
                    to="/news"
                    activeClassName="active"
                  >
                    Мэдээ мэдээлэл
                  </NavLink>
                </div>
                <div className="nav-item">
                  <NavLink
                    className="p-2 hover-1"
                    to="/contactus"
                    activeClassName="active"
                  >
                    Холбоо барих
                  </NavLink>
                </div>
              </nav>
              {/* <a className="btn" href="#">
                <IoIosSearch />
              </a> */}
            </div>
          </div>
        </div>
      </header>
    );
  }
}
