export class PageService {
  constructor (server) {
    'ngInject';
    this.server = server
  }

  list () {
    return this.server.get('/page')
  }
  get ({id}) {
    return this.server.get(`/page/${id}`)
  }
  update (data) {
    return this.server.put(`/page/${data.id}`, data)
  }
  remove (data) {
    return this.server.delete(`/page/${data.id}`)
  }
  create (data) {
    return this.server.post(`/page`, data)
  }
}
