'use strict'
import create from './create'
import list from './list'
import all from './all'
import get from './get'
import edit from './edit'
import del from './delete'
import update from './update'
export default ({router, passport, checkLoggedIn, multipartMiddleware, wrap, checkRootAuth}) => {
  router.route('/project')
    .post(checkLoggedIn, wrap(create))
    .get(wrap(all[0]), all[1])
  router.route('/project/list')
    .get(wrap(list))
  router.route('/project/:id')
    .delete(checkLoggedIn, wrap(del))
    .get(wrap(get[0]), get[1])
    .put(checkLoggedIn, wrap(update))
  router.route('/project/edit/:id')
    .get(wrap(edit))
}
