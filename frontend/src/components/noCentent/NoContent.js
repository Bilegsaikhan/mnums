import React, {Component} from 'react';
import './NoContent.css';

class NoContent extends Component {
    render() {
        return (
            <div className="">
                <p>
                    Одоогоор мэдээлэл байхгүй...
                </p>
            </div>
        );
    }
}

export default NoContent
