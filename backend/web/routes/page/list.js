'use strict'
import { Page } from '../../../model'

export default async function listPage (req, res) {
  const err = req.validationErrors()
  if (err) return res.callback(err)
  const pages = await Page.findAll()
  res.callback(pages)
}
