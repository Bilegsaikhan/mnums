import {
    LOAD_REPORT_LIST, LOAD_REPORT_LIST_SUCCESS, LOAD_REPORT_LIST_ERROR
} from "../constants";

export function loadReport(id) {
  return {
    type: "LOAD_REPORT",
    payload: id
  };
}

export function loadReportFilter(query) {
  return {
    type: "LOAD_REPORT_FILTER",
    payload: query
  };
}

export function loadReportHomeFilter(query) {
  return {
    type: "LOAD_REPORT_HOME_FILTER",
    payload: query
  };
}


export function loadReportList({params = {}}) {
    return async(dispatch, getState, {fetch}) => {
        // eslint-disable-next-line no-param-reassign
        const payload = {};

        dispatch({
            type: LOAD_REPORT_LIST,
            payload
        });

        try {
            let response = await fetch(`report${params}`, {
                method: "GET",
                credentials: 'include'
            });
            let data = await response.json();
            if (!response.ok) {
                const message = data.message || data;
                if (data) throw message;
            }

            dispatch({
                type: LOAD_REPORT_LIST_SUCCESS,
                payload: data
            });

        } catch (error) {
            dispatch({
                type: LOAD_REPORT_LIST_ERROR,
                error: error,
            });
            return false;
        }
        return true;
    };
}