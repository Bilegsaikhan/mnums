import React, {Component} from 'react';
import {Link, IndexLink} from 'react-router'
import Social from '../../components/social'
import './Job.css';
import {connect} from "react-redux";
import language from "../../constants/language"
import {loadStatic} from "../../actions/static";

class Job extends Component {
    componentWillMount() {
        const {loadStatic} = this.props;
        loadStatic('address')
    }
    render() {
        const {lang, staticReducer} = this.props;
        return (
            <div>
                <div className="g01v1-this-page-in">
                    <div className="container">
                        <div className="row">
                            <div className="col-xs-5 col-sm-6">
                                <div className="breadcrumb_wbr wb_breadcrumb">
                                    <div className="g04v1-text">
                                        <ol className="breadcrumb">
                                            <li><Link to={`/`}>{language.homepage[lang]}</Link></li>
                                            <li><a>{language.careers[lang]}</a></li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="container">
                    <div className="titlepar parsys">
                        <div className="title section">
                            <div className="row">
                                <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div className="c01v1-page-title">{language.careers[lang]}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="par parsys">
                        <div className="gridlayout parbase section">
                            <div className="row">
                                <div className="col-md-2 col-sm-12 col-xs-12 col-lg-2">
                                    <div>
                                        <div className="grid_2_par_0 parsys">
                                            <div className="remove-padding-grid">
                                                <div className="gridlayout parbase section">
                                                    <div className="row hidden-xs hidden-sm">
                                                        <div className="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                                            <div>
                                                                <div className="grid_12_par_0 parsys">
                                                                    <div className="remove-padding-grid">
                                                                        <hr className="hidden-lg hidden-md"/>
                                                                        <div
                                                                            className="n01v1_left_navigation parbase section">
                                                                            <div
                                                                                className="n01v1-nav-list dbm-left-nav hidden-sm hidden-xs">
                                                                                <ul>
                                                                                    <li>
                                                                                        <IndexLink to="/job" activeClassName="active">{language.currentOpenings[lang]}</IndexLink>
                                                                                    </li>
                                                                                    <li>
                                                                                        <Link to="/job/programs" activeClassName="active">{language.programsInternships[lang]}</Link>
                                                                                    </li>
                                                                                    <li>
                                                                                        <Link to="/job/workonbank" activeClassName="active">{language.workAtDBM[lang]}</Link>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                            <div className="clearfix"/>
                                                                        </div>
                                                                        <hr/>
                                                                        <div className="reference parbase section">
                                                                            <div className="cq-dd-paragraph">
                                                                                <div
                                                                                    className="country_office_conta country_office_contact_input">
                                                                                    <div id="lr01v1"
                                                                                         className=" clearfix">
                                                                                        <h6>{language.contactus[lang]}</h6>
                                                                                        <div className="mrg-b-20" dangerouslySetInnerHTML={{
                                                                                            __html: staticReducer.address && staticReducer.address.data
                                                                                        }}/>
                                                                                        <Link className="btn btn-default btn-sm mrg-t-15"
                                                                                              to="/contact">{language.contactus[lang]} »
                                                                                        </Link>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <hr/>
                                                                        <div className="reference parbase section">
                                                                            <div className="cq-dd-paragraph">
                                                                                <Social/>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="clearfix"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-10 col-lg-10 col-xs-12 col-sm-12">
                                    <div>
                                        <div className="grid_10_par_1 parsys">
                                            <div className="remove-padding-grid">
                                                <div className="gridlayout parbase section pageContent">

                                                    {this.props.children}
                                                    {/*child node is here*/}

                                                    <div className="clearfix"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="clearfix"/>
                        </div>
                        <div className="gridlayout parbase section">
                            <div className="row hidden-md hidden-lg">
                                <div className="col-md-12 col-sm-12 col-lg-12 col-xs-12">
                                    <hr className="hr-top"/>
                                    <div>
                                        <div className="grid_12_par_0 parsys">
                                            <div className="remove-padding-grid">
                                                <div className="gridlayout parbase section">
                                                    <div className="row">
                                                        <div className="col-md-6 col-sm-6 col-xs-6">
                                                            <div>
                                                                <div className="grid_6_par_0 parsys">
                                                                    <div className="remove-padding-grid">
                                                                        <div className="reference parbase section">
                                                                            <div className="cq-dd-paragraph">
                                                                                <div
                                                                                    className="country_office_conta country_office_contact_input">
                                                                                    <div id="lr01v1"
                                                                                         className=" clearfix">
                                                                                        <h6>{language.contactus[lang]}</h6>
                                                                                        <div className="mrg-b-20" dangerouslySetInnerHTML={{
                                                                                            __html: staticReducer.address && staticReducer.address.data
                                                                                        }}/>
                                                                                        <Link className="btn btn-default btn-sm mrg-t-15"
                                                                                              to="/contact">{language.contactus[lang]} »
                                                                                        </Link>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-6 col-xs-6 col-sm-6">
                                                            <div>
                                                                <div className="grid_6_par_1 parsys">
                                                                    <div className="remove-padding-grid">
                                                                        <div className="reference parbase section">
                                                                            <div className="cq-dd-paragraph">
                                                                                <Social/>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="clearfix"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="clearfix"/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


const mapStateToProps = (state) => {
    const {config, staticReducer} = state;
    return {
        lang: config.language,
        staticReducer
    };
};

const mapDispatch = {loadStatic};

export default connect(mapStateToProps, mapDispatch)(Job);
