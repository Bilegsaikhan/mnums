/**
 * Created by enxtur on 5/22/17.
 */
import timelineData from '../raw/timelines'
import { Timeline } from '../model'
import moment from 'moment'
const debug = require('../modules/debug')(__dirname)
module.exports = () => {
  Timeline.bulkCreate(timelineData.map(({date, content}) => {
    return {
      date,
      content,
      date_en: moment(date, 'YYYY оны M сарын D').format('MMM Do, YYYY'),
      content_en: content
    }
  })).then(() => {
    debug('success')
  }).catch((err) => {
    debug(err)
  })
}

