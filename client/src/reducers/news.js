import {
  LOAD_NEWS_ITEM,
  LOAD_NEWS_ITEM_SUCCESS,
  LOAD_NEWS_ITEM_ERROR,
  LOAD_NEWS_LIST,
  LOAD_NEWS_LIST_SUCCESS,
  LOAD_NEWS_LIST_ERROR
} from '../constants';
import newsData from '../constants/newsData.json';

const initialAuthState = {
  error: false,
  isFetching: false,
  newsData,
  newsItem: {}
};

const news = (state = initialAuthState, action) => {
  switch (action.type) {
    case LOAD_NEWS_ITEM: {
      return {
        ...state,
        isFetching: true,
        error: null
      };
    }
    case LOAD_NEWS_ITEM_SUCCESS: {
      const { payload } = action;
      return {
        ...state,
        newsItem: payload,
        isFetching: false
      };
    }
    case LOAD_NEWS_ITEM_ERROR: {
      return {
        ...state,
        error: action.error,
        isFetching: false
      };
    }

    case LOAD_NEWS_LIST: {
      return {
        ...state,
        isFetching: true,
        error: null
      };
    }
    case LOAD_NEWS_LIST_SUCCESS: {
      const { payload } = action;
      return {
        ...state,
        data: payload.list,
        total: payload.total,
        isFetching: false
      };
    }
    case LOAD_NEWS_LIST_ERROR: {
      return {
        ...state,
        error: action.error,
        isFetching: false
      };
    }

    default:
      return state;
  }
};

export default news;
