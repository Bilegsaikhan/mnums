import {createStore, applyMiddleware, compose} from 'redux'
import thunk from 'redux-thunk'
import { createLogger } from 'redux-logger'
import rootReducer from '../reducers'
import createHelpers from './helper';

const configureStore = initialState => {
    const helpers = createHelpers();
    let middleware = [thunk.withExtraArgument(helpers)];
    middleware.push(createLogger({collapsed: true}));
    const devFuncs = [
        window.devToolsExtension ? window.devToolsExtension() : f => f // See https://github.com/zalmoxisus/redux-devtools-extension
    ];

    const store = createStore(
        rootReducer,
        initialState,
        compose(
            applyMiddleware(...middleware),
            ...devFuncs,
        )
    )

    if (module.hot) {
        // Enable Webpack hot module replacement for reducers
        module.hot.accept('../reducers', () => {
            const nextRootReducer = require('../reducers').default
            store.replaceReducer(nextRootReducer)
        })
    }

    return store
}

export default configureStore
