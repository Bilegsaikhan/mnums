'use strict'
const debug = require('../../../modules/debug')(__filename)
import create from './create'
import update from './update'
import get from './get'
import edit from './edit'
import del from './delete'
import list from './list'
import all from './all'
export default ({ router, passport, checkLoggedIn, multipartMiddleware, wrap, checkRootAuth }) => {
  router.route('/product')
    .get(wrap(all[0]), all[1]) /* frontend */
    // .post(checkLoggedIn, wrap(create))
    .post(wrap(create))
  router.route('/product/list')
    .get(wrap(list[0]), list[1]) /* admin */
  router.route('/product/edit/:id')
    .get(wrap(edit)) /* admin */
  router.route('/product/:id')
    .put(checkLoggedIn, wrap(update))
    .get(wrap(get[0]), get[1]) /* frontend */
    .delete(checkLoggedIn, wrap(del))
}
