import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import Home from './home';
import Page from './page';
import ContactUs from './contactus';
import Event from './event';
import News from './news';
import NewsRead from './newsRead';
import Shilen from './shilen';
import NoMatch from './noMatch';

import { loadNewsList } from '../actions/news';

const Main = ({ store }) => {
  const loadData = (nextState, replace, cb) => {
    Promise.all([store.dispatch(loadNewsList())]).then(
      res => {
        console.log('res: ', res);
        cb();
      },
      reason => {
        console.log('reason: ', reason);
        cb();
      }
    );
  };
  return (
    <main>
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/page/:id" component={Page} />
        <Route path="/event" component={Event} />
        <Route path="/shilendans" component={Shilen} />
        <Route path="/news" component={News} />
        <Route path="/read/:id" component={NewsRead} />
        <Route path="/contactus" component={ContactUs} />
        <Redirect from="/old-match" to="/page" />
        <Route component={NoMatch} />
      </Switch>
    </main>
  );
};

Main.propTypes = {};
export default Main;
