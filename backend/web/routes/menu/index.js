'use strict'

import { Menu } from '../../../model'
import order from './order'
import edit from './edit'
import update from './update'
import create from './create'
import all from './all'
// import get from './get'
import del from './delete'
// import password from './password'
export default ({router, passport, checkLoggedIn, multipartMiddleware, wrap, checkRootAuth}) => {
  router.route('/menu')
    .get(wrap(all[0]), all[1])
    .put(checkRootAuth, wrap(order))
    .post(checkRootAuth, wrap(create))
  router.route('/menu/edit/:id')
    .get(wrap(edit))
  router.route('/menu/:id')
    .put(checkRootAuth, wrap(update))
    .delete(checkRootAuth, wrap(del))
  router.route('/menu/list')
    .get(wrap(async (req, res) => {
      const menus = await Menu.list()
      res.callback(menus)
    }))
}
