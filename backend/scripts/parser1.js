let request = require('request');
let cheerio = require('cheerio');
let fall = require('async-waterfall')
let fs = require('fs')

const pageString = 'page/'
const baseUrl = 'http://www.uyanbest.mn/product/wayne-12-volt-transfer-pump/'
let urls = [];

const between = (string) => {
  const i = string.indexOf('url(');
  const j = string.indexOf(')');
  return string.substring(i + 4, j)
}

// console.log(between('background-image: url("http://www.uyanbest.mn/wp-content/uploads/2017/01/27-12.jpg"); width: 415px; margin-right: 10px;'));

const init = () => {
  fs.readFile('aa.json', (err, data) => {
    let jsonData = JSON.parse(data);
    let tasks = [];

    jsonData.forEach((page) => {
      page.links.forEach((link, index) => {
        let task = (callback) => {
          try {
            console.log('start task', link, page.id)
            request(link, (err, response, body) => {
              console.log('response', link, page.id)
              if (err) {
                throw err;
              }
              $ = cheerio.load(body);
  
              let item = {
                images: [],
                link: link
              }

              $('.gallery-top.swiper-container > .swiper-wrapper > .swiper-slide').each((index, element) => {
                let image = between($(element).attr('style'));
                console.log('!!!', image);
                item.images.push(image);
              })
        
              item.title = $('.wpb_column.vc_column_container div.description > div.title').text().trim();
        
              $('.wpb_column.vc_column_container div.description > div.desc').each((index, element) => {
                if (index === 0) {
                  item.shortDetail = $(element).html().trim();
                }
                if (index === 1) {
                  item.category = $(element).find('>a').text();
                }
              })
        
              item.longDetail = $('#featured_product_slider_wrapper1').html().trim();
  
              fs.readFile('bb.json', (err, data) => {
                if (err) {
                  throw err;
                }
                let tempData = JSON.parse(data);
                if (tempData.find((found) => {
                  return found.link === item.link
                })) {
                  return callback();
                } else {
                  tempData.push(item)
                  fs.writeFile('bb.json', JSON.stringify(tempData, null, 2), (err) => {
                    if (err) {
                      throw err;
                    }
                    return callback();
                  })
                }
              })
            }) 
          } catch (error) {
            return callback(error);
          }
        }
        tasks.push(task);
      })
    })

    fall(tasks, (error) => {
      if (error) {
        console.log('errraa', error);
      }
      console.log('done');
    })
  })
}

init();
