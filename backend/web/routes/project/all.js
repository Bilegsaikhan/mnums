/**
 * Created by enxtur on 5/5/17.
 */

import { Project } from '../../../model'
import { format } from '../../../modules/formatter'
export default [
  async (req, res, next) => {
    req.checkQuery('offset', 'bad_offset').optional().isInt()
    req.checkQuery('limit', 'bad_limit').optional().isInt()
    req.checkQuery('branch_id', 'bad_branch_id').optional().isInt()
    req.checkQuery('region_id', 'bad_region_id').optional().isInt()
    req.checkQuery('source_id', 'bad_source_id').optional().isInt()
    req.checkQuery('string', 'bad_string').optional().len(1, 255)
    req.checkQuery('status', 'bad_status').optional().isIn(['ongoing', 'finished'])
    const err = req.validationErrors()
    if (err) return res.callback(err)
    const {offset = 0, limit = 10, branch_id, region_id, source_id, string, status} = req.query
    const lang = req.session.lang || 'mn'
    const params = {
      lang,
      offset: parseInt(offset),
      limit: parseInt(limit),
      branch_id,
      region_id,
      source_id,
      string,
      status
    }
    const total = await Project.toCount(params)
    if (total === 0) {
      return res.callback({
        total: 0,
        list: []
      })
    }
    const list = await Project.list(params).then(list => list.map(o => o.get({plain: true})))
    res.data = {total, list}
    next()
    // res.callback(projects)
  },
  format({
    list: [{
      program: true,
      loan_type: true,
      borrower: true,
      implementer: true,
      objective: true,
      sankhuu_amount: true,
      economic: true,
      social: true,
      introduction: true,
      start_date: true,
      end_date: true,
      name: true,
      togtool_amount: true,
      geree_amount: true,
      duty: true,
      interpretation: true
    }]
  })
]
