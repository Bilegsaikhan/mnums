'use strict'
import { EmailTemplate } from 'email-templates'
import path from 'path'
import html from 'html'

const templateDir = path.join(__dirname, 'templates', 'welcome')
const welcome = new EmailTemplate(templateDir)
const user = {name: 'Enxtur Enxbat'}

export default function () {
  return welcome.render(user).then((res) => {
    let prettyData = html.prettyPrint(res.html, {indent_size: 2})
    process.stdout.write(prettyData)
    return res.html
  })
}
