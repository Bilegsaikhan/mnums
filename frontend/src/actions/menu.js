import {LOAD_MENU, LOAD_MENU_SUCCESS, LOAD_MENU_ERROR} from "../constants";

export function loadMenu() {
    return async (dispatch, getState, {fetch}) => {
        // eslint-disable-next-line no-param-reassign
        const payload = {};

        dispatch({
            type: LOAD_MENU,
            payload
        });

        try {
            let response = await fetch(`menu`, {
                method: "GET",
                credentials: 'include'
            });

            let data = await response.json();
            if (!response.ok) {
                const message = data.message || data;
                if (data) throw message;
            }
            dispatch({
                type: LOAD_MENU_SUCCESS,
                payload: data
            });
            return data;
        } catch (error) {
            const message = error.message || error;
            dispatch({
                type: LOAD_MENU_ERROR,
                error: error.message
            });
            throw new Error(message);
        }
    };
}
