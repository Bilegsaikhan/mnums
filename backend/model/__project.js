'use strict'
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('project', {
    image: DataTypes.STRING,
    branch_id: DataTypes.INTEGER,
    region_id: DataTypes.INTEGER,
    source_id: DataTypes.INTEGER,
    mn: DataTypes.BOOLEAN,
    en: DataTypes.BOOLEAN,
    performance: DataTypes.INTEGER,
    program: DataTypes.STRING,
    program_en: DataTypes.STRING,
    loan_type: DataTypes.STRING,
    loan_type_en: DataTypes.STRING,
    borrower: DataTypes.STRING,
    borrower_en: DataTypes.STRING,
    implementer: DataTypes.STRING,
    implementer_en: DataTypes.STRING,
    objective: DataTypes.STRING,
    objective_en: DataTypes.STRING,
    sankhuu_amount: DataTypes.STRING,
    sankhuu_amount_en: DataTypes.STRING,
    economic: DataTypes.STRING,
    economic_en: DataTypes.STRING,
    social: DataTypes.STRING,
    social_en: DataTypes.STRING,
    introduction: DataTypes.STRING,
    introduction_en: DataTypes.STRING,
    start_date: DataTypes.STRING,
    start_date_en: DataTypes.STRING,
    end_date: DataTypes.STRING,
    end_date_en: DataTypes.STRING,
    name: DataTypes.STRING,
    name_en: DataTypes.STRING,
    togtool_amount: DataTypes.STRING,
    togtool_amount_en: DataTypes.STRING,
    geree_amount: DataTypes.STRING,
    geree_amount_en: DataTypes.STRING,
    duty: DataTypes.STRING,
    duty_en: DataTypes.STRING,
    interpretation: DataTypes.STRING,
    interpretation_en: DataTypes.STRING,
    // created_at: DataTypes.DATE,
    // updated_at: DataTypes.DATE
  }, {
    timestamps: true,
    classMethods: {
      toCount ({lang, branch_id: branchId, region_id: regionId, source_id: sourceId, string, status}) {
        const where = []
        const attrs = []
        if (lang === 'mn') {
          where.push('mn is true')
        } else if (lang === 'en') {
          where.push('en is true')
        }
        if (branchId) {
          where.push('branch_id = ?')
          attrs.push(branchId)
        }
        if (regionId) {
          where.push('region_id = ?')
          attrs.push(regionId)
        }
        if (sourceId) {
          where.push('source_id = ?')
          attrs.push(sourceId)
        }
        if (string && string.length) {
          where.push('(name like ? OR name_en like ?)')
          attrs.push(`%${string}%`)
          attrs.push(`%${string}%`)
        }
        if (status === 'ongoing') {
          where.push('performance < ?')
          attrs.push(100)
        } else if (status === 'finished') {
          where.push('performance >= ?')
          attrs.push(100)
        }
        return this.count({
          where: where.length > 0 ? [where.join(' AND '), ...attrs] : undefined
        })
      },
      list ({lang, branch_id: branchId, region_id: regionId, source_id: sourceId, offset = 0, limit = 10, string, status}) {
        const where = []
        const attrs = []
        if (lang === 'mn') {
          where.push('mn is true')
        } else if (lang === 'en') {
          where.push('en is true')
        }
        if (branchId) {
          where.push('branch_id = ?')
          attrs.push(branchId)
        }
        if (regionId) {
          where.push('region_id = ?')
          attrs.push(regionId)
        }
        if (sourceId) {
          where.push('source_id = ?')
          attrs.push(sourceId)
        }
        if (string && string.length) {
          where.push('(name like ? OR name_en like ?)')
          attrs.push(`%${string}%`)
          attrs.push(`%${string}%`)
        }
        if (status === 'ongoing') {
          where.push('performance < ?')
          attrs.push(100)
        } else if (status === 'finished') {
          where.push('performance >= ?')
          attrs.push(100)
        }
        return this.findAll({
          order: 'id desc',
          offset: offset * limit,
          limit: parseInt(limit),
          where: where.length > 0 ? [where.join(' AND '), ...attrs] : undefined
        })
      }
    }
  })
}

