'use strict'
import inc from './include'
const tableName = 'menu'

module.exports = function (sequelize, DataTypes) {
  return sequelize.define(tableName, {
    parent_id: DataTypes.INTEGER,
    mn: DataTypes.BOOLEAN,
    en: DataTypes.BOOLEAN,
    name: DataTypes.STRING,
    name_en: DataTypes.STRING,
    type: DataTypes.ENUM('module', 'type'),
    page_id: DataTypes.INTEGER,
    module: DataTypes.STRING,
    order: DataTypes.INTEGER,
    is_main: DataTypes.BOOLEAN
  }, {
    classMethods: {
      all ({lang = 'mn'}) {
        const include = inc(sequelize.models)
        let where = {
          is_main: true
        }
        if (lang === 'mn') {
          where = {
            ...where,
            mn: true
          }
        } else {
          where = {
            ...where,
            en: true
          }
        }
        return this.findAll({
          where,
          order: '`order` asc, `menus.order` asc, `menus.menus.order` asc',
          include: include({
            menus: {
              menus: true
            }
          })
        }).then(menus => {
          return menus.map(_menu => {
            const menu = _menu.get({plain: true})
            menu.menus = menu.menus.filter(menu => lang === 'mn' ? menu.mn : menu.en)
            menu.menus = menu.menus.map(menu => {
              menu.menus = menu.menus.filter(menu => lang === 'mn' ? menu.mn : menu.en)
              return menu
            })
            return menu
          })
        })
      },
      list () {
        const include = inc(sequelize.models)
        return this.findAll({
          where: ['`menu`.`is_main` is true'],
          order: '`order` asc, `menus.order` asc, `menus.menus.order` asc',
          include: include({
            page: true,
            menus: {
              page: true,
              menus: {
                page: true
              }
            }
          })
        })
      },
      async updateOrder (ids) {
        const idsJoined = ids.join()
        const query1 = 'set @ordering=0;'
        const query2 = `update ${tableName} set \`order\`=(@ordering:=@ordering+10) where id in (${idsJoined}) order by field(id,${idsJoined});`
        await sequelize.query(query1, {type: sequelize.QueryTypes.UPDATE})
        await sequelize.query(query2, {type: sequelize.QueryTypes.UPDATE})
      },
      edit (id) {
        const include = inc(sequelize.models)
        return this.find({
          where: {id},
          include: include({
            page: true,
            parent: true
          })
        })
      }
    }
  })
}
