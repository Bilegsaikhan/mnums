import React, {Component} from 'react';
import './Partner.css';
import OwlCarousel from 'react-owl-carousel2'
import {connect} from 'react-redux'

class Partner extends Component {
    render() {

        const options = {
            items: this.props.userAgent.mobile ? 1 : 5,
            nav: false,
            dots: false
        };

        return (
                <div className="partners">
                    <OwlCarousel ref="partnerCarousel" options={options}>
                            <div>
                                <a className="partnetItem" target="_blank" href="https://www.mof.gov.mn">
                                    <img src="/assets/images/partners/yam1.jpg" alt="yam1"/>
                                </a>
                            </div>
                            <div>
                                <a className="partnetItem" target="_blank" href="https://mcud.gov.mn">
                                    <img src="/assets/images/partners/yam2.jpg" alt="yam1"/>
                                </a>
                            </div>
                            <div>
                                <a className="partnetItem" target="_blank" href="http://www.nso.mn">
                                    <img src="/assets/images/partners/yam3.jpg" alt="yam1"/>
                                </a>
                            </div>
                            <div>
                                <a className="partnetItem" target="_blank" href="http://mofa.gov.mn">
                                    <img src="/assets/images/partners/yam5.jpg" alt="yam1"/>
                                </a>
                            </div>
                            <div>
                                <a className="partnetItem" target="_blank" href="https://www.mongolbank.mn/">
                                    <img src="/assets/images/partners/yam6.jpg" alt="yam1"/>
                                </a>
                            </div>
                    </OwlCarousel>
                </div>
        );
    }
}


const mapStateToProps = (state) => {
    const {userAgent} = state

    return {
        userAgent
    }
}

export default connect(mapStateToProps, {})(Partner)
