export class StaticListController {
  constructor(staticService) {
    'ngInject';
    this.pageService = staticService
    this.pageCount = 0
    this.search = ''
    this.allData = 0
    this.count = 0
    this.listData = []
    this.page = 1
    this.limit = 10
    this.isTableLoading = true
    this.order = 'id DESC'

  }

  load() {
    this.pageService.list().then(pages => {
      this.listData = pages
      this.count = pages.length
    })
  }

}
