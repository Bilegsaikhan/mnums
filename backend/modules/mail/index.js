'use strict'
import conf from '../../config/index'
import { mail as helper } from 'sendgrid'
import { EmailTemplate } from 'email-templates'
import { join } from 'path'

const debug = require('../../modules/debug')(__filename)
const mailer = require('sendgrid')(conf.SENDGRID_APIKEY)
const from = 'noreply@ubtrade.mn'
const opts = {
  'welcome': [
    'UBTrade.mn-д тавтай морил.',
    '9403e814-dfbd-4788-b157-45aaead1e879',
    new EmailTemplate(join(__dirname, 'templates', 'welcome'))
  ],
  'forgot': [
    'UBTrade.mn-ий Нууц үг сэргээх.',
    'ccb2aec7-f125-4c4b-a399-797abe154a05',
    new EmailTemplate(join(__dirname, 'templates', 'forgot'))
  ],
  'verify': [
    'UBTrade.mn, и-мэйл хаягаа баталгаажуулна уу.',
    'e20d3ac1-2a4e-4182-ac68-b6c82f470ab2',
    new EmailTemplate(join(__dirname, 'templates', 'verify'))
  ]
}
function makeSender (type) {
  return (to, data) => {
    let [subject, templateId, template] = opts[type]
    return template.render(data)
      .then(({html}) => {
        let fromEmail = new helper.Email(from)
        let toEmail = new helper.Email(to)
        let content = new helper.Content('text/html', html)
        let mail = new helper.Mail(fromEmail, subject, toEmail, content)
        mail.personalizations[0].addSubstitution(new helper.Substitution('-name-', data.name))
        mail.setTemplateId(templateId)
        return mailer.emptyRequest({
          method: 'POST',
          path: '/v3/mail/send',
          body: mail.toJSON()
        })
      })
      .then(request => mailer.API(request))
      .then((res) => {
        debug(`${type} mail sent to ${to}`)
        return res
      })
  }
}
let welcome = makeSender('welcome')
let forgot = makeSender('forgot')
let verify = makeSender('verify')

export { welcome, forgot, verify }
