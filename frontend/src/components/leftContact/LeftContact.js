import React, {Component} from 'react';
import './LeftContact.css';

class Social extends Component {
    render() {
        return (
            <div className="s03v2_stay_connected section">
                <div id="s03v2" className="">
                    <h6 className="mrg-b-15">Сошиал холбоос</h6>
                    <div className="s03v2-icon" style={{paddingBottom: '34px', paddingTop: '10px'}}>
                        <ul>
                            <li>
                                <a href="https://web.facebook.com/DevelopmentBankofMongolia?_rdc=1&_rdr" className="social-icon-img">
                                    <img src="/assets/images/svg/icons-facebook.svg" title="Image" alt="facebook"/>
                                </a>
                                <div className="s03v2-text">
                                    <a href="https://web.facebook.com/DevelopmentBankofMongolia?_rdc=1&_rdr"> <span>Facebook</span></a>
                                </div>
                            </li>
                            <li>
                                <a href="https://twitter.com/DBofMongolia" className="social-icon-img">
                                    <img src="/assets/images/svg/icons-twitter.svg" title="Image" alt="twitter"/>
                                </a>
                                <div className="s03v2-text">
                                    <a href="https://twitter.com/DBofMongolia"> <span>Twitter</span></a>
                                </div>
                            </li>
                            <li>
                                <a href="#" className="social-icon-img">
                                    <img src="/assets/images/svg/icons-rss.svg" title="Image" alt="rss"/>
                                </a>
                                <div className="s03v2-text">
                                    <a href=""> <span>RSS</span></a>
                                </div>
                            </li>
                            <li>
                                <a href="http://www.youtube.com/DBofMongolia" className="social-icon-img">
                                    <img src="/assets/images/svg/icons-youtube.svg" title="Image" alt="youtube"/>
                                </a>
                                <div className="s03v2-text">
                                    <a href="http://www.youtube.com/DBofMongolia"> <span>Youtube</span></a>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div className="clearfix"/>
                </div>
                <div className="clearfix"/>
            </div>
        );
    }
}

export default Social
