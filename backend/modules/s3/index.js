'use strict'
import knox from 'knox'
import http from 'http'
import https from 'https'
import conf from '../../config/index'

const knoxClient = knox.createClient({
  key: 'AKIAJB2PD7FAKRRHQZHA',
  secret: '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',
  bucket: 'ubinfo-s3'
})
const debug = require('../debug')(__filename)

export function upload (srcpath, s3path) {
  return new Promise((resolve, reject) => {
    knoxClient.putFile(srcpath, s3path, {
      'x-amz-acl': 'public-read',
      'Cache-Control': 'max-age=604800'
    }, (err, res) => {
      if (err) return reject(err)
      resolve(res)
    })
  })
}

export function uploadURL (url, s3path) {
  const fetcher = url.indexOf('https') === 0 ? https : http
  return new Promise((resolve, reject) => {
    fetcher.get(url, function (res) {
      debug('########################')
      debug(url)
      debug('########################')
      debug('headers')
      debug(res.headers)
      let headers = {
        'x-amz-acl': 'public-read',
        'Cache-Control': 'max-age=604800',
        'Content-Length': res.headers['content-length'],
        'Content-Type': res.headers['content-type']
      }
      knoxClient.putStream(res, s3path, headers, (err, res) => {
        if (err) return reject(err)
        resolve(res)
      })
    }).on('error', reject)
  })
}

export function uploadBuffer (buffer, s3path, contentType) {
  return new Promise((resolve, reject) => {
    knoxClient.putBuffer(buffer, s3path, {
      'Content-Type': contentType,
      'x-amz-acl': 'public-read'
    }, (err, res) => {
      if (err) return reject(err)
      resolve(res)
    })
  })
}
