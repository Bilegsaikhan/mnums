import {
  LOAD_PAGE, LOAD_PAGE_SUCCESS, LOAD_PAGE_ERROR,
  LOAD_MENU, LOAD_MENU_SUCCESS, LOAD_MENU_ERROR
} from "../constants";

const defaultState = {
  data: [],
  indexedData: {},
  pageLoading: false
};

// reducer
export default function menu(state = defaultState, action) {
  switch (action.type) {
    case LOAD_MENU: {
      return { ...state };
    }

    case LOAD_MENU_SUCCESS: {
      let indexedData = {};
      const data = action.payload;
      data.forEach(item => {
        indexedData[item.module] = item;
        item.menus.forEach(child => {
          indexedData[child.module] = child;
          child.menus.forEach(child1 => {
            indexedData[child1.module] = child1;
          });
        });
      });
      return { ...state, data: data, indexedData: indexedData };
    }

    case LOAD_MENU_ERROR: {
      return { ...state };
    }

    case LOAD_PAGE: {
      return { ...state, pageLoading: true };
    }

    case LOAD_PAGE_SUCCESS: {

      let { payload, moduleString } = action

      return {
        ...state, pageLoading: false, indexedData: {
          ...state.indexedData,
          [moduleString]: {
            ...state.indexedData[moduleString],
            pageData: payload
          }
        }
      };
    }

    case LOAD_PAGE_ERROR: {
      return { ...state, pageLoading: false };
    }

    default: {
      return state;
    }
  }
}
