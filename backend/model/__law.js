'use strict'
// import inc from './include'
const tableName = 'law'
module.exports = function (sequelize, DataTypes) {
  return sequelize.define(tableName, {
    type: DataTypes.ENUM('law', 'resolution'),
    mn: DataTypes.BOOLEAN,
    en: DataTypes.BOOLEAN,
    name: DataTypes.STRING,
    name_en: DataTypes.STRING,
    pdf: DataTypes.STRING,
    pdf_en: DataTypes.STRING
  }, {
    timestamps: true,
    classMethods: {
      toCount ({lang, type, string}) {
        const where = []
        const attrs = []
        if (lang === 'mn') {
          where.push('mn is true')
        } else if (lang === 'en') {
          where.push('en is true')
        }
        if (type) {
          where.push('type = ?')
          attrs.push(type)
        }
        if (string) {
          where.push('(name like ? OR name_en like ?)')
          attrs.push(`%${string}%`)
          attrs.push(`%${string}%`)
        }
        return this.count({
          where: where.length > 0 ? [where.join(' AND '), ...attrs] : undefined
        })
      },
      list ({offset = 0, limit = 10, lang, type, string}) {
        const where = []
        const attrs = []
        if (lang === 'mn') {
          where.push('mn is true')
        } else if (lang === 'en') {
          where.push('en is true')
        }
        if (type) {
          where.push('type = ?')
          attrs.push(type)
        }
        if (string) {
          where.push('(name like ? OR name_en like ?)')
          attrs.push(`%${string}%`)
          attrs.push(`%${string}%`)
        }
        return this.findAll({
          where: where.length > 0 ? [where.join(' AND '), ...attrs] : undefined,
          offset: offset * limit,
          limit: parseInt(limit),
          order: 'created_at DESC'
        })
      },
      get (id) {
        return this.find({
          where: {id}
        })
      }
    }
  })
}
