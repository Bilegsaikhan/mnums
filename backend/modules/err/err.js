/**
 * Created by enxtur on 2/15/17.
 */
'use strict'
const debugInternalError = require('../debug')(__filename)
const debug = require('../debug')(__filename)
export default class Err {
  constructor (status, message, param) {
    this._status = status
    this._message = message
    this._param = param
  }

  toJSON () {
    return {
      message: this._message,
      status: this._status,
      param: this._param
    }
  }

  static middleware (req, res, next) {
    res.err = {
      userExist: Err.userExist,
      badRequest: Err.badRequest,
      badCode: Err.badCode,
      unauthorized: Err.unauthorized,
      internal: Err.internal,
      sequelize: Err.sequelize,
      notfound: Err.notfound,
      expectationfailed: Err.expectationfailed
    }
    next()
  }

  get status () { return this._status }

  set status (status) { this._status = status }

  static userExist (email = '') { return new Err(409, `Таны ${email} и-мэйл бүртгэлтэй байна!`) }

  static notfound (message = 'not found') { return new Err(404, message) }

  static badRequest (_message, param) {
    let message = _message || 'Хүсэлт буруу!'
    return new Err(400, message, param)
  }

  static badCode (message = 'Зөв код оруулна уу') { return new Err(400, message) }

  static unauthorized (message = 'Зөвшөөрөгдөөгүй') { return new Err(401, message) }

  static expectationfailed (message = 'expectation failed') { return new Err(417, message) }

  static internal (message = 'Серверийн алдаа') {
    debugInternalError(`internal server error detected with "${message}" message`)
    return new Err(500, message)
  }

  static sequelize (error) {
    if (!error) return new Err(500, 'Unhandled Sequelize Error')
    if (error.parent && error.parent.code === 'ER_NO_DEFAULT_FOR_FIELD') {
      debug(error.parent.message)
      return new Err(400, 'NO VALUE IN REQUIRED FIELD') // TODO change later by common error message
    }
    if (error.parent && error.parent.code === 'ER_NO_REFERENCED_ROW_2') {
      debug(error.parent.message)
      return new Err(400, 'FOREIGN KEY CONSTRAINT ERROR') // TODO change later by common error message
    }
    if (error.parent && error.parent.code === 'ER_DUP_ENTRY') {
      debug(error)
      return new Err(409, 'DUPLICATED ENTRY') // TODO change later by common error message
    }
    debug(error)
    return new Err(500, error)
  }
}
