'use strict'
import { Strategy as LocalStrategy } from 'passport-local'
import multipart from 'connect-multiparty'
import appRoute from './app'
import adminRoute from './admin'
import articleRoute from './article'
import articleCopyRoute from './article_copy'
import articleCatRoute from './article_cat'
import pageRoute from './page'
import projectRoute from './project'
import projectBranchRoute from './project_branch'
import projectRegionRoute from './project_region'
import projectSourceRoute from './project_source'
import jobRoute from './job'
import menuRoute from './menu'
import slideRoute from './slide'
import instaRoute from './insta'
import timelineRoute from './timeline'
import pdfRoute from './pdf'
import reportRoute from './report'
import storeRoute from './store'
import lawRoute from './law'
import hrRoute from './human_resource'
import product from './product'
import productCat from './product_cat'
import productSubCat from './product_sub_cat'
import productImage from './product_image'
import galleryCat from './gallery_cat'
import provider from './provider'
import mail from './mail'
import { Admin } from '../../model'
import Err from '../../modules/err'
import { wrap } from '../../modules/wrapper'

const multipartMiddleware = multipart()
const routes = [
  appRoute,
  adminRoute,
  articleRoute,
  articleCopyRoute,
  articleCatRoute,
  pageRoute,
  projectRoute,
  projectBranchRoute,
  projectRegionRoute,
  projectSourceRoute,
  jobRoute,
  menuRoute,
  slideRoute,
  instaRoute,
  timelineRoute,
  pdfRoute,
  reportRoute,
  storeRoute,
  lawRoute,
  hrRoute,
  product,
  productCat,
  productImage,
  galleryCat,
  productSubCat,
  provider,
  mail
]
export default (router, passport) => {
  passport.use(new LocalStrategy({ // this handler will call when login request
    usernameField: 'username',
    passwordField: 'password'
  }, async (username, password, done) => {
    const admin = await Admin.findByUsername(username)
    if (!admin) return done(null, false)
    if (!admin.validPassword(password)) return done(null, false)
    done(null, admin)
  }))
  passport.serializeUser(function serializeUser(user, done) {
    done(null, user.id)
  })
  passport.deserializeUser(async (id, done) => {
    const admin = await Admin.get(id)
    if (!admin) return done(null, false)
    done(null, admin)
  })

  const checkLoggedIn = (req, res, next) => {
    if (req.isAuthenticated()) {
      return next()
    }
    res.callback(Err.unauthorized())
  }

  const checkRootAuth = (req, res, next) => {
    if (req.isAuthenticated() && req.user.root) {
      return next()
    }
    res.callback(Err.unauthorized())
  }

  for (let route of routes) {
    route({ router, passport, multipartMiddleware, wrap, checkLoggedIn, checkRootAuth })
  }
}
