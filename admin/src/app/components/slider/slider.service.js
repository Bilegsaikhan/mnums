export class SliderService {
  constructor (server) {
    'ngInject';
    this.server = server
  }

  list () {
    return this.server.get('/slide')
  }
  get ({id}) {
    return this.server.get(`/slide/${id}`)
  }
  update (data) {
    return this.server.put(`/slide/${data.id}`, data)
  }
  remove (data) {
    return this.server.delete(`/slide/${data.id}`)
  }
  create (data) {
    return this.server.post(`/slide`, data)
  }
}
