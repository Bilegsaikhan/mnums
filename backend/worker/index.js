'use strict'
/**
 * This index.js file for pm2 starter
 * http://pm2.keymetrics.io
 */
require('babel-register')
const app = require('./rabbitmq/app')
app.start()
