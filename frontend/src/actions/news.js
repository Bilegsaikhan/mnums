import {
    LOAD_NEWS_LIST, LOAD_NEWS_LIST_SUCCESS, LOAD_NEWS_LIST_ERROR,
    LOAD_NEWS_ITEM, LOAD_NEWS_ITEM_SUCCESS, LOAD_NEWS_ITEM_ERROR
} from "../constants";

export function loadNews(id) {
    return async(dispatch, getState, {fetch}) => {
        // eslint-disable-next-line no-param-reassign
        const payload = {};

        dispatch({
            type: LOAD_NEWS_ITEM,
            payload
        });

        try {
            let response = await fetch(`article/${id}`, {
                method: "GET",
                credentials: 'include'
            });
            let data = await response.json();
            if (!response.ok) {
                const message = data.message || data;
                if (data) throw message;
            }

            dispatch({
                type: LOAD_NEWS_ITEM_SUCCESS,
                payload: data
            });

        } catch (error) {
            dispatch({
                type: LOAD_NEWS_ITEM_ERROR,
                error: error,
            });
            throw new Error(error)
        }
    };
}

export function loadNewsFilter(query) {
  return {
    type: "LOAD_NEWS_FILTER",
    payload: query
  };
}

export function loadNewsHomeFilter(query) {
  return {
    type: "LOAD_NEWS_HOME_FILTER",
    payload: query
  };
}

export function loadNewsList({params = {}}) {
    return async(dispatch, getState, {fetch}) => {
        // eslint-disable-next-line no-param-reassign
        const payload = {};

        dispatch({
            type: LOAD_NEWS_LIST,
            payload
        });

        try {
            let response = await fetch(`article${params}`, {
                method: "GET",
                credentials: 'include'
            });
            let data = await response.json();
            if (!response.ok) {
                const message = data.message || data;
                if (data) throw message;
            }

            dispatch({
                type: LOAD_NEWS_LIST_SUCCESS,
                payload: data
            });

        } catch (error) {
            dispatch({
                type: LOAD_NEWS_LIST_ERROR,
                error: error,
            });
            return false;
        }
        return true;
    };
}