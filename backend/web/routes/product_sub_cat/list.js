'use strict'
import { ProductSubCat } from '../../../model'

export default async function getSubCats (req, res) {
  const cats = await ProductSubCat.findAll({order: '`order` ASC'})
  res.callback(cats)
}
