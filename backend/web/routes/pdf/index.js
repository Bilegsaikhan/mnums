'use strict'
import { Pdf as Model } from '../../../model'
import Err from '../../../modules/err'
import { pick } from 'underscore'

export default ({router, passport, checkLoggedIn, multipartMiddleware, wrap, checkRootAuth}) => {
  router.route('/pdf')
    .post(checkLoggedIn, wrap(async (req, res) => { // create
      req.checkBody('name', 'bad_name').len(1, 255)
      req.checkBody('thumb', 'bad_thumb').len(1, 255)
      req.checkBody('url', 'bad_url').len(1, 255)
      const err = req.validationErrors()
      if (err) return res.callback(err)
      const instance = await Model.create(pick(req.body, 'name', 'thumb', 'url'))
      res.callback(instance)
    }))
    .get(wrap(async (req, res) => { // list
      if (req.body.offset) {
        req.checkBody('offset', 'bad_offset').isInt()
      }
      if (req.body.limit) {
        req.checkBody('limit', 'bad_limit').isInt()
      }
      if (req.body.string) {
        req.checkBody('string', 'bad_string').len(1, 255)
      }
      const {offset, limit, string} = req.query
      const total = await Model.toCount({string})
      if (total === 0) {
        return res.callback({total: 0, list: []})
      }
      const list = await Model.list({offset, limit, string})
      res.callback({total, list})
    }))
  router.route('/pdf/:id')
    .delete(checkLoggedIn, wrap(async (req, res) => { // delete
      const instance = await Model.findById(req.params.id)
      if (!instance) return res.callback(Err.notfound())
      await instance.destroy()
      res.callback()
    }))
    .get(wrap(async (req, res) => { // get
      const instance = await Model.findById(req.params.id)
      if (!instance) return res.callback(Err.notfound())
      res.callback(instance)
    }))
    .put(checkLoggedIn, wrap(async (req, res) => { // update
      req.checkBody('name', 'bad_name').len(1, 255)
      req.checkBody('thumb', 'bad_thumb').len(1, 255)
      req.checkBody('url', 'bad_url').len(1, 255)
      const err = req.validationErrors()
      if (err) return res.callback(err)
      const instance = await Model.findById(req.params.id)
      if (!instance) return res.callback(Err.notfound())
      const updatedInstance = await instance.updateAttributes(pick(req.body, 'name', 'thumb', 'url'))
      res.callback(updatedInstance)
    }))
}
