'use strict'
import { Log } from '../../../model'
// import Err from '../../../modules/err'

const debug = require('../../../modules/debug')(__dirname)
const moduleIndex = {
  admin: 'Админ',
  article: 'Мэдээ мэдээлэл',
  cat: 'Мэдээний ангилал',
  law: 'Хууль эрх зүй',
  menu: 'Цэс',
  page: 'Хуудас',
  pdf: 'PDF',
  project: 'Төсөл',
  project_branch: 'Төслийн салбар',
  project_region: 'Төслийн бүс',
  project_source: 'Төслийн эх үүсвэр',
  report: 'Тайлан',
  slide: 'Онцлох',
  store: 'Статик',
  timeline: 'Үйл явдлын товчоон',
  upload: 'Хуулах'
}

export default async function logs (req, res, next) {
  if (req.query.offset) {
    req.checkQuery('offset', 'bad_offset').isInt()
  }
  if (req.query.limit) {
    req.checkQuery('limit', 'bad_limit').isInt()
  }
  const err = req.validationErrors()
  if (err) return res.callback(err)
  const {offset, limit} = req.query
  const total = await Log.count()
  const list = await Log.list({offset, limit}).then(list => list.map(o => o.get({plain: true})))
  convertLogs(list)
  res.callback({total, list})
}

function convertLogs (logs) {
  logs.forEach(log => {
    const [, , module, extra] = log.path.split('/')
    log.module = moduleIndex[module]
    if (log.method === 'POST') {
      log.action = 'Оруулсан'
    } else if (log.method === 'PUT' && parseInt(extra)) {
      log.action = 'Зассан'
      log.source = extra
    } else if (log.method === 'PUT') {
      log.action = 'Дараалал өөрчилсөн'
    } else if (log.method === 'DELETE') {
      log.action = 'Устгасан'
      log.source = extra
    } else {
      log.action = 'Тодорхойлогдоогүй'
    }
    if (module === 'upload') {
      if (extra === 'image') {
        log.module = 'Зураг'
      } else if (extra === 'pdf') {
        log.module = 'PDF'
      }
      log.action = 'Хуулсан'
    } else if (module === 'login') {
      log.module = 'Админ'
      log.action = 'Нэвтэрсэн'
    } else if (module === 'logout') {
      log.module = 'Админ'
      log.action = 'Гарсан'
    }
  })
}
