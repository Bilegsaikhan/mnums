'use strict'
const tableName = 'gallery_item'
module.exports = function (sequelize, DataTypes) {
  return sequelize.define(tableName, {
    cat_id: DataTypes.INTEGER,
    url: DataTypes.STRING,
  })
}

