'use strict'
import { Product } from '../../../model'
import Err from '../../../modules/err'

export default async function deleteProduct (req, res) {
  req.checkParams('id', 'bad_id').isInt()
  const err = req.validationErrors()
  if (err) return res.callback(err)
  const {id} = req.params
  let product = await Product.findById(id)
  if (!product) return res.callback(Err.notfound())
  await product.destroy()
  res.callback()
}
