/**
 * Created by enxtur on 5/5/17.
 */

import { Project } from '../../../model'
import { format } from '../../../modules/formatter'
export default async (req, res, next) => {
  req.checkQuery('offset', 'bad_offset').optional().isInt()
  req.checkQuery('limit', 'bad_limit').optional().isInt()
  req.checkQuery('branch_id', 'bad_branch_id').optional().isInt()
  req.checkQuery('region_id', 'bad_region_id').optional().isInt()
  req.checkQuery('source_id', 'bad_source_id').optional().isInt()
  req.checkQuery('string', 'bad_string').optional().len(1, 255)
  const err = req.validationErrors()
  if (err) return res.callback(err)
  const {offset, limit, branch_id, region_id, source_id, string} = req.query
  console.log(offset, limit, branch_id, region_id, source_id)
  const params = {
    offset,
    limit,
    branch_id,
    region_id,
    source_id,
    string
  }
  const total = await Project.toCount(params)
  if (total === 0) {
    return res.callback({
      total: 0,
      list: []
    })
  }
  const list = await Project.list(params)
  res.callback({total, list})
}
