import React, { Component } from 'react';
import { connect } from 'react-redux';

export class Event extends Component {
  static propTypes = {};

  render() {
    return (
      <div>
        <li className="dropdown ecosystem">
          <a
            href="#"
            className="dropdown-toggle"
            data-toggle="dropdown"
            role="button"
            aria-haspopup="true"
            aria-expanded="false"
          >
            Resources
          </a>
          <ul className="dropdown-menu">
            <li>
              <a id="btn-header-businesses-resource-center" href="/resources">
                Resource Center
              </a>
            </li>
            <li className="split">
              <ul>
                <li>
                  <span>Assets</span>
                </li>
                <li>
                  <a className="sm" href="/resources/articles">
                    Articles
                  </a>
                </li>
                <li>
                  <a className="sm" href="/resources/case-studies">
                    Case Studies
                  </a>
                </li>
              </ul>
              <ul>
                <li>
                  <span>Media</span>
                </li>
                <li>
                  <a className="sm" href="/resources/webinars">
                    Webinars
                  </a>
                </li>
                <li>
                  <a className="sm" href="/resources/videos">
                    Videos
                  </a>
                </li>
                <li>
                  <a
                    className="sm"
                    href="https://betontheweb.ionicframework.com/episodes"
                    target="_blank"
                  >
                    Podcast <icon-external />
                  </a>
                </li>
              </ul>
            </li>
            <li className="divider" />
            <li>
              <a href="/what-is-ionic">What Is Ionic?</a>
            </li>
            <li>
              <a href="https://forum.ionicframework.com">Ionic Forum</a>
            </li>
            <li>
              <a id="btn-header-resources-pwas" href="/pwa">
                PWAs
              </a>
            </li>
            <li>
              <a href="https://ionic.zendesk.com/">Help Center</a>
            </li>
          </ul>
        </li>
      </div>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Event);
