export class ReportFormController {
  constructor($log, reportService, $state, toastr, modalService) {
    'ngInject';
    this.$log = $log.log
    this.service = reportService
    this.toastr = toastr
    this.$state = $state
    this.stateName = $state.current.name
    this.modalService = modalService
    this.clear()
    if (this.stateName === 'root.content.report.edit') this.load()

    this.types = [
      {
        name: 'Аудитлагдсан тайлан',
        value: 'audit'
      }, {
        name: 'Жилийн тайлан',
        value: 'year'
      }
    ]

    this.dates = []
    const thisYear = new Date().getFullYear()

    for (let i = 2011; i <= thisYear; i++) {
      this.dates.push(i.toString())
    }
  }

  clear() {
    this.item = {
      mn: true,
      en: false,
      type: "",
      year: "",
      name: "2013 оны жилийн эцсийн тайлан",
      name_en: "Financial Statements and Independent Auditor’s Report 2013",
      pdf: '',
      pdf_en: ''
    }
  }

  create() {
    this.service.create(this.item).then(item => {
      this.item = item
      this.$state.go('root.content.report.list');
    })
  }

  update() {
    this.service.update(this.item).then(item => {
      this.item = item
      this.toastr.success('Таны хүсэлт амжилттай!');
    }, () => {
      this.toastr.error('Таны хүсэлт амжилтгүй!');
    })
  }

  remove() {
    this.modalService.danger('Та уг үүнийг устгахдаа итгэлтэй байна уу', true).then(() => {
      this.service.remove({id: this.$state.params.id}).then(() => {
        this.toastr.success('Таны хүсэлт амжилттай!');
        this.$state.go('root.content.report.list');
      }, () => {
        this.toastr.error('Таны хүсэлт амжилтгүй!');
      })
    })
  }

  load() {
    this.service.get({id: this.$state.params.id}).then(item => {
      this.item = item
    }, () => {
      this.toastr.error('Таны хүсэлт амжилтгүй!');
    })
  }


}
