export class UserService {
  constructor (server) {
    'ngInject';
    this.profile = {
      user: null,
      logged: false,
      overlay: false
    }
    this.server = server
  }

  isRoot() {
    return this.profile.user && this.profile.user.root
  }

  login ({username, password}) {
    return this.server.post('/login', {username, password}).then(user => {
      this.profile.user = user
      this.profile.logged = true
      this.profile.overlay = false
      return user
    })
  }

  logout () {
    return this.server.post('/logout').then(res => {
      this.profile.user = null
      this.profile.logged = false
      this.profile.overlay = false
      return res
    })
  }

  logged () {
    return this.server.get('/profile').then(res => {
      if (res.user) {
        this.profile.user = res.user
        this.profile.logged = true
        this.profile.overlay = false
      } else {
        this.profile.user = null
        this.profile.logged = false
        this.profile.overlay = false
      }
      return res
    })
  }

  list () {
    return this.server.get('/admin')
  }
  get ({id}) {
    return this.server.get(`/admin/${id}`)
  }
  update (data) {
    return this.server.put(`/admin/${data.id}`, data)
  }
  remove (data) {
    return this.server.delete(`/admin/${data.id}`)
  }
  create (data) {
    return this.server.post(`/admin`, data)
  }
}
