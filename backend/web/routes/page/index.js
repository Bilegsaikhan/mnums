'use strict'
const debug = require('../../../modules/debug')(__filename)
import create from './create'
import update from './update'
import get from './get'
import del from './delete'
import list from './list'
import getByModule from './get_by_module'
export default ({router, passport, checkLoggedIn, multipartMiddleware, wrap, checkRootAuth}) => {
  router.route('/page')
    .get(wrap(list))
    .post(checkLoggedIn, wrap(create))
  router.route('/page/module/:module')
    .get(wrap(getByModule[0]), getByModule[1])
  router.route('/page/:id')
    .put(checkLoggedIn, wrap(update))
    .get(wrap(get))
    .delete(checkLoggedIn, wrap(del))

}
