export class ProjectFormController {
  constructor ($log,
               projectService,
               sectorService,
               regionService,
               fundingService,
               server,
               $state,
               toastr,
               modalService) {
    'ngInject';
    this.$log = $log.log
    this.sectorService = sectorService
    this.regionService = regionService
    this.fundingService = fundingService
    this.projectService = projectService
    this.server = server
    this.toastr = toastr
    this.$state = $state
    this.stateName = $state.current.name
    this.modalService = modalService
    this.clear()
    this.loadSectors()
    this.loadRegions()
    this.loadFundings()
    if (this.stateName === 'root.content.project.edit') this.load()
  }

  clear () {
    this.project = {
      mn: true,
      en: false,
      image: '',
      branch_id: '',
      region_id: '',
      source_id: '',
      performance: 0,
      program: '',
      program_en: '',
      loan_type: '',
      loan_type_en: '',
      borrower: '',
      borrower_en: '',
      implementer: '',
      implementer_en: '',
      objective: '',
      objective_en: '',
      sankhuu_amount: '',
      sankhuu_amount_en: '',
      economic: '',
      economic_en: '',
      social: '',
      social_en: '',
      introduction: '',
      introduction_en: '',
      start_date: '',
      start_date_en: '',
      end_date: '',
      end_date_en: '',
      name: '',
      name_en: '',
      togtool_amount: '',
      togtool_amount_en: '',
      geree_amount: '',
      geree_amount_en: '',
      duty: '',
      duty_en: '',
      interpretation: '',
      interpretation_en: ''
    }
  }

  loadSectors () {
    this.sectorService.list().then(items => {
      this.sectors = items
    })
  }

  loadRegions () {
    this.regionService.list().then(items => {
      this.regions = items
    })
  }

  loadFundings () {
    this.fundingService.list().then(items => {
      this.fundings = items
    })
  }

  create () {
    this.$log(this.project)

    this.projectService.create(this.project).then(project => {
      this.project = project
      this.toastr.success('Таны хүсэлт амжилттай!');
      this.$state.go('root.content.project.list');
    }, () => {
      this.toastr.error('Таны хүсэлт амжилтгүй!');
    })
  }

  update () {
    this.projectService.update(this.project).then(project => {
      this.project = project
      this.toastr.success('Таны хүсэлт амжилттай!');
    }, () => {
      this.toastr.error('Таны хүсэлт амжилтгүй!');
    })
  }

  remove () {
    this.modalService.danger('Та уг үүнийг устгахдаа итгэлтэй байна уу', true).then(() => {
      this.projectService.remove({id: this.$state.params.id}).then(() => {
        this.toastr.success('Таны хүсэлт амжилттай!');
        this.$state.go('root.content.project.list');
      }, () => {
        this.toastr.error('Таны хүсэлт амжилтгүй!');
      })
    })
  }

  load () {
    this.projectService.get({id: this.$state.params.id}).then(project => {
      this.project = project
    }, () => {
      this.toastr.error('Таны хүсэлт амжилтгүй!');
    })
  }

  onReady () {
    this.$log('ready')
  }

  uploadDone (image) {
    this.project.image = image
  }
}

