'use strict'
import moment from 'moment'
const booleanPossiblities = [true, 'true', 1, '1', false, 'false', 0, '0']
export default {
  isArray: value => Array.isArray(value),
  isBoolean: value => booleanPossiblities.indexOf(value) !== -1,
  isDate: value => moment(value, 'YYY-MM-DDTHH:mm:ssZ').isValid()
}
