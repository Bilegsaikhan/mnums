import React, {Component} from 'react';
import './Shilendans.css';
import {connect} from 'react-redux'
import language from "../../../constants/language"

class Shilendans extends Component {
    render() {
        const {lang} = this.props
        return (
            <div>
                <div className="g01v1-this-page-in">
                    <div className="container">
                        <div className="row">
                            <div className="col-xs-5 col-sm-6">
                                <div className="breadcrumb_wbr wb_breadcrumb">
                                    <div className="g04v1-text">
                                        <ol className="breadcrumb">
                                            <li><a>Hүүр хуудас</a></li>
                                            <li><a>Ил тод байдал</a></li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="container">
                    <div className="titlepar parsys">
                        <div className="title section">
                            <div className="row">
                                <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div className="c01v1-page-title">Шилэн Данс</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="par parsys">
                        <div className="gridlayout parbase section">
                            <div className="row">
                                <div className="col-sm-12">
                                    <div>
                                        <div className="grid_10_par_1 parsys">
                                            <div className="remove-padding-grid">
                                                <div className="gridlayout parbase section pageContent loaderBox">

                                                    <div className="loading">
                                                        <div className="showbox">
                                                            <div className="loader">
                                                                <svg className="circular" viewBox="25 25 50 50">
                                                                    <circle className="path" cx="50" cy="50" r="20"
                                                                            fill="none" strokeWidth="2"
                                                                            strokeMiterlimit="10"/>
                                                                </svg>
                                                            </div>
                                                            {language.loading[lang]}...
                                                        </div>
                                                    </div>

                                                    <div className="row">
                                                        <div className="col-md-12">
                                                            <div className="">
                                                                <div className="row dbm-page-content">
                                                                    <div className="col-md-12">
                                                                        <iframe width="100%" height="650"
                                                                                frameBorder="0" allowFullScreen=""
                                                                                src="https://shilendans.gov.mn/embed/5195"
                                                                                style={{border: 0}}>
                                                                        </iframe>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="clearfix"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="clearfix"/>
                        </div>

                    </div>
                </div>
            </div>

        );
    }
}

const mapStateToProps = (state) => {
    const {config} = state

    return {
        lang: config.language
    }
}

const mapDispatch = {};

export default connect(mapStateToProps, mapDispatch)(Shilendans)