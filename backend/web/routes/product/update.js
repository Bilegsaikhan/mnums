'use strict'
import { Product, ProductImage, ProductCat, ProductSubCat, Provider } from '../../../model'
import Err from '../../../modules/err'
import fall from 'async-waterfall'

export default async function updateProduct(req, res) {

  req.checkBody('id', 'bad_id').isInt()
  req.checkBody('product_cat_id', 'bad_product_cat_id').isInt()
  req.checkBody('provider_id', 'bad_provider_id').isInt()
  req.checkBody('mn', 'bad_mn').isBoolean()
  req.checkBody('en', 'bad_en').isBoolean()

  let { mn, en } = req.body;

  mn = (mn === true || mn === 'true')
  en = (en === true || en === 'true')

  if (mn) {
    req.checkBody('name', 'bad_name').len(1, 255)
    req.checkBody('desc', 'bad_desc').len(1, 255)
  }
  if (en) {
    req.checkBody('name_en', 'bad_name_en').len(1, 255)
    req.checkBody('desc_en', 'bad_desc_en').len(1, 255)
  }

  const err = req.validationErrors()
  if (err) return res.callback(err);

  if (!req.body.product_sub_cat_id) {
    req.body.product_sub_cat_id = null;
  }

  if (!(req.body.images && req.body.images.length)) {
    return res.callback(new Error('bad_images'));
  }

  let checker = false;

  req.body.images.forEach((entry) => {
    if (entry.is_main === 'true' || entry.is_main === true) {
      checker = true;
    }
  })


  if (checker === false) {
    req.body.images[0].is_main = true;
  }

  try {
    const { id, name, name_en, desc, desc_en, content, content_en, product_cat_id, product_sub_cat_id, provider_id, price } = req.body;

    const product = await Product.find({ where: { id: id } })

    if (!product) return res.callback(Err.notfound())

    let createData = {
      id,
      product_cat_id,
      product_sub_cat_id,
      provider_id,
      mn,
      en,
      price
    }
    if (mn) {
      createData = {
        ...createData,
        name,
        desc,
        content
      }
    }
    if (en) {
      createData = {
        ...createData,
        name_en,
        desc_en,
        content_en
      }
    }


    await ProductImage.destroy({ where: { product_id: id } });

    let tasks = [];

    req.body.images.forEach((entry) => {
      let task = (callback) => {
        let image = {
          url: entry.url,
          product_id: id,
          is_main: (entry.is_main === true || entry.is_main === 'true')
        }

        ProductImage.create(image).then(
          () => {
            console.log('done!!!')
            callback();
          },
          (e) => {
            callback(e);
          }
        )
      }

      tasks.push(task);
    })

    fall(tasks, async (e) => {
      console.log('!!!!!', e);
      await product.updateAttributes(createData);
      const productResponse = await Product.find({
        where: { id: id },
        include: [{
          model: ProductImage,
          as: 'images'
        }, {
          model: ProductCat,
          as: 'productCat'
        }, {
          model: ProductSubCat,
          as: 'productSubCat'
        }, {
          model: Provider,
          as: 'provider'
        }]
      })


      res.callback(productResponse)
    })

  } catch (err) {
    res.callback(Err.sequelize(err))
  }
}
