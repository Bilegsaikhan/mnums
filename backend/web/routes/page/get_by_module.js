'use strict'
import { Page, Menu } from '../../../model'
import Err from '../../../modules/err'
import { format } from '../../../modules/formatter'

// const debug = require('../../../modules/debug')(__dirname)
export default [
  async (req, res, next) => {
    req.checkParams('module', 'bad_module').len(1, 255)
    const err = req.validationErrors()
    if (err) return res.callback(err)
    const {module} = req.params
    const menu = await Menu.find({
      where: {module}
    })
    if (!menu) return res.callback(Err.notfound('menu not found'))
    const lang = req.session.lang || 'mn'
    if (lang === 'mn' && !menu.mn) return res.callback(Err.notfound('menu not found'))
    if (lang === 'en' && !menu.en) return res.callback(Err.notfound('menu not found'))
    if (!menu.page_id) return res.callback(Err.notfound('menu has no page id'))
    let page = await Page.find({
      where: {id: menu.page_id},
      raw: true
    })
    // .then(o => o.get({plain: true}))
    if (!page) return res.callback(Err.notfound('page not found'))
    console.log(page)
    res.data = page
    next()
  },
  format({name: true, content: true})
]
