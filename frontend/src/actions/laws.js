import {LOAD_LAWS, LOAD_LAWS_SUCCESS, LOAD_LAWS_ERROR} from "../constants";

export function loadLaws(key) {
    return async (dispatch, getState, {fetch}) => {
        // eslint-disable-next-line no-param-reassign
        const payload = {};

        // console.log('key: ', key)
        // console.log('key: ', qs.stringify({key}))

        dispatch({
            type: LOAD_LAWS,
            payload
        });

        try {
            let response = await fetch(`law?type=${key}`, {
                method: "GET",
                credentials: 'include'
            });

            let data = await response.json();
            if (!response.ok) {
                const message = data.message || data;
                if (data) throw message;
            }
            dispatch({
                type: LOAD_LAWS_SUCCESS,
                payload: data,
                key: key
            });
            return data;
        } catch (error) {
            const message = error.message || error;
            dispatch({
                type: LOAD_LAWS_ERROR,
                error: error.message
            });
            throw new Error(message);
        }
    };
}
