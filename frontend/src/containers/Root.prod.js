import React from 'react'
import PropTypes from 'prop-types';
import { Provider } from 'react-redux'
import routes from '../routes'
import { Router } from 'react-router'

const Root = ({ store, history }) => (
    <Provider store={store}>
        <div>
            <Router onUpdate={() => window.scrollTo(0, 0)} history={history}>
                {routes(store)}
            </Router>
        </div>
    </Provider>
)

Root.propTypes = {
  store: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired
}
export default Root
