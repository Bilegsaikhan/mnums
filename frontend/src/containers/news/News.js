import React, {Component} from 'react';
import {Link} from 'react-router'
import {connect} from 'react-redux'
import ReactPaginate from 'react-paginate';
import TextTruncate from 'react-text-truncate';
import {browserHistory} from 'react-router'
import moment from 'moment'
import './News.css';
import {loadNewsList} from '../../actions/news'
import language from "../../constants/language"

class News extends Component {

    constructor(props) {
        super(props);

        this.state = {
            data: [],
            offset: 0,
            searchString: ''
        }
        this.searchChange = this.searchChange.bind(this);
        this.doSearch = this.doSearch.bind(this);
    }

    componentDidMount() {
        const {location: {query}} = this.props
        this.setState({searchString: query.string ? query.string : ''});
        this.maybeFetchData();
    }

    componentWillUpdate(nextProps) {
        if (nextProps.location.search !== this.props.location.search) this.maybeFetchData(nextProps);
    }

    maybeFetchData(props) {
        const {loadNewsList, location: {search}} = props || this.props;
        loadNewsList({params: search});
    }

    doSearch = (event) => {
        const {location: {query, pathname}} = this.props

        let search = {
            ...query,
            string: this.state.searchString ? this.state.searchString : undefined,
            offset: 0
        };
        browserHistory.push({
            pathname: pathname,
            query: search
        })

        event.preventDefault();
    };

    searchChange = (event) => {
        this.setState({searchString: event.target.value});
    };

    handlePageClick = (data) => {
        const {location: {query, pathname}} = this.props
        let search = {...query, offset: data.selected};
        browserHistory.push({
            pathname: pathname,
            query: search
        })
    };


    render() {

        const {news, total, basic, location: {query}, isFetching, lang} = this.props

console.log('isFetching: ', isFetching);
        // const isEmpty = news.length === 0;
        let pageCount = total / 10
        let currentPage = query && parseInt(query.offset || 0, 10)
        return (
            <div>
                <div className="g01v1-this-page-in">
                    <div className="container">
                        <div className="row">
                            <div className="col-xs-5 col-sm-6">
                                <div className="breadcrumb_wbr wb_breadcrumb">
                                    <div className="g04v1-text">
                                        <ol className="breadcrumb">
                                            <li><Link to={`/`}>{language.homepage[lang]}</Link></li>
                                            <li><a>{language.news[lang]}</a></li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="container">
                    <div className="titlepar parsys">
                        <div className="title section">
                            <div className="row">
                                <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div className="c01v1-page-title">{language.news[lang]}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="par parsys">
                        <div className="gridlayout parbase section">
                            <div className="row">
                                <div className="col-lg-3 col-md-3 col-sm-3 col-xs-12 hidden-xs">
                                    <div className="panel panel-default left-column refine-by-nav hidden-xs hidden-sm">
                                        <div className="panel-heading header">
                                            <div className="direction">{language.category[lang]} :</div>
                                        </div>
                                        <div className="panel-body">
                                            <ul>
                                                <li className="h7">{language.type[lang]}</li>
                                                {
                                                    basic.cats.map(cat => {
                                                        return (
                                                            <li key={`ncf_${cat.id}`}>
                                                                <div className="sub-link">
                                                                    <Link className={`active`} to={{
                                                                        pathname: 'news',
                                                                        query: {...query, cat_id: cat.id, offset: 0}
                                                                    }}>{cat.name}</Link>
                                                                    {parseInt(query.cat_id, 10) === cat.id &&
                                                                    <Link to={{
                                                                        pathname: 'news',
                                                                        query: {...query, offset: 0, cat_id: undefined}
                                                                    }}
                                                                          className="glyphicon glyphicon-remove-circle mrg-l-5 delete-item">&nbsp;
                                                                        <span className="screen-reader-text">clear filter</span></Link>
                                                                    }
                                                                </div>
                                                            </li>
                                                        )
                                                    })
                                                }
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                    <div className="center-padding-left center-left-border">
                                        <div className="row">
                                            <div className="col-md-12 col-xs-12">
                                                <div className="search-listing-searchbox">
                                                    <div className="serch-term dbmItemSearch">
                                                        <form className="input-group" onSubmit={this.doSearch}>
                                                            <input type="text"
                                                                   value={this.state.searchString}
                                                                   onChange={this.searchChange}
                                                                   placeholder={language.searchNews[lang]}
                                                                   className="f02v4-pulldown search-text"
                                                            />
                                                            <button className="f02v4-submit-icon" type="submit"
                                                                    alt="listingsearch">
                                                                <img src="/assets/images/search-icon-box.png" alt=""/>
                                                            </button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-xs-12 mrg-b-10">
                                                <div className="row">
                                                    <div className="col-xs-6 pad-t-10">
                                                        <label className="h7">
                                                            {lang === 'en' ?
                                                                `${total} results`
                                                                :
                                                                `Нийт ${total} Мэдээ байна.`
                                                            }

                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-12 col-sm-12 col-xs-12">
                                                <hr className="listing-hr"/>
                                                <div className={`projectLoading ${isFetching ? 'showLoading' : ''}`}>
                                                    <div className="showbox">
                                                        <div className="loader">
                                                            <svg className="circular" viewBox="25 25 50 50">
                                                                <circle className="path" cx="50" cy="50" r="20"
                                                                        fill="none" strokeWidth="2"
                                                                        strokeMiterlimit="10"/>
                                                            </svg>
                                                        </div>
                                                        {language.loading[lang]}...
                                                    </div>
                                                </div>
                                                <div className={`n07v4 dbmNewsList ${isFetching ? 'showLoading' : ''}`}>
                                                    <ul>

                                                        {
                                                            news.map(item => {
                                                                return (
                                                                    <li key={`newsItem_${item.id}`}>
                                                                        <div id="n07v4-content">
                                                                            <div className="n07v4-title">
                                                                                <Link to={`/news/${item.id}`}>
                                                                                    {item.title}
                                                                                </Link>
                                                                            </div>
                                                                            <div className="n07v4-blurb">
                                                                                <TextTruncate
                                                                                    line={2}
                                                                                    truncateText="…"
                                                                                    text={item.desc}
                                                                                    textTruncateChild={<Link
                                                                                        to={`/news/${item.id}`}>{language.seeMore[lang]}</Link>}
                                                                                />
                                                                            </div>
                                                                            <span className="n07v4-info">
                                                                                    <span
                                                                                        className="n07v4-info-title">{language.date[lang]}:</span>
                                                                                {moment(item.created_at).format("YYYY MM DD")}
                                                                                </span>
                                                                            <span className="n07v4-info">
                                                                                    <span className="n07v4-info-title">
                                                                                        {language.type[lang]}:
                                                                                    </span>
                                                                                {item.cat.name}
                                                                                </span>
                                                                        </div>
                                                                    </li>
                                                                )
                                                            })
                                                        }
                                                    </ul>
                                                </div>

                                                { total > 10 &&
                                                <ReactPaginate previousLabel={<i className="fa fa-arrow-left"/>}
                                                               nextLabel={<i className="fa fa-arrow-right"/>}
                                                               breakLabel={false}
                                                               breakClassName={"break-me"}
                                                               pageCount={pageCount}
                                                               forcePage={currentPage}
                                                               marginPagesDisplayed={5}
                                                               pageRangeDisplayed={5}
                                                               onPageChange={this.handlePageClick}
                                                               containerClassName={"pagination right"}
                                                               subContainerClassName={"pages pagination"}
                                                               activeClassName={"active"}/>
                                                }
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="clearfix"/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


const mapStateToProps = (state) => {
    const {core, news, basic, config} = state

    return {
        lang: config.language,
        basic,
        core,
        news: news.data,
        total: news.total,
        isFetching: news.isFetching
    }
}

export default connect(mapStateToProps, {
    loadNewsList
})(News)
