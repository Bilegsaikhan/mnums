export function PaginateDirective () {
  'ngInject';

  let directive = {
    restrict: 'AE',
    scope: {
      total_pages: '=pageCount',
      ntotal: '=pageTotal', // :10 утга энэ үед дугаар хүртэл хуудаслана.
      current_page: '=ngModel',
      ngChange: "&"
    },
    template: '<ul class="pagination-sm pagination" ng-show="total_pages > 1">' +
    '<li ng-class="{ disabled: first_disabled }"><a ng-click="firstPage()" href>Эхний</a></li>' +
    '<li ng-class="{ disabled: first_disabled }"><a ng-click="prevPage()" href>Өмнөх</a></li>' +
    '<li ng-repeat="page in pages" ng-class="{ active: page.active, disabled: page.disabled }" >' +
    '<a ng-hide="$last" ng-click="setPage(page)" href>{{ page.number }}</a>' +
    '</li>' +
    '<li ng-show="ntotal < total_pages && current_page < total_pages - 2 && pages[pages.length].number < total_pages - 2" class="disabled"><a>...</a></li>' +
    '<li ng-class="{ active: last_disabled }"><a ng-click="lastPage()" href>{{ total_pages }}</a></li>' +
    '<li ng-class="{ disabled: last_disabled }"><a ng-click="nextPage()" href>Дараах</a></li>' +
    '<li ng-class="{ disabled: last_disabled }"><a ng-click="lastPage()" href>Сүүлийн</a></li>' +
    '</ul>',
    replace: true,
    link: linkFunc
  };

  return directive;

  function linkFunc (scope) {
// console.log('it works')

    var left_step, right_step, temp_current_page;
    temp_current_page = parseInt(scope.ntotal / 2) + 1;
    left_step = parseInt(scope.ntotal / 2);
    right_step = scope.ntotal - temp_current_page;

    var paginate = function (/*total_pages*/) {

      scope.first_disabled = false;
      scope.last_disabled = false;

      if (!scope.current_page) {
        scope.current_page = 1;
      }

      if (scope.current_page == 1)
        scope.first_disabled = true;

      if (scope.current_page == scope.total_pages)
        scope.last_disabled = true;

      scope.pages = [];
      var first_count = 1;
      var last_count = scope.total_pages;

      if (scope.current_page - left_step > 0)
        first_count = scope.current_page - left_step;

      if (scope.current_page + right_step <= scope.total_pages)
        last_count = scope.current_page + right_step;

      if (scope.current_page - left_step <= 0)
        last_count = scope.ntotal;

      if (scope.current_page + right_step > scope.total_pages)
        first_count = scope.total_pages - (scope.ntotal - 1);

      if (scope.total_pages < scope.ntotal) {
        first_count = 1;
        last_count = scope.total_pages;
      }

      for (var i = first_count; i <= last_count; i++) {
        var page = {};
        page.number = i;

        if (scope.current_page == i)
          page.active = true;

        scope.pages.push(page);
      }

      scope.nextPage = function () {
        if (scope.current_page < scope.total_pages)
          scope.current_page++;
      };

      scope.prevPage = function () {
        if (scope.current_page > 1)
          scope.current_page--;
      };

      scope.firstPage = function () {
        scope.current_page = 1;
      };

      scope.lastPage = function () {
        scope.current_page = scope.total_pages;
      };

      scope.setPage = function (page) {
        scope.current_page = page.number;
      }
    };

    var pageChange = function () {
      // Бусад controller -д байгаа pageChange фунцыг дуудах.
      // scope.$emit('pageChanged', scope.current_page);
      scope.ngChange();
    };

    // Доорх хувьсагчидыг сонсох утга өөрчлөгдсөн үед функцуудыг дуудаж ажилуулна.
    scope.$watch('total_pages', paginate);
    scope.$watch('current_page', function () {
      pageChange();
      paginate();
    });
  }

}


