/**
 * Created by enxtur on 5/5/17.
 */

import { Admin } from '../../../model'
export default async (req, res) => {
  req.checkBody('offset', 'bad_offset').optional().len(1, 255)
  req.checkBody('limit', 'bad_limit').optional().len(1, 255)
  const err = req.validationErrors()
  if (err) return res.callback(err)
  const {offset, limit} = req.body
  const admins = await Admin.list({offset, limit})
  res.callback(admins)
}
