'use strict'
import md5 from 'md5'
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('admin', {
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    root: DataTypes.BOOLEAN
  }, {
    classMethods: {
      list ({offset = 0, limit = 10}) {
        return this.findAll({
          offset: offset * limit,
          limit: parseInt(limit),
          attributes: ['id', 'username', 'root']
        })
      },
      get (id) {
        return this.find({
          where: {id},
          attributes: ['id', 'username', 'root']
        })
      },
      findByUsername (username) {
        return this.find({
          where: {username}
        })
      }
    },
    instanceMethods: {
      validPassword (_password) {
        let password = md5(`FIRST${_password}EPISODE`)
        return this.password === password
      },
      updatePassword (_password) {
        let password = md5(`FIRST${_password}EPISODE`)
        return this.updateAttributes({password})
      }
    },
    hooks: {
      beforeCreate (admin, opt, cb) {
        admin.password = md5(`FIRST${admin.password}EPISODE`)
        cb(null, opt)
      }
    }
  })
}
