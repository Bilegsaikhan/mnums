'use strict'
import { Product, ProductCat, Provider } from '../../../model'

// const debug = require('../../../modules/debug')(__dirname)

export default async function (req, res, next) {
  res.callback({
    product: await Product.count(),
    product_cat: await ProductCat.count(),
    provider: await Provider.count(),
  })
}
