import React from "react";
import PropTypes from "prop-types";
import GoogleMap from "react-google-map";
import GoogleMapLoader from "react-google-maps-loader";
import "./GoogleMap.css";
import iconMarker from "./iconMarker.png";
const MY_API_KEY = "AIzaSyCf1s702VqOz5YX4y4ux0vPlKqmRn1IB08"; // fake

const Map = (
  { googleMaps } // GoogleMap component has a 100% height style. // You have to set the DOM parent height. // So you can perfectly handle responsive with differents heights.
) => (
    <div className={`map`}>
      <GoogleMap
        googleMaps={googleMaps}
        // You can add and remove coordinates on the fly.
        // The map will rerender new markers and remove the old ones.
        coordinates={[
          {
            title: "Харвардын судалгааны нэгж байгууллага - Mongolian Health Initiative",
            position: { lat: 47.890632, lng: 106.806255 },
            onLoaded: (googleMaps, map, marker) => {
                // Set Marker animation

                const icon = {
                    url: iconMarker,
                    anchor: new googleMaps.Point(10, 5),
                    scaledSize: new googleMaps.Size(50, 50)
                };

                marker.setIcon(icon);
            }
        }
        ]}
        center={{ lat: 47.890632, lng: 104.806255 }}
        zoom={4}
        onLoaded={(googleMaps, map) => {
          map.setMapTypeId(googleMaps.MapTypeId.ROADMAP);
        }}
        styles={[
          {
            featureType: "administrative",
            elementType: "labels.text.fill",
            stylers: [
              {
                color: "#444444"
              }
            ]
          },
          {
            featureType: "landscape",
            elementType: "all",
            stylers: [
              {
                color: "#f2f2f2"
              }
            ]
          },
          {
            featureType: "poi",
            elementType: "all",
            stylers: [
              {
                visibility: "off"
              }
            ]
          },
          {
            featureType: "road",
            elementType: "all",
            stylers: [
              {
                saturation: -100
              },
              {
                lightness: 45
              }
            ]
          },
          {
            featureType: "road.highway",
            elementType: "all",
            stylers: [
              {
                visibility: "simplified"
              }
            ]
          },
          {
            featureType: "road.arterial",
            elementType: "labels.icon",
            stylers: [
              {
                visibility: "off"
              }
            ]
          },
          {
            featureType: "transit",
            elementType: "all",
            stylers: [
              {
                visibility: "off"
              }
            ]
          },
          {
            featureType: "water",
            elementType: "all",
            stylers: [
              {
                color: "#cbcbcc"
              },
              {
                visibility: "on"
              }
            ]
          }
        ]}
      />
    </div>
  );

Map.propTypes = {
  googleMaps: PropTypes.object.isRequired
};

export default GoogleMapLoader(Map, {
  libraries: ["places"],
  key: MY_API_KEY
});
