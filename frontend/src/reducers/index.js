import * as ActionTypes from "../actions";
import {routerReducer as routing} from "react-router-redux";
import {combineReducers} from "redux";
import core from "./core";
import projects from "./projects";
import news from "./news";
import report from "./report";
import menu from "./menu";
import home from "./home";
import staticReducer from "./static";
import search from "./search";
import laws from "./laws";

const defaultState = {
    language: "mn"
};

// Updates error message to notify about the failed fetches.
const errorMessage = (state = {}, action) => {
    const {type, error} = action;
    if (type === ActionTypes.RESET_ERROR_MESSAGE) {
        return null;
    } else if (error) {
        return error;
    }
    return state;
};

// Updates configuration here!
const basic = (state = {}, action) => {
    if(action.type === ActionTypes.LOAD_BASICINFO_SUCCESS) {
        return action.payload
    }
    return state;
}
const config = (state = defaultState, action) => {
    switch (action.type) {
        case ActionTypes.LOAD_LANGUAGE: {
            return {
                ...state
            };
        }
        case ActionTypes.LOAD_LANGUAGE_SUCCESS: {
            return {
                ...state,
                language: action.payload
            };
        }
        case ActionTypes.LOAD_LANGUAGE_ERROR: {
            return {
                ...state
            };
        }

        case ActionTypes.CHANGE_LANGUAGE: {
            return {
                ...state
            };
        }
        case ActionTypes.CHANGE_LANGUAGE_SUCCESS: {
            return {
                ...state
            };
        }
        case ActionTypes.CHANGE_LANGUAGE_ERROR: {
            return {
                ...state
            };
        }

        default: {
            return state;
        }
    }
};

// Updates userAgent here!
const userAgent = (state = {}, action) => {
    const {type} = action;
    if (type === ActionTypes.LOAD_AGENT) {
        return action.payload;
    }
    return state;
};

const rootReducer = combineReducers({
    home,
    laws,
    search,
    staticReducer,
    basic,
    menu,
    report,
    news,
    userAgent,
    projects,
    core,
    config,
    errorMessage,
    routing
});

export default rootReducer;
