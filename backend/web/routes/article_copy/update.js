'use strict'
import { ArticleCopy as Article } from '../../../model'
import Err from '../../../modules/err'
import moment from 'moment'

export default async function updateArticle (req, res) {
  console.log(req.body)
  req.checkBody('title', 'bad_title').len(1, 255)
  req.checkBody('desc', 'bad_desc').len(1, 2000)

  const err = req.validationErrors()
  if (err) return res.callback(err)
  const {id} = req.params
  const {title, desc, content, picture} = req.body
  let article = await Article.findById(id)
  if (!article) return res.callback(Err.notfound())
  let updateData = {
    title,
    desc,
    content,
    picture
  }
  article = await article.updateAttributes(updateData)
  res.callback(article)
}
