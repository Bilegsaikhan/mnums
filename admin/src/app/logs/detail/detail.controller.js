export class LogsDetailController {
  constructor($log, lawsService, $state, toastr, modalService) {
    'ngInject';
    this.$log = $log.log
    this.service = lawsService
    this.toastr = toastr
    this.$state = $state
    this.stateName = $state.current.name
    this.modalService = modalService
    this.clear()
    if (this.stateName === 'root.content.laws.edit') this.load()

    this.types = [
      {
        name: 'Засгийн газрын тогтоол',
        value: 'resolution'
      }, {
        name: 'Хууль тогтоомж',
        value: 'law'
      }
    ]

    this.dates = []
    const thisYear = new Date().getFullYear()

    for (let i = 2011; i <= thisYear; i++) {
      this.dates.push(i.toString())
    }
  }

  clear() {
    this.item = {
      type: "",
      mn: true,
      en: false,
      name: "",
      name_en: "",
      pdf: "",
      pdf_en: ""
    }
  }

  create() {
    this.service.create(this.item).then(item => {
      this.item = item
      this.$state.go('root.content.laws.list');
    })
  }

  update() {
    this.service.update(this.item).then(item => {
      this.item = item
      this.toastr.success('Таны хүсэлт амжилттай!');
    }, () => {
      this.toastr.error('Таны хүсэлт амжилтгүй!');
    })
  }

  remove() {
    this.modalService.danger('Та уг үүнийг устгахдаа итгэлтэй байна уу', true).then(() => {
      this.service.remove({id: this.$state.params.id}).then(() => {
        this.toastr.success('Таны хүсэлт амжилттай!');
        this.$state.go('root.content.laws.list');
      }, () => {
        this.toastr.error('Таны хүсэлт амжилтгүй!');
      })
    })
  }

  load() {
    this.service.get({id: this.$state.params.id}).then(item => {
      this.item = item
    }, () => {
      this.toastr.error('Таны хүсэлт амжилтгүй!');
    })
  }


}
