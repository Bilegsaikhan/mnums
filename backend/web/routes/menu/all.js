/**
 * Created by enxtur on 5/5/17.
 */

import { Menu } from '../../../model'
import { format } from '../../../modules/formatter'

// const debug = require('../../../modules/debug')(__dirname)
export default [
  async (req, res, next) => {
    const lang = req.session.lang || 'mn'
    res.data = await Menu.all({lang})
    next()
  },
  format([{
    name: true,
    menus: [{
      name: true,
      menus: [{
        name: true
      }]
    }]
  }])
]
