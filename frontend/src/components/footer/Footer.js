import React, { Component } from 'react';
import { Link } from 'react-router';
import { connect } from "react-redux";
import _find from "lodash/find";
import './Footer.css';
import logo from '../header/logo.jpg';
import logoEn from '../header/logo-en.jpg';
import language from "../../constants/language";

class Footer extends Component {
  render() {
    const {
      lang,
      facebook,
      youtube,
      twitter,
      linkedin,
    } = this.props;
    return (
      <footer>
        <div className="full-row-footer-components">
          <div className="container">
            <div className="footer">
              <div className="footer-top">
                <div className="row">
                  <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div className="footer-menu-item">
                      <div className="row">
                        <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6 visible-lg visible-md">

                          { lang === 'en' ?
                            <img className="footerLogo" width="100" src={logoEn} alt="" />
                            :
                            <img className="footerLogo" width="100" src={logo} alt="" />
                          }

                        </div>
                        <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                          <ul className="footerNav">
                            <li><Link to="/news">{language.news[lang]}</Link></li>
                            <li><Link to="/job">{language.careers[lang]}</Link></li>
                            <li><Link to="/contact">{language.contactus[lang]}</Link></li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <hr className="visible-sm visible-xs" />
                    <div className="row">
                      <div className="col-lg-6 col-md-12 col-sm-6 col-xs-12">
                        <div className="social-icon">
                          <h4>{language.social[this.props.lang]}</h4>
                          <ul>
                            <li>
                              <a href={facebook && facebook.data} target="_blank" className="social-icon-img">
                                <img src="/assets/images/svg/icons-facebook.svg" title="Image" alt="facebook" />
                              </a>
                            </li>
                            <li>
                              <a href={twitter && twitter.data} className="social-icon-img" target="_blank">
                                <img src="/assets/images/svg/icons-twitter.svg" title="Image" alt="twitter" />
                              </a>
                            </li>
                            <li>
                              <a href={linkedin && linkedin.data} className="social-icon-img" target="_blank">
                                <img src={`${process.env.PUBLIC_URL}/assets/images/svg/linkedln.svg`} height={25} title="linkedln" alt="linkedln" />
                              </a>
                            </li>
                            <li>
                              <a href={youtube && youtube.data} className="social-icon-img" target="_blank">
                                <img src="/assets/images/svg/icons-youtube.svg" title="Image" alt="youtube" />
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div className="col-lg-6 col-md-12 col-sm-6 col-xs-12">
                        <div className="footer-newsletter">
                          <h4>{language.email[lang]}</h4>
                          <div className="search-section">
                            <p>
                              <a style={{ color: '#fff' }} href="mailto:info@mhi.mn">info@mhi.mn</a>
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="" style={{ backgroundColor: '#46439f', borderTop: '1px solid #503131' }}>
          <div className="container">
            <div
              className="footer"
              style={{ padding: '10px 0' }}
            >
              <div className="footer-copy-right text-center">
                {language.copyRight[lang]}
              </div>
            </div>
          </div>
        </div>
      </footer>
    );
  }
}

const mapStateToProps = (state) => {
  const { config, basic } = state;
  const facebook = _find(basic.stores, { key: 'facebook' });
  const youtube = _find(basic.stores, { key: 'youtube' });
  const twitter = _find(basic.stores, { key: 'twitter' });
  const linkedin = _find(basic.stores, { key: 'linkedin' });

  return {
    lang: config.language,
    facebook,
    youtube,
    twitter,
    linkedin,
  };
};

const mapDispatch = {};

export default connect(mapStateToProps, mapDispatch)(Footer);
