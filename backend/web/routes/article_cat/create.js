'use strict'
import { ArticleCat } from '../../../model'
import Err from '../../../modules/err'

export default async function createCat (req, res) {
  req.checkBody('name', 'bad_name').len(1, 255)
  const err = req.validationErrors()
  if (err) return res.callback(err)
  try {
    const cat = await ArticleCat.create({
      name: req.body.name
    })
    res.callback(cat)
  } catch (err) {
    res.callback(Err.sequelize(err))
  }
}
