'use strict'
import { Provider } from '../../../model'
import Err from '../../../modules/err'

export default async function updateProvider (req, res) {
  req.checkParams('id', 'bad_id').isInt()
  const err = req.validationErrors()
  if (err) return res.callback(err)
  const {id} = req.params
  let cat = await Provider.findById(id)
  if (!cat) return res.callback(Err.notfound())
  cat = await cat.updateAttributes(req.body)
  res.callback(cat)
}
