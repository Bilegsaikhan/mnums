const schema = [{
  name: true,
  menus: [{
    name: true,
    menus: [{
      name: true
    }]
  }],
  page: {
    name: true,
    articles: [{
      content: true,
      cat: {
        name: true
      }
    }]
  }
}]

const menus = [{
  name: 'Меню',
  name_en: 'Menu',
  menus: [{
    name: 'Дэд меню',
    name_en: 'Sub menu',
    menus: []
  }],
  page: {
    name: 'Хуудас',
    name_en: 'Page',
    articles: [{
      content: 'Мэдээ',
      content_en: 'News',
      cat: {
        name: 'Үйл явдал',
        name_en: 'Event'
      }
    }, {
      content: 'Мэдээ 2',
      content_en: 'News 2',
      cat: {
        name: 'Ярилцлага',
        name_en: 'Interview'
      }
    }]
  }
}]

class Formatter {
  constructor (lang = 'mn') {
    this.lang = lang
  }

  format (_data, schema) {
    let data = null
    if (Array.isArray(_data)) {
      data = _data.map(obj => {
        if (obj.get === 'function') return obj.get({plain: true})
        return obj
      })
    } else if (_data.get === 'function') {
      data = _data.get({plain: true})
    } else {
      data = _data
    }
    this.format2(data, schema)
    return data
  }

  format2 (data, schema) {
    if (Array.isArray(data)) {
      this.formatArray(data, schema)
    } else {
      this.formatObject(data, schema)
    }
  }

  formatArray (array, schema) {
    array.forEach(obj => {
      this.formatObject(obj, schema[0])
    })
  }

  formatObject (object, schema) {
    for (let key in schema) {
      if (typeof schema[key] === 'object') {
        this.format2(object[key], schema[key])
      } else {
        this.formatField(object, key)
      }
    }
  }

  formatField (object, field) {
    const langField = `${field}_${this.lang}`
    if (this.lang === 'en') {
      object[field] = object[langField]
    }
    delete object[langField]
  }
}
const formatter = new Formatter('en')
const data = formatter.format(menus, schema)
console.log(JSON.stringify(data, false, 4))
