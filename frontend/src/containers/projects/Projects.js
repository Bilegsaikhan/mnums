import React, {Component} from 'react';
import {Link} from 'react-router'
import {connect} from 'react-redux'
import language from "../../constants/language"
import {loadProjectsList, clearFilter} from '../../actions/project';
import './Projects.css';
import {browserHistory} from 'react-router'
import ReactPaginate from 'react-paginate';

class Projects extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            offset: 0,
            searchString: ''
        }
        this.searchChange = this.searchChange.bind(this);
        this.doSearch = this.doSearch.bind(this);
    }

    componentDidMount() {
        const {location: {query}} = this.props
        this.setState({searchString: query.string ? query.string : ''});
        this.maybeFetchData()
    }

    componentWillUpdate(nextProps) {
        let {location} = nextProps

        if (location.search !== this.props.location.search) {
            this.maybeFetchData(nextProps)
        }
    }

    maybeFetchData(nextProps) {
        const {loadProjectsList, location: {search}} = nextProps || this.props;
        let projectQuery = ''

        if (!search) {
            projectQuery = `?limit=12`
        } else {
            projectQuery = `${search}&limit=12`
        }
        loadProjectsList({params: projectQuery})
    }

    doSearch = (event) => {
        const {location: {query, pathname}} = this.props
        let search = {
            ...query,
            string: this.state.searchString ? this.state.searchString : undefined,
            offset: 0
        };
        browserHistory.push({
            pathname: pathname,
            query: search
        })

        event.preventDefault();
    };

    searchChange = (event) => {
        this.setState({searchString: event.target.value});
    };

    handlePageClick = (data) => {
        const {location: {query, pathname}} = this.props

        let search = {...query, offset: data.selected};
        browserHistory.push({
            pathname: pathname,
            query: search
        })
    };

    render() {

        const {total, projects, basic, location: {query}, lang} = this.props

        let pageCount = total / 10
        let currentPage = query && parseInt(query.offset || 0, 10)

        return (
            <div>
                <div className="g01v1-this-page-in">
                    <div className="container">
                        <div className="row">
                            <div className="col-xs-5 col-sm-6">
                                <div className="breadcrumb_wbr wb_breadcrumb">
                                    <div className="g04v1-text">
                                        <ol className="breadcrumb">
                                            <li><Link to={`/`}>{language.homepage[lang]}</Link></li>
                                            <li><a>{language.projects[lang]}</a></li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="container">
                    <div className="titlepar parsys">
                        <div className="title section">
                            <div className="row">
                                <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div className="c01v1-page-title">{language.projects[lang]}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="par parsys">
                        <div className="gridlayout parbase section">
                            <div className="row">
                                <div className="col-lg-3 col-md-3 col-sm-3 col-xs-12 hidden-xs">
                                    <div className="panel panel-default left-column refine-by-nav hidden-xs hidden-sm">
                                        <div className="panel-body">
                                            <ul>
                                                <li className="h7">{language.projectStatus[lang]}</li>

                                                <li>
                                                    <div className="sub-link">
                                                        <Link className={`active`} to={{
                                                            pathname: 'projects',
                                                            query: {...query, status: 'ongoing'}
                                                        }}>{language.ongoing[lang]}</Link>
                                                        {query.status === 'ongoing' &&
                                                        <Link to={{
                                                            pathname: 'projects',
                                                            query: {...query, status: undefined}
                                                        }}
                                                              className="glyphicon glyphicon-remove-circle mrg-l-5 delete-item">&nbsp;
                                                        </Link>
                                                        }
                                                    </div>
                                                </li>
                                                <li>
                                                    <div className="sub-link">
                                                        <Link className={`active`} to={{
                                                            pathname: 'projects',
                                                            query: {...query, status: 'finished'}
                                                        }}>{language.completed[lang]}</Link>
                                                        {query.status === 'finished' &&
                                                        <Link to={{
                                                            pathname: 'projects',
                                                            query: {...query, status: undefined}
                                                        }}
                                                              className="glyphicon glyphicon-remove-circle mrg-l-5 delete-item">&nbsp;
                                                        </Link>
                                                        }
                                                    </div>
                                                </li>

                                            </ul>
                                            <ul>
                                                <li className="h7">{language.sector[lang]}</li>
                                                {
                                                    basic.branches.map(item => {
                                                        if (parseInt(query.branch_id, 10) === item.id || !query.branch_id) {
                                                            return (
                                                                <li key={item.id}>
                                                                    <div className="sub-link">
                                                                        <Link className={`active`} to={{
                                                                            pathname: 'projects',
                                                                            query: {...query, branch_id: item.id}
                                                                        }}>{item.name}</Link>
                                                                        {parseInt(query.branch_id, 10) === item.id &&
                                                                        <Link to={{
                                                                            pathname: 'projects',
                                                                            query: {...query, branch_id: undefined}
                                                                        }}
                                                                              className="glyphicon glyphicon-remove-circle mrg-l-5 delete-item">&nbsp;
                                                                            <span className="screen-reader-text">clear filter</span></Link>
                                                                        }
                                                                    </div>

                                                                </li>
                                                            )
                                                        } else {
                                                            return null
                                                        }
                                                    })
                                                }
                                            </ul>
                                            <ul>
                                                <li className="h7">{language.region[lang]}</li>
                                                {
                                                    basic.regions.map(item => {
                                                        if (parseInt(query.region_id, 10) === item.id || !query.region_id) {
                                                            return (
                                                                <li key={item.id}>
                                                                    <div className="sub-link">
                                                                        <Link className={`active`} to={{
                                                                            pathname: 'projects',
                                                                            query: {...query, region_id: item.id}
                                                                        }}>{item.name}</Link>
                                                                        {parseInt(query.region_id, 10) === item.id &&
                                                                        <Link to={{
                                                                            pathname: 'projects',
                                                                            query: {...query, region_id: undefined}
                                                                        }}
                                                                              className="glyphicon glyphicon-remove-circle mrg-l-5 delete-item">&nbsp;
                                                                            <span className="screen-reader-text">clear filter</span></Link>
                                                                        }
                                                                    </div>

                                                                </li>
                                                            )
                                                        } else {
                                                            return null
                                                        }
                                                    })
                                                }
                                            </ul>
                                            <ul>
                                                <li className="h7">{language.funding[lang]}</li>
                                                {
                                                    basic.sources.map(item => {
                                                        if (parseInt(query.source_id, 10) === item.id || !query.source_id) {
                                                            return (
                                                                <li key={item.id}>
                                                                    <div className="sub-link">
                                                                        <Link className={`active`} to={{
                                                                            pathname: 'projects',
                                                                            query: {...query, source_id: item.id}
                                                                        }}>{item.name}</Link>
                                                                        {parseInt(query.source_id, 10) === item.id &&
                                                                        <Link to={{
                                                                            pathname: 'projects',
                                                                            query: {...query, source_id: undefined}
                                                                        }}
                                                                              className="glyphicon glyphicon-remove-circle mrg-l-5 delete-item">&nbsp;
                                                                            <span className="screen-reader-text">clear filter</span></Link>
                                                                        }
                                                                    </div>

                                                                </li>
                                                            )
                                                        } else {
                                                            return null
                                                        }
                                                    })
                                                }
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                    <div className="center-padding-left center-left-border">
                                        <div className="row">
                                            <div className="col-md-12 col-xs-12">
                                                <div className="search-listing-searchbox">
                                                    <div className="serch-term dbmItemSearch">
                                                        <form className="input-group" onSubmit={this.doSearch}>
                                                            <input type="text"
                                                                   value={this.state.searchString}
                                                                   onChange={this.searchChange}
                                                                   placeholder={language.searchNews[lang]}
                                                                   className="f02v4-pulldown search-text"
                                                            />
                                                            <button className="f02v4-submit-icon" type="submit"
                                                                    alt="listingsearch">
                                                                <img src="/assets/images/search-icon-box.png" alt=""/>
                                                            </button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-xs-12 mrg-b-10">
                                                <div className="row">
                                                    <div className="col-xs-6 pad-t-10">
                                                        <label className="h7">
                                                            {lang === 'en' ?
                                                                `${total} results`
                                                                :
                                                                `Нийт ${total} төсөл хөтөлбөр байна.`
                                                            }
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-12 col-sm-12 col-xs-12">
                                                <div
                                                    className={`projectLoading ${this.state.loading ? 'showLoading' : ''}`}>
                                                    <div className="showbox">
                                                        <div className="loader">
                                                            <svg className="circular" viewBox="25 25 50 50">
                                                                <circle className="path" cx="50" cy="50" r="20"
                                                                        fill="none" strokeWidth="2"
                                                                        strokeMiterlimit="10"/>
                                                            </svg>
                                                        </div>
                                                        {language.loading[lang]}...
                                                    </div>
                                                </div>
                                                <div
                                                    className={`row projectListBox ${this.state.loading ? 'projectHide' : ''}`}>
                                                    {
                                                        projects.map(item => {
                                                            return (
                                                                <div className="col-md-4" key={`pli_${item.id}`}>
                                                                    <figure className="snip1477">
                                                                        <img
                                                                            className="plitemImage"
                                                                            style={{backgroundImage: `url(${item.image ? item.image : 'http://ubinfo-s3.s3.amazonaws.com/static/PROJECT-PLACEHOLDER-METROBC.jpg'})`}}
                                                                            src={`${process.env.PUBLIC_URL}/assets/images/spacer.gif`}
                                                                            alt="sample38"/>
                                                                        <div className="title">
                                                                            <div>
                                                                                <img src="/assets/images/logo-w.png"
                                                                                     height={50} alt=""/>
                                                                            </div>
                                                                        </div>
                                                                        <figcaption>
                                                                            <p>{item.name}</p>
                                                                        </figcaption>
                                                                        <Link to={`/project/${item.id}`}/>
                                                                    </figure>
                                                                </div>
                                                            )
                                                        })
                                                    }
                                                </div>
                                                { total > 10 &&
                                                <ReactPaginate previousLabel={<i className="fa fa-arrow-left"/>}
                                                               nextLabel={<i className="fa fa-arrow-right"/>}
                                                               breakLabel={false}
                                                               breakClassName={"break-me"}
                                                               pageCount={pageCount}
                                                               forcePage={currentPage}
                                                               marginPagesDisplayed={5}
                                                               pageRangeDisplayed={5}
                                                               onPageChange={this.handlePageClick}
                                                               containerClassName={"pagination right"}
                                                               subContainerClassName={"pages pagination"}
                                                               activeClassName={"active"}/>
                                                }
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="clearfix"/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


const mapStateToProps = (state) => {
    const {core, projects, basic, config} = state

    return {
        basic,
        core,
        lang: config.language,
        projects: projects.data,
        total: projects.total
    }
}

const mapDispatch = {
    loadProjectsList,
    clearFilter
};

export default connect(mapStateToProps, mapDispatch)(Projects)
