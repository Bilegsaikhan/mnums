import {
    LOAD_MARQUEE, LOAD_MARQUEE_SUCCESS, LOAD_MARQUEE_ERROR,
    LOAD_NEWS_LIST, LOAD_NEWS_LIST_SUCCESS, LOAD_NEWS_LIST_ERROR,
    LOAD_NEWS_ITEM, LOAD_NEWS_ITEM_SUCCESS, LOAD_NEWS_ITEM_ERROR
} from "../constants";

const defaultState = {
    data: [],
    total: 0,
    newsDetail: {}
};

// reducer
export default function news(state = defaultState, action) {
    switch (action.type) {

        case LOAD_NEWS_ITEM: {
            return {
                ...state,
                isItemFetching: true,
                error: null,
            };
        }
        case LOAD_NEWS_ITEM_SUCCESS: {
            const { payload } = action
            return {
                ...state,
                newsDetail: payload,
                isItemFetching: false,
            };
        }
        case LOAD_NEWS_ITEM_ERROR: {
            return {
                ...state,
                error: action.error,
                isItemFetching: false,
            };
        }

        case LOAD_NEWS_LIST: {
            return {
                ...state,
                isFetching: true,
                error: null,
            };
        }
        case LOAD_NEWS_LIST_SUCCESS: {
            const { payload } = action
            return {
                ...state,
                data: payload.list,
                total: payload.total,
                isFetching: false,
            };
        }
        case LOAD_NEWS_LIST_ERROR: {
            return {
                ...state,
                error: action.error,
                isFetching: false,
            };
        }

        case LOAD_MARQUEE: {
            return {
                ...state
            };
        }
        case LOAD_MARQUEE_SUCCESS: {
            const { payload } = action
            return {
                ...state,
                marquee: payload
            };
        }
        case LOAD_MARQUEE_ERROR: {
            return {
                ...state
            };
        }

        case 'LOAD_NEWS': {
            return {
                ...state,
                newsDetail: state.indexedData[action.payload.id]
            }
        }

        case 'LOAD_NEWS_HOME_FILTER': {
            return {
                ...state,
            }
        }

        case 'LOAD_NEWS_FILTER': {
            return {
                ...state,
            }
        }

        default: {
            return state;
        }
    }
}