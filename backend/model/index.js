'use strict'
/**
 * Created with MacVim
 * User: enxtur
 * Date: 2016/10/19
 */
import path from 'path'
import conf from '../config/index'
import initiateRelation from './initiate_relation'
import { setTimeout } from 'timers';

const debug = require('../modules/debug')(__filename)
const key = Symbol.for('mn.slide.dbm.database')

let sequelize = global[key]
if (!sequelize) {
  const Sequelize = require('sequelize')
  Sequelize.Promise.onPossiblyUnhandledRejection(err => { throw err })
  sequelize = new Sequelize(conf.MYSQL_DB, conf.MYSQL_USER, conf.MYSQL_PASS, {
    host: conf.MYSQL_HOST,
    port: conf.MYSQL_PORT,
    dialect: 'mysql',
    dialectOptions: {
      charset: 'utf8mb4'
    },
    define: {
      underscored: true,
      freezeTableName: true,
      timestamps: false
    },
    logging: debug
    // logging: console.log
  })

  sequelize.import(path.join(__dirname, '__gallery_cat'))
  sequelize.import(path.join(__dirname, '__gallery_item'))
  sequelize.import(path.join(__dirname, '__article_cat'))
  sequelize.import(path.join(__dirname, '__article'))
  sequelize.import(path.join(__dirname, '__article_copy'))
  sequelize.import(path.join(__dirname, '__page'))
  sequelize.import(path.join(__dirname, '__admin'))
  sequelize.import(path.join(__dirname, '__project_branch'))
  sequelize.import(path.join(__dirname, '__project_region'))
  sequelize.import(path.join(__dirname, '__project_source'))
  sequelize.import(path.join(__dirname, '__project'))
  sequelize.import(path.join(__dirname, '__job'))
  sequelize.import(path.join(__dirname, '__menu'))
  sequelize.import(path.join(__dirname, '__slide'))
  sequelize.import(path.join(__dirname, '__timeline'))
  sequelize.import(path.join(__dirname, '__pdf'))
  sequelize.import(path.join(__dirname, '__report'))
  sequelize.import(path.join(__dirname, '__store'))
  sequelize.import(path.join(__dirname, '__law'))
  sequelize.import(path.join(__dirname, '__log'))
  sequelize.import(path.join(__dirname, '__human_resource'))
  sequelize.import(path.join(__dirname, '__product'))
  sequelize.import(path.join(__dirname, '__product_cat'))
  sequelize.import(path.join(__dirname, '__product_sub_cat'))
  sequelize.import(path.join(__dirname, '__product_image'))
  sequelize.import(path.join(__dirname, '__provider'))
  initiateRelation(sequelize.models, false)

  global[key] = sequelize
}


const GalleryCat = sequelize.models.gallery_cat
const GalleryItem = sequelize.models.gallery_item
const Article = sequelize.models.article
const ArticleCopy = sequelize.models.article_copy
const ArticleCat = sequelize.models.article_cat
const Page = sequelize.models.page
const Admin = sequelize.models.admin
const ProjectBranch = sequelize.models.project_branch
const ProjectRegion = sequelize.models.project_region
const ProjectSource = sequelize.models.project_source
const Project = sequelize.models.project
const Job = sequelize.models.job
const Menu = sequelize.models.menu
const Slide = sequelize.models.slide
const Timeline = sequelize.models.timeline
const Pdf = sequelize.models.pdf
const Report = sequelize.models.report
const Store = sequelize.models.store
const Law = sequelize.models.law
const Log = sequelize.models.log
const HR = sequelize.models.human_resource
const Product = sequelize.models.product
const ProductCat = sequelize.models.product_cat
const ProductSubCat = sequelize.models.product_sub_cat
const ProductImage = sequelize.models.product_image
const Provider = sequelize.models.provider

export {
  Article,
  ArticleCopy,
  ArticleCat,
  Page,
  Admin,
  ProjectBranch,
  ProjectRegion,
  ProjectSource,
  Project,
  Job,
  Menu,
  Slide,
  Timeline,
  Pdf,
  Report,
  Store,
  Law,
  Log,
  HR,
  Product,
  ProductSubCat,
  ProductCat,
  ProductImage,
  Provider,
  GalleryCat,
  GalleryItem,
}
