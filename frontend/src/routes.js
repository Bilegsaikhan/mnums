import React from "react";
import { Route, IndexRoute } from "react-router";
import MobileDetect from "mobile-detect";

import App from "./containers/App";
import Homepage from "./containers/home";
import Search from "./containers/search";

import News from "./containers/news";
import NewsDetail from "./containers/newsDetail";

import Contact from "./containers/contact";
import NoMatch from "./containers/noMatch";
import Page from "./containers/page";

import { loadAgent, loadLanguage, loadBasicInfo } from "./actions";
import { loadMenu } from "./actions/menu";
import { loadPage } from "./actions/page";

export default (store) => {
  const loadData = (nextState, replace, cb) => {
    let md = new MobileDetect(window.navigator.userAgent);

    if (md.mobile()) {
      document.body.classList.add("touchMode");
    } else {
      document.body.classList.remove("touchMode");
    }

    Promise.all([
      store.dispatch(
        loadAgent({
          mobile: md.mobile(),
          phone: md.phone(),
          tablet: md.tablet(),
          userAgent: md.userAgent(),
          os: md.os(),
        }),
      ),
      store.dispatch(loadMenu()),
      store.dispatch(loadLanguage()),
      store.dispatch(loadBasicInfo()),
    ]).then(
      (res) => {
        if (res[2] === 'en') { document.body.classList.add("lang-en"); } else { document.body.classList.remove("lang-en"); }

        if (res[2] === 'mn') { document.body.classList.add("lang-mn"); } else { document.body.classList.remove("lang-mn"); }

        cb();
      },
      (reason) => {
        console.log("reason: ", reason);
        cb();
      },
    );
  };

  const getComponent = (nextState, next) => {
    store.dispatch(loadPage(nextState.params.pageId)).then(
      () => {
        next(null, Page);
      },
      (err) => {
        console.log("err: ", err);
        next(null, NoMatch);
      },
    );
  };

  return (
    <Route path="/" name="Нүүр хуудас" component={App} onEnter={loadData}>
      <IndexRoute component={Homepage} />
      <Route path="/search" component={Search} />
      <Route path="/news" component={News} />
      <Route path="/news/:id" component={NewsDetail} />
      <Route path="/contact-us" component={Contact} />
      <Route path="/:pageId" getComponent={getComponent}>
        <Route path="/:parentPageId(/:subPageId)/:pageId" component={Page} />
      </Route>
      <Route path="*" component={NoMatch} status={404} />
    </Route>
  );
};
