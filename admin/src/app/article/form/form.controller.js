export class ArticleFormController {
  constructor ($log, articleService, catService, server, $state, toastr, modalService) {
    'ngInject';
    this.$log = $log.log
    this.catService = catService
    this.articleService = articleService
    this.server = server
    this.toastr = toastr
    this.$state = $state
    this.stateName = $state.current.name
    this.modalService = modalService
    this.clear()
    this.loadCats()
    if (this.stateName === 'root.content.article.edit') this.load()
  }

  clear () {
    this.article = {
      cat_id: '',
      title: '',
      desc: '',
      content: '',
      title_en: '',
      desc_en: '',
      content_en: '',
      mn: true,
      en: false
    }
  }

  loadCats () {
    this.catService.cats().then(cats => {
      this.cats = cats
    })
  }

  create () {
    this.$log('this.article: ', JSON.stringify(this.article))
    
    // let content = angular.copy(this.article.content)
    // let contentEn = angular.copy(this.article.content_en)

    // if (!this.article.desc) this.article.desc = this.article.content ? (String(content).replace(/<[^>]+>/gm, '')).substring(0, 255) : ''
    // if (!this.article.desc_en) this.article.desc_en = this.article.content_en ? (String(contentEn).replace(/<[^>]+>/gm, '')).substring(0, 255) : ''

    // this.articleService.create(this.article).then(article => {
    //   this.article = article
    //   this.toastr.success('Таны хүсэлт амжилттай!');
    //   this.$state.go('root.content.article.list');
    // }, () => {
    //   this.toastr.error('Таны хүсэлт амжилтгүй!');
    // })
  }

  update () {

    let content = angular.copy(this.article.content)
    let contentEn = angular.copy(this.article.content_en)

    if (!this.article.desc) this.article.desc = this.article.content ? (String(content).replace(/<[^>]+>/gm, '')).substring(0, 255) : ''
    if (!this.article.desc_en) this.article.desc_en = this.article.content_en ? (String(contentEn).replace(/<[^>]+>/gm, '')).substring(0, 255) : ''
    this.articleService.update(this.article).then(article => {
      this.article = article
      this.toastr.success('Таны хүсэлт амжилттай!');
    }, () => {
      this.toastr.error('Таны хүсэлт амжилтгүй!');
    })
  }

  remove () {
    this.modalService.danger('Та уг үүнийг устгахдаа итгэлтэй байна уу', true).then(() => {
      this.articleService.remove({id: this.$state.params.id}).then(() => {
        this.toastr.success('Таны хүсэлт амжилттай!');
        this.$state.go('root.content.article.list');
      }, () => {
        this.toastr.error('Таны хүсэлт амжилтгүй!');
      })
    })
  }

  load () {
    this.articleService.get({id: this.$state.params.id}).then(article => {
      this.article = article
      // this.article.pdf = {}
    }, () => {
      this.toastr.error('Таны хүсэлт амжилтгүй!');
    })
  }

  onReady () {
    this.$log('ready')
  }

  uploadMnPdfDone (pdf, thumb) {
    this.$log('upload mn pdf done')
    this.article.pdf = pdf
    this.article.pdfthumb = thumb

  }

  removeMnPdf () {
    this.$log('remove mn pdf called')
    this.article.pdf = null
    this.article.pdfthumb = null
  }

  uploadEnPdfDone (pdf, thumb) {
    this.$log('upload en pdf done')
    this.article.pdf_en = pdf
    this.article.pdfthumb_en = thumb
  }

  removeEnPdf () {
    this.$log('remove en pdf called')
    this.article.pdf_en = null
    this.article.pdfthumb_en = null
  }
}

