'use strict'
import { uploadBuffer } from '../modules/s3'
import gm from 'gm'
import fs from 'fs'
import http from 'http'
import { v1 } from 'uuid'
import promisify from 'es6-promisify'
import PROMISE from 'bluebird'
PROMISE.promisifyAll(gm.prototype)

const debug = require('../modules/debug')(__filename)
const unlink = promisify(fs.unlink)
const exists = promisify(fs.exists)
const DIMENS = {
  'items/': [
    [150, 150],
    [226, 226],
    [640, 640]
  ],
  'users/': [
    [100, 100],
    [380, 380],
    [640, 640]
  ]
}

export default function (data) {
  let prefix = data.prefix
  if (data.files) {
    debug('image upload task from file')
    let files = data.files
    let dimensions = DIMENS[prefix]
    let tasks = files.map(file => {
      let srcPath = file.path
      return uploadSingleFile(srcPath, prefix, dimensions).then(result => {
        return unlink(srcPath).then(() => {
          return exists(srcPath).then(exsts => {
            debug(exsts ? `warning: temp file ${srcPath} not removed` : `temp file ${srcPath} removed`)
            return result
          })
        })
      })
    })
    return Promise.all(tasks)
  } else if (data.urls) {
    debug('image upload task from urls')
    let dimensions = DIMENS[prefix]
    return Promise.all(data.urls.map(url => {
      return uploadLoremPixel(url, prefix, dimensions).then(results => {
        debug('RESULT OF UPLOAD LOREM PIXEL')
        debug(results)
        return results
      })
    }))
  } else if (data.base64) {
    debug('image upload task from buffer')
    // let files = data.files
    // let dimensions = DIMENS[prefix]
    // let tasks = files.map(file => {
    //   let srcPath = file.path
    //   return uploadBase64(data.base64, prefix, dimensions).then(result => {
    //     return unlink(srcPath).then(() => {
    //       return exists(srcPath).then(exsts => {
    //         debug(exsts ? `warning: temp file ${srcPath} not removed` : `temp file ${srcPath} removed`)
    //         return result
    //       })
    //     })
    //   })
    // })
    // return Promise.all(tasks)
    let dimensions = DIMENS[prefix]
    return uploadBase64(data.base64, prefix, dimensions)
  } else {
    debug('image upload task from nothing')
    Promise.resolve([])
  }
}

function uploadSingleFile (srcPath, prefix, dimensions) {
  let uid = v1()
  let s3pathOriginal = `${prefix}${uid}.jpg`
  // gm(srcPath).autoOrient().noProfile().strip().interlace('Line').toBuffer('JPEG', (err, buffer) => {
  return gm(srcPath).autoOrient().noProfile().strip().interlace('Line').toBufferAsync('JPEG').then(buffer => {
    // if (err) return reject(err)
    return uploadBuffer(buffer, s3pathOriginal, 'image/jpeg').then(() => {
      let tasks = dimensions.map(dimension => {
        let [w, h] = dimension
        let s3path = `${s3pathOriginal}_${w}x${h}.jpg`
        return gm(srcPath).resize(w, h).autoOrient().noProfile().strip().interlace('Line').toBufferAsync('JPEG').then(buffer => {
          return uploadBuffer(buffer, s3path, 'image/jpeg')
        })
      })
      return Promise.all(tasks).then(() => {
        return s3pathOriginal
      })
    })
  })
}
function uploadLoremPixel (url, prefix, dimensions) {
  let uid = v1()
  let s3pathOriginal = `${prefix}${uid}.jpg`
  // let url = 'http://lorempixel.com/640/480/business/'
  return urlToBuffer(url).then(buffer => {
    return gm(buffer).autoOrient().noProfile().strip().interlace('Line').toBufferAsync('JPEG').then((_buffer) => {
      return uploadBuffer(_buffer, s3pathOriginal, 'image/jpeg').then(() => {
        let tasks = dimensions.map(dimension => {
          let [w, h] = dimension
          let s3path = `${s3pathOriginal}_${w}x${h}.jpg`
          return gm(buffer).resize(w, h).autoOrient().noProfile().strip().interlace('Line').toBufferAsync('JPEG').then(_buffer => {
            return uploadBuffer(_buffer, s3path, 'image/jpeg')
          })
        })
        return Promise.all(tasks).then(() => {
          return s3pathOriginal
        })
      })
    })
  })
}

function urlToBuffer (url) {
  return new Promise((resolve) => {
    http.get(url, (res) => {
      let buffs = []
      res.on('data', (chunk) => {
        buffs.push(chunk)
      })
      res.on('end', () => {
        resolve(Buffer.concat(buffs))
      })
    })
  })
}

function uploadBase64 (base64, prefix, dimensions) {
  console.log('########################')
  console.log(base64)
  const buffer = new Buffer(base64, 'base64')
  let uid = v1()
  let s3pathOriginal = `${prefix}${uid}.jpg`
  // gm(srcPath).autoOrient().noProfile().strip().interlace('Line').toBuffer('JPEG', (err, buffer) => {
  return gm(buffer).autoOrient().noProfile().strip().interlace('Line').toBufferAsync('JPEG').then(buffer => {
    // if (err) return reject(err)
    return uploadBuffer(buffer, s3pathOriginal, 'image/jpeg').then(() => {
      let tasks = dimensions.map(dimension => {
        let [w, h] = dimension
        let s3path = `${s3pathOriginal}_${w}x${h}.jpg`
        return gm(buffer).resize(w, h).autoOrient().noProfile().strip().interlace('Line').toBufferAsync('JPEG').then(buffer => {
          return uploadBuffer(buffer, s3path, 'image/jpeg')
        })
      })
      return Promise.all(tasks).then(() => {
        return s3pathOriginal
      })
    })
  })
}
