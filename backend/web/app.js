'use strict'
// import os from 'os'
import fs from 'fs'
import path from 'path'
import conf from '../config/index'
import express from 'express'
import callback from '../modules/callback'
import routes from './routes'
import bodyParser from 'body-parser'
import cookieParser from 'cookie-parser'
import Err from '../modules/err'
import session from 'express-session'
import sessionOption from '../config/session_option'
import compression from 'compression'
import passport from 'passport'
import helmet from 'helmet'
import cors from 'cors'
import morgan from 'morgan'
import expressValidator from 'express-validator'
import customValidators from '../config/custom_validators'
import { setup as setupCommon } from '../modules/common'
import meta from '../modules/meta'
import { wrap } from '../modules/wrapper'

const debug = require('../modules/debug')(__filename)
// const indexHTML = fs.readFileSync(path.join(__dirname, '..', '..', 'frontend', 'build', 'index.html')).toString('utf-8')
const app = express()
const router = express.Router()
app.use(morgan('tiny'))
app.use(cors({ // TODO configure later cross origin request sharing (cors), now allowed all request
  origin(origin, callback) {
    callback(null, true)
  },
  credentials: true,
  allowedHeaders: ['Origin', 'X-Requested-With', 'Content-Type', 'Accept']
}))
// app.use(raven.middleware.express.requestHandler(conf.SENTRY_DSN))
app.use(helmet()) // disable unnecessary headers etc, x-power-by
app.use(cookieParser())
app.use(bodyParser.json({ limit: '50mb' }))
app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }))
app.use(expressValidator({
  errorFormatter: (param, msg/*, value */) => {
    return Err.badRequest(msg, param)
  },
  customValidators
})) // express-validator must be after body-parser
app.set('trust proxy', 1)
app.use(session(sessionOption)) // redis based session
app.use(compression()) // using gzip compression
app.use(passport.initialize())
app.use(passport.session()) // persistent login session
app.use(Err.middleware) // initialize static error of Err class to response, etc res.err.badRequest
app.use(callback) // initialize callback to response
routes(router, passport) // initialize routes to router
app.use('/images', express.static(path.join(__dirname, '..', '..', 'uploads')))
app.use((req, res, next) => {
  // debug(`worker: ${process.env.WORKER_NAME} handling ..`)
  next()
})
app.use('/api', router) // connect router to app

async function indexHtmlHandler(req, res) {
  const metaTag = await meta(req.originalUrl)
  debug('metaTag', metaTag)
  const html = indexHTML.replace('<meta type="dbmMeta">', metaTag)
  res.setHeader('Content-Type', 'text/html')
  res.status(200).send(html)
}

app.use('/admin/styles/font/summernote.woff', express.static(path.join(__dirname, '..', 'summernote.woff')))
app.use('/admin', express.static(path.join(__dirname, '..', '..', 'admin', 'dist')))
app.use('/bower_components', express.static(path.join(__dirname, '..', '..', 'admin', 'bower_components')))

// app.get('/', wrap(indexHtmlHandler))
app.use('/', express.static(path.join(__dirname, '..', '..', 'frontend', 'build')))
// app.get('/*', wrap(indexHtmlHandler))

app.use((req, res) => {
  debug(`Not found api's path is ${req.originalUrl}`)
  res.status(404).send({
    _status: 404,
    _message: 'API Not Found'
  })
})
app.use((err, req, res) => {
  res.status(500).json({
    _status: 500,
    _message: err
  })
})

function start() {
  setup().then(() => {
    app.listen(conf.APP_HTTP_PORT, () => {
      debug(`Listening on ${conf.APP_HTTP_PORT} port`)
    })
  })
}
function setup() {
  return Promise.all([
    setupCommon()
  ]).then(([common]) => {
    return [common]
  })
}

export { setup, start, app }

process.on('SIGINT', () => {
  debug('mysql connection closed')
  debug('elastic connection closed')
  process.exit(0)
})
