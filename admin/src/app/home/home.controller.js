export class HomeController {
  constructor (toastr, staticService) {
    'ngInject';
    this.toastr = toastr;
    this.staticService = staticService;
    this.homeStat = {}
    this.homeLogs = {}
    this.load()
  }

  load() {
    this.staticService.getHome().then((res) => {
      this.homeStat = res
    })
    this.staticService.getLogs().then((res) => {
      this.homeLogs = res
    })
  }

  renderMethod(method) {
    switch (method) {
      case 'GET': {
        return 'Үзсэн'
      }
      case 'PUT': {
        return 'Зассан';
      }
      case 'POST': {
        return 'Нэмсэн';
      }
      case 'DELETE': {
        return 'Устгасан';
      }

      default: {
        return 'UNKNOWING';
      }
    }
  }

}
