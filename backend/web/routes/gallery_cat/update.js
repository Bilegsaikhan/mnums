'use strict'
import { GalleryCat, GalleryItem } from '../../../model'
import Err from '../../../modules/err'

export default async function updateCat (req, res) {
  req.checkParams('id', 'bad_id').isInt()
  req.checkBody('name', 'bad_name').len(1, 255)
  const err = req.validationErrors()
  if (err) return res.callback(err)
  const {id} = req.params
  const {name} = req.body
  let cat = await GalleryCat.findById(id)
  if (!cat) return res.callback(Err.notfound())
  cat = await cat.updateAttributes({name})

  await GalleryItem.destroy({where: {cat_id: cat.id}})

  let tasks = [];

  req.body.images.forEach(entry => {
    let task = (callback) => {
      let image = {
        url: entry.url,
        cat_id: cat.id
      }

      GalleryItem.create(image).then(
        () => {
          callback()
        },
        (e) => {
          callback(e)
        }
      )
    }

    tasks.push(task);
  })

  fall(tasks, (e) => {
    res.callback(cat);
  })
}
