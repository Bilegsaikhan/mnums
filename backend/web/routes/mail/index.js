'use strict'
import Err from '../../../modules/err'
import nodemailer from 'nodemailer'

const transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'uyanbest.mail@gmail.com',
    pass: 'mail.uyanbest'
  }
})

let mailOptions = {
  from: '',
  to: 'stormrage0527@gmail.com',
  subject: 'from website',
  text: ''
}



export default ({router, wrap}) => {
  router.route('/mail')
    .post(wrap((req, res) => {

      if (!req.body.email) {
        return res.status(403).send('bad_email')
      }
      if (!req.body.body) {
        return res.status(403).send('bad_body')
      }

      let tempMailOption = JSON.parse(JSON.stringify(mailOptions));

      if (req.body.title) {
        tempMailOption.subject = req.body.title;
      }

      tempMailOption.from = req.body.email;
      tempMailOption.text = `Хэрэглэгчийн нэр: ${req.body.name}\n Хэрэглэгчийн мэйл: ${req.body.email} \n${req.body.body}`

      transporter.sendMail(tempMailOption, (error, info) => {
        if (err) {
          console.log('errrrrrrr', err)
        }
      })
      res.send('success')
    }))
}
