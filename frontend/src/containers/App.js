import React, {Component} from 'react';
import {connect} from 'react-redux'
import './App.css';
import Header from '../components/header'
import Footer from '../components/footer'
import $ from 'jquery';
import {updateStep} from '../actions';
import {Helmet} from "react-helmet";
import language from "../constants/language"


$(window).scroll(function () {
    $('.fixed-bar').mouseenter(function () {
        $('.fixed-bar').removeClass('hideFbar');
    })
    $('.fixed-bar').mouseleave(function () {
        $('.fixed-bar').addClass('hideFbar');
    })
    if ($(window).scrollTop() > 0) {
        $('.fixed-bar').addClass('hideFbar');
    } else {
        $('.fixed-bar').removeClass('hideFbar');
    }
});

class App extends Component {
    constructor() {
        super();

        this.state = {
            modalIsOpen: false,
            isShowError: false,
            step: 0,
        };

        this.openModal = this.openModal.bind(this);
        this.sayYes = this.sayYes.bind(this);
        this.hideError = this.hideError.bind(this);
        this.showError = this.showError.bind(this);
        this.goRegister = this.goRegister.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.afterOpenModal = this.afterOpenModal.bind(this);
    }

    componentDidMount() {
    }

    sayYes = (step) => {
        setTimeout(() => {
            this.setState({step})
        },300)
        this.refs.modalSlider.goTo(step)
    }

    hideError() {
        this.setState({isShowError: false});
    }

    showError = (step) => {
        this.setState({isShowError: true});
        this.props.updateStep(step)
    }

    goRegister() {
    }

    openModal() {
        this.setState({modalIsOpen: true});
    }

    afterOpenModal() {
        // references are now sync'd and can be accessed.
        this.setState({
            step: 0,
            isShowError: false
        });
    }

    closeModal() {
        this.setState({modalIsOpen: false});
    }

    render() {
        const {children, lang} = this.props

        return (
            <div className="dbm">
                <Helmet
                    encodeSpecialCharacters={true}
                    titleTemplate={`%s | ${language.title[lang]}`}
                    defaultTitle={language.title[lang]}
                >
                    <html lang="en" amp/>
                    <title itemProp="name" lang="mn">{language.title[lang]}</title>
                    <link rel="canonical" href="http://mhi.mn"/>
                </Helmet>
                <Header {...this.props}/>
                <div className="main-section-box">
                    {children}
                </div>
                <Footer/>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    const {core, userAgent, config} = state

    return {
        lang: config.language,
        modalStep: core.modalStep,
        userAgent: userAgent
    }
}

export default connect(mapStateToProps, {
    updateStep,
})(App)
