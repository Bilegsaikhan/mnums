'use strict'
module.exports = [{
  name: 'article',
  relations: [{
    belongsTo: 'article_cat',
    foreignKey: 'cat_id',
    as: 'cat',
    backas: 'articles',
    many: true
  }]
}, {
  name: 'project',
  relations: [{
    belongsTo: 'project_branch',
    foreignKey: 'branch_id',
    as: 'branch',
    backas: 'projects',
    many: true
  }, {
    belongsTo: 'project_region',
    foreignKey: 'region_id',
    as: 'region',
    backas: 'projects',
    many: true
  }, {
    belongsTo: 'project_source',
    foreignKey: 'source_id',
    as: 'source',
    backas: 'projects',
    many: true
  }]
}, {
  name: 'menu',
  relations: [{
    belongsTo: 'menu',
    foreignKey: 'parent_id',
    as: 'parent',
    backas: 'menus',
    many: true
  }, {
    belongsTo: 'page',
    foreignKey: 'page_id',
    as: 'page',
    backas: 'menu'
  }]
}, {
  name: 'log',
  relations: [{
    belongsTo: 'admin',
    foreignKey: 'user_id',
    as: 'admin',
    backas: 'logs',
    many: true
  }]
}, {
  name: 'product',
  relations: [{
    belongsTo: 'product_cat',
    foreignKey: 'product_cat_id',
    as: 'productCat',
    backas: 'products',
    many: true
  }, {
    belongsTo: 'product_sub_cat',
    foreignKey: 'product_sub_cat_id',
    as: 'productSubCat',
    backas: 'products',
    many: true
  }, {
    belongsTo: 'provider',
    foreignKey: 'provider_id',
    as: 'provider',
    backas: 'products',
    many: true
  }]
}, {
  name: 'product_image',
  relations: [{
    belongsTo: 'product',
    foreignKey: 'product_id',
    as: 'product',
    backas: 'images',
    many: true
  }]
}, {
  name: 'product_sub_cat',
  relations: [{
    belongsTo: 'product_cat',
    foreignKey: 'product_cat_id',
    as: 'productCat',
    backas: 'productSubCats',
    many: true
  }]
}, {
  name: 'gallery_item',
  relations: [{
    belongsTo: 'gallery_cat',
    foreignKey: 'cat_id',
    as: 'galleryCat',
    backas: 'images',
    many: true
  }]
}]

