'use strict'
// import { indexBy } from 'underscore'
// import { series } from 'async'
const debug = require('../debug')(__filename)
const key = Symbol.for('mn.slide.ubtrade.common')

let globalKeys = Object.getOwnPropertySymbols(global)
let hasKey = globalKeys.indexOf(key) !== -1
if (!hasKey) {
  global[key] = {}
}
let data = global[key]

function setup () {
  if (!hasKey) {
    return Promise.all([
      'https://s3.aws.com/'
    ]).then(([s3prefix, categoryIndex, category]) => {
      Object.defineProperties(global[key], {
        s3prefix: {
          get: () => s3prefix
        },
        category_index: {
          get: () => categoryIndex
        },
        category: {
          get: () => category
        }
      })
      Object.freeze(global[key])
      // debug('common data initialized')
      return global[key]
    })
  } else {
    return Promise.resolve(global[key])
  }
}
export { data, setup }
