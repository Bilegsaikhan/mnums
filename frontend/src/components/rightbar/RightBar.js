import React, {Component} from 'react';
import {Link} from 'react-router'
import './Rightbar.css';
import language from "../../constants/language"
import {connect} from "react-redux";

class RightBar extends Component {
    render() {
        const {lang} = this.props
        return (
            <div className="grid_4_par_1 parsys rightbar">
                <div className="remove-padding-grid">
                    <div className="gridlayout parbase section">
                        <div className="row">
                            <div className="col-md-12 col-sm-12 col-lg-12 col-xs-12">
                                <div className="grid_12_par_0 parsys">
                                    <div className="remove-padding-grid">
                                        <hr className="hidden-lg hidden-md"/>
                                        <div className="s01v1_related section">
                                            <div id="s01v1-body" className="">
                                                <h6>{language.featured[lang]}</h6>
                                                <div className="owl-item active owl-zIndex">
                                                    <div className="no6v1IndepthItem">
                                                        <div className="n06v1 ">
                                                            <div className="thumbnail n06v1-img">
                                                                <Link to={`/project/257`}>
                                                                    <img className="img-responsive"
                                                                         src="/assets/images/news/2.jpg"
                                                                         alt="news"/>
                                                                </Link>
                                                            </div>
                                                            { lang === 'en' ?
                                                                <div className="n06v1-title">
                                                                    <Link to="/project/257">Tavantolgoi power plant</Link>
                                                                </div>
                                                                :
                                                                <div className="n06v1-title">
                                                                    <Link to="/project/257">ТАВАНТОЛГОЙН ЦАХИЛГААН <br/>СТАНЦ</Link>
                                                                </div>
                                                            }
                                                            <div className="n06v1-text"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="clearfix"/>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    const {config} = state;
    return {
        lang: config.language
    };
};

const mapDispatch = {};

export default connect(mapStateToProps, mapDispatch)(RightBar);
