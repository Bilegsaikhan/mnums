/**
 * Created by enxtur on 1/27/17.
 */
require('babel-core/register')
require('babel-polyfill')
// import chai from 'chai'
import { setup, app } from '../web/app'
import { start as startTasker } from '../worker/rabbitmq/app'

// chai.should()
before(function () {
  startTasker()
  return setup().then(() => {
    global.app = app
    return true
  })
})
