/**
 * Created by enxtur on 5/5/17.
 */

import { Project } from '../../../model'
import Err from '../../../modules/err'
export default async (req, res) => {
  req.checkBody('mn', 'bad_mn').isBoolean()
  req.checkBody('en', 'bad_en').isBoolean()
  if (req.body.branch_id) {
    req.checkBody('branch_id', 'bad_branch_id').isInt()
  }
  if (req.body.region_id) {
    req.checkBody('region_id', 'bad_region_id').isInt()
  }
  if (req.body.source_id) {
    req.checkBody('source_id', 'bad_source_id').isInt()
  }
  const err = req.validationErrors()
  if (err) return res.callback(err)
  const createData = Object.assign({}, req.body)
  createData.mn = (createData.mn === true || createData.mn === 'true')
  createData.en = (createData.en === true || createData.en === 'true')
  try {
    const project = await Project.create(createData)
    res.callback(project)
  } catch (err) {
    res.callback(Err.sequelize(err))
  }
}
