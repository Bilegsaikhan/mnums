import React, {Component} from 'react';
import OwlCarousel from 'react-owl-carousel2';
import './DbmCarousel.css';

class DbmCarousel extends Component {

    constructor() {
        super();

        this.state = {
            isShowError: false,
            step: 0,
            error: {
                1: `“Уучлаарай, МУХБ-ны тухай хуулийн 10.1.1-т заасны дагуу Хөгжлийн банк нь “Монгол Улсын тогтвортой хөгжлийн үзэл баримтлал, дунд хугацааны хөгжлийн бодлогод нийцсэн...” төсөл хөтөлбөрийг санхүүжүүлэх үүрэгтэй байдаг. Иймд, таны төслийг эрх зүйн орчны хувьд санхүүжүүлэх боломжгүй байна. `,
                2: `“Уучлаарай, МУХБ-ны тухай хуулийн 10.1.3-т Хөгжлийн банк нь “техник, эдийн засгийн үндэслэл, зураг төсөл, төсөв хийгдсэн...” төсөл, хөтөлбөрийг санхүүжүүлнэ гэж заасан байдаг. Иймд, таны төслийг эрх зүйн хувьд санхүүжүүлэх боломжгүй байна.`,
                3: `Хөгжлийн банк нь “Тогтвортой санхүүжилт (ТоС)”-ийн хөтөлбөрийн зарчмыг хэрэгжүүлэгч байгууллагын хувьд“байгаль орчинд сөрөг нөлөөтэй” аливаа үйл ажиллагааг санхүүжүүлэхгүй тул таны хүсэлтийг хүлээн авах боломжгүй байна. `
            }
        };

        this.hideError = this.hideError.bind(this);
        this.showError = this.showError.bind(this);
    }

    sayYes(step) {
        this.refs.modalSlider.goTo(step)
        // this.setState({step: step});
    }

    hideError() {
        this.setState({isShowError: false});
    }

    showError(step) {
        this.setState({isShowError: true, step: step});
    }

    render() {
        const options = {
            items: 1,
            nav: true,
            rewind: true,
            mouseDrag: true,
            touchDrag: false
        };
        return (
            <div>

            </div>
        )
    }

}

export default DbmCarousel
