import React, { Component } from "react";
import { browserHistory, Link, IndexLink } from "react-router";
import { connect } from "react-redux";
import _find from "lodash/find";
import "./Header.css";
import { triggerLanguage } from "../../actions";
import language from "../../constants/language";

import logo from './logo.jpg';
import logoEn from './logo-en.jpg';

class Header extends Component {
    static propTypes = {};

    constructor(props) {
      super(props);
      this.state = {
        openSearch: false,
        searchString: '',
        opened: false,
        menuOpen: false,
        count2: 6,
      };
      this.searchChange = this.searchChange.bind(this);
      this.doSearch = this.doSearch.bind(this);
    }

    componentWillReceiveProps(props) {
      const { location: { query } } = props;
      this.setState({ searchString: query.string ? query.string : '' });
      this.setState({ menuOpen: false, openSearch: false });
    }

    openMobileMenu = () => {
      this.setState({ menuOpen: !this.state.menuOpen });
    };

    triggerLang = () => {
      this.props.triggerLanguage().then(() => {
        window.location.reload();
      });
    };

    searchChange = (event) => {
      this.setState({ searchString: event.target.value });
    };

    focusSearch = () => {
      this.setState({ openSearch: true });
    };

    searchOnBlur = () => {
      this.setState({ openSearch: false });
    };

    doSearch = (event) => {
      this.setState({ openSearch: false });
      browserHistory.push({ pathname: '/search',
        query: {
          string: this.state.searchString ? this.state.searchString : undefined,
          offset: 0,
        } });
      event.preventDefault();
    };

    render() {
      const {
        menuData, lang,
        facebook,
        youtube,
        twitter,
        linkedin,
      } = this.props;

      return (
        <header className="siteMainNavigation">
          <nav className="navbar navbar-default">
            <div className="nav-wrap">
              <div className="container">
                <div className="home-page-header-lang">
                  <div className="clearfix">
                    <div className="logo-box pull-left">
                      <Link to="/" className={`main-logo`}>
                        <img
                          title={language.title[lang]}
                          alt={language.title[lang]}
                          src={lang === 'en' ? logoEn : logo}
                          width="70"
                          height="70"
                          className="img-responsive logo"
                        />
                        <span className="main-logo-text">{language.title[lang]}</span>
                      </Link>
                    </div>
                    <div className="toggle-box hidden-md hidden-lg">
                      <div className="news-navigation mobileLangChanger">
                        <a className={`mobileSearchBtn ${this.state.openSearch ? 'dsOpened' : 'dsClosed'}`} onClick={() => this.focusSearch()} role="navigation">
                          <i className="fa fa-search" />
                        </a>
                        <a onClick={() => this.triggerLang()}>
                          {lang === "mn" ? "EN" : "MN"}
                        </a>
                      </div>
                      <a className={`nav-open ${this.state.menuOpen ? "nav-close" : ""}`} onClick={() => this.openMobileMenu()}>
                        <div className="inner" />
                      </a>
                    </div>
                    <span className="hsubLinks hidden-sm">
                      <div
                        className="news-navigation"
                        style={{
                          borderRight: "none",
                          paddingRight: 0,
                          marginRight: "-15px",
                        }}
                      >

                        <div
                          className={`positionRelative dbmSearch ${this.state.openSearch ? 'dsOpened' : 'dsClosed'}`}
                        >
                          <form onSubmit={this.doSearch}>
                            <input
                              placeholder={`${language.search[lang]}`}
                              type="input"
                              className="searchInput"
                              value={this.state.searchString}
                              onChange={this.searchChange}
                              onClick={() => this.focusSearch()}
                              onBlur={() => this.searchOnBlur()}
                            />
                            <button type="submit" className="searchBtn"><i className="fa fa-search" /></button>
                          </form>
                        </div>


                      </div>
                      <div
                        className="news-navigation hidden-xs"
                        style={{ height: "16px", borderRight: "none" }}
                      >
                        <a onClick={() => this.triggerLang()} className="languageChanger">
                          {lang === "mn" ?
                            <div className="langcItem">
                              <img src={`${process.env.PUBLIC_URL}/assets/images/en.png`} alt="" />
                                English
                            </div>
                            :
                            <div className="langcItem">
                              <img src={`${process.env.PUBLIC_URL}/assets/images/mn.png`} alt="" />
                                Монгол
                            </div>
                          }
                        </a>
                      </div>
                    </span>
                    <span className="hsnav hidden-sm">
                      <div
                        className="news-navigation hidden-xs "
                        style={{ borderRight: "none", paddingRight: 0 }}
                      >
                        <a href={linkedin && linkedin.data} target="_blank">
                          <img
                            src={`${process.env.PUBLIC_URL}/assets/images/svg/linkedln.svg`}
                            title="linkedln"
                            alt="linkedln"
                            height="18"
                          />
                        </a>
                      </div>
                      <div
                        className="news-navigation hidden-xs"
                        style={{ borderRight: "none", paddingRight: 0 }}
                      >
                        <a
                          href={youtube && youtube.data}
                          target="_blank"
                        >
                          <img
                            src="/assets/images/svg/icons-youtube.svg"
                            title="youtube"
                            alt="youtube"
                            height="18"
                          />
                        </a>
                      </div>
                      <div
                        className="news-navigation hidden-xs"
                        style={{ borderRight: "none", paddingRight: 0 }}
                      >
                        <a href={twitter && twitter.data} target="_blank">
                          <img
                            src="/assets/images/svg/icons-twitter.svg"
                            title="twitter"
                            alt="twitter"
                            height="18"
                          />
                        </a>
                      </div>
                      <div
                        className="news-navigation hidden-xs"
                        style={{ borderRight: "none", paddingRight: 0 }}
                      >
                        <a
                          href={facebook && facebook.data}
                          target="_blank"
                        >
                          <img
                            src="/assets/images/svg/icons-facebook.svg"
                            title="facebook"
                            alt="facebook"
                            height="18"
                          />
                        </a>
                      </div>
                    </span>
                  </div>
                </div>
                <div className="clearfix" />
                <ul
                  className={`group ${this.state.menuOpen ? "menuOpened" : ""}`}
                  id="main-nav"
                >
                  <li className="menu-item current_page_item">
                    <IndexLink to="/" activeClassName="active">
                      <i className="fa fa-home" />
                    </IndexLink>
                  </li>
                  {menuData.map(item => (
                    <li className="menu-item" key={`mmdk_${item.id}`}>
                      <Link
                        to={`/${item.module}`}
                        activeClassName="active"
                        className={`${item.menus.length ? "cursorDefault" : ""}`}
                        onClick={
                          item.menus.length ? e => e.preventDefault() : null
                        }
                      >
                        {item.name}
                      </Link>
                      {item.menus.length
                        ? <div className="mega-container">
                          <ul>
                            {item.menus.map((subItem) => {
                              if (subItem.menus.length) {
                                return (
                                  <ul key={`mmsik_${subItem.id}`}>
                                    <li className="title">
                                      <span>{subItem.name}</span>
                                    </li>
                                    {subItem.menus.map(childItem => (
                                      <li key={`mmcik_${childItem.id}`}>
                                        {childItem.type === "module"
                                          ? <Link
                                            to={`/${childItem.module}`}
                                            activeClassName="active"
                                          >
                                            {`${childItem.name} »`}
                                          </Link>
                                          : <Link
                                            to={`/${item.module}/${subItem.module}/${childItem.module}`}
                                            activeClassName="active"
                                          >
                                            {`${childItem.name} »`}
                                          </Link>}
                                      </li>
                                    ))}
                                  </ul>
                                );
                              }
                              return (
                                <li key={`mmsik_${subItem.id}`}>
                                  {subItem.type === "module"
                                    ? <Link
                                      to={`/${subItem.module}`}
                                      activeClassName="active"
                                    >
                                      {`${subItem.name} »`}
                                    </Link>
                                    : <Link
                                      to={`/${item.module}/${subItem.module}`}
                                      activeClassName="active"
                                    >
                                      {`${subItem.name} »`}
                                    </Link>}
                                </li>
                              );
                            })}
                          </ul>
                        </div>
                        : null}
                    </li>
                  ))}
                </ul>
              </div>
              <hr className="mainMenuTopLine" />
            </div>
          </nav>
        </header>
      );
    }
}

const mapStateToProps = (state) => {
  const { core, menu, config, basic } = state;
  const facebook = _find(basic.stores, { key: 'facebook' });
  const youtube = _find(basic.stores, { key: 'youtube' });
  const twitter = _find(basic.stores, { key: 'twitter' });
  const linkedin = _find(basic.stores, { key: 'linkedin' });
  return {
    core,
    menuData: menu.data,
    lang: config.language,
    facebook,
    youtube,
    twitter,
    linkedin,
  };
};

const mapDispatch = {
  triggerLanguage,
};

export default connect(mapStateToProps, mapDispatch)(Header);
