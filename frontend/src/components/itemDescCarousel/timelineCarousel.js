import React, {Component} from "react";
import PropTypes from "prop-types";
import OwlCarousel from "react-owl-carousel2";

class TimelineCarousel extends Component {
    static propTypes = {
        items: PropTypes.array
    };

    constructor(props) {
        super(props);
        this.state = {
            activeItem: props.activeItem
        };
    }

    componentDidMount() {
        this.forceUpdate();
    }

    shouldComponentUpdate(nextProps) {
        return nextProps.activeItem !== this.props.activeItem;
    }

    render() {
        let {
            items,
            itemCarousel,
            activeItem,
            onClickItem,
            onChangeCarousel,
            agent
        } = this.props;

        const options = {
            items: agent.mobile ? 1 : 3,
            nav: false,
            dots: false
        };

        return (
            <OwlCarousel
                ref={node => {
                    itemCarousel(node);
                }}
                options={options}
                events={{onChanged: onChangeCarousel}}
            >
                { items &&
                items.map((item, index) => {
                    return (
                        <div
                            className={`tcItem ${activeItem === index ? "active" : ""}`}
                            key={`imc_${index}`}
                            onClick={() => onClickItem(index)}
                        >
                            <span className="tcItemCaption">{item.date}</span>
                            <img
                                className="tcItemImage"
                                src={`/assets/images/default3.png`}
                                alt=""
                            />
                            <div className="tcLine"/>
                        </div>
                    );
                })
                }
            </OwlCarousel>
        );
    }
}

export default TimelineCarousel;
