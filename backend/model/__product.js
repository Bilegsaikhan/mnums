'use strict'
import inc from './include'
import { ProductCat, ProductSubCat, Provider, ProductImage } from './';

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('product', {
    name: DataTypes.STRING,
    name_en: DataTypes.STRING,
    price: DataTypes.STRING,
    mn: DataTypes.BOOLEAN,
    en: DataTypes.BOOLEAN,
    desc: DataTypes.STRING,
    desc_en: DataTypes.STRING,
    content: DataTypes.STRING,
    content_en: DataTypes.STRING,
    product_cat_id: DataTypes.INTEGER,
    product_sub_cat_id: DataTypes.INTEGER,
    provider_id: DataTypes.INTEGER,
  }, {
      classMethods: {
        toCount({ lang = 'mn', product_sub_cat_id: subCatId, product_cat_id: catId, provider_id: providerId,  string }) {
          let where = []
          const attrs = []
          if (lang === 'mn') {
            where.push('mn is true')
          } else if (lang === 'en') {
            where.push('en is true')
          }
          if (catId) {
            where.push('product_cat_id = ?')
            attrs.push(catId)
          }
          if (subCatId) {
            where.push('product_sub_cat_id = ?')
            attrs.push(subCatId)
          }
          if (providerId) {
            where.push('provider_id = ?')
            attrs.push(providerId)
          }
          if (string) {
            where.push('(name like ? OR `desc` like ? OR content like ? OR name_en like ? OR `desc_en` like ? OR content_en like ?)')
            const wildCardedString = `%${string}%`
            attrs.push(wildCardedString)
            attrs.push(wildCardedString)
            attrs.push(wildCardedString)
            attrs.push(wildCardedString)
            attrs.push(wildCardedString)
            attrs.push(wildCardedString)
          }
          return this.count({
            where: where.length > 0 ? [where.join(' AND '), ...attrs] : undefined
          })
        },
        toCountList({ product_sub_cat_id: subCatId, product_cat_id: catId, provider_id: providerId, string }) {
          let where = []
          const attrs = []
          if (catId) {
            where.push('product_cat_id = ?')
            attrs.push(catId)
          }
          if (subCatId) {
            where.push('product_sub_cat_id = ?')
            attrs.push(subCatId)
          }
          if (providerId) {
            where.push('provider_id = ?')
            attrs.push(providerId)
          }
          if (string) {
            where.push('(name like ? OR `desc` like ? OR content like ? OR name_en like ? OR `desc_en` like ? OR content_en like ?)')
            const wildCardedString = `%${string}%`
            attrs.push(wildCardedString)
            attrs.push(wildCardedString)
            attrs.push(wildCardedString)
            attrs.push(wildCardedString)
            attrs.push(wildCardedString)
            attrs.push(wildCardedString)
          }
          return this.count({
            where: where.length > 0 ? [where.join(' AND '), ...attrs] : undefined
          })
        },
        list({ offset = 0, limit = 24, product_cat_id: catId, product_sub_cat_id: subCatId, provider_id: providerId, string }) {
          const include = inc(sequelize.models)
          let where = []
          const attrs = []
          if (catId) {
            where.push('product_cat_id = ?')
            attrs.push(catId)
          }
          if (subCatId) {
            where.push('product_sub_cat_id = ?')
            attrs.push(subCatId)
          }
          if (providerId) {
            where.push('provider_id = ?')
            attrs.push(providerId)
          }
          if (string) {
            where.push('(name like ? OR `desc` like ? OR content like ? OR name_en like ? OR `desc_en` like ? OR content_en like ?)')
            const wildCardedString = `%${string}%`
            attrs.push(wildCardedString)
            attrs.push(wildCardedString)
            attrs.push(wildCardedString)
            attrs.push(wildCardedString)
            attrs.push(wildCardedString)
            attrs.push(wildCardedString)
          }

          return this.findAll({
            offset: offset * limit,
            limit: parseInt(limit),
            where: where.length > 0 ? [where.join(' AND '), ...attrs] : undefined,
            order: 'id DESC',
            include: [{
              model: ProductCat,
              as: 'productCat'
            }, {
              model: ProductSubCat,
              as: 'productSubCat'
            }, {
              model: ProductImage,
              as: 'images'
            }, {
              model: Provider,
              as: 'provider'
            }]
          })
        },
        all({ lang = 'mn', offset = 0, limit = 24, product_cat_id: catId, product_sub_cat_id: subCatId, provider_id: providerId, string }) {
          const include = inc(sequelize.models)
          let where = []
          const attrs = []
          if (lang === 'mn') {
            where.push('mn is true')
          } else {
            where.push('en is true')
          }
          if (catId) {
            where.push('product_cat_id = ?')
            attrs.push(catId)
          }
          if (subCatId) {
            where.push('product_sub_cat_id = ?')
            attrs.push(subCatId)
          }
          if (providerId) {
            where.push('provider_id = ?')
            attrs.push(providerId)
          }
          if (string) {
            where.push('(name like ? OR `desc` like ? OR content like ? OR name_en like ? OR `desc_en` like ? OR content_en like ?)')
            const wildCardedString = `%${string}%`
            attrs.push(wildCardedString)
            attrs.push(wildCardedString)
            attrs.push(wildCardedString)
            attrs.push(wildCardedString)
            attrs.push(wildCardedString)
            attrs.push(wildCardedString)
          }

          return this.findAll({
            offset: offset * limit,
            limit: parseInt(limit),
            where: where.length > 0 ? [where.join(' AND '), ...attrs] : undefined,
            order: 'id DESC',
            include: [{
              model: ProductCat,
              as: 'productCat'
            }, {
              model: ProductSubCat,
              as: 'productSubCat'
            }, {
              model: ProductImage,
              as: 'images'
            }, {
              model: Provider,
              as: 'provider'
            }]
          })
        }
      },
    })
}
