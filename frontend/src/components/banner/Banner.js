/* eslint-disable */
import React, {Component} from 'react';
import './Banner.css';
import {connect} from "react-redux";
import {loadStatic} from "../../actions/static";
import bannerImg from "./banner.jpg";

jQuery(document).ready(function ($) {
    $('.counter').counterUp({
        delay: 10,
        time: 1000
    });
});

class Banner extends Component {

    componentDidMount() {
        this.maybeFetchData();
    }

    maybeFetchData() {
        const {loadStatic} = this.props;
        loadStatic('finance_bundle')
    }

    render() {
        const {lang, staticReducer} = this.props
        return (
            <div className={`banner`} style={{backgroundImage: `url(${staticReducer.finance_bundle && staticReducer.finance_bundle.data})`}}>
            </div>
        );
    }
}


const mapStateToProps = state => {
    const {config, staticReducer} = state;
    return {
        lang: config.language,
        staticReducer: staticReducer
    };
};

const mapDispatch = {
    loadStatic
};

export default connect(mapStateToProps, mapDispatch)(Banner);
