'use strict'
import Sequelize from 'sequelize';
import { Product, ProductCat, ProductImage, ProductSubCat, Provider } from '../../../model'
import Err from '../../../modules/err'
import { format } from '../../../modules/formatter'

export default async (req, res, next) => {
  req.checkParams('id', 'bad_id').isInt()
  const err = req.validationErrors()
  if (err) return res.callback(err)
  const { id } = req.params
  let product = await Product.find({
    where: { id: id },
    include: [{
      model: ProductCat,
      as: 'productCat'
    }, {
      model: ProductSubCat,
      as: 'productSubCat'
    }, {
      model: ProductImage,
      as: 'images'
    }, {
      model: Provider,
      as: 'provider'
    }]
  }).then(o => o.get({ plain: true }))
  if (!product) return res.callback(Err.notfound())

  let orderString = [`abs(${product.product_cat_id} - product.product_cat_id) asc`];

  if (req.body.product_sub_cat_id) {
    orderString.push(`abs(${product.product_sub_cat_id} - product.product_sub_cat_id) asc`)
  }
  orderString.push(`abs(${product.provider_id} - product.provider_id) asc`)

  const similar = await Product.findAll({
    where: ['id <> ?', product.id],
    limit: 4,
    order: orderString.join()
  })

  product.similar = similar;

  res.data = product;
  
  next()
}
