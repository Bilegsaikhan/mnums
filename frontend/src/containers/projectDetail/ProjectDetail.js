import React, {Component} from 'react';
import './ProjectDetail.css';
import {connect} from 'react-redux';
import {Link} from 'react-router'
import {loadProject} from '../../actions/project';
import language from "../../constants/language"
import moment from 'moment'

class ProjectDetail extends Component {

    componentDidMount() {
        if(window.FB) window.FB.XFBML.parse();
        this.maybeFetchData()
    }

    maybeFetchData() {
        const {params, loadProject} = this.props;
        loadProject({id: params.id});
    }

    render() {
        const {detail, lang, isItemFetching} = this.props;
        return (
            <div className="projectDetail">
                <div className="g01v1-this-page-in">
                    <div className="container">
                        <div className="row">
                            <div className="col-xs-5 col-sm-6">
                                <div className="breadcrumb_wbr wb_breadcrumb">
                                    <div className="g04v1-text">
                                        <ol className="breadcrumb">
                                            <li><Link to={`/`}>{language.homepage[lang]}</Link></li>
                                            <li><Link to={`/projects`}>{language.projects[lang]}</Link></li>
                                            <li>{detail.name}</li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="container">

                    <div className={`projectLoading ${isItemFetching ? 'showLoading' : ''}`}>
                        <div className="showbox">
                            <div className="loader">
                                <svg className="circular" viewBox="25 25 50 50">
                                    <circle className="path" cx="50" cy="50" r="20"
                                            fill="none" strokeWidth="2"
                                            strokeMiterlimit="10"/>
                                </svg>
                            </div>
                            Уншиж байна...
                        </div>
                    </div>

                    <div className={`titlepar parsys dbmProjectItemblock ${isItemFetching ? 'showLoading' : ''}`}>
                        <div className="gridlayout parbase section">
                            <div className="row">
                                <div className="col-md-12 col-sm-12 col-lg-12 col-xs-12">
                                    <div>
                                        <div className="grid_12_par_0 parsys">
                                            <div className="remove-padding-grid">
                                                <div className="title section">
                                                    <div className="row">
                                                        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <h1 className="flipboard-title dbmProjectTitle">{detail.name}</h1>
                                                            <div className="time-stamp">{language.updatedAt[lang]}: {moment(detail.updated_at).format("YYYY.MM.DD")}</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr className="hr-bottom"/>
                                </div>
                            </div>
                            <div className="clearfix"/>
                        </div>
                        <div className="gridlayout parbase section">
                            <div className="row">
                                <div className="col-md-8 col-xs-12 col-sm-12">
                                    <div className="">
                                        <div className="grid_8_par_0 parsys">
                                            <div className="remove-padding-grid">
                                                <div className="gridlayout parbase section">
                                                    <div className="row">
                                                        <div className="col-md-12 col-sm-12 col-xs-12">
                                                            <div>
                                                                <div className="grid_12_par_0 parsys">
                                                                    <div className="remove-padding-grid">
                                                                        <div
                                                                            className="c14v1_static_content section mr-20">
                                                                            <section>
                                                                                <div className="c14v1-body c14v1-body-text flipboard-keep ">
                                                                                    <div className="projectSection">
                                                                                        <img className="projectImg"
                                                                                             src={detail.image ? detail.image : 'http://ubinfo-s3.s3.amazonaws.com/static/PROJECT-PLACEHOLDER-METROBC.jpg'}
                                                                                             alt=""/>
                                                                                    </div>


                                                                                    <div
                                                                                        className="projectSection projectDate textRight">
                                                                                        <div className="pdStart"><span
                                                                                            className="dateLabel">{language.projectStartDate[lang]}: </span>{detail.start_date}
                                                                                        </div>
                                                                                        <div className="pdEnd"><span
                                                                                            className="dateLabel">{language.projectEndDate[lang]}:</span> {detail.end_date}
                                                                                        </div>
                                                                                    </div>
                                                                                    {detail.program &&
                                                                                        <div className="projectSection">
                                                                                            <h5>{language.projectProgramme[lang]}</h5>
                                                                                            <div>{detail.program}</div>
                                                                                        </div>
                                                                                    }
                                                                                    {detail.objective &&
                                                                                        <div className="projectSection">
                                                                                            <h5>{language.projectObjective[lang]}</h5>
                                                                                            <div>{detail.objective}</div>
                                                                                        </div>
                                                                                    }
                                                                                    {detail.economic &&
                                                                                        <div className="projectSection">
                                                                                            <h5>{language.projectEconomic[lang]}</h5>
                                                                                            <div>{detail.economic}</div>
                                                                                        </div>
                                                                                    }
                                                                                    {detail.social &&
                                                                                        <div className="projectSection">
                                                                                            <h5>{language.projectSocial[lang]}</h5>
                                                                                            <div>{detail.social}</div>
                                                                                        </div>
                                                                                    }

                                                                                    {detail.interpretation &&
                                                                                        <div className="projectSection">
                                                                                            <h5>{language.projectInterpretation[lang]}</h5>
                                                                                            <div>{detail.interpretation}</div>
                                                                                        </div>
                                                                                    }

                                                                                    {detail.introduction &&
                                                                                        <div className="projectSection">
                                                                                            <h5>{language.projectIntroduction[lang]}</h5>
                                                                                            <div>{detail.introduction}</div>
                                                                                        </div>
                                                                                    }
                                                                                </div>
                                                                            </section>
                                                                            <div className="clearfix"/>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="clearfix"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-4 col-sm-12 col-lg-4 col-xs-12">
                                    <div>
                                        <div className="grid_4_par_1 parsys">
                                            <div className="remove-padding-grid">
                                                <div className="gridlayout parbase section">
                                                    <div className="row">
                                                        <div className="col-md-12 col-xs-12 col-sm-12">
                                                            <div>
                                                                <div className="grid_12_par_0 parsys">
                                                                    <div className="remove-padding-grid">
                                                                        <hr className="hidden-lg hidden-md"/>
                                                                        {detail.implementer &&
                                                                            <div>
                                                                                <div
                                                                                    className="c10v1_resources parbase section">
                                                                                    <div className="projectRightSection">
                                                                                        <h6>{language.projectImplementer[lang]}</h6>
                                                                                        <div
                                                                                            className="prsContent">{detail.implementer}</div>
                                                                                        <div className="clearfix"/>
                                                                                    </div>
                                                                                </div>
                                                                                <hr/>
                                                                            </div>
                                                                        }
                                                                        {detail.performance &&
                                                                            <div>
                                                                                <div
                                                                                    className="c10v1_resources parbase section">
                                                                                    <div className="" id="c10v1-body">
                                                                                        <h6>{language.projectPerformance[lang]}</h6>
                                                                                        <div className="progressBox">
                                                                                            <div className="progressNum">
                                                                                                {detail.performance}%
                                                                                            </div>
                                                                                            <div className="projectProgress">
                                                                                                <div className="activeProgress"
                                                                                                     style={{width: `${detail.performance}%`}}/>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div className="clearfix"/>
                                                                                    </div>
                                                                                </div>
                                                                                <hr/>
                                                                            </div>
                                                                        }
                                                                        <div
                                                                            className="c10v1_resources parbase section">
                                                                            <div className="projectRightSection">
                                                                                <h6>{language.projectFinancingAmount[lang]}</h6>
                                                                                <div className="prsContent">
                                                                                    <ul className="projectrl list-unstyled">
                                                                                        {detail.togtool_amount &&
                                                                                            <li>
                                                                                                <span className="prlLabel">{language.projectTogtoolAmount[lang]}:</span>
                                                                                                {detail.togtool_amount}
                                                                                            </li>
                                                                                        }
                                                                                        {detail.geree_amount &&
                                                                                            <li>
                                                                                                <span className="prlLabel">{language.projectGereeAmount[lang]}:</span>
                                                                                                {detail.geree_amount}
                                                                                            </li>
                                                                                        }
                                                                                        {detail.sankhuu_amount &&
                                                                                            <li>
                                                                                                <span className="prlLabel">{language.projectSankhuuAmount[lang]}:</span>
                                                                                                {detail.sankhuu_amount}
                                                                                            </li>
                                                                                        }
                                                                                    </ul>
                                                                                </div>
                                                                                <div className="clearfix"/>
                                                                            </div>
                                                                        </div>
                                                                        <hr/>
                                                                        {detail.loan_type &&
                                                                            <div>
                                                                                <div
                                                                                    className="c10v1_resources parbase section">
                                                                                    <div className="projectRightSection">
                                                                                        <h6>{language.projectLoanType[lang]}</h6>
                                                                                        <div
                                                                                            className="prsContent">{detail.loan_type}</div>
                                                                                        <div className="clearfix"/>
                                                                                    </div>
                                                                                </div>
                                                                                <hr/>
                                                                            </div>
                                                                        }
                                                                        {detail.borrower &&
                                                                            <div>
                                                                                <div
                                                                                    className="c10v1_resources parbase section">
                                                                                    <div className="projectRightSection">
                                                                                        <h6>{language.projectBorrower[lang]}</h6>
                                                                                        <div
                                                                                            className="prsContent">{detail.borrower}</div>
                                                                                        <div className="clearfix"/>
                                                                                    </div>
                                                                                </div>
                                                                                <hr/>
                                                                            </div>
                                                                        }
                                                                        {detail.duty &&
                                                                            <div>
                                                                                <div
                                                                                    className="c10v1_resources parbase section">
                                                                                    <div className="projectRightSection">
                                                                                        <h6>Хүчин чадал</h6>
                                                                                        <div
                                                                                            className="prsContent">{detail.duty}</div>
                                                                                        <div className="clearfix"/>
                                                                                    </div>
                                                                                </div>
                                                                                <hr/>
                                                                            </div>
                                                                        }
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="clearfix"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="clearfix"/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


const mapState = (state, props) => ({
    detail: state.projects.detail,
    isItemFetching: state.projects.isItemFetching,
    lang: state.config.language
});

const mapDispatch = {
    loadProject,
};

const EnhancedComponent = connect(mapState, mapDispatch)(ProjectDetail);
export default EnhancedComponent;
