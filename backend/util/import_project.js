/**
 * Created by enxtur on 5/22/17.
 */
import { projectsData } from '../raw/projectData'
import { Project } from '../model'
import moment from 'moment'
const debug = require('../modules/debug')(__dirname)
module.exports = () => {
  Project.bulkCreate(projectsData.map(({
                                         id,
                                         type: branch_id, type2: region_id, type3: source_id, updatedAt,
                                         name,
                                         program,
                                         typeOfLoan: loan_type,
                                         projectImplementer: implementer,
                                         borrower,
                                         objective,
                                         togtoolAmount: togtool_amount,
                                         gereeAmount: geree_amount,
                                         sankhuuAmount: sankhuu_amount,
                                         startDate: start_date,
                                         endDate: end_date,
                                         performance: _performance,
                                         duty,
                                         economicEfficiency: economic,
                                         socialBenefits: social,
                                         projectInterpretation: interpretation,
                                         briefIntroduction: introduction
                                       }) => {
    const performance = _performance || null
    return {
      created_at: moment(updatedAt, 'M-р сар. DD, YYYY').toDate(),
      updated_at: moment(updatedAt, 'M-р сар. DD, YYYY').toDate(),
      image: `http://ubinfo-s3.s3.amazonaws.com/dbm/projects/${id}.jpg`,
      branch_id,
      region_id,
      source_id,
      name,
      program,
      loan_type,
      implementer,
      borrower,
      objective,
      togtool_amount,
      geree_amount,
      sankhuu_amount,
      start_date,
      end_date,
      performance,
      duty,
      economic,
      social,
      interpretation,
      introduction
    }
  })).then(() => {
    debug('success')
  }).catch((err) => {
    debug(err)
  })
}

