'use strict'
import { welcome, forgot, verify } from '../modules/mail'

const debug = require('../modules/debug')(__filename)

export default function (data) {
  debug('mail task received on handler')
  let style = data.style
  let prom
  switch (style) {
    case 'welcome': {
      prom = sendWelcome(data)
      break
    }
    case 'forgot': {
      prom = sendForgot(data)
      break
    }
    case 'verify': {
      prom = sendVerify(data)
      break
    }
    default: {
      prom = Promise.resolve()
    }
  }
  return prom
}

function sendWelcome (data) {
  let {name = 'Хэрэглэгч', to, verification_token: verificationToken} = data
  let link = `http://ubtrade.com/api/user/verify/${verificationToken}`
  return welcome(to, {name, link})
}

function sendForgot (data) {
  let {name = 'Хэрэглэгч', to, token} = data
  let link = `http://10.201.187.115:3001/reset/${token}`
  return forgot(to, {name, link})
}

function sendVerify (data) {
  let {name = 'Хэрэглэгч', to, verification_token: verificationToken} = data
  let link = `http://ubtrade.com/api/user/verify/${verificationToken}`
  return verify(to, {name, link})
}
