/**
 * Created by enxtur on 5/5/17.
 */

import { Project } from '../../../model'
import Err from '../../../modules/err'
import { format } from '../../../modules/formatter'
export default [async (req, res, next) => {
  const {id} = req.params
  let project = await Project.findById(id).then(o => o ? o.get({plain: true}) : null)
  if (!project) return res.callback(Err.notfound())
  const lang = req.session.lang || 'mn'
  if (lang === 'mn' && !project.mn) return res.callback(Err.notfound())
  if (lang === 'en' && !project.en) return res.callback(Err.notfound())
  res.data = project
  next()
}, format({
  program: true,
  loan_type: true,
  borrower: true,
  implementer: true,
  objective: true,
  sankhuu_amount: true,
  economic: true,
  social: true,
  introduction: true,
  start_date: true,
  end_date: true,
  name: true,
  togtool_amount: true,
  geree_amount: true,
  duty: true,
  interpretation: true
})]
