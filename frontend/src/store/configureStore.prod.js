import {createStore, applyMiddleware} from 'redux'
import thunk from 'redux-thunk'
import rootReducer from '../reducers'
import createHelpers from './helper';

const configureStore = initialState => {
    const helpers = createHelpers();
    let middleware = [thunk.withExtraArgument(helpers)];
    return createStore(rootReducer, initialState, applyMiddleware(...middleware))
}

export default configureStore