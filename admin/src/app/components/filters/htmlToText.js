export function htmlToPlaintext() {
  'ngInject';
  return function (text) {
    return text ? String(text).replace(/<[^>]+>/gm, '') : '';
  };

}
