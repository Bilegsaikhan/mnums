import {
    LOAD_SEARCH_RESULT, LOAD_SEARCH_RESULT_SUCCESS, LOAD_SEARCH_RESULT_ERROR,
} from "../constants";

export function loadSearchResult({params = {}}) {
    return async(dispatch, getState, {fetch}) => {
        // eslint-disable-next-line no-param-reassign
        const payload = {};

        dispatch({
            type: LOAD_SEARCH_RESULT,
            payload
        });

        try {
            let response = await fetch(`search${params}`, {
                method: "GET",
                credentials: 'include'
            });
            let data = await response.json();
            if (!response.ok) {
                const message = data.message || data;
                if (data) throw message;
            }

            dispatch({
                type: LOAD_SEARCH_RESULT_SUCCESS,
                payload: data
            });

        } catch (error) {
            dispatch({
                type: LOAD_SEARCH_RESULT_ERROR,
                error: error,
            });
            return false;
        }
        return true;
    };
}