export class MilestoneService {
  constructor (server) {
    'ngInject';
    this.server = server
  }

  list () {
    return this.server.get('/timeline')
  }
  get ({id}) {
    return this.server.get(`/timeline/${id}`)
  }
  update (data) {
    return this.server.put(`/timeline/${data.id}`, data)
  }
  remove (data) {
    return this.server.delete(`/timeline/${data.id}`)
  }
  create (data) {
    return this.server.post(`/timeline`, data)
  }
  order ({ids}) {
    return this.server.put('/timeline', {ids})
  }
}
