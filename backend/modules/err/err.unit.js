/**
 * Created by enxtur on 2/15/17.
 */
import Err from './'
describe.skip('Err', () => {
  it('#it should not be failed', () => {
    Err.should.be.a.Function()

    Err.badRequest().should.be.an.instanceOf(Err)
    Err.badRequest().status.should.equal(400)

    Err.userExist().should.be.an.instanceOf(Err)
    Err.userExist().status.should.equal(409)

    Err.notfound().should.be.an.instanceOf(Err)
    Err.notfound().status.should.equal(404)

    Err.badCode().should.be.an.instanceOf(Err)
    Err.badCode().status.should.equal(400)

    Err.unauthorized().should.be.an.instanceOf(Err)
    Err.unauthorized().status.should.equal(401)

    Err.expectationfailed().should.be.an.instanceOf(Err)
    Err.expectationfailed().status.should.equal(417)

    Err.internal().should.be.an.instanceOf(Err)
    Err.internal().status.should.equal(500)

    Err.sequelize().should.be.an.instanceOf(Err)
    Err.sequelize().status.should.equal(500)
  })
})
