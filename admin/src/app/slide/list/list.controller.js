export class ListSlideController {
  constructor(sliderService) {
    'ngInject';
    this.service = sliderService
    this.pageCount = 0
    this.search = ''
    this.allData = 0
    this.count = 0
    this.listData = []
    this.page = 1
    this.limit = 10
    this.isTableLoading = true
    this.order = 'id DESC'
    this.load()
  }

  load() {
    this.service.list().then(pages => {
      this.listData = pages
      this.count = pages.length
    })
  }

}
