export class LogsService {
  constructor (server) {
    'ngInject';
    this.server = server
  }

  list (obj) {
    let str = Object.keys(obj).map(function(key){
      return encodeURIComponent(key) + '=' + encodeURIComponent(obj[key]);
    }).join('&');
    return this.server.get(`/log?${str}`)
  }
  get ({id}) {
    return this.server.get(`/law/${id}`)
  }
}
