'use strict'
import { ArticleCopy as Article } from '../../../model'
import Err from '../../../modules/err'

export default async function deleteArticle (req, res) {
  req.checkParams('id', 'bad_id').isInt()
  const err = req.validationErrors()
  if (err) return res.callback(err)
  const {id} = req.params
  let article = await Article.findById(id)
  if (!article) return res.callback(Err.notfound())
  await article.destroy()
  res.callback()
}
