export class UserFormController {
  constructor($log, userService, $state, toastr, modalService) {
    'ngInject';
    this.$log = $log.log
    this.service = userService
    this.toastr = toastr
    this.$state = $state
    this.stateName = $state.current.name
    this.modalService = modalService
    this.clear()
    if (this.stateName === 'root.content.user.edit') this.load()
  }

  clear() {
    this.item = {
      username: '',
      password: ''
    }
  }

  create() {
    this.service.create(this.item).then(item => {
      this.item = item
      this.$state.go('root.content.user.list');
    })
  }

  update() {
    this.service.update(this.item).then(item => {
      this.item = item
      this.toastr.success('Таны хүсэлт амжилттай!');
    }, () => {
      this.toastr.error('Таны хүсэлт амжилтгүй!');
    })
  }

  remove() {
    this.modalService.danger('Та уг үүнийг устгахдаа итгэлтэй байна уу', true).then(() => {
      this.service.remove({id: this.$state.params.id}).then(() => {
        this.toastr.success('Таны хүсэлт амжилттай!');
        this.$state.go('root.content.user.list');
      }, () => {
        this.toastr.error('Таны хүсэлт амжилтгүй!');
      })
    })
  }

  load() {
    this.service.get({id: this.$state.params.id}).then(item => {
      this.item = item
    }, () => {
      this.toastr.error('Таны хүсэлт амжилтгүй!');
    })
  }


}
