'use strict'
import { updateLang, getLang } from './lang'
import { login, logout, profile } from './auth'
import { uploadImage, uploadPdf } from './upload'
import info from './info'
import search from './search'
import stat from './stat'
import log from './log'
import backup from './backup'
import sitemap from './sitemap'

export default ({router, passport, checkLoggedIn, multipartMiddleware, wrap, checkRootAuth}) => {
  router.route('/lang')
    .put(wrap(updateLang))
    .get(wrap(getLang))
  router.route('/login').post(wrap(login({passport})))
  router.route('/logout').post(logout)
  router.route('/profile').get(profile)
  router.route('/upload/image').post(multipartMiddleware, wrap(uploadImage))
  router.route('/upload/pdf').post(checkLoggedIn, multipartMiddleware, wrap(uploadPdf))
  router.route('/info').get(wrap(info[0]), info[1])
  router.route('/search').get(search)
  router.route('/stat').get(checkLoggedIn, stat)
  router.route('/log').get(checkRootAuth, log)
  router.route('/backup').get(checkRootAuth, backup)
  router.route('/sitemap').get(sitemap)
}
