/**
 * Created by enxtur on 5/23/17.
 */
import { ProjectBranch, ProjectRegion, ProjectSource, ArticleCat, Slide, Timeline, Store } from '../../../model/index'
import { format } from '../../../modules/formatter/index'
export default [
  async (req, res, next) => {
    const lang = req.session.lang || 'mn'
    let slidesWhere = {};
    if (lang === 'mn') {
      slidesWhere = { mn: true }
    } else {
      slidesWhere = { en: true }
    }
    const branches = await ProjectBranch.findAll({order: '`order` ASC'}).then(list => list.map(o => o.get({plain: true})))
    const regions = await ProjectRegion.findAll({order: '`order` ASC'}).then(list => list.map(o => o.get({plain: true})))
    const sources = await ProjectSource.findAll({order: '`order` ASC'}).then(list => list.map(o => o.get({plain: true})))
    const cats = await ArticleCat.findAll({order: '`order` ASC'}).then(list => list.map(o => o.get({plain: true})))
    const slides = await Slide.findAll({order: '`order` ASC', where: slidesWhere}).then(list => list.map(o => o.get({plain: true})))
    const stores = await Store.findByKeys(['facebook', 'twitter', 'youtube', 'linkedin']).then(list => list.map(o => o.get({plain: true})))
    let where = {}
    if (lang === 'mn') {
      where = {
        ...where,
        mn: true
      }
    } else {
      where = {
        ...where,
        en: true
      }
    }
    const timelines = await Timeline.findAll({
      where,
      order: '`order` ASC'
    }).then(list => list.map(o => o.get({plain: true})))
    res.data = {
      branches,
      regions,
      sources,
      cats,
      slides,
      timelines,
      stores
    }
    next()
  },
  format({
    cats: [{
      name: true
    }],
    branches: [{
      name: true
    }],
    regions: [{
      name: true
    }],
    sources: [{
      name: true
    }],
    slides: [{
      name: true,
      desc: true
    }],
    timelines: [{
      date: true,
      content: true
    }],
    stores: [{
      data: true
    }]
  })
]
