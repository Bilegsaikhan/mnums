export class MenuController {
  constructor ($log, modalService, menuService, pageService, toastr) {
    'ngInject';
    this.$log = $log.log
    this.modalService = modalService
    this.menuService = menuService
    this.pageService = pageService
    this.toastr = toastr
    this.$log('menu controller initialized')
    this.dndMainAllowedTypes = ['level-1']
    this.reload()
  }

  clear () {
    this.menu = {
      name: '',
      name_en: '',
      parent_id: '',
      type: '',
      module: '',
      en: false,
      mn: false,
      page_id: ''
    }
    this.parent = null
    this.sub = null
    this.selected = null
    this.page = null
  }

  reload () {
    this.clear()
    this.menuService.list().then(menus => {
      this.parents = menus
      this.parentIndex = indexMenu(menus)
      this.menus = convertMenus(menus)
      this.$log(this.menus)
      this.pageService.list().then(pages => {
        this.pages = pages
        this.pageIndex = indexPage(pages)
      })
    })
  }

  dragEnd (menus) {
    const ids = menus.map(m => m.id)
    this.$log('ordering menu', ids)
    this.menuService.order({ids})
  }

  edit (menu) {
    this.$log('selected', this.selected)
    this.menuService.edit(menu.id).then(menu => {
      this.menu = menu
      this.page = menu.page ? this.pageIndex[menu.page.id] : null
      if (menu.parent) {
        if (menu.parent.parent_id) {
          this.parent = this.parentIndex[menu.parent.parent_id]
          this.sub = this.parentIndex[menu.parent.id]
        } else {
          this.parent = this.parentIndex[menu.parent.id]
          this.sub = null
        }
      } else {
        this.parent = null
        this.sub = null
      }
    })
  }

  canSubmit () {
    return this.menuForm.$invalid
  }

  submit () {
    // this.$log('menu')
    // this.$log('page', this.page)
    (this.menu.id ? this.update() : this.create()).then(() => {
      this.toastr.success('Таны хүсэлт амжилттай!');
      this.reload()
    }, err => {
      this.$log(err)
      this.toastr.error('Таны хүсэлт амжилтгүй!');
    })
  }

  update () {
    this.$log('update called')
    let parent_id = null
    let page_id = null
    if (this.sub) {
      parent_id = this.sub.id
    } else if (this.parent) {
      parent_id = this.parent.id
    }
    if (this.page) {
      page_id = this.page.id
    }
    return this.menuService.update({
      id: this.menu.id,
      name: this.menu.name,
      name_en: this.menu.name_en,
      module: this.menu.module,
      en: this.menu.en,
      mn: this.menu.mn,
      parent_id,
      page_id
    })
  }

  create () {
    this.$log('create called', this.menu)
    let parent_id = null
    let page_id = null
    if (this.sub) {
      parent_id = this.sub.id
    } else if (this.parent) {
      parent_id = this.parent.id
    }
    if (this.page) {
      page_id = this.page.id
    }
    return this.menuService.create({
      name: this.menu.name,
      name_en: this.menu.name_en,
      module: this.menu.module,
      en: this.menu.en,
      mn: this.menu.mn,
      parent_id,
      page_id
    })

  }

  remove () {
    this.$log('remove called')
    this.modalService.danger('Та уг менюг устгахдаа итгэлтэй байна уу', true).then(() => {
      return this.menuService.delete(this.menu.id).then(() => {
        this.reload()
      })
    })

  }

  hasGrandChildren (menu) {
    if (!menu) return true
    if (!menu.subs || menu.subs.length === 0) return false
    for (let i = 0; i < menu.subs.length; i++) {
      const sub = menu.subs[i]
      if (sub.subs && sub.subs.length > 0) return true
    }
    return false
  }
}
function convertMenus (_menus) {
  const menus = _menus.slice()
  menus.forEach(menu1 => {
    menu1.dndType = 'level-1'
    menu1.dndAllowedTypes = [`level-2-${menu1.id}`]

    menu1.menus.forEach(menu2 => {
      menu2.dndType = `level-2-${menu1.id}`
      menu2.dndAllowedTypes = [`level-3-${menu2.id}`]

      menu2.menus.forEach(menu3 => {
        menu3.dndType = `level-3-${menu2.id}`
      })
      if (menu2.menus && menu2.menus.length > 0) {
        menu2.subs = menu2.menus
        menu2.menus = [menu2.menus]
      } else {
        menu2.menus = []
      }
    })
    if (menu1.menus && menu1.menus.length > 0) {
      menu1.subs = menu1.menus
      menu1.menus = [menu1.menus]
    } else {
      menu1.menus = []
    }
  })
  return {
    menus
  }
}
function indexMenu (menus) {
  const result = {}
  menus.forEach(menu => {
    result[menu.id] = menu
    if (menu.menus && menu.menus.length) {
      menu.menus.forEach(menu => {
        result[menu.id] = menu
      })
    }
  })
  return result
}
function indexPage (pages) {
  const result = {}
  pages.forEach(page => {
    result[page.id] = page
  })
  return result
}
