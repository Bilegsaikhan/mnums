import React, {Component} from "react";
import PropTypes from "prop-types";
import OwlCarousel from "react-owl-carousel2";
import "./Slider.css";
import TextTruncate from "react-text-truncate";
import language from "../../constants/language"
import {connect} from "react-redux";

class Slider extends Component {
    static propTypes = {
        slides: PropTypes.array
    };

    constructor(props) {
        super(props);
        this.state = {
            index: 0,
            count: 0
        };
    }

    shouldComponentUpdate() {
        return false;
    }

    render() {
        const {slides, lang} = this.props;
        const options = {
            items: 1,
            nav: false,
            dots: false,
            rewind: true,
            autoplay: false,
            loop: true
        };

        return (
            <div className="c15v3_featured section dbmHomeSlider">
                <div className="">
                    <div className="c15v3-top" style={{padding: "3px"}}>
                        <h6 className="feature mrg-b-5">{language.featured[lang]}</h6>
                        <div className="pagination">
                            <a
                                className="prev"
                                onClick={() => this.refs.homeMainSlide.prev()}
                            >
                                <i
                                    className="fa fa-chevron-left"
                                    rel="popover"
                                    alt="Previous"
                                    title="Previous"
                                />
                                <span className="screen-reader-text">Өмнөх</span>
                            </a>
                            <a
                                className="next"
                                onClick={() => this.refs.homeMainSlide.next()}
                            >
                                <i
                                    className="fa fa-chevron-right"
                                    rel="popover"
                                    alt="Next"
                                    title="Next"
                                />
                                <span className="screen-reader-text">Дараах</span>
                            </a>
                        </div>
                    </div>
                    <div id="c15v3">
                        <OwlCarousel
                            ref="homeMainSlide"
                            options={options}
                            events={{onChanged: this.onChangeSlide}}
                        >
                            { slides &&
                                slides.map((slide, index) => {
                                    if (!slide[lang]) return null;
                                    return (
                                        <div className="item" key={`hms_${index}`}>
                                            <div className="fc-image">
                                                <img
                                                    src={slide.image}
                                                    alt="slide"
                                                />
                                            </div>
                                            <div className="fc-content">
                                                <div className="row">
                                                    <div className="col-lg-12">
                                                        <div id="c15v3-body1">
                                                            <div className="c15v3-title">
                                                                <a href={slide.link} target="_blank">
                                                                    <TextTruncate
                                                                        line={2}
                                                                        truncateText="…"
                                                                        text={slide.name}
                                                                    />
                                                                </a>
                                                            </div>
                                                            <div className="c15v3-body-text">
                                                                <span>{slide.date} </span> &nbsp;
                                                                —&nbsp; {slide.desc}
                                                            </div>
                                                            <div className="c15v3-links">
                                                                <a href={slide.link} target="_blank">
                                                                    {language.seeMore[lang]} »
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    );
                                })
                            }
                        </OwlCarousel>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    const {config, basic} = state;
    return {
        lang: config.language,
        slides: basic.slides
    }
}

const mapDispatch = {};

export default connect(mapStateToProps, mapDispatch)(Slider);
