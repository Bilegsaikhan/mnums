'use strict'
import { Product_image } from '../../../model'
import Err from '../../../modules/err'

export default async function createCat (req, res) {
  req.checkBody('url', 'bad_url').len(1, 255)
  req.checkBody('product_id', 'bad_product_id').len(1, 255)
  let is_main = req.body.is_main;
  is_main = (is_main === true || is_main === 'true')
  const err = req.validationErrors()
  if (err) return res.callback(err)
  try {
    const cat = await Product_image.create({
      url: req.body.url,
      is_main: is_main,
      product_id: req.body.product_id
    })
    res.callback(cat)
  } catch (err) {
    res.callback(Err.sequelize(err))
  }
}
