/**
 * Created by enxtur on 5/5/17.
 */

import { Project } from '../../../model'
import Err from '../../../modules/err'
export default async (req, res) => {
  req.checkParams('id', 'bad_id').isInt()
  req.checkBody('mn', 'bad_mn').isBoolean()
  req.checkBody('en', 'bad_en').isBoolean()
  if (req.body.branch_id) {
    req.checkBody('branch_id', 'bad_branch_id').isInt()
  }
  if (req.body.region_id) {
    req.checkBody('region_id', 'bad_region_id').isInt()
  }
  if (req.body.source_id) {
    req.checkBody('source_id', 'bad_source_id').isInt()
  }
  const err = req.validationErrors()
  if (err) return res.callback(err)
  const {id} = req.params
  const updateData = Object.assign({}, req.body)
  updateData.mn = (updateData.mn === true || updateData.mn === 'true')
  updateData.en = (updateData.en === true || updateData.en === 'true')
  let project = await Project.findById(id)
  if (!project) return res.callback(Err.notfound())
  project = await project.updateAttributes(updateData)
  res.callback(project)
}
