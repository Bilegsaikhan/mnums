'use strict'
import fs from 'fs'
import os from 'os'
// import path from 'path'
import mime from 'mime'
import moment from 'moment'
import { exec as _exec } from 'child_process'
import { MYSQL_HOST, MYSQL_USER, MYSQL_PASS, MYSQLDUMP, BACKUP_DIR } from '../../../config'
import { Store } from '../../../model'

const debug = require('../../../modules/debug')(__dirname)
// export default async function (req, res, next) {
//   const tmpdir = os.tmpdir()
//   const date = moment().utcOffset(8).format('YYYY_MM_DD_HH_mm_ss')
//   const filename = `${date}.sql`
//   const file = `${tmpdir}/${filename}`
//   const command = `${MYSQLDUMP} -h ${MYSQL_HOST} -u ${MYSQL_USER} -p${MYSQL_PASS} dbm > ${file}`
//   debug(command)
//   await exec(command)
//   const mimetype = mime.lookup(file)
//
//   res.setHeader('Content-disposition', 'attachment; filename=' + filename)
//   res.setHeader('Content-type', mimetype)
//
//   const filestream = fs.createReadStream(file)
//   filestream.pipe(res)
// }

export default async function (req, res, next) {

  const file = `${BACKUP_DIR}/dump.sql`
  const command = `${MYSQLDUMP} -h ${MYSQL_HOST} -u ${MYSQL_USER} -p${MYSQL_PASS} dbm > ${file}`
  debug(command)
  await exec(command)
  let store = await Store.findByKey('backup')
  const date = moment().utcOffset(8).format('YYYY-MM-DD HH:mm:ss')
  store = await store.updateAttributes({
    data: date,
    data_en: date
  })
  res.callback(store)
}

function exec (command) {
  return new Promise((resolve, reject) => {
    _exec(command, (err, stdout) => {
      if (err) return reject(err)
      resolve(stdout)
    })
  })
}
