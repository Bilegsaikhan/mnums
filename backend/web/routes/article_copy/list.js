'use strict'
import { ArticleCopy as Article } from '../../../model'

export default async function listArticle (req, res) {
  req.checkQuery('limit', 'bad_limit').optional().isInt()
  req.checkQuery('offset', 'bad_offset').optional().isInt()
  req.checkQuery('string', 'bad_string').optional().len(1, 255)
  const err = req.validationErrors()
  if (err) return res.callback(err)
  const {limit, offset, string} = req.query
  const count = await Article.toCount({string})
  if (count === 0) {
    return res.callback({
      total: 0,
      list: []
    })
  }
  const articles = await Article.list({offset, limit, string})
  res.callback({
    total: count,
    list: articles
  })
}
