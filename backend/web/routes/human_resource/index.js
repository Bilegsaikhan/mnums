'use strict'
import { HR as Model } from '../../../model'
import Err from '../../../modules/err'

export default ({ router, passport, checkLoggedIn, multipartMiddleware, wrap, checkRootAuth }) => {
  router.route('/hr')
    .post(checkLoggedIn, wrap(async (req, res) => {
      const data = Object.assign({}, req.body)
      data.mn = (data.mn === true || data.mn === 'true')
      data.en = (data.en === true || data.en === 'true')
      const instance = await Model.create(data)
      res.callback(instance)
    }))
    .get(wrap(async (req, res) => {
      const list = await Model.findAll({ order: '`order` ASC' })
      res.callback(list)
    }))
    .put(checkLoggedIn, wrap(async (req, res) => {
      if (!req.body.ids || !Array.isArray(req.body.ids)) return res.callback(Err.badRequest('bad ids'))
      const ids = req.body.ids
      await Model.updateOrder(ids)
      res.callback()
    }))
  router.route('/hr/:id')
    .delete(checkLoggedIn, wrap(async (req, res) => {
      const instance = await Model.findById(req.params.id)
      if (!instance) return res.callback(Err.notfound())
      await instance.destroy()
      res.callback()
    }))
    .get(wrap(async (req, res) => {
      const instance = await Model.findById(req.params.id)
      if (!instance) return res.callback(Err.notfound())
      res.callback(instance)
    }))
    .put(checkLoggedIn, wrap(async (req, res) => {
      const instance = await Model.findById(req.params.id)
      if (!instance) return res.callback(Err.notfound())
      const data = Object.assign({}, req.body)
      data.mn = (data.mn === true || data.mn === 'true')
      data.en = (data.en === true || data.en === 'true')
      const updatedInstance = await instance.updateAttributes(data)
      res.callback(updatedInstance)
    }))
}
