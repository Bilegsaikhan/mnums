'use strict'
const tableName = 'pdf'
module.exports = function (sequelize, DataTypes) {
  return sequelize.define(tableName, {
    name: DataTypes.STRING,
    thumb: DataTypes.STRING,
    url: DataTypes.STRING
  }, {
    timestamps: true,
    classMethods: {
      toCount ({string}) {
        return this.count({
          where: (string && string.length) ? (['name like ?', `%${string}%`]) : undefined,
          order: 'created_at DESC'
        })
      },
      list ({offset = 0, limit = 10, string}) {
        return this.findAll({
          where: (string && string.length) ? (['name like ?', `%${string}%`]) : undefined,
          offset: offset * limit,
          limit: parseInt(limit),
          order: 'created_at DESC'
        })
      }
    }
  })
}
