export const RESET_ERROR_MESSAGE = "RESET_ERROR_MESSAGE";
export const LOAD_AGENT = "LOAD_AGENT";

export const LOAD_LANGUAGE = "LOAD_LANGUAGE";
export const LOAD_LANGUAGE_SUCCESS = "LOAD_LANGUAGE_SUCCESS";
export const LOAD_LANGUAGE_ERROR = "LOAD_LANGUAGE_ERROR";

export const CHANGE_LANGUAGE = "CHANGE_LANGUAGE";
export const CHANGE_LANGUAGE_SUCCESS = "CHANGE_LANGUAGE_SUCCESS";
export const CHANGE_LANGUAGE_ERROR = "CHANGE_LANGUAGE_ERROR";

export const LOAD_BASICINFO = "LOAD_BASICINFO";
export const LOAD_BASICINFO_SUCCESS = "LOAD_BASICINFO_SUCCESS";
export const LOAD_BASICINFO_ERROR = "LOAD_BASICINFO_ERROR";

import {
    LOAD_TOP_PROJECT, LOAD_TOP_PROJECT_SUCCESS, LOAD_TOP_PROJECT_ERROR,
    LOAD_TOP_REPORT, LOAD_TOP_REPORT_SUCCESS, LOAD_TOP_REPORT_ERROR,
    LOAD_TOP_NEWS, LOAD_TOP_NEWS_SUCCESS, LOAD_TOP_NEWS_ERROR
} from "../constants";

export function loadTopProjects(status) {
    return async(dispatch, getState, {fetch}) => {
        // eslint-disable-next-line no-param-reassign
        const payload = {};

        dispatch({
            type: LOAD_TOP_PROJECT,
            payload
        });

        try {
            let response = await fetch(`project?limit=6${status}`, {
                method: "GET",
                credentials: 'include'
            });
            let data = await response.json();
            if (!response.ok) {
                const message = data.message || data;
                if (data) throw message;
            }

            dispatch({
                type: LOAD_TOP_PROJECT_SUCCESS,
                payload: data
            });

        } catch (error) {
            dispatch({
                type: LOAD_TOP_PROJECT_ERROR,
                error: error,
            });
            return false;
        }
        return true;
    };
}
export function loadTopReport(type) {
    return async(dispatch, getState, {fetch}) => {
        // eslint-disable-next-line no-param-reassign
        const payload = {};

        dispatch({
            type: LOAD_TOP_REPORT,
            payload
        });

        try {
            let response = await fetch(`report?limit=6${type ? '&type='+type:''}`, {
                method: "GET",
                credentials: 'include'
            });
            let data = await response.json();
            if (!response.ok) {
                const message = data.message || data;
                if (data) throw message;
            }

            dispatch({
                type: LOAD_TOP_REPORT_SUCCESS,
                payload: data
            });

        } catch (error) {
            dispatch({
                type: LOAD_TOP_REPORT_ERROR,
                error: error,
            });
            return false;
        }
        return true;
    };
}
export function loadTopNews(id) {
    return async(dispatch, getState, {fetch}) => {
        // eslint-disable-next-line no-param-reassign
        const payload = {};

        dispatch({
            type: LOAD_TOP_NEWS,
            payload
        });

        try {
            let response = await fetch(`article?limit=6${id ? '&cat_id='+id:''}`, {
                method: "GET",
                credentials: 'include'
            });
            let data = await response.json();
            if (!response.ok) {
                const message = data.message || data;
                if (data) throw message;
            }

            dispatch({
                type: LOAD_TOP_NEWS_SUCCESS,
                payload: data
            });

        } catch (error) {
            dispatch({
                type: LOAD_TOP_NEWS_ERROR,
                error: error,
            });
            return false;
        }
        return true;
    };
}

export function loadBasicInfo() {
    return async (dispatch, getState, {fetch}) => {
        // eslint-disable-next-line no-param-reassign
        const payload = {};

        dispatch({
            type: LOAD_BASICINFO,
            payload
        });

        try {
            let response = await fetch(`info`, {
                method: "GET",
                credentials: 'include'
            });

            let data = await response.json();
            if (!response.ok) {
                const message = data.message || data;
                if (data) throw message;
            }
            dispatch({
                type: LOAD_BASICINFO_SUCCESS,
                payload: data
            });
            return data;
        } catch (error) {
            const message = error.message || error;
            dispatch({
                type: LOAD_BASICINFO_ERROR,
                error: error.message
            });
            throw new Error(message);
        }
    };
}


export function loadLanguage() {
    return async (dispatch, getState, {fetch}) => {
        // eslint-disable-next-line no-param-reassign
        const payload = {};

        dispatch({
            type: LOAD_LANGUAGE,
            payload
        });

        try {
            let response = await fetch(`lang`, {
                method: "GET",
                credentials: 'include'
            });

            let data = await response.json();
            if (!response.ok) {
                const message = data.message || data;
                if (data) throw message;
            }
            dispatch({
                type: LOAD_LANGUAGE_SUCCESS,
                payload: data
            });
            return data;
        } catch (error) {
            const message = error.message || error;
            dispatch({
                type: LOAD_LANGUAGE_ERROR,
                error: error.message
            });
            throw new Error(message);
        }
    };
}

export function triggerLanguage() {
    return async (dispatch, getState, {fetch}) => {
        // eslint-disable-next-line no-param-reassign
        const payload = {};

        dispatch({
            type: CHANGE_LANGUAGE,
            payload
        });

        try {
            let response = await fetch(`lang`, {
                method: "PUT",
                credentials: 'include'
            });

            let data = await response.json();
            if (!response.ok) {
                const message = data.message || data;
                if (data) throw message;
            }
            dispatch({
                type: CHANGE_LANGUAGE_SUCCESS,
                payload: data
            });
            return data;
        } catch (error) {
            const message = error.message || error;
            dispatch({
                type: CHANGE_LANGUAGE_ERROR,
                error: error.message
            });
            throw new Error(message);
        }
    };
}

// Resets the currently visible error message.
export const resetErrorMessage = () => ({
    type: RESET_ERROR_MESSAGE
});

export const loadAgent = userAgent => ({
    type: LOAD_AGENT,
    payload: userAgent
});

export const updateStep = step => ({
    type: "UPDATE_STEP",
    payload: step
});
