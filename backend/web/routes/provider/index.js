'use strict'
// const debug = require('../../../modules/debug')(__filename)
import create from './create'
import get from './get'
import update from './update'
import del from './delete'
import list from './list'
import order from './order'

export default ({router, passport, checkLoggedIn, multipartMiddleware, wrap, checkRootAuth}) => {
  router.route('/provider')
    .get(wrap(list))
    .post(checkLoggedIn, wrap(create))
    .put(checkLoggedIn, wrap(order))
  router.route('/provider/:id')
    .get(wrap(get))
    .put(checkLoggedIn, wrap(update))
    .delete(checkLoggedIn, wrap(del))
}
