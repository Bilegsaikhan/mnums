'use strict'
import { ArticleCat } from '../../../model'
import Err from '../../../modules/err'

export default async function updateCat (req, res) {
  req.checkParams('id', 'bad_id').isInt()
  req.checkBody('name', 'bad_name').len(1, 255)
  const err = req.validationErrors()
  if (err) return res.callback(err)
  const {id} = req.params
  const {name} = req.body
  let cat = await ArticleCat.findById(id)
  if (!cat) return res.callback(Err.notfound())
  cat = await cat.updateAttributes({name})
  res.callback(cat)
}
