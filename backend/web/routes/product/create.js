'use strict'

import { Product, ProductImage } from '../../../model'
import Err from '../../../modules/err'
import moment from 'moment'
import fall from 'async-waterfall'

export default async function createProduct(req, res) {
  req.checkBody('product_cat_id', 'bad_product_cat_id').isInt()
  // req.checkBody('provider_id', 'bad_provider_id').isInt()
  req.checkBody('mn', 'bad_mn').isBoolean()
  req.checkBody('en', 'bad_en').isBoolean()

  let { mn, en } = req.body;

  mn = (mn === true || mn === 'true')
  en = (en === true || en === 'true')

  if (mn) {
    req.checkBody('name', 'bad_name').len(1, 255)
    // req.checkBody('desc', 'bad_desc').len(1, 255)
  }
  if (en) {
    req.checkBody('name_en', 'bad_name_en').len(1, 255)
    // req.checkBody('desc_en', 'bad_desc_en').len(1, 255)
  }

  const err = req.validationErrors()
  if (err) return res.callback(err);

  if (!req.body.product_sub_cat_id) {
    req.body.product_sub_cat_id = null;
  }

  // if (!(req.body.images && req.body.images.length)) {
  //   return res.callback(new Error('bad_images'));
  // }

  let checker = false;

  if (!req.body.images || !req.body.images.length) {
    req.body.images = [{
      url: '/product_images/44c6ba6cd7aa6c793adeb4a04796d273.jpg',
      is_main: true
    }]
  }

  req.body.images.forEach((entry) => {
    if (entry.is_main === 'true' || entry.is_main === true) {
      checker = true;
    }
  })

  if (req.body.images.length && req.checker === false) {
    req.body.images[0].is_main = true;
  }

  const { name, name_en, desc, desc_en, content, content_en, product_cat_id, product_sub_cat_id, provider_id, price } = req.body;
  let createData = {
    product_cat_id,
    product_sub_cat_id,
    provider_id,
    mn,
    en,
    price
  }
  if (mn) {
    createData = {
      ...createData,
      name,
      desc,
      content
    }
  }
  if (en) {
    createData = {
      ...createData,
      name_en,
      desc_en,
      content_en
    }
  }

  if (!createData.provider_id) {
    delete createData.provider_id;
  }

  if (!createData.desc) {
    delete createData.desc
  }

  if (!createData.desc_en) {
    delete createData.desc_en
  }

  try {
    const product = await Product.create(createData);

    let tasks = [];

    req.body.images.forEach((entry) => {
      let task = (callback) => {
        let image = {
          url: entry.url,
          product_id: product.id,
          is_main: (entry.is_main === true || entry.is_main === 'true')
        }

        ProductImage.create(image).then(
          () => {
            console.log('done!!!')
            callback();
          },
          (e) => {
            callback(e);
          }
        )
      }

      tasks.push(task);
    })

    fall(tasks, (e) => {
      console.log('!!!!!', e);
      res.callback(product);
    })
  } catch (err) {
    res.callback(Err.sequelize(err))
  }
}
