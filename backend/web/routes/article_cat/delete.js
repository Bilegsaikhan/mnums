'use strict'
import { ArticleCat } from '../../../model'
import Err from '../../../modules/err'

export default async function deleteCat (req, res) {
  req.checkParams('id', 'bad_id').isInt()
  const err = req.validationErrors()
  if (err) return res.callback(err)
  const {id} = req.params
  let cat = await ArticleCat.findById(id)
  if (!cat) return res.callback(Err.notfound())
  await cat.destroy()
  res.callback()
}
