'use strict'
import { Article } from '../../../model'
import Err from '../../../modules/err'

export async function updateLang (req, res) {
  const lang = req.session.lang || 'mn'
  req.session.lang = lang === 'mn' ? 'en' : 'mn'
  res.callback({lang: req.session.lang})
}
export async function getLang (req, res) {
  const lang = req.session.lang || 'mn'
  res.callback(lang)
}
