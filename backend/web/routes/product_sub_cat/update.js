'use strict'
import { ProductSubCat } from '../../../model'
import Err from '../../../modules/err'

export default async function updateSubCat (req, res) {
  req.checkParams('id', 'bad_id').isInt()
  const err = req.validationErrors()
  if (err) return res.callback(err)
  const {id} = req.params
  const {name, name_en} = req.body
  let cat = await ProductSubCat.findById(id)
  if (!cat) return res.callback(Err.notfound())
  cat = await cat.updateAttributes(req.body)
  res.callback(cat)
}
