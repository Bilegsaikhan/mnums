'use strict'
import { Report as Model } from '../../../model'
import Err from '../../../modules/err'
import { pick } from 'underscore'
import { format } from '../../../modules/formatter'

export default ({router, passport, checkLoggedIn, multipartMiddleware, wrap, checkRootAuth}) => {
  router.route('/report')
    .post(checkLoggedIn, wrap(async (req, res) => { // create
      req.checkBody('mn', 'bad_mn').isBoolean()
      req.checkBody('en', 'bad_en').isBoolean()
      req.checkBody('type', 'bad_type').isIn(['year', 'audit'])
      req.checkBody('year', 'bad_year').len(4, 4)
      let err = req.validationErrors()
      if (err) return res.callback(err)

      let {mn, en, type, year} = req.body
      if (!parseInt(year)) return res.callback(Err.badRequest('bad year integer'))
      mn = (mn === true || mn === 'true')
      en = (en === true || en === 'true')
      if (mn) {
        req.checkBody('name', 'bad_name').len(1, 255)
        req.checkBody('pdf', 'bad_pdf').len(1, 255)
      }
      if (en) {
        req.checkBody('name_en', 'bad_name_en').len(1, 255)
        req.checkBody('pdf_en', 'bad_pdf_en').len(1, 255)
      }
      err = req.validationErrors()
      if (err) return res.callback(err)
      const {name, name_en, pdf, pdf_en} = req.body
      let data = {mn, en, type, year}
      if (mn) {
        data = {
          ...data,
          name,
          pdf
        }
      }
      if (en) {
        data = {
          ...data,
          name_en,
          pdf_en
        }
      }
      const instance = await Model.create(data)
      res.callback(instance)
    }))
    .get(wrap(async (req, res, next) => { // all
      const lang = req.session.lang || 'mn'
      if (req.query.offset) {
        req.checkQuery('offset', 'bad_offset').isInt()
      }
      if (req.query.limit) {
        req.checkQuery('limit', 'bad_limit').isInt()
      }
      if (req.query.type) {
        req.checkQuery('type', 'bad_type').isIn(['year', 'audit'])
      }
      if (req.query.year) {
        req.checkQuery('year', 'bad_year').isInt().len(4, 4)
      }
      const err = req.validationErrors()
      if (err) return res.callback(err)
      const {offset, limit, type, year} = req.query
      const total = await Model.toCount({lang, type, year})
      if (total === 0) {
        return res.callback({total: 0, list: []})
      }
      const list = await Model.list({offset, limit, lang, type, year}).then(list => list.map(o => o.get({plain: true})))
      res.data = {total, list}
      next()
    }), format({
      list: [{
        name: true,
        pdf: true
      }]
    }))
  router.route('/report/list')
    .get(wrap(async (req, res) => { // list
      if (req.query.offset) {
        req.checkQuery('offset', 'bad_offset').isInt()
      }
      if (req.query.limit) {
        req.checkQuery('limit', 'bad_limit').isInt()
      }
      if (req.query.type) {
        req.checkQuery('type', 'bad_type').isIn(['year', 'audit'])
      }
      if (req.query.year) {
        req.checkQuery('year', 'bad_year').isInt().len(4, 4)
      }
      if (req.query.string) {
        req.checkQuery('string', 'bad_string').len(1, 255)
      }
      const err = req.validationErrors()
      if (err) return res.callback(err)
      const {offset, limit, type, year, string} = req.query
      const total = await Model.toCount({type, year, string})
      if (total === 0) {
        return res.callback({total: 0, list: []})
      }
      const list = await Model.list({offset, limit, type, year, string})
      res.callback({total, list})
    }))
  router.route('/report/edit/:id')
    .get(wrap(async (req, res) => { // get
      const instance = await Model.get(req.params.id)
      if (!instance) return res.callback(Err.notfound())
      res.callback(instance)
    }))
  router.route('/report/:id')
    .delete(checkLoggedIn, wrap(async (req, res) => { // delete
      const instance = await Model.findById(req.params.id)
      if (!instance) return res.callback(Err.notfound())
      await instance.destroy()
      res.callback()
    }))
    .get(wrap(async (req, res, next) => { // get
      const instance = await Model.get(req.params.id).then(o => o.get({plain: true}))
      if (!instance) return res.callback(Err.notfound())
      res.data = instance
      next()
    }), format({
      name: true,
      pdf: true
    }))
    .put(checkLoggedIn, wrap(async (req, res) => { // update
      req.checkBody('mn', 'bad_mn').isBoolean()
      req.checkBody('en', 'bad_en').isBoolean()
      req.checkBody('type', 'bad_type').isIn(['year', 'audit'])
      req.checkBody('year', 'bad_year').len(4, 4)
      let err = req.validationErrors()
      if (err) return res.callback(err)

      let {mn, en, type, year} = req.body
      if (!parseInt(year)) return res.callback(Err.badRequest('bad year integer'))
      mn = (mn === true || mn === 'true')
      en = (en === true || en === 'true')
      if (mn) {
        req.checkBody('name', 'bad_name').len(1, 255)
        req.checkBody('pdf', 'bad_pdf').len(1, 255)
      }
      if (en) {
        req.checkBody('name_en', 'bad_name_en').len(1, 255)
        req.checkBody('pdf_en', 'bad_pdf_en').len(1, 255)
      }
      err = req.validationErrors()
      if (err) return res.callback(err)
      const {name, name_en, pdf, pdf_en} = req.body
      let data = {mn, en, type, year}
      if (mn) {
        data = {
          ...data,
          name,
          pdf
        }
      }
      if (en) {
        data = {
          ...data,
          name_en,
          pdf_en
        }
      }
      const instance = await Model.get(req.params.id)
      if (!instance) return res.callback(Err.notfound())
      const updatedInstance = await instance.updateAttributes(data)
      res.callback(updatedInstance)
    }))
}
