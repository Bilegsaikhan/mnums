'use strict'
const tableName = 'product_image'
module.exports = function (sequelize, DataTypes) {
  return sequelize.define(tableName, {
    url: DataTypes.STRING,
    is_main: DataTypes.BOOLEAN,
    product_id: DataTypes.INTEGER,
  })
}

