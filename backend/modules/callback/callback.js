/**
 * Created by enxtur on 2/15/17.
 */
'use strict'
import Err from '../err'
import { Log } from '../../model'

const debug = require('../../modules/debug')(__dirname)
export default function (req, res, next) {
  res.callback = (...params) => {
    setTimeout(() => {
      try {
        Log.add({
          user_id: req.user ? req.user.id : null,
          method: req.method,
          path: req.originalUrl,
          data: JSON.stringify({
            req: req.body,
            res: params
          })
        }).then((added) => {
          if (added) {
            debug('log added')
          }
        })
      } catch (e) {
        debug('error occurred on logging')
        debug(e)
      }
    }, 100)

    if (!params.length) {
      return res.json({
        status: 200
      })
    }
    if (params.length === 1 && (params[0].then || params[0].catch)) {
      let promise = params[0]
      promise.then((data) => {
        res.json(data)
      }).catch((err) => {
        throw err
      })
      return
    }
    let param = params[0]
    if ((Array.isArray(param) && param[0] instanceof Err) || param instanceof Err) {
      let err = Array.isArray(param) ? param[0] : param
      return res.status(err.status).json(err)
    }
    if (param instanceof Error) {
      let error = param
      return next(error)
    }

    if (param) {
      let data = param
      return res.json(data)
    }
    if (!param && params[1]) {
      let data = params[1]
      return res.json(data)
    }
  }
  next()
}
