'use strict'
const tableName = 'search'
module.exports = function (sequelize, DataTypes) {
  return sequelize.define(tableName, {
    type: DataTypes.ENUM('article', 'page', 'project'),
    title: DataTypes.STRING,
    title_en: DataTypes.STRING,
    content: DataTypes.STRING,
    content_en: DataTypes.STRING
  }, {
    classMethods: {
      async toCount ({string, lang = 'mn'}) {
        const query = createSearchQuery({string, is_count: true, lang})
        const res = await sequelize.query(query)
        return res[0][0].count
      },
      async search ({string, offset = 0, limit = 10, lang = 'mn'}) {
        const query = createSearchQuery({string, is_count: false, offset: offset * limit, limit, lang})
        return await sequelize.query(query, {model: this})
      }
    }
  })
}
function createSearchQuery ({string, is_count: isCount, offset = 0, limit = 10, lang = 'mn'}) {
  const fieldString = isCount ? 'count(*) as count' : '*'
  const whereString = `'%${string}%'`
  const orderString1 = `'${string} %'`
  const orderString2 = `'${string}%'`
  const orderString3 = `'% ${string}%'`
  const titleField = lang === 'mn' ? 'title' : 'title_en'
  const contentField = lang === 'mn' ? 'content' : 'content_en'
  const limitString = isCount ? '' : `LIMIT ${offset}, ${limit}`
  return `
    select ${fieldString} from search where 
      ${titleField} like ${whereString} OR
      ${contentField} like ${whereString}
      
      order by case 
      when ${titleField} like ${orderString1} then 0
      when ${titleField} like ${orderString2} then 1
      when ${titleField} like ${orderString3} then 2
      
      when ${contentField} like ${orderString1} then 3
      when ${contentField} like ${orderString2} then 4
      when ${contentField} like ${orderString3} then 5
      
      else 6
      end, title
      
      ${limitString};
    `
}
