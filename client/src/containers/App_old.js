import React, { Component } from 'react';
import './App.scss';

import { Header, Footer } from '../companents';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <img
          src="https://ld-wp.template-help.com/elementor_templates/healthen/wp-content/uploads/2018/06/home14-img1.jpg"
          alt=""
          width="100%"
        />
        <Footer />
        <div className="page-preloader-cover" hidden>
          <div className="page-preloader" />
        </div>
      </div>
    );
  }
}

export default App;
