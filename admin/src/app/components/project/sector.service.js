export class SectorService {
  constructor (server) {
    'ngInject';
    this.server = server
  }

  list () {
    return this.server.get('/project_branch')
  }

  update ({id, name, name_en}) {
    return this.server.put(`/project_branch/${id}`, {name, name_en})
  }

  create ({name, name_en}) {
    return this.server.post('/project_branch', {name, name_en})
  }

  delete (id) {
    return this.server.delete(`/project_branch/${id}`)
  }

  order ({ids}) {
    return this.server.put('/project_branch', {ids})
  }
}
