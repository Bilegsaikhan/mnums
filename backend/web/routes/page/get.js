'use strict'
import { Page } from '../../../model'
import Err from '../../../modules/err'

export default async function getPage (req, res) {
  req.checkParams('id', 'bad_id').isInt()
  const err = req.validationErrors()
  if (err) return res.callback(err)
  const {id} = req.params
  let page = await Page.findById(id)
  if (!page) return res.callback(Err.notfound())
  res.callback(page)
}
