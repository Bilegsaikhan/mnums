import {
    LOAD_PROJECT_LIST, LOAD_PROJECT_LIST_SUCCESS, LOAD_PROJECT_LIST_ERROR,
    LOAD_PROJECT_ITEM, LOAD_PROJECT_ITEM_SUCCESS, LOAD_PROJECT_ITEM_ERROR
} from "../constants";

export function clearFilter() {
  return {
    type: "CLEAR_FILTER"
  };
}

export function loadProjectFilter(query) {
  return {
    type: "LOAD_PROJECT_FILTER",
    payload: query
  };
}


export function loadProject(params) {
    return async(dispatch, getState, {fetch}) => {
        // eslint-disable-next-line no-param-reassign
        const payload = {};

        dispatch({
            type: LOAD_PROJECT_ITEM,
            payload
        });

        try {
            let response = await fetch(`project/${params.id}`, {
                method: "GET",
                credentials: 'include'
            });
            let data = await response.json();
            if (!response.ok) {
                const message = data.message || data;
                if (data) throw message;
            }

            dispatch({
                type: LOAD_PROJECT_ITEM_SUCCESS,
                payload: data
            });

        } catch (error) {
            dispatch({
                type: LOAD_PROJECT_ITEM_ERROR,
                error: error,
            });
            return false;
        }
        return true;
    };
}

export function loadProjectsList({params = {}}) {
    return async(dispatch, getState, {fetch}) => {
        // eslint-disable-next-line no-param-reassign
        const payload = {};

        dispatch({
            type: LOAD_PROJECT_LIST,
            payload
        });

        try {
            let response = await fetch(`project${params}`, {
                method: "GET",
                credentials: 'include'
            });
            let data = await response.json();
            if (!response.ok) {
                const message = data.message || data;
                if (data) throw message;
            }

            dispatch({
                type: LOAD_PROJECT_LIST_SUCCESS,
                payload: data
            });

        } catch (error) {
            dispatch({
                type: LOAD_PROJECT_LIST_ERROR,
                error: error,
            });
            return false;
        }
        return true;
    };
}