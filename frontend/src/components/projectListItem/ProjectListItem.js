import React, { Component } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router";
import "./ProjectListItem.css";
import TextTruncate from "react-text-truncate";
import moment from 'moment'

class ListItem extends Component {
  static propTypes = {
    items: PropTypes.array
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { items } = this.props;

    return (
      <section>
        <div className="c14v1-body c14v1-body-text flipboard-keep ">
          <div className="n02v5-body">
            <ul className="clearfix">
              {items.map((item, index) => {
                return (
                  <li className="media" key={`limc_${index}`}>
                    <Link to={`/project/${item.id}`} className="n02v5-img">
                      <img
                        className="media-object"
                        title="news"
                        alt="news"
                        src={item.image ? item.image : 'http://ubinfo-s3.s3.amazonaws.com/static/PROJECT-PLACEHOLDER-METROBC.jpg'}
                      />
                    </Link>
                    <div className="n02v5-text media-body">
                      <div className="hammer">{moment(item.updated_at).format("YYYY.MM.DD")}</div>
                      <h5>
                        <Link to={`/project/${item.id}`}>
                          <TextTruncate
                            line={2}
                            truncateText="…"
                            text={item.name}
                          />
                        </Link>
                      </h5>
                    </div>
                  </li>
                );
              })}
            </ul>
          </div>
        </div>
      </section>
    );
  }
}

export default ListItem;
