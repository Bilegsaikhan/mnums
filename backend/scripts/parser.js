let request = require('request');
let cheerio = require('cheerio');
let fall = require('async-waterfall')
let fs = require('fs')

const pageString = 'page/'
const baseUrl = 'http://www.uyanbest.mn/product/'
let urls = [];

const init = () => {

  let tasks = [];

  for (let i = 160; i <= 182; i++ ) {
    let task = (callback) => {
      console.log('function start', i)
      request(`${baseUrl}${pageString}${i}`, (err, response, body) => {
        console.log('response', i)
        if (err) {
          return callback(err);
        }

        try {
          let $ = cheerio.load(body);
          let a = 0;
          let bla = {
            id: i,
            links: []
          }
          $('ul.products.se li').each((index, element) => {
            let link = $(element).find('.product-meta-wrapper>a');
            bla.links.push($(link).attr('href'));
          })
          fs.readFile('aa.json', (err, data) => {
            if (err) {
              throw err;
            }
            let tempData = JSON.parse(data);
            tempData.push(bla)
            fs.writeFile('aa.json', JSON.stringify(tempData, null, 2), (err) => {
              if (err) {
                throw err;
              }
              console.log('done', i)
              return callback();
            })
          })
          console.log('bla bla')
        } catch (error) {
          return callback(error)
        }
      })
    }
    tasks.push(task);
  }

  fall(tasks, (error) => {
    console.log('done');
    if (error) {
      console.log('error occured', error);
    }
  })

}

init();
