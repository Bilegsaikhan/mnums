/**
 * Created by enxtur on 5/22/17.
 */
'use strict'
require('babel-core/register')
require('babel-polyfill')
const debug = require('../modules/debug')(__dirname)
debug(process.argv[2].split('=')[1])
const util = process.argv[2].split('=')[1]
require(`./${util}`)()
