/**
 * Created by enxtur on 5/25/17.
 */
export class LoginController {
  constructor($state, userService, toastr) {
    'ngInject';
    this.awesomeThings = [];
    this.toastr = toastr
    this.$state = $state
    this.userService = userService
    this.user = {}
  }

  login() {
    return this.userService.login(this.user).then(() => {
      this.toastr.success('Таны хүсэлт амжилттай!');
      this.$state.go('root.content.home');
    }, () => {
      this.toastr.error('Хэрэглэгчийн нэр эсвэл нууц үг буруу байна!');
    })
  }
}
