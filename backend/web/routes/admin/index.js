'use strict'
import create from './create'
import list from './list'
import get from './get'
import del from './delete'
import password from './password'
export default ({router, passport, checkLoggedIn, multipartMiddleware, wrap, checkRootAuth}) => {
  router.route('/admin')
    .post(checkRootAuth, wrap(create))
    .get(wrap(list))
  router.route('/admin/:id')
    .delete(checkRootAuth, wrap(del))
    .get(wrap(get))
    .put(checkRootAuth, wrap(password))
}
