'use strict'
import { Law as Model } from '../../../model'
import Err from '../../../modules/err'
// import { pick } from 'underscore'
import { format } from '../../../modules/formatter'

export default ({router, passport, checkLoggedIn, multipartMiddleware, wrap, checkRootAuth}) => {
  router.route('/law')
    .post(checkLoggedIn, wrap(async (req, res) => { // create
      req.checkBody('mn', 'bad_mn').isBoolean()
      req.checkBody('en', 'bad_en').isBoolean()
      req.checkBody('type', 'bad_type').isIn(['law', 'resolution'])
      let err = req.validationErrors()
      if (err) return res.callback(err)

      let {mn, en, type} = req.body
      mn = (mn === true || mn === 'true')
      en = (en === true || en === 'true')
      if (mn) {
        req.checkBody('name', 'bad_name').len(1, 255)
        req.checkBody('pdf', 'bad_pdf').len(1, 255)
      }
      if (en) {
        req.checkBody('name_en', 'bad_name_en').len(1, 255)
        req.checkBody('pdf_en', 'bad_pdf_en').len(1, 255)
      }
      err = req.validationErrors()
      if (err) return res.callback(err)
      const {name, name_en, pdf, pdf_en} = req.body
      let data = {mn, en, type}
      if (mn) {
        data = {
          ...data,
          name,
          pdf
        }
      }
      if (en) {
        data = {
          ...data,
          name_en,
          pdf_en
        }
      }
      const instance = await Model.create(data)
      res.callback(instance)
    }))
    .get(wrap(async (req, res, next) => { // all
      const lang = req.session.lang || 'mn'
      if (req.query.offset) {
        req.checkQuery('offset', 'bad_offset').isInt()
      }
      if (req.query.limit) {
        req.checkQuery('limit', 'bad_limit').isInt()
      }
      if (req.query.type) {
        req.checkQuery('type', 'bad_type').isIn(['law', 'resolution'])
      }
      const err = req.validationErrors()
      if (err) return res.callback(err)
      const {offset, limit, type} = req.query
      const total = await Model.toCount({lang, type})
      if (total === 0) {
        return res.callback({total: 0, list: []})
      }
      const list = await Model.list({offset, limit, lang, type}).then(list => list.map(o => o.get({plain: true})))
      res.data = {total, list}
      next()
    }), format({
      list: [{
        name: true,
        pdf: true
      }]
    }))
  router.route('/law/list')
    .get(wrap(async (req, res) => { // list
      if (req.query.offset) {
        req.checkQuery('offset', 'bad_offset').isInt()
      }
      if (req.query.limit) {
        req.checkQuery('limit', 'bad_limit').isInt()
      }
      if (req.query.type) {
        req.checkQuery('type', 'bad_type').isIn(['law', 'resolution'])
      }
      if (req.query.string) {
        req.checkQuery('string', 'bad_string').len(1, 255)
      }
      const err = req.validationErrors()
      if (err) return res.callback(err)
      const {offset, limit, type, string} = req.query
      const total = await Model.toCount({type, string})
      if (total === 0) {
        return res.callback({total: 0, list: []})
      }
      const list = await Model.list({offset, limit, type, string})
      res.callback({total, list})
    }))
  router.route('/law/edit/:id')
    .get(wrap(async (req, res) => { // get
      const instance = await Model.get(req.params.id)
      if (!instance) return res.callback(Err.notfound())
      res.callback(instance)
    }))
  router.route('/law/:id')
    .delete(checkLoggedIn, wrap(async (req, res) => { // delete
      const instance = await Model.findById(req.params.id)
      if (!instance) return res.callback(Err.notfound())
      await instance.destroy()
      res.callback()
    }))
    .get(wrap(async (req, res, next) => { // get
      const instance = await Model.get(req.params.id).then(o => o.get({plain: true}))
      if (!instance) return res.callback(Err.notfound())
      res.data = instance
      next()
    }), format({
      name: true,
      pdf: true
    }))
    .put(checkLoggedIn, wrap(async (req, res) => { // update
      req.checkBody('mn', 'bad_mn').isBoolean()
      req.checkBody('en', 'bad_en').isBoolean()
      req.checkBody('type', 'bad_type').isIn(['law', 'resolution'])
      let err = req.validationErrors()
      if (err) return res.callback(err)

      let {mn, en, type} = req.body
      mn = (mn === true || mn === 'true')
      en = (en === true || en === 'true')
      if (mn) {
        req.checkBody('name', 'bad_name').len(1, 255)
        req.checkBody('pdf', 'bad_pdf').len(1, 255)
      }
      if (en) {
        req.checkBody('name_en', 'bad_name_en').len(1, 255)
        req.checkBody('pdf_en', 'bad_pdf_en').len(1, 255)
      }
      err = req.validationErrors()
      if (err) return res.callback(err)
      const {name, name_en, pdf, pdf_en} = req.body
      let data = {mn, en, type}
      if (mn) {
        data = {
          ...data,
          name,
          pdf
        }
      }
      if (en) {
        data = {
          ...data,
          name_en,
          pdf_en
        }
      }
      const instance = await Model.get(req.params.id)
      if (!instance) return res.callback(Err.notfound())
      const updatedInstance = await instance.updateAttributes(data)
      res.callback(updatedInstance)
    }))
}
