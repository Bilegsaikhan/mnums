export class ServerService {
  constructor ($log, $http) {
    'ngInject'

    this.$error = $log.error
    this.$log = $log.log
    this.$http = $http
    this.apiHost = 'http://localhost:5000/api'
    this.uploadApiHost = 'http://mhi.mn/api'
  }

  get (path) {
    return this.send('GET', path)
  }

  post (path, data) {
    return this.send('POST', path, data)
  }

  put (path, data) {
    return this.send('PUT', path, data)
  }

  delete (path) {
    return this.send('DELETE', path)
  }

  send (method, path, data) {
    const url = `${this.apiHost}${path}`
    if (data) {
      for (let key in data) {
        if (data.hasOwnProperty(key) && angular.isUndefined(data[key])) {
          data[key] = ''
        }
      }
    }
    return this.$http({method, url, data}).then(res => {
      return res.data
    }).catch(err => {
      this.$error(err)
      throw err
    })
  }

  uploadPdf (file) {
    const url = this.uploadApiHost + '/upload/pdf'
    const fd = new FormData()
    fd.append('upload', file)
    return this.$http.post(url, fd, {
      transformRequest: angular.identity,
      headers: {'Content-Type': undefined}
    }).then(res => {
      return res.data
    }).catch(err => {
      this.$error(err)
      throw err
    })
  }

  uploadImage (file) {
    const url = this.uploadApiHost + '/upload/image'
    const fd = new FormData()
    fd.append('upload', file)
    return this.$http.post(url, fd, {
      transformRequest: angular.identity,
      headers: {'Content-Type': undefined}
    }).then(res => {
      return res.data
    }).catch(err => {
      this.$error(err)
      throw err
    })
  }
}
