'use strict'
import { Store as Model } from '../../../model'
import Err from '../../../modules/err'
import { format } from '../../../modules/formatter'
import { pick } from 'underscore'

const debug = require('../../../modules/debug')(__dirname)
export default ({router, passport, checkLoggedIn, multipartMiddleware, wrap, checkRootAuth}) => {
  router.route('/store')
    .get(wrap(async (req, res, next) => {
      if (!Array.isArray(req.query.key)) return res.callback(Err.badRequest('bad key'))
      debug(req.query)
      const {key} = req.query
      const list = await Model.findByKeys(key).then(list => list.map(o => o.get({plain: true})))
      if (!list) return res.callback(Err.notfound())
      res.data = list
      next()
    }), format([{
      data: true
    }]))
  router.route('/store/edit/:key')
    .get(wrap(async (req, res) => {
      const instance = await Model.findByKey(req.params.key)
      if (!instance) return res.callback(Err.notfound())
      res.callback(instance)
    }))
  router.route('/store/:key')
    .get(wrap(async (req, res, next) => {
      const instance = await Model.findByKey(req.params.key).then(o => o.get({plain: true}))
      if (!instance) return res.callback(Err.notfound())
      res.data = instance
      next()
    }), format({
      data: true
    }))
    .put(checkLoggedIn, wrap(async (req, res) => {
      const instance = await Model.findByKey(req.params.key)
      if (!instance) return res.callback(Err.notfound())
      const data = pick(req.body, 'mn', 'en', 'data', 'data_en')
      data.mn = (data.mn === true || data.mn === 'true')
      data.en = (data.en === true || data.en === 'true')
      const updatedInstance = await instance.updateAttributes(data)
      res.callback(updatedInstance)
    }))
}
