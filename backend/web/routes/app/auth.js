'use strict'

export function login ({passport, wrap}) {
  return (req, res, next) => {
    passport.authenticate('local', async (err, admin) => {
      if (err) return res.callback(err)
      if (!admin) return res.callback(res.err.unauthorized('Нэр эсвэл нууц үг буруу!'))
      await signin(req, admin)
      res.callback(admin)
    })(req, res, next)
  }
}
export function logout (req, res) {
  req.logout()
  res.callback()
}
export function profile (req, res) {
  res.callback({user: req.user ? req.user : null})

}
function signin (req, user) {
  return new Promise((resolve, reject) => {
    req.login(user, err => {
      if (err) return reject(err)
      resolve()
    })
  })
}
