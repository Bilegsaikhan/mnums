/**
 * Created by enxtur on 1/31/17.
 */
import completer from 'prefix-completer'
import _ from 'underscore'

const config = require('../../config')
const indexer = completer.create({
  key: 'ubtrade:completion',
  host: config.REDIS_HOST
})
const limit = 10

function add (string) {
  return new Promise((resolve, reject) => {
    let strings = convertString(string)
    indexer.addList(strings, (err) => {
      if (err) return reject(err)
      resolve()
    })
  })
}

function convertString (string) {
  let res = []
  let words = string.toLowerCase().split(' ')
  let temp = ''
  for (let i = 0, length = words.length; i < length; i++) {
    for (let j = i; j < length; j++) {
      temp += ' ' + words[j]
      res.push(temp)
    }

    temp = ''
  }
  return res
}

function complete (string) {
  // indexer.leaves(console.log)
  return new Promise((resolve, reject) => {
    indexer.complete(string, limit, (err, input, output) => {
      console.log(input)
      console.log(output)
      if (err) return reject(err)
      resolve(_.sortBy(output, function (str) {
        return str.length
      }))
    })
  })
}
export { add, complete }
