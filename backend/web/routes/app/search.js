'use strict'
import { Search, Page } from '../../../model'
// import Err from '../../../modules/err'
import { format } from '../../../modules/formatter'

const debug = require('../../../modules/debug')(__dirname)

export default [async function search (req, res, next) {
  req.checkQuery('string', 'bad_string').len(1, 255)
  if (req.query.offset) {
    req.checkQuery('offset', 'bad_offset').isInt()
  }
  if (req.query.limit) {
    req.checkQuery('limit', 'bad_limit').isInt()
  }
  const err = req.validationErrors()
  if (err) return res.callback(err)
  const {string, offset, limit} = req.query
  const lang = req.session.lang || 'mn'
  const total = await Search.toCount({string, lang})
  // debug(count)
  if (total === 0) {
    return res.callback({
      total: 0,
      list: []
    })
  }
  const list = await Search.search({string, offset, limit, lang}).then(list => list.map(o => o.get({plain: true})))
  var pageIndex = {}
  const pageIds = list.filter(o => o.type === 'page').map(o => o.id)
  if (pageIds.length > 0) {
    const pages = await Page.findByIdsWIthMenu(pageIds)
    pages.forEach(page => (pageIndex[page.id] = page))
  }
  list.forEach((search) => {
    if (search.type === 'article') {
      search.link = `/news/${search.id}`
    } else if (search.type === 'project') {
      search.link = `/project/${search.id}`
    } else if (search.type === 'page') {
      if (pageIndex[search.id]) {
        const page = pageIndex[search.id]
        let menus = []
        if (page.menu) {
          menus.unshift(page.menu.module)
          if (page.menu.parent) {
            menus.unshift(page.menu.parent.module)
            if (page.menu.parent.parent) {
              menus.unshift(page.menu.parent.parent.module)
            }
          }
        }
        if (menus.length > 0) {
          const path = menus.join('/')
          search.link = `/${path}`
        }
      }
    }
  })
  res.data = {total, list}
  next()
}, format({
  list: [{
    title: true,
    content: true
  }]
})]
