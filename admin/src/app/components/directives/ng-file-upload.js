'use strict';

angular.module('furnitureAdminApp').directive(

  'ngFileUpload',

  ['$timeout', 'HelperService', 'UserService', '$upload', 'Configure', 'ModalService',

    function($timeout, helperService, userService, $upload, config, modalService) {
      return {
        restrict: 'EA',
        scope: {
          files: '=ngModel',
          data: '=data',
          loading: '='
        },
        require: 'ngModel',
        templateUrl: 'views/partials/ng-file-upload.html',
        link: function(scope, element, attrs) {
          scope.helpers = helperService;
          scope.user_data = userService.userData;

          $('#fileDraggableElement').bind({
            dragover: function(event) {
              if (event != null)
                event.preventDefault();

              $(this).addClass('draggable');
              event.originalEvent.dataTransfer.effectAllowed = 'copy';

              return false;
            },
            dragenter: function(event) {
              if (event != null)
                event.preventDefault();

              $(this).removeClass('draggable');
              event.originalEvent.dataTransfer.effectAllowed = 'copy';

              return false;
            },
            dragleave: function(event) {
              if (event != null)
                event.preventDefault();

              $(this).removeClass('draggable');
              event.originalEvent.dataTransfer.effectAllowed = 'copy';

              return false;
            },
            drop: function(event) {
              if (event != null)
                event.preventDefault();

              $(this).removeClass('draggable');
              scope.handleFile(event.originalEvent.dataTransfer.files);

              return false;
            }
          });

          scope.removeFile = function(item, index) {
            // modalService.danger('Та уг зургийг устгахдаа итгэлтэй байна уу!', function() {
              scope.files.splice(index, 1);
              if (item.is_main && scope.files.length > 0) {
                scope.makeMain(scope.files[0], 0);
              }
            // }, true)
          }

          scope.makeMain = function (item, index) {
            removeAllMain();
            item.is_main = true;
          }

          scope.selectFile = function() {
            $timeout(function() {
              element.find('#fileSelectElement').click();
            });
          }

          var validMimeTypes = attrs.acceptFileType;
          var validFileSize = attrs.maxFileSize;

          scope.handleFile = function(files) {
            if (scope.files && scope.files.length >= 10) return;
            var filesLength = scope.files ? scope.files.length : 0;

            // scope.$apply(function() {
            //   helperService.toaster('loading', true);
            // });

            angular.forEach(files, function(file) {
              filesLength++;

              if (filesLength <= 10) {
                var name, reader, size, type;
                reader = new FileReader();

                reader.onload = function(event) {
                  name = file.name;
                  type = file.type;
                  size = file.size;

                  if (checkSizeFn(validFileSize, size) && isTypeValidFn(validMimeTypes, type)) {
                    scope.$apply(function() {

                      var data = {
                        type: type,
                        hidden: false,
                        size: size,
                        result: helperService.isImage(type) ? event.target.result : null,
                        progress: 0,
                        status: null,
                        loading: true,
                        is_main: false
                      }

                      if (scope.files.length == 0) {
                        data.is_main = true;
                      }

                      scope.files.push(data);
                      scope.loading = true;

                      // helperService.toaster('loading', false);

                      var tempLink = config._APP_HOST + ':' + config._APP_PORT + '/rest/blog/image_upload';

                      if (attrs.link)
                        tempLink = helperService.base_url + attrs.link;

                      $upload.upload({
                        url: tempLink,
                        file: file
                      }).progress(function(event) {
                        var progressPercentage = parseInt(100.0 * event.loaded / event.total);
                        data.progress = progressPercentage >= 100 ? null : progressPercentage;
                      }).error(function(response, status, headers, config) {
                        data.loading = false;
                        data.status = 'error';

                        data.id = null;
                        scope.loading = checkAllloadingFn();
                      }).success(function(response, status, headers, config) {
                        data.loading = false;
                        data.status = 'success';
                        data.link = response.data.link;
                        delete data.result;
                        $timeout(function () {
                          data.hidden = true;
                        }, 3000);

                        scope.loading = checkAllloadingFn();
                      });
                    })
                  }
                }

                reader.readAsDataURL(file);
              }
            });
          }

          function checkAllloadingFn() {
            var loading = false;

            angular.forEach(scope.files, function(item) {
              if (item.loading == true) {
                loading = true;
              }
            })

            return loading;
          }

          function removeAllMain() {
            angular.forEach(scope.files, function (item) {
              item.is_main = false;
            })
          }

          function checkSizeFn(maxFileSize, size) {
            var _ref;
            if (((_ref = maxFileSize) === (void 0) || _ref === '') || (size / 1024) / 1024 < maxFileSize) {
              return true;
            } else {
              scope.bad_image = true;
              helperService.toaster('error', 'Файлын хэмжээ хамгийн ихдээ [' + maxFileSize + 'MB].');
              console.log('checkSizeFn:', maxFileSize)
              return false;
            }
          }

          function isTypeValidFn(validMimeTypes, type) {
            if (validMimeTypes === '****' && _.indexOf([].concat(helperService.mimes.gif, helperService.mimes.jpeg, helperService.mimes.jpg, helperService.mimes.jpe, helperService.mimes.png, helperService.mimes.mp4, helperService.mimes.mp3, helperService.mimes.docx, helperService.mimes.xlsx, helperService.mimes.pptx, helperService.mimes.xls, helperService.mimes.ppt, helperService.mimes.doc, helperService.mimes.pdf, helperService.mimes.zip, helperService.mimes.rar), type) != -1) {
              return true;
            }

            if ((validMimeTypes === (void 0) || validMimeTypes === '') || type && validMimeTypes.indexOf(type) > -1) {
              return true;
            } else {
              scope.bad_image = true;
              helperService.toaster('error', 'Файлын төрөл зөвхөн ' + validMimeTypes + '.');
              console.log('isTypeValidFn:', validMimeTypes);
              return false;
            }
          }
        }
      }
    }
  ]);
