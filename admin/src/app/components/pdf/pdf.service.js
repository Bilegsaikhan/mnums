export class PdfService {
  constructor (server) {
    'ngInject';
    this.server = server
  }

  list (obj) {
    let str = Object.keys(obj).map(function(key){
      return encodeURIComponent(key) + '=' + encodeURIComponent(obj[key]);
    }).join('&');
    return this.server.get(`/pdf?${str}`)
  }

  update ({id, name, url, thumb}) {
    return this.server.put(`/pdf/${id}`, {name, url, thumb})
  }

  create ({name, url, thumb}) {
    return this.server.post('/pdf', {name, url, thumb})
  }

  delete (id) {
    return this.server.delete(`/pdf/${id}`)
  }
}
