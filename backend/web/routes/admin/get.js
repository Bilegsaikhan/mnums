/**
 * Created by enxtur on 5/5/17.
 */

import { Admin } from '../../../model'
import Err from '../../../modules/err'
export default async (req, res) => {
  const {id} = req.params
  let admin = await Admin.get(id)
  if (!admin) return res.callback(Err.notfound())
  res.callback(admin)
}
