/**
 * Created by enxtur on 5/5/17.
 */

import { Menu } from '../../../model'
import Err from '../../../modules/err'
export default async (req, res) => {
  req.checkBody('mn', 'bad_mn').isBoolean()
  req.checkBody('en', 'bad_en').isBoolean()
  let { mn, en } = req.body
  mn = (mn === true || mn === 'true')
  en = (en === true || en === 'true')
  if (mn === true || mn === 'true') {
    req.checkBody('name', 'bad_name').len(1, 255)
  }
  if (en === true || en === 'true') {
    req.checkBody('name_en', 'bad_name_en').len(1, 255)
  }
  req.checkBody('module', 'bad_module').len(1, 255)
  if (req.body.parent_id) {
    req.checkBody('parent_id', 'bad_parent_id').isInt()
  }
  if (req.body.page_id) {
    req.checkBody('page_id', 'bad_page_id').isInt()
  }
  const err = req.validationErrors()
  if (err) return res.callback(err)
  const { name, name_en: nameEn, parent_id: parentId, module, page_id: pageId } = req.body

  let createData = {
    mn,
    en,
    module,
    name: mn ? name : null,
    name_en: en ? nameEn : null
  }

  if (parentId) {
    createData = {
      ...createData,
      parent_id: parentId,
      is_main: false
    }
  } else {
    createData = {
      ...createData,
      parent_id: null,
      is_main: true
    }
  }
  if (pageId) {
    createData = {
      ...createData,
      type: 'page',
      page_id: pageId
    }
  }

  try {
    const menu = await Menu.create(createData)
    res.callback(menu)
  } catch (err) {
    res.callback(Err.sequelize(err))
  }
}
