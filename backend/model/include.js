'use strict'
/**
 * Created with IntelliJ IDEA.
 * User: enxtur
 * Date: 4/1/14
 * Time: 3:31 PM
 * To change this template use File | Settings | File Templates.
 */

const debug = require('../modules/debug')(__filename)

export default function (database) {

  const {
    article: articles,
    article_cat: cat,
    gallery_cat,
    gallery_item,
    menu,
    menu: menus,
    menu: parent,
    product_cat,
    product_sub_cat,
    page,
    pdf,
    pdf: pdf_en,
    admin
  } = database
  const models = {
    articles,
    cat,
    gallery_cat,
    gallery_item,
    menu,
    menus,
    parent,
    product_cat,
    product_sub_cat,    
    page,
    pdf,
    pdf_en,
    admin
  }
  return function (rels) {
    // return doInclude(rels, [])
    return (function doInclude (rels, includes) {
      for (let as in rels) {
        const model = models[as]
        if (model) {
          const include = {
            model,
            as,
            include: []
          }
          includes.push(include)
          if (typeof rels[as] === 'object') doInclude(rels[as], include.include)
        } else {
          debug('#####################################################')
          debug(`####### model not found by relation key: ${as} #######`)
          debug('#####################################################')
        }
      }
      return includes
    })(rels, [])
  }
}
