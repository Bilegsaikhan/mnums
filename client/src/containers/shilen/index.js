import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Loading } from '../../companents';

export class Shilen extends Component {
  static propTypes = {};

  render() {
    return (
      <div>
        <Loading />
        <iframe
          title="АНАГААХ УХААНЫ ХҮРЭЭЛЭН"
          width="100%"
          height="800"
          frameBorder="0"
          allowFullScreen=""
          src="https://shilendans.gov.mn/embed/1117"
          style={{ border: 0, position: 'relative' }}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Shilen);
