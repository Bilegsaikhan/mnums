export class RootController {
  constructor ($log, $state, userService) {
    'ngInject';
    this.awesomeThings = [];
    this.auth = userService
    this.$log = $log.log
    this.$state = $state
    this.ckeditorOptions = ckeditorOptions
    this.modules = modules
    this.admin = {
      header: 'bg-white',
      logo: 'bg-info-alt',
      sidebar: 'bg-dark'
    }
  }

  gotoState (newState) {
    this.$log(newState)
    this.$state.go(newState)
  }
}
const modules = [{
  name: 'Хуудас',
  module: 'page'
}, {
  name: 'Мэдээ',
  module: 'news'
}, {
  name: 'Тайлан',
  module: 'investors'
}, {
  name: 'Холбоо барих',
  module: 'contact'
}]

const ckeditorOptions = {
  language: 'en',
  allowedContent: true,
  entities: false,
  extraPlugins: 'uploadimage',
  uploadUrl: 'http://www.dbm.mn/api/upload/image'
  // uploadUrl: 'http://localhost:5000/api/upload/image'
}
