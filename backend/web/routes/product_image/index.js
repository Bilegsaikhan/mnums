'use strict'
// const debug = require('../../../modules/debug')(__filename)
import create from './create'
import get from './get'
import update from './update'
import del from './delete'
import list from './list'

export default ({router, passport, checkLoggedIn, multipartMiddleware, wrap, checkRootAuth}) => {
  router.route('/product_image')
    .get(wrap(list))
    .post(checkLoggedIn, wrap(create))
  router.route('/product_image/:id')
    .get(wrap(get))
    .put(checkLoggedIn, wrap(update))
    .delete(checkLoggedIn, wrap(del))
}
