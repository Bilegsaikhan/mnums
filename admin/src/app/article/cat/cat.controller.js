export class ArticleCatController {
  constructor ($log, catService, modalService) {
    'ngInject'
    this.log = $log.log
    this.catService = catService
    this.modalService = modalService
    this.count = 0
    this.reload()
  }

  reload () {
    this.catService.cats().then(cats => {
      this.clear()
      this.cats = cats
      this.count = cats.length
    })

  }

  clear () {
    this.cat = {
      id: '',
      name: '',
      name_en: ''
    }
  }

  edit (cat) {
    this.cat = Object.assign({}, cat)
  }

  canSubmit () {
    return this.catForm.$invalid
  }

  update () {
    return this.catService.update(this.cat)
  }

  remove () {
    this.modalService.danger('Та уг ангилалыг устгахдаа итгэлтэй байна уу', true).then(() => {
      this.catService.delete(this.cat.id).then(() => {
        this.reload()
      })
    })
  }

  create () {
    return this.catService.create(this.cat)
  }

  submit () {
    (this.cat.id ? this.update() : this.create()).then(() => {
      this.reload()
    })
  }

  dragEnd () {
    const ids = this.cats.map(cat => cat.id)
    return this.catService.order({ids})
  }
}
