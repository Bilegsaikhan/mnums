'use strict'
const tableName = 'store'
module.exports = function (sequelize, DataTypes) {
  return sequelize.define(tableName, {
    key: DataTypes.STRING,
    mn: DataTypes.BOOLEAN,
    en: DataTypes.BOOLEAN,
    data: DataTypes.STRING,
    data_en: DataTypes.STRING
  }, {
    timestamps: true,
    classMethods: {
      findByKey (key) {
        return this.find({
          where: {key}
        })
      },
      findByKeys (key) {
        return this.findAll({
          where: {key}
        })
      }
    }
  })
}
