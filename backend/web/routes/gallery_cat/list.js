'use strict'
import { GalleryCat, GalleryItem } from '../../../model'

export default async function getCats (req, res) {
  const cats = await GalleryCat.findAll({order: '`order` ASC', include: [{
    model: GalleryItem, as: 'images'
  }]})
  res.callback(cats)
}
