export class MilestoneController {
  constructor ($log, milestoneService, modalService, toastr) {
    'ngInject'
    this.log = $log.log
    this.toastr = toastr
    this.service = milestoneService
    this.modalService = modalService
    this.count = 0
    this.reload()
  }

  reload () {
    this.service.list().then(items => {
      this.clear()
      this.items = items
      this.count = items.length
    })

  }

  clear () {
    this.item = {
      id: null,
      date: "",
      date_en: "",
      content: "",
      content_en: ""
    }
  }

  edit (item) {
    this.item = Object.assign({}, item)
  }

  canSubmit () {
    return this.itemForm.$invalid
  }

  update () {
    return this.service.update(this.item).then(() => {
      this.toastr.success('Таны хүсэлт амжилттай!');
    }, () => {
      this.toastr.error('Таны хүсэлт амжилтгүй!');
    })
  }

  remove () {
    this.modalService.danger('Та уг ангилалыг устгахдаа итгэлтэй байна уу', true).then(() => {
      this.service.delete(this.item.id).then(() => {
        this.toastr.success('Таны хүсэлт амжилттай!');
        this.reload()
      }, () => {
        this.toastr.error('Таны хүсэлт амжилтгүй!');
      })
    })
  }

  create () {
    return this.service.create(this.item).then(() => {
      this.toastr.success('Таны хүсэлт амжилттай!');
    }, () => {
      this.toastr.error('Таны хүсэлт амжилтгүй!');
    })
  }

  submit () {
    (this.item.id ? this.update() : this.create()).then(() => {
      this.reload()
    })
  }

  dragEnd () {
    const ids = this.items.map(item => item.id)
    return this.service.order({ids})
  }
}
