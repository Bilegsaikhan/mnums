/**
 * Created by enxtur on 2/15/17.
 */
import request from 'supertest'
import express from 'express'
import callback from './'
import Err from '../err'
import { expect } from 'chai'

describe.skip('Callback', () => {
  const app = express()
  app.use(callback)
  app.use('/error', (req, res) => {
    res.callback(new Error('Dummy Error For Test Purpose'))
  })
  app.use('/err', (req, res) => {
    res.callback(Err.badRequest('Dummy Err'))
  })
  app.use('/data', (req, res) => {
    res.callback({
      message: 'hello'
    })
  })

  it('#It to responds internal server error (500)', (done) => {
    request(app)
      .get('/error')
      .expect(500, done)
  })
  it('#It to responds bad request err (400)', (done) => {
    request(app)
      .get('/err')
      .expect(400)
      .end((err, res) => {
        if (err) return done(err)
        let data = res.body
        expect(data.message).to.be.a('string')
        expect(data.status).to.be.a('number').and.equal(400)
        done()
      })
  })
  it('#It to responds json data (200)', (done) => {
    request(app)
      .get('/data')
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        let data = res.body
        expect(data).to.be.an('object')
        done()
      })
  })
})
