'use strict'
import { Article } from '../../../model'
import { format } from '../../../modules/formatter'
export default async (req, res, next) => {
    req.checkQuery('limit', 'bad_limit').optional().isInt()
    req.checkQuery('offset', 'bad_offset').optional().isInt()
    req.checkQuery('cat_id', 'bad_cat_id').optional().isInt()
    req.checkQuery('string', 'bad_string').optional().len(1, 255)
    const err = req.validationErrors()
    if (err) return res.callback(err)
    const {limit, offset, cat_id, string} = req.query
    const count = await Article.toCount({cat_id, string})
    if (count === 0) {
      return res.callback({
        total: 0,
        list: []
      })
    }
    const articles = await Article.all({
      offset,
      limit,
      cat_id,
      string
    }).then((list) => list.map(o => o.get({plain: true})))

    res.callback({
      total: count,
      list: articles
    })
  }
