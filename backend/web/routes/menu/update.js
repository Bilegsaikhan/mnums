/**
 * Created by enxtur on 5/5/17.
 */

import { Menu } from '../../../model'
import Err from '../../../modules/err'
export default async (req, res) => {
  req.checkBody('mn', 'bad_mn').isBoolean()
  req.checkBody('en', 'bad_en').isBoolean()

  let {mn, en} = req.body
  mn = (mn === true || mn === 'true')
  en = (en === true || en === 'true')
  if (mn === true || mn === 'true') {
    req.checkBody('name', 'bad_name').len(1, 255)
  }
  if (en === true || en === 'true') {
    req.checkBody('name_en', 'bad_name_en').len(1, 255)
  }

  if (req.body.module) {
    req.checkBody('module', 'bad_module').len(1, 255)
  }
  if (req.body.parent_id) {
    req.checkBody('parent_id', 'bad_parent_id').optional().isInt()
  }
  if (req.body.page_id) {
    req.checkBody('page_id', 'bad_page_id').optional().isInt()
  }
  const err = req.validationErrors()
  if (err) return res.callback(err)
  const {id} = req.params
  const {name, name_en: nameEn, module, parent_id: parentId, page_id: pageId} = req.body

  let menu = await Menu.findById(id)
  if (!menu) return res.callback(Err.notfound())
  let updateData = {
    mn,
    en,
    module,
    name: mn ? name : null,
    name_en: en ? nameEn : null
  }

  if (parentId) { // can update parent_id, if menu is not main
    updateData = {
      ...updateData,
      parent_id: parentId,
      is_main: false
    }
  } else {
    updateData = {
      ...updateData,
      parent_id: null,
      is_main: true
    }
  }

  if (menu.type !== 'module') {
    if (pageId) {
      updateData = {
        ...updateData,
        type: 'page',
        page_id: pageId
      }
    } else {
      updateData = {
        ...updateData,
        type: null,
        page_id: null
      }
    }
  }
  menu = await menu.updateAttributes(updateData)
  res.callback(menu)
}
