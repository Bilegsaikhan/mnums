export class ReportService {
  constructor (server) {
    'ngInject';
    this.server = server
  }

  list (obj) {
    let str = Object.keys(obj).map(function(key){
      return encodeURIComponent(key) + '=' + encodeURIComponent(obj[key]);
    }).join('&');
    return this.server.get(`/report/list?${str}`)
  }
  get ({id}) {
    return this.server.get(`/report/edit/${id}`)
  }
  update (data) {
    return this.server.put(`/report/${data.id}`, data)
  }
  remove (data) {
    return this.server.delete(`/report/${data.id}`)
  }
  create (data) {
    return this.server.post(`/report`, data)
  }
}
