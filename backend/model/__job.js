'use strict'
const tableName = 'job'
module.exports = function (sequelize, DataTypes) {
  return sequelize.define(tableName, {
    position: DataTypes.STRING,
    department: DataTypes.STRING,
    order: DataTypes.INTEGER,
  }, {
    timestamps: true,
    classMethods: {
      async updateOrder (ids) {
        const idsJoined = ids.join()
        const query1 = 'set @ordering=0;'
        const query2 = `update ${tableName} set \`order\`=(@ordering:=@ordering+10) order by field(id,${idsJoined});`
        await sequelize.query(query1, {type: sequelize.QueryTypes.UPDATE})
        await sequelize.query(query2, {type: sequelize.QueryTypes.UPDATE})
      }
    }
  })
}
