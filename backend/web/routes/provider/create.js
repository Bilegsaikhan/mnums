'use strict'
import { Provider } from '../../../model'
import Err from '../../../modules/err'

export default async function createCat (req, res) {
  req.checkBody('name', 'bad_name').len(1, 255)
  req.checkBody('name_en', 'bad_name_en').len(1, 255)
  req.checkBody('logo', 'bad_logo').len(1, 255)
  const err = req.validationErrors()
  if (err) return res.callback(err)
  try {
    const cat = await Provider.create({
      name: req.body.name,
      name_en: req.body.name_en,
      logo: req.body.logo
    })
    res.callback(cat)
  } catch (err) {
    res.callback(Err.sequelize(err))
  }
}
