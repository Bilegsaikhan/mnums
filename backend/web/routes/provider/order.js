'use strict'
import { Provider } from '../../../model'
import Err from '../../../modules/err'

export default async function updateProvider (req, res) {
  if (!req.body.ids || !Array.isArray(req.body.ids)) return res.callback(Err.badRequest('bad ids'))
  const ids = req.body.ids
  await Provider.updateOrder(ids)
  res.callback()
}
