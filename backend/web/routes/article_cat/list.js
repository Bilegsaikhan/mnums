'use strict'
import { ArticleCat } from '../../../model'

export default async function getCats (req, res) {
  const cats = await ArticleCat.findAll({order: '`order` ASC'})
  res.callback(cats)
}
