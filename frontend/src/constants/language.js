module.exports = {
  homepage: {
    en: 'Home page',
    mn: 'Hүүр хуудас',
  },
  careers: {
    en: 'Careers',
    mn: 'Ажлын байр',
  },
  currentOpenings: {
    en: 'Current Openings',
    mn: 'Нээлттэй ажлын байр',
  },
  workAtDBM: {
    en: 'Working at the Монголын эрүүл мэндийн үүсгэл Судалгааны төв',
    mn: 'Монголын эрүүл мэндийн үүсгэл Судалгааны төв-нд ажиллах',
  },
  programsInternships: {
    en: 'Programs and Internships',
    mn: 'Сургалт & семинар',
  },
  contactus: {
    en: 'Contact us',
    mn: 'Холбоо барих',
  },
  social: {
    en: 'Social Links',
    mn: 'Сошиал холбоос',
  },
  finencialNews: {
    en: 'DAILY TREASURY REPORT',
    mn: 'Зах зээлийн мэдээ',
  },
  featured: {
    en: 'Featured',
    mn: 'Онцлох',
  },
  keyMilestones: {
    en: 'Key milestones',
    mn: 'Үйл явдлын товчоон',
  },
  reports: {
    en: 'Reports',
    mn: 'Тайлан',
  },
  projects: {
    en: 'Projects',
    mn: 'Төсөл хөтөлбөрүүд',
  },
  news: {
    en: 'News',
    mn: 'Мэдээ мэдээлэл',
  },
  email: {
    en: 'Email',
    mn: 'Имэйл',
  },
  emailAddress: {
    en: 'Email address',
    mn: 'Имэйл хаяг',
  },
  copyRight: {
    en: 'All right reserved © 2017  Institute of Medical Sciences Mongolia.',
    mn: 'Бүх эрх хуулиар хамгаалагдсан © 2017 Т.Шагдарсүрэнгийн Нэрэмжит Анагаах Ухааны Хүрээлэн.',
  },
  loanSystem: {
    en: 'Loan system',
    mn: 'Зээлийн хүсэлт бүртгүүлэх систем',
  },
  title: {
    en: 'Institute of Medical Sciences Mongolia',
    mn: 'Т.Шагдарсүрэнгийн Нэрэмжит Анагаах Ухааны Хүрээлэн',
  },
  address: {
    en: 'Address',
    mn: 'Хаяг',
  },
  name: {
    en: 'Name',
    mn: 'Нэр',
  },
  position: {
    en: 'Position',
    mn: 'Албан тушаал',
  },
  phone: {
    en: 'Phone',
    mn: 'Утас',
  },
  clerk: {
    en: 'Timetable',
    mn: 'Цагийн хуваарь',
  },
  topProjects: {
    en: 'Top projects',
    mn: 'Онцлох төсөл',
  },
  search: {
    en: 'Search',
    mn: 'Хайлт хийх',
  },
  category: {
    en: 'Category',
    mn: 'Ангилал',
  },
  type: {
    en: 'Type',
    mn: 'Төрөл',
  },
  searchNews: {
    en: 'Search news',
    mn: 'Мэдээлэл хайх',
  },
  date: {
    en: 'Date',
    mn: 'Огноо',
  },
  seeMore: {
    en: 'See more',
    mn: 'Дэлгэрэнгүй',
  },
  sector: {
    en: 'By Sector',
    mn: 'Салбараар',
  },
  region: {
    en: 'By Region',
    mn: 'Бүс нутгаар',
  },
  funding: {
    en: 'By Funding',
    mn: 'Эх үүсвэрээр',
  },
  searchProject: {
    en: 'Search project',
    mn: 'Төсөл хөтөлбөрүүд хайх',
  },
  updatedAt: {
    en: 'Updated At',
    mn: 'Шинэчлэгдсэн огноо',
  },
  projectStartDate: {
    en: 'Project start date',
    mn: 'Хөтөлбөр эхлэх огноо',
  },
  projectEndDate: {
    en: 'Project end date',
    mn: 'Хөтөлбөр дуусах огноо',
  },
  program: {
    en: 'program',
    mn: 'Хөтөлбөр',
  },
  share: {
    en: 'Share',
    mn: 'Хуваалцах',
  },
  tweet: {
    en: 'Tweet',
    mn: 'Жиргэх',
  },
  media_source: {
    en: 'Media source',
    mn: 'Эх сурвалж',
  },
  ongoing: {
    en: 'Ongoing',
    mn: 'Хэрэгжиж буй',
  },
  completed: {
    en: 'Completed',
    mn: 'Хэрэгжиж дууссан',
  },
  projectStatus: {
    en: 'Project status',
    mn: 'Төслийн гүйцэтгэл',
  },
  loading: {
    en: 'Loading',
    mn: 'Уншиж байна',
  },
  projectProgramme: {
    en: 'Programme',
    mn: 'Хөтөлбөр',
  },
  projectObjective: {
    en: 'Objective',
    mn: 'Зорилго',
  },
  projectEconomic: {
    en: 'Economic',
    mn: 'Эдийн засгийн үр ашиг',
  },
  projectSocial: {
    en: 'Social',
    mn: 'Нийгмийн үр ашиг',
  },
  projectInterpretation: {
    en: 'Interpretation',
    mn: 'Төслийн ач холбогдол',
  },
  projectIntroduction: {
    en: 'Brief introduction',
    mn: 'Товч мэдээлэл',
  },
  projectImplementer: {
    en: 'Implementer',
    mn: 'Төсөл хэрэгжүүлэгчийн нэр',
  },
  projectPerformance: {
    en: 'Performance',
    mn: 'Гүйцэтгэлийн хувь',
  },
  projectFinancingAmount: {
    en: 'Financing amount',
    mn: 'Санхүүжилтийн дүн',
  },
  projectTogtoolAmount: {
    en: 'Resolution amount',
    mn: 'Тогтоолын дүн',
  },
  projectGereeAmount: {
    en: 'Contract amoun',
    mn: 'Гэрээний дүн',
  },
  projectSankhuuAmount: {
    en: 'Financing amount',
    mn: 'Санхүүжүүлсэн дүн',
  },
  projectLoanType: {
    en: 'Type of loan',
    mn: 'Зээлийн хэлбэр',
  },
  projectBorrower: {
    en: 'Borrower',
    mn: 'Зээлдэгчийн нэр',
  },
  projectDuty: {
    en: 'Capacity of the Project',
    mn: 'Хүчин чадал',
  },
  audited: {
    en: 'Audited',
    mn: 'Аудитлагдсан',
  },
  annual: {
    en: 'Annual',
    mn: 'Жилийн',
  },
  searchResult: {
    en: 'Search Result',
    mn: 'Хайлтын үр дүн',
  },
  noResult: {
    en: 'No Result',
    mn: 'Хайлын үр дүн олдсонгүй',
  },
  transparent: {
    en: 'Transparent',
    mn: 'Ил тод байдал',
  },
  legal: {
    en: 'Legal',
    mn: 'Хууль эрх зүй',
  },
  governmentResolutions: {
    en: 'Government Resolutions',
    mn: 'Засгийн газрын тогтоол',
  },
  laws: {
    en: 'Laws',
    mn: 'Хууль тогтоомж',
  },
  press: {
    en: 'Press release',
    mn: 'Хэвлэл мэдээлэл',
  },
  events: {
    en: 'Events',
    mn: 'Үйл явдал',
  },
  research: {
    en: 'Research articles',
    mn: 'Эрдэм шинжилгээний нийтлэлүүд',
  },


};
