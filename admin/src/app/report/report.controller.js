export class ReportController {
  constructor() {
    'ngInject';
    this.pageCount = 0
    this.search = ''
    this.allData = 0
    this.count = 0
    this.page = 1
    this.limit = 10
    this.isTableLoading = true
    this.order = 'id DESC'
  }
}
