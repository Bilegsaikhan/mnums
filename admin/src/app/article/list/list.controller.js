export class ArticleListController {
  constructor($log, articleService, $timeout, catService) {
    'ngInject';
    this.$log = $log.log
    this.$timeout = $timeout
    this.catService = catService
    this.$log('ArticleListController initialized')
    this.articleService = articleService
    this.filterCat = ''
    this.pageCount = 0
    this.search = ''
    this.allData = 0
    this.count = 0
    this.total = 0
    this.listData = []
    this.page = 1
    this.limit = 10
    this.isTableLoading = true
    this.order = 'id DESC'
    this.reqeust = {
      limit: this.limit
    }
    this.loadCats()
  }

  doSearch() {
    this.$timeout(() => {
      if (this.search.length > 2 || this.search === '') {
        this.page = 1;
        this.load();
      }
    }, 500)
  }

  loadCats() {
    this.catService.cats().then(cats => {
      this.cats = cats
    })
  }

  load() {
    if (this.page) this.reqeust.offset = this.page - 1
    if (this.search)
      this.reqeust.string = this.search
    else
      delete this.reqeust.string

    if (this.filterCat)
      this.reqeust.cat_id = this.filterCat
    else
      delete this.reqeust.cat_id

    this.articleService.list(this.reqeust).then(articles => {
      this.pageCount = Math.ceil(articles.total / this.limit);
      this.listData = articles.list
      this.total = articles.total
    })
  }
}
