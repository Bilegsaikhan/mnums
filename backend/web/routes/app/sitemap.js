'use strict'
import { Menu } from '../../../model'
import moment from 'moment'

const debug = require('../../../modules/debug')(__dirname)
// const domain = 'http://www.dbm.mn'
const domain = ''
export default async function (req, res, next) {
  const menus = await Menu.all({})
  const date = moment().format('YYYY-MM-DD')
  const list = []
  menus.forEach(menu => {
    let link = `${menu.module}`
    if (menu.type === 'module' || (menu.menus && menu.menus.length === 0)) {
      list.push({link, priority: 1.0})
    }
    if (menu.menus && menu.menus.length > 0) {
      menu.menus.forEach(menu => {
        let link2 = `${link}/${menu.module}`
        if (menu.type === 'module') {
          list.push({link: `${menu.module}`, priority: 1.0})
        } else if (menu.menus && menu.menus.length === 0) {
          list.push({link: link2, priority: 0.5})
        }
        if (menu.menus && menu.menus.length > 0) {
          let link3 = `${link2}/${menu.module}`
          if (menu.type === 'module') {
            list.push({link: `${menu.module}`, priority: 1.0})
          } else {
            list.push({link: link3, priority: 0.5})
          }
        }
      })
    }
  })
  debug(list)
  res.set('Content-Type', 'text/xml')
  res.send(`<?xml version="1.0" encoding="utf-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
${list.map(item => `
    <url>
        <loc>http://www.dbm.mn/${item.link}</loc>
        <lastmod>${date}</lastmod>
        <changefreq>daily</changefreq>
        <priority>0.9</priority>
    </url>
`).join('')}
</urlset>
  `)
}
