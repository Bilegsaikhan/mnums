import React, {Component, PropTypes} from 'react';
import OwlCarousel from 'react-owl-carousel2';
import TextTruncate from 'react-text-truncate';
import {Link} from 'react-router'
import './ItemCarousel.css';

class ItemCarousel extends Component {

    static propTypes = {
        items: PropTypes.array
    }

    constructor(props) {
        super(props);
        this.state = {
            index: 0,
            count: 0
        };
    }

    onChangeSlide = (event) => {
    };

    render() {
        const {items} = this.props
        const options = {
            items: 4,
            nav: true,
            rewind: true
        };

        return (
            <div className="">
                <div className="grid_12_par_0 parsys">
                    <div style={{paddingLeft: 0, paddingRight: 0}}
                         className="remove-padding-grid">
                        <div className="n06v1_indepth section">
                            <div id="n06v1-body" className=" clearfix">
                                <div className="n06v1-top">
                                    <div className="n06v1-section-title">
                                        <h6>Онцлох төсөл хөтөлбөрүүд</h6>
                                    </div>
                                </div>
                                <div className="carousel-block">
                                    <OwlCarousel ref="itemCarousel" options={options} events={{onChanged: this.onChangeSlide}}>
                                        {
                                            items.map((slide, index) => {
                                                return (
                                                    <div className="no6v1IndepthItem" key={`imc_${index}`}>
                                                        <div className="n06v1 ">
                                                            <div
                                                                className="thumbnail n06v1-img">
                                                                <Link to={`/project/${slide.id}`}>
                                                                    <img
                                                                        className="img-responsive"
                                                                        src={`/uploads/${slide.id}.jpg`}
                                                                        alt={`imc_${index}`}/>
                                                                </Link>
                                                            </div>
                                                            <div className="n06v1-title">
                                                                <Link to={`/project/${slide.id}`}>
                                                                    <TextTruncate
                                                                        line={2}
                                                                        truncateText="…"
                                                                        text={slide.name}
                                                                    />

                                                                    </Link>
                                                            </div>
                                                            <div className="n06v1-text"/>
                                                        </div>
                                                    </div>
                                                )
                                            })
                                        }
                                    </OwlCarousel>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ItemCarousel
