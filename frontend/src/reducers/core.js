
const defaultState = {
    mainSlide: [],
    timelines: [],
};
// reducer
export default function core(state = defaultState, action) {
    switch (action.type) {

        case 'UPDATE_STEP': {
            return {
                ...state,
                modalStep: action.payload
            }
        }

        default: {
            return state;
        }
    }
}