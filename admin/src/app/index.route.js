export function routerConfig ($stateProvider, $urlRouterProvider) {
  'ngInject';

  const _requireRoot = function () {
    return {
      UserData: ['userService', function (userService) {
        return userService.logged().then(res => {
          if (!res.user.root) {
            return Promise.reject('ACCESS_DENIED')
          }
          return Promise.resolve()
        });
      }]
    }
  }

  $stateProvider
    .state('public', {
      abstract: true,
      url: '',
      templateUrl: 'app/public/public.html',
      controller: 'PublicController',
      controllerAs: 'public',
      resolve: {
        UserData: function ($log, userService) {
          return userService.logged().then(res => {
            if (res.user) {
              return Promise.reject('logged')
            }
            return Promise.resolve()
          })
        }
      }
    })
    .state('public.login', {
      url: '/login',
      templateUrl: 'app/login/login.html',
      controller: 'LoginController',
      controllerAs: 'vm'
    })
    .state('root', {
      abstract: true,
      url: '',
      templateUrl: 'app/root/root.html',
      controller: 'RootController',
      controllerAs: 'root',
      resolve: {
        UserData: function ($log, userService) {
          return userService.logged().then(res => {
            $log.log('res: ', res)
            if (!res.user) {
              return Promise.reject('login')
            }
            return Promise.resolve()
          })
        }
      }
    })
    .state('root.content', {
      abstract: true,
      url: '',
      views: {
        content: {
          templateUrl: 'app/content/content.html',
          controller: 'ContentController'
        },
        sidebar: {
          templateUrl: 'app/sidebar/sidebar.html',
          controller: 'SidebarController'
        },
        header: {
          templateUrl: 'app/header/header.html',
          controller: 'HeaderController',
          controllerAs: 'vm'
        },
        footer: {
          templateUrl: 'app/footer/footer.html',
          controller: 'FooterController'
        }
      }
    })
    .state('root.content.menu', {
      url: '/menu',
      templateUrl: 'app/menu/menu.html',
      controller: 'MenuController',
      controllerAs: 'vm',
      resolve: _requireRoot()
    })

    .state('root.content.home', {
      url: '/',
      templateUrl: 'app/home/home.html',
      controller: 'HomeController',
      controllerAs: 'main'
    })
    .state('root.content.about', {
      url: '/about',
      templateUrl: 'app/about/about.html',
      controller: 'AboutController',
      controllerAs: 'about'
    })

    .state('root.content.user', {
      abstract: true,
      url: '/user',
      templateUrl: 'app/user/user.html',
      controller: 'UserController',
      controllerAs: 'vm',
      resolve: _requireRoot()
    })
    .state('root.content.user.list', {
      url: '',
      templateUrl: 'app/user/list/list.html',
      controller: 'UserListController',
      controllerAs: 'vm'
    })
    .state('root.content.user.new', {
      url: '/new',
      templateUrl: 'app/user/form/form.html',
      controller: 'UserFormController',
      controllerAs: 'vm'
    })
    .state('root.content.user.edit', {
      url: '/edit/:id',
      templateUrl: 'app/user/form/form.html',
      controller: 'UserFormController',
      controllerAs: 'vm'
    })

    .state('root.content.laws', {
      abstract: true,
      url: '/laws',
      templateUrl: 'app/laws/laws.html',
      controller: 'LawsController',
      controllerAs: 'vm'
    })
    .state('root.content.laws.list', {
      url: '',
      templateUrl: 'app/laws/list/list.html',
      controller: 'LawsListController',
      controllerAs: 'vm'
    })
    .state('root.content.laws.new', {
      url: '/new',
      templateUrl: 'app/laws/form/form.html',
      controller: 'LawsFormController',
      controllerAs: 'vm'
    })
    .state('root.content.laws.edit', {
      url: '/edit/:id',
      templateUrl: 'app/laws/form/form.html',
      controller: 'LawsFormController',
      controllerAs: 'vm'
    })

    .state('root.content.logs', {
      abstract: true,
      url: '/logs',
      templateUrl: 'app/logs/logs.html',
      controller: 'LogsController',
      controllerAs: 'vm'
    })
    .state('root.content.logs.list', {
      url: '',
      templateUrl: 'app/logs/list/list.html',
      controller: 'LogsListController',
      controllerAs: 'vm'
    })

    .state('root.content.report', {
      abstract: true,
      url: '/report',
      templateUrl: 'app/report/report.html',
      controller: 'ReportController',
      controllerAs: 'vm'
    })
    .state('root.content.report.list', {
      url: '',
      templateUrl: 'app/report/list/list.html',
      controller: 'ReportListController',
      controllerAs: 'vm'
    })
    .state('root.content.report.new', {
      url: '/new',
      templateUrl: 'app/report/form/form.html',
      controller: 'ReportFormController',
      controllerAs: 'vm'
    })
    .state('root.content.report.edit', {
      url: '/edit/:id',
      templateUrl: 'app/report/form/form.html',
      controller: 'ReportFormController',
      controllerAs: 'vm'
    })

    .state('root.content.page', {
      abstract: true,
      url: '/page',
      templateUrl: 'app/page/page.html',
      controller: 'PageController',
      controllerAs: 'vm'
    })
    .state('root.content.page.list', {
      url: '',
      templateUrl: 'app/page/list/list.html',
      controller: 'ListPageController',
      controllerAs: 'vm'
    })
    .state('root.content.page.new', {
      url: '/new',
      templateUrl: 'app/page/form/form.html',
      controller: 'PageFormController',
      controllerAs: 'vm'
    })
    .state('root.content.page.edit', {
      url: '/edit/:id',
      templateUrl: 'app/page/form/form.html',
      controller: 'PageFormController',
      controllerAs: 'vm'
    })

    .state('root.content.banner', {
      url: '/banner',
      templateUrl: 'app/banner/banner.html',
      controller: 'BannerController',
      controllerAs: 'vm'
    })

    .state('root.content.static', {
      abstract: true,
      url: '/static',
      templateUrl: 'app/static/static.html',
      controller: 'StaticController',
      controllerAs: 'static'
    })
    .state('root.content.static.list', {
      url: '',
      templateUrl: 'app/static/list/list.html',
      controller: 'StaticListController',
      controllerAs: 'vm'
    })
    .state('root.content.static.new', {
      url: '/new',
      templateUrl: 'app/static/form/form.html',
      controller: 'StaticFormController',
      controllerAs: 'vm'
    })
    .state('root.content.static.edit', {
      url: '/edit/:id',
      templateUrl: 'app/static/form/form.html',
      controller: 'StaticFormController',
      controllerAs: 'vm'
    })

    .state('root.content.article', {
      abstract: true,
      url: '/article',
      templateUrl: 'app/article/article.html',
      controller: 'ArticleController',
      controllerAs: ''
    })
    .state('root.content.article.list', {
      url: '/list',
      templateUrl: 'app/article/list/list.html',
      controller: 'ArticleListController',
      controllerAs: 'vm'
    })
    .state('root.content.article.new', {
      url: '/new',
      templateUrl: 'app/article/form/form.html',
      controller: 'ArticleFormController',
      controllerAs: 'vm'
    })
    .state('root.content.article.edit', {
      url: '/edit/:id',
      templateUrl: 'app/article/form/form.html',
      controller: 'ArticleFormController',
      controllerAs: 'vm'
    })
    .state('root.content.cat', {
      url: '/article/cat',
      templateUrl: 'app/article/cat/cat.html',
      controller: 'ArticleCatController',
      controllerAs: 'vm'
    })

    .state('root.content.project', {
      abstract: true,
      url: '/project',
      templateUrl: 'app/project/project.html',
      controller: 'ProjectController',
      controllerAs: ''
    })
    .state('root.content.project.list', {
      url: '/list',
      templateUrl: 'app/project/list/list.html',
      controller: 'ProjectListController',
      controllerAs: 'vm'
    })
    .state('root.content.project.new', {
      url: '/new',
      templateUrl: 'app/project/form/form.html',
      controller: 'ProjectFormController',
      controllerAs: 'vm'
    })
    .state('root.content.project.edit', {
      url: '/edit/:id',
      templateUrl: 'app/project/form/form.html',
      controller: 'ProjectFormController',
      controllerAs: 'vm'
    })

    .state('root.content.sector', {
      url: '/project/sector',
      templateUrl: 'app/project/sector/sector.html',
      controller: 'ProjectSectorController',
      controllerAs: 'vm'
    })
    .state('root.content.region', {
      url: '/project/region',
      templateUrl: 'app/project/region/region.html',
      controller: 'ProjectRegionController',
      controllerAs: 'vm'
    })
    .state('root.content.funding', {
      url: '/project/funding',
      templateUrl: 'app/project/funding/funding.html',
      controller: 'ProjectFundingController',
      controllerAs: 'vm'
    })

    .state('root.content.milestone', {
      url: '/milestone',
      templateUrl: 'app/milestone/milestone.html',
      controller: 'MilestoneController',
      controllerAs: 'vm'
    })

    .state('root.content.slide', {
      abstract: true,
      url: '/slide',
      templateUrl: 'app/slide/slide.html',
      controller: 'SlideController',
      controllerAs: 'vm'
    })
    .state('root.content.slide.list', {
      url: '',
      templateUrl: 'app/slide/list/list.html',
      controller: 'ListSlideController',
      controllerAs: 'vm'
    })
    .state('root.content.slide.new', {
      url: '/new',
      templateUrl: 'app/slide/form/form.html',
      controller: 'SlideFormController',
      controllerAs: 'vm'
    })
    .state('root.content.slide.edit', {
      url: '/edit/:id',
      templateUrl: 'app/slide/form/form.html',
      controller: 'SlideFormController',
      controllerAs: 'vm'
    })

    .state('root.content.timeline', {
      abstract: true,
      url: '/timeline',
      templateUrl: 'app/timeline/timeline.html',
      controller: 'TimelineController',
      controllerAs: 'vm'
    })
    .state('root.content.timeline.list', {
      url: '',
      templateUrl: 'app/timeline/list/list.html',
      controller: 'ListTimelineController',
      controllerAs: 'vm'
    })
    .state('root.content.timeline.new', {
      url: '/new',
      templateUrl: 'app/timeline/form/form.html',
      controller: 'TimelineFormController',
      controllerAs: 'vm'
    })
    .state('root.content.timeline.edit', {
      url: '/edit/:id',
      templateUrl: 'app/timeline/form/form.html',
      controller: 'TimelineFormController',
      controllerAs: 'vm'
    })

    .state('root.content.pdf', {
      url: '/pdf',
      templateUrl: 'app/pdf/pdf.html',
      controller: 'PdfController',
      controllerAs: 'vm'
    })

    .state('root.content.fileBrowser', {
      url: '/fileBrowser',
      templateUrl: 'app/fileBrowser/fileBrowser.html',
      controller: 'fileBrowserController',
      controllerAs: 'vm'
    })

    .state('root.content.backup', {
      url: '/backup',
      templateUrl: 'app/backup/backup.html',
      controller: 'BackUpController',
      controllerAs: 'vm'
    })

  $urlRouterProvider.otherwise('/');
}
