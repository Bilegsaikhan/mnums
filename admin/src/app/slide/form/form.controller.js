export class SlideFormController {
  constructor ($log, sliderService, $state, toastr, modalService) {
    'ngInject';
    this.$log = $log.log
    this.service = sliderService
    this.toastr = toastr
    this.$state = $state
    this.stateName = $state.current.name
    this.modalService = modalService
    this.clear()
    this.sidebarData = [
      {
        name: 'Үйл явдал',
        value: 'event'
      }, {
        name: 'Төсөл хөтөлбөр',
        value: 'project'
      }, {
        name: 'Тайлан',
        value: 'report'
      }
    ]
    if (this.stateName === 'root.content.slide.edit') this.load()
  }

  clear () {
    this.item = {
      mn: true,
      en: false,
      image: "",
      name: "",
      name_en: "",
      desc: "",
      desc_en: "",
      link: null
    }
  }

  create () {
    this.service.create(this.item).then(item => {
      this.item = item
      this.$state.go('root.content.slide.list');
    })
  }

  update () {
    this.service.update(this.item).then(item => {
      this.item = item
      this.toastr.success('Таны хүсэлт амжилттай!');
    }, () => {
      this.toastr.error('Таны хүсэлт амжилтгүй!');
    })
  }

  remove () {
    this.modalService.danger('Та уг үүнийг устгахдаа итгэлтэй байна уу', true).then(() => {
      this.service.remove({id: this.$state.params.id}).then(() => {
        this.toastr.success('Таны хүсэлт амжилттай!');
        this.$state.go('root.content.slide.list');
      }, () => {
        this.toastr.error('Таны хүсэлт амжилтгүй!');
      })
    })
  }

  load () {
    this.service.get({id: this.$state.params.id}).then(item => {
      this.item = item
    }, () => {
      this.toastr.error('Таны хүсэлт амжилтгүй!');
    })
  }

  uploadDone (image) {
    this.item.image = image
  }
}
