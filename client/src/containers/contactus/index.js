import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker
} from 'react-google-maps';
import logoIcon from '../../assets/icon.png';
// import { Link } from "react-router-dom";
// import GoogleMap from "../../companents/googleMap";
import './style.scss';

const MyMapComponent = withScriptjs(
  withGoogleMap(props => (
    <GoogleMap
      defaultZoom={17}
      defaultCenter={{ lat: 47.914423, lng: 106.85703 }}
      gestureHandling="cooperative"
    >
      {props.isMarkerShown && (
        <Marker
          position={{ lat: 47.914423, lng: 106.85703 }}
          title="Т.Шагдарсүрэнгийн Нэрэмжит Анагаах Ухааны Хүрээлэн"
          icon={{
            url: logoIcon,
            size: { height: 68, width: 50 },
            scaledSize: { height: 68, width: 50 }
          }}
        />
      )}
    </GoogleMap>
  ))
);

class ContactUs extends Component {
  static propTypes = {};

  render() {
    return (
      <div>
        <MyMapComponent
          isMarkerShown
          googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyCydGnuyxaKFkqSak6H6sgsVlWdNUR2L8o&v=3.exp&libraries=geometry,drawing,places"
          loadingElement={<div style={{ height: `100%` }} />}
          containerElement={
            <div style={{ height: `400px`, marginTop: '-1em' }} />
          }
          mapElement={<div style={{ height: `100%` }} />}
        />
        <div className="container">
          <div className="blog-post py-4">
            <h2 className="section-title">Холбоо барих</h2>
          </div>
          <div className="row">
            <div className="col-md-4">
              <address className="mb-0">
                <span className="d-block u-text-light mb-1">
                  +(976) 70287809
                </span>
                <span className="d-block u-text-light mb-1">
                  contact@imsm.mn
                </span>
                <span className="d-block u-text-light mb-1 address">
                  Монгол улс, Улаанбаатар хот 16081, Баянгол дүүрэг, Ард Аюушийн
                  гудамж-1, Улсын Гуравдугаар Төв Эмнэлэг
                </span>
              </address>
            </div>
            <div className="col-md-8">
              <table
                className="contact-table table table-sm table-borderless"
                cellPadding="2"
                cellSpacing="0"
                width="100%"
              >
                <tbody>
                  <tr className="tbl-hdr">
                    <td>
                      <h6>Нэр</h6>
                    </td>
                    <td>
                      <h6>Албан тушаал</h6>
                    </td>
                    <td>
                      <h6>Утас</h6>
                    </td>
                    <td>
                      <h6>Имэйл хаяг</h6>
                    </td>
                  </tr>
                  <tr className="zebra-striping">
                    <td>П.Эрхэмбулган</td>
                    <td>Захирал</td>
                    <td>99117364</td>
                    <td>munkhbatb@mnums.edu.mn</td>
                  </tr>
                  <tr>
                    <td>Л.Тулгаа</td>
                    <td>Эрдэмтэн нарийн бичгийн дарга </td>
                    <td>99011565</td>
                    <td>tulgaa.ims@mnums.edu.mn</td>
                  </tr>
                  <tr className="zebra-striping">
                    <td>Г.Биндэръяа</td>
                    <td>Хүний нөөцийн ажилтан</td>
                    <td>99859070</td>
                    <td>binderiya.ims@mnums.edu.mn</td>
                  </tr>
                  <tr>
                    <td>Б.Содгэрэл</td>
                    <td>Зүрхний мэс заслын секторын эрхлэгч </td>
                    <td>99070490</td>
                    <td>sodgerel.ims@mnums.edu.mn</td>
                  </tr>
                  <tr className="zebra-striping">
                    <td>Я.Эрдэнэчимэг</td>
                    <td>Мэдрэл судлалын секторын эрхлэгч</td>
                    <td>99087925</td>
                    <td>erdenechimeg.ims@mnums.edu.mn</td>
                  </tr>
                  <tr>
                    <td>Л.Галсумъяа</td>
                    <td>Сургалтын менежер</td>
                    <td>991874648</td>
                    <td>galsumiya.ims@mnums.edu.mn</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ContactUs);
