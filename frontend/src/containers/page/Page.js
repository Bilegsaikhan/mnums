import React, {Component} from "react";
import {Link} from "react-router";
import {connect} from "react-redux";
import {browserHistory} from 'react-router'
import "./Page.css";
import language from "../../constants/language"
import Social from "../../components/social";
import {loadStatic} from "../../actions/static";

class Page extends Component {

    componentWillMount() {
        const {loadStatic} = this.props;
        if (!this.props.thisMenu) browserHistory.push('/')
        loadStatic('address')
    }

    render() {
        const {thisMenu, thisPage, lang, staticReducer} = this.props;

        if (!thisMenu) return null

        return (
            <div className="dbm-page-container-box">
                <div className="g01v1-this-page-in">
                    <div className="container">
                        <div className="row">
                            <div className="col-xs-5 col-sm-6">
                                <div className="breadcrumb_wbr wb_breadcrumb">
                                    <div className="g04v1-text">
                                        <ol className="breadcrumb">
                                            <li><Link to={`/`}>{language.homepage[lang]}</Link></li>
                                            <li><a>{thisMenu.name}</a></li>
                                            <li><a>{thisPage.name}</a></li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="container dbmCpageTitle">
                    <div className="titlepar parsys">
                        <div className="title section">
                            <div className="row">
                                <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div className="c01v1-page-title hidden-xs hidden-sm">{thisPage.name}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="par parsys">
                        <div className="gridlayout parbase section">
                            <div className="row">
                                <div className="col-md-2 col-sm-12 col-xs-12 col-lg-2">
                                    <div>
                                        <div className="grid_2_par_0 parsys">
                                            <div className="remove-padding-grid">
                                                <div className="gridlayout parbase section">
                                                    <div className="row hidden-xs hidden-sm">
                                                        <div className="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                                            <div>
                                                                <div className="grid_12_par_0 parsys">
                                                                    <div className="remove-padding-grid">
                                                                        <hr className="hidden-lg hidden-md"/>
                                                                        <div
                                                                            className="n01v1_left_navigation parbase section">
                                                                            <div
                                                                                className="n01v1-nav-list dbm-left-nav hidden-sm hidden-xs">
                                                                                { thisMenu &&
                                                                                <ul>
                                                                                    {thisMenu.menus.map(item => {
                                                                                        if (item.menus.length) {
                                                                                            return (
                                                                                                <div
                                                                                                    key={`plmisad_${item.id}`}>
                                                                                                    <li
                                                                                                        className="title"
                                                                                                        key={`plmi_${item.id}`}
                                                                                                    >
                                                                                                        <h6>{item.name}</h6>
                                                                                                    </li>
                                                                                                    {item.menus.map(subItem => {
                                                                                                        return (
                                                                                                            <li
                                                                                                                key={`plmi_${subItem.id}`}
                                                                                                            >
                                                                                                                {subItem.type ===
                                                                                                                "module"
                                                                                                                    ?
                                                                                                                    <Link
                                                                                                                        to={`/${subItem.module}`}
                                                                                                                        activeClassName="active"
                                                                                                                    >
                                                                                                                        {`${subItem.name} »`}
                                                                                                                    </Link>
                                                                                                                    :
                                                                                                                    <Link
                                                                                                                        to={`/${thisMenu.module}/${item.module}/${subItem.module}`}
                                                                                                                        activeClassName="active"
                                                                                                                    >
                                                                                                                        {`${subItem.name} »`}
                                                                                                                    </Link>}
                                                                                                            </li>
                                                                                                        );
                                                                                                    })}
                                                                                                </div>
                                                                                            );
                                                                                        } else {
                                                                                            return (
                                                                                                <li key={`plmi_${item.id}`}>
                                                                                                    {item.type === "module"
                                                                                                        ? <Link
                                                                                                            to={`/${item.module}`}
                                                                                                            activeClassName="active"
                                                                                                        >
                                                                                                            {`${item.name} »`}
                                                                                                        </Link>
                                                                                                        : <Link
                                                                                                            to={`/${thisMenu.module}/${item.module}`}
                                                                                                            activeClassName="active"
                                                                                                        >
                                                                                                            {`${item.name} »`}
                                                                                                        </Link>}
                                                                                                </li>
                                                                                            );
                                                                                        }
                                                                                    })}
                                                                                </ul>
                                                                                }
                                                                            </div>
                                                                            <div className="clearfix"/>
                                                                        </div>
                                                                        <hr />
                                                                        <div className="reference parbase section">
                                                                            <div className="cq-dd-paragraph">
                                                                                <div
                                                                                    className="country_office_conta country_office_contact_input">
                                                                                    <div
                                                                                        id="lr01v1"
                                                                                        className=" clearfix"
                                                                                    >
                                                                                        <h6>{language.contactus[lang]}</h6>
                                                                                        <div className="mrg-b-20" dangerouslySetInnerHTML={{
                                                                                            __html: staticReducer.address && staticReducer.address.data
                                                                                        }}/>
                                                                                        <Link
                                                                                            className="btn btn-default btn-sm mrg-t-15"
                                                                                            to="/contact"
                                                                                        >
                                                                                            {language.contactus[lang]}
                                                                                            »
                                                                                        </Link>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <hr />
                                                                        <div className="reference parbase section">
                                                                            <div className="cq-dd-paragraph">
                                                                                <Social />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="clearfix"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-10 col-lg-10 col-xs-12 col-sm-12">
                                    <div>
                                        <div className="grid_10_par_1 parsys">
                                            <div className="remove-padding-grid">
                                                <div className="gridlayout parbase section pageContent">

                                                    <div className="row">
                                                        <div
                                                            className={`col-md-12 col-lg-12 col-sm-12 col-xs-12`}
                                                        >
                                                            <div
                                                                className={`${thisPage.sidebar ? "padding-right-20 center-right-border" : ""}`}
                                                            >
                                                                <div className="row dbm-page-content">
                                                                    <div className="col-md-12">
                                                                        {/* <h4 className="text-uppercase mrg-b-15">
                                                                            {thisPage.name}
                                                                        </h4> */}
                                                                        <div
                                                                            className="dangerouslySetInnerHTML"
                                                                            dangerouslySetInnerHTML={{
                                                                                __html: thisPage.content
                                                                            }}
                                                                        />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="clearfix"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="clearfix"/>
                        </div>
                        <div className="gridlayout parbase section">
                            <div className="row hidden-md hidden-lg">
                                <div className="col-md-12 col-sm-12 col-lg-12 col-xs-12">
                                    <hr className="hr-top"/>
                                    <div>
                                        <div className="grid_12_par_0 parsys">
                                            <div className="remove-padding-grid">
                                                <div className="gridlayout parbase section">
                                                    <div className="row">
                                                        <div className="col-md-6 col-sm-6 col-xs-6">
                                                            <div>
                                                                <div className="grid_6_par_0 parsys">
                                                                    <div className="remove-padding-grid">
                                                                        <div className="reference parbase section">
                                                                            <div className="cq-dd-paragraph">
                                                                                <div
                                                                                    className="country_office_conta country_office_contact_input">
                                                                                    <div
                                                                                        id="lr01v1"
                                                                                        className=" clearfix"
                                                                                    >
                                                                                        <h6>{language.contactus[lang]}</h6>
                                                                                        <div className="mrg-b-20" dangerouslySetInnerHTML={{
                                                                                            __html: staticReducer.address && staticReducer.address.data
                                                                                        }}/>
                                                                                        <Link
                                                                                            className="btn btn-default btn-sm mrg-t-15"
                                                                                            to="/contact"
                                                                                        >
                                                                                            {language.contactus[lang]}
                                                                                            »
                                                                                        </Link>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-6 col-xs-6 col-sm-6">
                                                            <div>
                                                                <div className="grid_6_par_1 parsys">
                                                                    <div className="remove-padding-grid">
                                                                        <div className="reference parbase section">
                                                                            <div className="cq-dd-paragraph">
                                                                                <Social />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="clearfix"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="clearfix"/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    const {params} = ownProps;
    const {menu, config, staticReducer} = state;
    return {
        lang: config.language,
        thisMenu: menu.indexedData[params.parentPageId],
        thisPage: menu.indexedData[params.pageId].pageData,
        staticReducer: staticReducer
    };
};

const mapDispatch = {
    loadStatic
};

export default connect(mapStateToProps, mapDispatch)(Page);
