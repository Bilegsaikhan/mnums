import React, { Component } from "react";
import PropTypes from "prop-types";
import CarouselTemplate from "./carouselTemplate";
import TimelineCarousel from "./timelineCarousel";
import "./ItemDescCarousel.css";
import { connect } from "react-redux";
import language from "../../constants/language"

class ItemDescCarousel extends Component {
  static propTypes = {
    items: PropTypes.array
  };

  constructor(props) {
    super(props);
    this.state = {
      contentIndex: props.items && props.items.length - 1,
      timelineIndex: 0,
      count: 0
    };
  }

  componentDidMount() {
    if(!this.props.items) return null
    let initIndex = this.props.userAgent.mobile
      ? this.props.items.length - 1
      : this.props.items.length - 1;
    this.itemCarousel.goTo(initIndex);
  }

  onChangeContentSlide = event => {
    this.setState({ contentIndex: event.item.index });
  };

  onChangeTimelineSlide = event => {
    this.setState({ timelineIndex: event.item.index });
  };

  render() {
    const { items, userAgent, lang } = this.props;
    const itemLength = items && items.length - 3;
    return (
      <div>
        <div className="grid_12_par_0 parsys">
          <div className="remove-padding-grid">
            <div className="n06v1_indepth section">
              <div id="n06v1-body" className=" clearfix">
                <hr />
                <div className="n06v1-top">
                  <div className="n06v1-section-title">
                    <h6>{language.keyMilestones[lang]}</h6>
                  </div>
                  <div className="clearfix" />
                </div>
                <div className="carousel-block CarouselTemplate">
                  <div className="carouselCustomDotsContainer clearfix">
                    <div className="carouselCustomDotsBlock">
                      <TimelineCarousel
                        items={items}
                        agent={userAgent}
                        itemCarousel={ref => this.itemCarousel = ref}
                        onChangeCarousel={this.onChangeTimelineSlide}
                        onClickItem={index => this.itemDescCarousel.goTo(index)}
                        activeItem={this.state.contentIndex}
                      />
                    </div>
                    <a
                      className={`ccdPrev ccdNav ${(this.itemCarousel && this.itemCarousel.currentPosition) === 0 ? "disabled" : ""}`}
                      onClick={() => this.itemCarousel.prev()}
                    >
                      <svg
                        fill="currentColor"
                        preserveAspectRatio="xMidYMid meet"
                        height="40px"
                        width="40px"
                        viewBox="0 0 40 40"
                      >
                        <g>
                          <path d="m26.5 12.1q0 0.3-0.2 0.6l-8.8 8.7 8.8 8.8q0.2 0.2 0.2 0.5t-0.2 0.5l-1.1 1.1q-0.3 0.3-0.6 0.3t-0.5-0.3l-10.4-10.4q-0.2-0.2-0.2-0.5t0.2-0.5l10.4-10.4q0.3-0.2 0.5-0.2t0.6 0.2l1.1 1.1q0.2 0.2 0.2 0.5z" />
                        </g>
                      </svg>
                    </a>

                    <a
                      className={`ccdNext ccdNav ${itemLength} ${itemLength === (this.itemCarousel && this.itemCarousel.currentPosition) ? "disabled" : ""}`}
                      onClick={() => this.itemCarousel.next()}
                    >
                      <svg
                        fill="currentColor"
                        preserveAspectRatio="xMidYMid meet"
                        height="40px"
                        width="40px"
                        viewBox="0 0 40 40"
                      >
                        <g>
                          <path d="m26.3 21.4q0 0.3-0.2 0.5l-10.4 10.4q-0.3 0.3-0.6 0.3t-0.5-0.3l-1.1-1.1q-0.2-0.2-0.2-0.5t0.2-0.5l8.8-8.8-8.8-8.7q-0.2-0.3-0.2-0.6t0.2-0.5l1.1-1.1q0.3-0.2 0.5-0.2t0.6 0.2l10.4 10.4q0.2 0.2 0.2 0.5z" />
                        </g>
                      </svg>
                    </a>
                  </div>
                  <div className="carouselTemplateContent">
                    <CarouselTemplate
                      items={items}
                      itemDescCarousel={ref => this.itemDescCarousel = ref}
                      onChangeCarousel={this.onChangeContentSlide}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  const { userAgent, config } = state;

  return {
    lang: config.language,
    userAgent
  };
};

export default connect(mapStateToProps, {})(ItemDescCarousel);
