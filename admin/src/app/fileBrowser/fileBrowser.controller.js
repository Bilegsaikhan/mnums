export class fileBrowserController {
  constructor($log, pdfService, modalService, $timeout, toastr) {
    'ngInject'
    this.$log = $log.log
    this.toastr = toastr
    this.$timeout = $timeout
    this.pdfService = pdfService
    this.modalService = modalService
    this.count = 0
    this.pageCount = 0
    this.search = ''
    this.limit = 50
    this.reqeust = {
      limit: this.limit
    }
  }

  // You can still access the clipboard.js event
  onSuccess(e) {
    this.toastr.success('Copied!');
    e.clearSelection();
  }

  onError() {
    this.toastr.error('Таны хүсэлт амжилтгүй!');
  }

  doSearch() {
    this.$timeout(() => {
      if (this.search.length > 2 || this.search === '') {
        this.page = 1;
        this.load();
      }
    }, 500)
  }

  load() {

    if (this.page) this.reqeust.offset = this.page - 1
    if (this.search)
      this.reqeust.string = this.search
    else
      delete this.reqeust.string

    this.pdfService.list(this.reqeust).then(res => {
      this.clear()
      this.pageCount = Math.ceil(res.total / this.limit);
      this.pdfs = res.list
      this.count = res.total
    })

  }

  clear() {
    this.pdf = {
      id: '',
      name: '',
      thumb: '',
      url: ''
    }
  }

  edit(cat) {
    this.pdf = Object.assign({}, cat)
  }

  canSubmit() {
    return this.pdfForm.$invalid
  }

  update() {
    return this.pdfService.update(this.pdf)
  }

  remove(id) {
    this.modalService.danger('Та уг ангилалыг устгахдаа итгэлтэй байна уу', true).then(() => {
      this.pdfService.delete(id).then(() => {
        this.load()
      })
    })
  }

  create() {
    return this.pdfService.create(this.pdf)
  }

  submit() {
    (this.pdf.id ? this.update() : this.create()).then(() => {
      this.load()
    })
  }

  uploadMnPdfDone(url, thumb) {
    this.$log('upload mn pdf done')
    this.pdf.url = url
    this.pdf.thumb = thumb

  }

}
