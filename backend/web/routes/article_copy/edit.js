'use strict'
import { ArticleCopy as Article } from '../../../model'
import Err from '../../../modules/err'

export default async function getArticle (req, res) {
  req.checkParams('id', 'bad_id').isInt()
  const err = req.validationErrors()
  if (err) return res.callback(err)
  const {id} = req.params
  let article = await Article.findById(id)
  if (!article) return res.callback(Err.notfound())
  res.callback(article)
}
