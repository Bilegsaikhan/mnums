'use strict'
import conf from '../../config/index'
import amqp from 'amqplib/callback_api'
import imageHandler from '../image_handler'
import mailHandler from '../mail_handler'

const debug = require('../../modules/debug')(__filename)

function send (ch, q, data) {
  ch.sendToQueue(q, new Buffer(JSON.stringify(data)))
}

function start () {
  let connection
  amqp.connect(`amqp://${conf.RABBITMQ_HOST}`, (err, conn) => {
    if (err) throw err
    connection = conn
    conn.createChannel((err, ch) => {
      if (err) throw err
      let q = 'ubtrade-queue'
      ch.assertQueue(q, {durable: false})
      debug(`waiting for message on worker ${process.env.WORKER_NAME}`)
      // process.send('ready')
      ch.consume(q, msg => {
        let data = JSON.parse(msg.content.toString())
        debug(data)
        let {callback: callbackQueue, action} = data
        let promise = null
        switch (action) {
          case 'image': {
            debug(`image task request received on worker ${process.env.WORKER_NAME}`)
            promise = imageHandler(data)
            break
          }
          case 'mail': {
            debug(`mail task request received on worker ${process.env.WORKER_NAME}`)
            promise = mailHandler(data)
            break
          }
          default: {
            debug(`ping task request received on worker ${process.env.WORKER_NAME}`)
            promise = Promise.resolve({message: 'ping response'})
            break
          }
        }
        promise.then(response => {
          debug('task successfuly done')
          if (callbackQueue) {
            send(ch, callbackQueue, response)
            debug('sent back response')
          }
          ch.ack(msg)
        }).catch(err => {
          debug(`error occured:`)
          debug(err)
          ch.ack(msg)
        })
      })
    })
  })

  process.on('SIGINT', () => {
    if (connection) {
      connection.close()
      debug('amqp connection closed')
    }
    process.exit(0)
  })
}
export { start }
