import React, { Component } from 'react';
import './style.scss';

class NoMatch extends Component {
  render() {
    return (
      <div className={'container'}>
        <h1>404</h1>
        <p>Sorry, the page you were trying to view does not exist.</p>
      </div>
    );
  }
}

export default NoMatch;
