'use strict'
import inc from './include'
module.exports = function (sequelize, DataTypes) {

  return sequelize.define('article_copy', {
    title: DataTypes.STRING,
    desc: DataTypes.STRING,
    content: DataTypes.STRING,
    picture: DataTypes.STRING,
    // created_at: DataTypes.DATE,
    // updated_at: DataTypes.DATE
  }, {
    timestamps: true,
    classMethods: {
      toCount ({cat_id: catId, string}) {
        let where = []
        const attrs = []
        if (catId) {
          where.push('cat_id = ?')
          attrs.push(catId)
        }
        if (string) {
          where.push('(title like ? OR `desc` like ? OR content like ?)')
          const wildCardedString = `%${string}%`
          attrs.push(wildCardedString)
          attrs.push(wildCardedString)
          attrs.push(wildCardedString)
        }
        return this.count({
          where: where.length > 0 ? [where.join(' AND '), ...attrs] : undefined
        })
      },
      list ({offset = 0, limit = 10, cat_id: catId, string}) {
        const include = inc(sequelize.models)
        let where = []
        const attrs = []
        if (catId) {
          where.push('cat_id = ?')
          attrs.push(catId)
        }
        if (string) {
          where.push('(title like ? OR `desc` like ? OR content like ?)')
          const wildCardedString = `%${string}%`
          attrs.push(wildCardedString)
          attrs.push(wildCardedString)
          attrs.push(wildCardedString)
        }
        return this.findAll({
          offset: offset * limit,
          limit: parseInt(limit),
          where: where.length > 0 ? [where.join(' AND '), ...attrs] : undefined,
          order: 'created_at DESC',
        })
      },
      all ({offset = 0, limit = 10, string}) {
        const include = inc(sequelize.models)
        let where = []
        const attrs = []
        if (string) {
          where.push('(title like ? OR `desc` like ? OR content like ?)')
          const wildCardedString = `%${string}%`
          attrs.push(wildCardedString)
          attrs.push(wildCardedString)
          attrs.push(wildCardedString)
        }
        return this.findAll({
          offset: offset * limit,
          limit: parseInt(limit),
          where: where.length > 0 ? [where.join(' AND '), ...attrs] : undefined,
          order: 'created_at DESC',
        })
      }
    }
  })
}

