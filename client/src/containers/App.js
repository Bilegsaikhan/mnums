import React, { Component } from 'react';
// import PropTypes from "prop-types";
// import { connect } from "react-redux";
// import { withRouter } from "react-router-dom";
import { Provider } from 'react-redux';
import Main from './Main';
// import { resetErrorMessage } from "../actions";
import { Header, Footer } from '../companents';
import './App.scss';

class App extends Component {
  render() {
    const { store } = this.props;
    return (
      <Provider store={store}>
        <div>
          <Header />
          <div className="py-3">
            <Main {...store} />
          </div>
          <Footer />
        </div>
      </Provider>
    );
  }
}

export default App;
