import React, { Component } from "react";
import PropTypes from "prop-types";
// import {Link} from 'react-router'
import "./ReportListItem.css";

class ReportListItem extends Component {
  static propTypes = {
    items: PropTypes.array
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { items } = this.props;

    return (
      <section>
        <div className="c14v1-body c14v1-body-text flipboard-keep report">
          <div className="n02v5-body">
            <ul className="clearfix">
              {items.map((item, index) => {
                return (
                  <li className="media" key={`limc_${index}`}>
                    <a
                      target="_blank"
                      href={item.pdf}
                      className="n02v5-img"
                    >
                      <img
                        className="reportThumb media-object"
                        title={item.name}
                        alt={item.name}
                        src={`/assets/images/default2.png`}
                      />
                    </a>
                    <div className="n02v5-text media-body">
                      <div className="hammer">{item.updatedAt}</div>
                      <h5>
                        <a
                          target="_blank"
                          href={item.pdf}
                        >
                          {item.name}
                        </a>
                      </h5>
                    </div>
                  </li>
                );
              })}
            </ul>
          </div>
        </div>
      </section>
    );
  }
}

export default ReportListItem;
