export class CatService {
  constructor (server) {
    'ngInject';
    this.server = server
  }

  cats () {
    return this.server.get('/cat')
  }

  update ({id, name, name_en}) {
    return this.server.put(`/cat/${id}`, {name, name_en})
  }

  create ({name, name_en}) {
    return this.server.post('/cat', {name, name_en})
  }

  delete (id) {
    return this.server.delete(`/cat/${id}`)
  }

  order ({ids}) {
    return this.server.put('/cat', {ids})
  }
}
