'use strict'
import { upload, uploadBuffer } from '../../../modules/s3'
import { v1 } from 'uuid'
import md5 from 'md5'
import gm from 'gm'
import fs from 'fs'
import path from 'path'
import { PDFImage } from 'pdf-image'
import PROMISE from 'bluebird'
import config from '../../../config';

// const gm = require('gm').subClass({imageMagick: true})
PROMISE.promisifyAll(gm.prototype)
const debug = require('../../../modules/debug')(__filename)
const imagePrefix = '/images/';
const gmPath = path.join(__dirname, '..', '..', '..', '..', 'uploads/');
if (!fs.existsSync(gmPath)) {
  fs.mkdirSync(gmPath)
}

export async function uploadImage(req, res) {
  debug(req.files)
  debug('upload image called')
  const srcPath = req.files.upload.path
  const uid = v1()
  const fileName = md5(`FILE_GEN_SALT${uid}`) + '.jpg'
  await uploadSingleJpeg(srcPath, fileName)
  const url = `${imagePrefix}${fileName}`
  debug(`upload sucess ${url}`)

  fs.unlink(srcPath, () => {
    console.log('unlinked!!!');
    res.callback({ fileName, url })
  });
}
export async function uploadPdf(req, res) {
  // debug(req.files)
  // debug('upload pdf called')
  // const srcPath = req.files.upload.path
  // const pdfImage = new PDFImage(srcPath)
  // debug('preparing pdf thumb')
  // let thumbSrcPath = await pdfImage.convertPage(0)
  // debug('prepared pdf thumb')
  // const uid = v1()
  // const name = md5(`FILE_GEN_SALT${uid}`)
  // const thumbS3path = `dbm/pdfthumb/${name}.png`
  // const pdfS3path = `dbm/pdf/${name}.pdf`
  // debug('pdf upload starting')
  // await uploadSinglePng(thumbSrcPath, thumbS3path, [1000, 1000])
  // debug('thumb image uploaded')
  // await upload(srcPath, pdfS3path)
  // debug('pdf uploaded')
  // res.callback({
  //   thumb: `${s3prefix}${thumbS3path}`,
  //   file: `${s3prefix}${pdfS3path}`
  // })
}
async function uploadSingleJpeg(srcPath, fileName, size) {
  return new Promise((resolve, reject) => {
    debug('srcPath', srcPath)
    let gmInstance = gm(srcPath)
    if (size && size.length) {
      gmInstance = gmInstance.resize(size[0], size[1])
    }
    gmInstance = gmInstance.autoOrient().noProfile().strip().interlace('Line')
    gmInstance.write(`${gmPath}${fileName}`, (err) => {
      if (err) return reject(err);
      resolve();
    })
  })



  // const buffer = await gmInstance.toBufferAsync('JPEG')
  // await uploadBuffer(buffer, fileName, 'image/jpeg')
}
async function uploadSinglePng(srcPath, s3path, size) {
  // debug('srcPath', srcPath)
  // let gmInstance = gm(srcPath)
  // if (size && size.length) {
  //   gmInstance = gmInstance.resize(size[0], size[1])
  // }
  // gmInstance = gmInstance.autoOrient().noProfile()
  // const buffer = await gmInstance.toBufferAsync('PNG')
  // await uploadBuffer(buffer, s3path, 'image/png')
}
