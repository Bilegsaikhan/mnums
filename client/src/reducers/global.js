import pageData from '../constants/pageData.json';

const initialAuthState = {
  error: false,
  loading: false,
  pageData
};

const global = (state = initialAuthState, action) => {
  switch (action.type) {
    default:
      return state;
  }
};

export default global;
