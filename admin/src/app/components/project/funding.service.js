export class FundingService {
  constructor (server) {
    'ngInject';
    this.server = server
  }

  list () {
    return this.server.get('/project_source')
  }

  update ({id, name, name_en}) {
    return this.server.put(`/project_source/${id}`, {name, name_en})
  }

  create ({name, name_en}) {
    return this.server.post('/project_source', {name, name_en})
  }

  delete (id) {
    return this.server.delete(`/project_source/${id}`)
  }

  order ({ids}) {
    return this.server.put('/project_source', {ids})
  }
}
