'use strict'
import { Page } from '../../../model'

export default async function createPage (req, res) {
  req.checkBody('name', 'bad_name').len(1, 255)
  req.checkBody('name_en', 'bad_name_en').len(1, 255)
  req.checkBody('content', 'bad_content')
  req.checkBody('content_en', 'bad_content_en')
  if (req.body.sidebar) {
    req.checkBody('sidebar', 'bad_sidebar').isIn(['event', 'project', 'report'])
  }
  const err = req.validationErrors()
  if (err) return res.callback(err)

  const {name, name_en, content, content_en, sidebar} = req.body
  const page = await Page.create({name, name_en, content, content_en, sidebar})
  res.callback(page)
}
