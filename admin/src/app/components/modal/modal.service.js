export class ModalService {
  constructor ($uibModal) {
    'ngInject'
    this.$uibModal = $uibModal
  }

  danger (message, confirm) {
    return openModal(this.$uibModal, 'danger', message, confirm)
  }
}
var openModal = function ($uibModal, type, message, confirm) {
  return $uibModal.open({
    animation: true,
    templateUrl: 'app/components/modal/modal.html',
    controller: 'ModalController',
    resolve: {
      data: function () {
        return {type, message, confirm}
      }
    }
  }).result
}
