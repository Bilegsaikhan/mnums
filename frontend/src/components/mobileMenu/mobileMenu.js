import React, {Component} from 'react'
import './mobileMenu.css'
import {Link, IndexLink} from 'react-router'
// import logo from './logo-small.png'
// import { browserHistory } from 'react-router'

class mobileMenu extends Component {
    static propTypes = {}

    constructor(props) {
        super(props);
        this.state = {
            opened: false,
            count2: 6
        };
    }

    render() {
        return (
            <nav className="mobileMenu">
                <div className="">
                    <div className="">
                        <ul className="group" id="mobile-nav">
                            <li className="menu-item current_page_item">
                                <IndexLink to="/" activeClassName="active">
                                    <img src="/assets/images/home.png" alt="home"
                                         style={{position: 'relative', top: '-2px'}}/>
                                </IndexLink>
                            </li>
                            <li className="menu-item">
                                <Link to={`/aboutus`} activeClassName="active" className={`cursorDefault`} onClick={(e)=>e.preventDefault()}>Бидний тухай</Link>
                                <div className="">
                                    <ul>
                                        <li>
                                            <Link to={`/aboutus`}>
                                                СЕО Мэндчилгээ »
                                            </Link>
                                        </li>
                                        <li>
                                            <Link to={`/aboutus/intro`}>Танилцуулга »</Link>
                                        </li>
                                        <li>
                                            <Link to={`/aboutus/stracture`}>Банкны бүтэц »</Link>
                                        </li>
                                        <li>
                                            <Link to={`/aboutus/board`}>Төлөөлөн удирдах зөвлөл »</Link>
                                        </li>
                                        <li>
                                            <Link to={`/aboutus/board2`}>Гүйцэтгэх удирдлага »</Link>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li className="menu-item">
                                <Link to="/activity" activeClassName="active" className={`cursorDefault`} onClick={(e)=>e.preventDefault()}>
                                    Бидний үйл ажиллагаа
                                </Link>
                                <div className="" style={{width: '570px'}}>
                                    <ul className="mc-cols">
                                        <li className="title"><span>Бүтээгдэхүүн үйлчилгээ</span></li>
                                        {/*<li>*/}
                                        {/*<Link to={`/activity`}>Гадаад худалдааны санхүүжилт »</Link>*/}
                                        {/*</li>*/}
                                        <li>
                                            <Link to={`/activity`}>Төслийн санхүүжилт үнэлгээ
                                                »</Link>
                                        </li>
                                        <li>
                                            <Link to={`/activity/proof`}>Баталгаа, батлан даалт »</Link>
                                        </li>
                                        <li>
                                            <Link to={`/activity/companyLoan`}>Байгууллагын зээл »</Link>
                                        </li>
                                        {/*<li>*/}
                                        {/*<Link to={`/activity/consultant`}>Зөвлөх үйлчилгээ »</Link>*/}
                                        {/*</li>*/}
                                        <li>
                                            <Link to={`/activity/investment`}>Хөрөнгө оруулалт »</Link>
                                        </li>
                                        <li>
                                            <Link to={`/activity/transmitLoan`}>Дамжуулан зээл »</Link>
                                        </li>
                                    </ul>
                                    <ul className="mc-cols">
                                        <li className="title">
                                            <span>Төсөл хөтөлбөрүүд »</span>
                                        </li>
                                        <li>
                                            <Link to={`/projects`}>Хэрэгжүүлж буй төсөл, хөтөлбөрүүд »</Link>
                                        </li>
                                        <li>
                                            <Link to={`/activity/projectClaim`}>Төсөл, хөтөлбөрт тавигдах шаардлага
                                                »</Link>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li className="menu-item">
                                <Link to="/investors" activeClassName="active" className={`cursorDefault`} onClick={(e)=>e.preventDefault()}>
                                    Хөрөнгө оруулагчид
                                </Link>
                                <div className="">
                                    <ul>
                                        <li>
                                            <Link to="/investors">Тайлан мэдээ »</Link>
                                        </li>
                                        <li>
                                            <Link to="/investors/loanLevel">Зээлжих зэрэглэл, үнэлгээ »</Link>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li className="menu-item">
                                <Link to="/legal" activeClassName="active" className={`cursorDefault`} onClick={(e)=>e.preventDefault()}>
                                    Хууль эрх зүй
                                </Link>
                                <div className="">
                                    <ul>
                                        <li>
                                            <Link to="/legal">Хууль тогтоомж »</Link>
                                        </li>
                                        <li>
                                            <Link to="/legal/govermentLaw">Засгийн газрын тогтоол »</Link>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li className="menu-item">
                                <Link to="/shilendans" activeClassName="active" className={`cursorDefault`} onClick={(e)=>e.preventDefault()}>
                                    Ил тод байдал
                                </Link>
                                <div className="">
                                    <ul>
                                        <li>
                                            <Link to="/shilendans">Шилэн Данс »</Link>
                                        </li>
                                        <li>
                                            <Link to="/publicity">Санхүүгийн ил тод байдал »</Link>
                                        </li>
                                        <li>
                                            <Link to="/publicity/huniinuuts">Хүний нөөцийн ил тод байдал »</Link>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li className="menu-item"><Link to="/news" activeClassName="active">Мэдээ мэдээлэл</Link></li>
                            <li className="menu-item"><Link to="/contact" activeClassName="active">Холбоо барих</Link></li>
                        </ul>
                    </div>
                    <hr className="mainMenuTopLine"/>
                </div>
            </nav>
        )
    }
}

export default mobileMenu
