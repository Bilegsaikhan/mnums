/*global FB*/
import React, {Component} from "react";
import "./NewsDetail.css";
import {connect} from "react-redux";
import {Link} from "react-router";
import {loadNews} from "../../actions/news";
import {Helmet} from "react-helmet";
import moment from 'moment'
import language from "../../constants/language"
import {browserHistory} from 'react-router'

class News extends Component {
    componentDidMount() {
        if (window.FB) window.FB.XFBML.parse();
        this.maybeFetchData();
    }

    maybeFetchData() {
        const {params, loadNews} = this.props;
        loadNews(params.id).then(() => {
        }, () => {
            browserHistory.push(`/`);
        })
    }

    handleFbShare = e => {
        e.preventDefault();
        window.FB.ui(
            {
                method: "share",
                href: `${window.location.host}${this.props.location.pathname}`
            },
            () => {
            }
        );
    };

    render() {
        const {newsDetail, isItemFetching, lang} = this.props;
        return (
            <div>
                <Helmet>
                    <title>{newsDetail.title}</title>
                </Helmet>
                <div className="g01v1-this-page-in">
                    <div className="container">
                        <div className="row">
                            <div className="col-xs-5 col-sm-6">
                                <div className="breadcrumb_wbr wb_breadcrumb">
                                    <div className="g04v1-text">
                                        <ol className="breadcrumb">
                                            <li><Link to={`/`}>{language.homepage[lang]}</Link></li>
                                            <li><Link to={`/news`}>{language.news[lang]}</Link></li>
                                            <li><span>{newsDetail.title}</span></li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="container">
                    <div className={`projectLoading ${isItemFetching ? 'showLoading' : ''}`}>
                        <div className="showbox">
                            <div className="loader">
                                <svg className="circular" viewBox="25 25 50 50">
                                    <circle className="path" cx="50" cy="50" r="20"
                                            fill="none" strokeWidth="2"
                                            strokeMiterlimit="10"/>
                                </svg>
                            </div>
                            {language.loading[lang]}...
                        </div>
                    </div>
                    <div className={`titlepar parsys dbmNewsItemblock ${isItemFetching ? 'showLoading' : ''}`}>
                        <div className="gridlayout parbase section">
                            <div className="row">
                                <div className="col-md-12 col-sm-12 col-lg-12 col-xs-12">
                                    <div>
                                        <div className="grid_12_par_0 parsys">
                                            <div className="remove-padding-grid">
                                                <div className="title section">
                                                    <div className="row">
                                                        <div className="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                            <h1 className="flipboard-title dbmNewsTitle">
                                                                {newsDetail.title}
                                                            </h1>

                                                        </div>
                                                        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <div className="newsSubHeader">
                                                                <div className="time-stamp">
                                                                    {moment(newsDetail.created_at).format("YYYY.MM.DD")}
                                                                </div>
                                                                <div className="socialBox">
                                                                    <ul className="list-unstyled">
                                                                        <li>
                                                                            <a
                                                                                className="socialBtn facebookBtn"
                                                                                onClick={e => this.handleFbShare(e)}
                                                                            >
                                                                                <i className="fa fa-facebook"/>
                                                                                <span className="socialBtn-text">
                                                                                    {language.share[lang]}
                                        </span>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a
                                                                                href={`https://twitter.com/intent/tweet?text=${newsDetail.title ? newsDetail.title.replace(/%/g, "%25") : ""} ${window.location.host}${this.props.location.pathname} @MongoliaHarvard&original_referer=${window.location.host}${this.props.location.pathname}`}
                                                                                className="socialBtn btn-twitter"
                                                                            >
                                                                                <i className="fa fa-twitter"/>
                                                                                <span className="socialBtn-text">
                                                                                    {language.tweet[lang]}
                                                                                </span>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr className="hr-bottom hr-top-10"/>
                                </div>
                            </div>
                            <div className="clearfix"/>
                        </div>
                        <div className="gridlayout parbase section">
                            <div className="row">
                                <div className="col-md-8 col-xs-12 col-sm-12">
                                    <div className="center-right-border">
                                        <div className="grid_8_par_0 parsys">
                                            <div className="remove-padding-grid">
                                                <div className="gridlayout parbase section">
                                                    <div className="row">
                                                        <div className="col-md-12 col-sm-12 col-xs-12">
                                                            <div>
                                                                <div className="grid_12_par_0 parsys">
                                                                    <div className="remove-padding-grid">
                                                                        <div
                                                                            className="c14v1_static_content section mr-20">
                                                                            <section>
                                                                                <div
                                                                                    className="c14v1-body c14v1-body-text flipboard-keep ndWyswyg">
                                                                                    <div
                                                                                        className="dangerouslySetInnerHTML"
                                                                                        dangerouslySetInnerHTML={{
                                                                                            __html: newsDetail.content
                                                                                        }}
                                                                                    />
                                                                                </div>
                                                                            </section>
                                                                            <div className="clearfix"/>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="clearfix"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-4 col-sm-12 col-lg-4 col-xs-12">
                                    <div>
                                        <div className="grid_4_par_1 parsys">
                                            <div className="remove-padding-grid">
                                                <div className="gridlayout parbase section">
                                                    <div className="row">
                                                        <div className="col-md-12 col-xs-12 col-sm-12">
                                                            <div>
                                                                <div className="grid_12_par_0 parsys">
                                                                    <div className="remove-padding-grid">
                                                                        <hr className="hidden-lg hidden-md"/>
                                                                        <div
                                                                            className="c10v1_resources parbase section">
                                                                            <div className="" id="c10v1-body">
                                                                                <h6>{language.media_source[lang]}</h6>
                                                                                <div className="c10v1">
                                                                                    <ul>
                                                                                        {/*<li>*/}
                                                                                        {/*<a href="">Lao PDR Portfolio*/}
                                                                                        {/*Factsheet</a>&nbsp;*/}
                                                                                        {/*</li>*/}
                                                                                    </ul>
                                                                                </div>
                                                                                <div className="clearfix"/>
                                                                            </div>
                                                                        </div>
                                                                        <hr />
                                                                        <div
                                                                            className="fb-page"
                                                                            data-href="https://www.facebook.com/MongoliaHarvard"
                                                                            data-tabs="timeline"
                                                                            data-small-header="false"
                                                                            data-adapt-container-width="true"
                                                                            data-hide-cover="false"
                                                                            data-show-facepile="true"
                                                                        >
                                                                            <blockquote
                                                                                cite="https://www.facebook.com/MongoliaHarvard"
                                                                                className="fb-xfbml-parse-ignore"
                                                                            >
                                                                                <a href="https://www.facebook.com/MongoliaHarvard">
                                                                                    
MHI - Харвардын стандартыг Монголд
                                                                                </a>
                                                                            </blockquote>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="clearfix"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="clearfix"/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapState = (state, props) => ({
    newsDetail: state.news.newsDetail,
    lang: state.config.language,
    isItemFetching: state.news.isItemFetching
});

const mapDispatch = {
    loadNews
};

const EnhancedComponent = connect(mapState, mapDispatch)(News);
export default EnhancedComponent;
