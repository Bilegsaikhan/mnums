const noneThumb = 'http://www.vacumedtedavisi.com/download/images/none.jpg'
const nonePdf = ''
export function UploadPdfDirective () {
  'ngInject';
  let directive = {
    restrict: 'EAC',
    templateUrl: 'app/components/uploadPdf/uploadPdf.html',
    replace: true,
    scope: {
      uploadDone: '=',
      remove: '=',
      uploadData: '=',
      scope: '=',
      ngModel: '='
    },
    controller: UploadPdfController,
    controllerAs: 'vm',
    bindToController: true
  };
  return directive;
}

class UploadPdfController {
  constructor ($log, server) {
    'ngInject';
    this.$log = $log.log
    this.server = server
    this.thumb = noneThumb
    this.pdf = nonePdf
    this.noneThumb = noneThumb
    this.$log('model', this.ngModel)
  }

  uploadFiles (file/*, errFiles*/) {
    // this.$log('file', file)
    // this.$log('errFiles', errFiles)
    if (file) {
      this.server.uploadPdf(file).then(res => {
        // this.$log('upload response', res)
        this.thumb = res.thumb
        this.pdf = res.file
        this.uploadDone.apply(this.scope, [res.file, res.thumb])
      })
    }
  }

  removePdf () {
    this.$log('ngModel', this.ngModel)
    // this.remove.apply(this.scope)
    // this.thumb = noneThumb
    // this.pdf = nonePdf
  }

}
