'use strict'
/**
 * This index.js file for pm2 starter
 * http://pm2.keymetrics.io
 */
require('babel-core/register')
require('babel-polyfill')
require('./web/app').start()
