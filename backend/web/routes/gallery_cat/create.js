'use strict'
import { GalleryCat, GalleryItem } from '../../../model'
import fall from 'async-waterfall'
import Err from '../../../modules/err'

export default async function createCat (req, res) {
  req.checkBody('name', 'bad_name').len(1, 255)
  const err = req.validationErrors()
  if (err) return res.callback(err)
  try {
    const cat = await GalleryCat.create({
      name: req.body.name
    })

    let tasks = []

    if (!req.body.images) req.body.images = [];

    req.body.images.forEach(entry => {
      let task = (callback) => {
        let image = {
          url: entry.url,
          cat_id: cat.id
        }

        GalleryItem.create(image).then(
          () => {
            callback();
          },
          (e) => {
            callback(e)
          }
        )
      }

      tasks.push(task);
    })

    fall(tasks, e => {
      res.callback(cat);
    })
  } catch (err) {
    res.callback(Err.sequelize(err))
  }
}
