/* eslint-disable import/prefer-default-export */
export const LOAD_PAGE = 'LOAD_PAGE';
export const LOAD_PAGE_SUCCESS = 'LOAD_PAGE_SUCCESS';
export const LOAD_PAGE_ERROR = 'LOAD_PAGE_ERROR';

export const LOAD_NEWS_LIST = 'LOAD_NEWS_LIST';
export const LOAD_NEWS_LIST_SUCCESS = 'LOAD_NEWS_LIST_SUCCESS';
export const LOAD_NEWS_LIST_ERROR = 'LOAD_NEWS_LIST_ERROR';

export const LOAD_NEWS_ITEM = 'LOAD_NEWS_ITEM';
export const LOAD_NEWS_ITEM_SUCCESS = 'LOAD_NEWS_ITEM_SUCCESS';
export const LOAD_NEWS_ITEM_ERROR = 'LOAD_NEWS_ITEM_ERROR';
