import { LOAD_PAGE, LOAD_PAGE_SUCCESS, LOAD_PAGE_ERROR } from "../constants";

export function loadPage(moduleString) {
  return async (dispatch, getState, { fetch }) => {
    // eslint-disable-next-line no-param-reassign
    const payload = {};

    dispatch({
      type: LOAD_PAGE,
      payload
    });

    try {
      let response = await fetch(`page/module/${moduleString}`, {
          method: "GET",
          credentials: 'include'
      });

      let data = await response.json();
      if (!response.ok) {
        const message = data.message || data;
        if (data) throw message;
      }
      dispatch({
        type: LOAD_PAGE_SUCCESS,
        payload: data,
        moduleString: moduleString
      });
      return data;
    } catch (error) {
      const message = error.message || error;
      dispatch({
        type: LOAD_PAGE_ERROR,
        error: error.message
      });
      throw new Error(message);
    }
  };
}
