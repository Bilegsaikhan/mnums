'use strict'
import { ArticleCopy as Article } from '../../../model'
import Err from '../../../modules/err'
import moment from 'moment'

export default async function createArticle (req, res) {
  if (req.body.date) {
    req.checkBody('date', 'bad_date').isDate()
  }
  let {mn, en, date = moment().format('YYY-MM-DDTHH:mm:ssZ')} = req.body
  mn = (mn === true || mn === 'true')
  en = (en === true || en === 'true')
  date = moment(date, 'YYY-MM-DDTHH:mm:ssZ').toDate()
  req.checkBody('title', 'bad_title').len(1, 255)
  req.checkBody('desc', 'bad_desc').len(1, 2000)

  const err = req.validationErrors()
  if (err) return res.callback(err)

  const {title, desc, content, picture} = req.body
  let createData = {
    title,
    desc,
    content,
    picture
  }
  try {
    const article = await Article.create(createData)
    res.callback(article)
  } catch (err) {
    res.callback(Err.sequelize(err))
  }
}
