export class PageFormController {
  constructor($log, pageService, $state, toastr, modalService) {
    'ngInject';
    this.$log = $log.log
    this.pageService = pageService
    this.toastr = toastr
    this.$state = $state
    this.stateName = $state.current.name
    this.modalService = modalService
    this.clear()
    this.sidebarData = [
      {
        name: 'Үйл явдал',
        value: 'event'
      }, {
        name: 'Төсөл хөтөлбөр',
        value: 'project'
      }, {
        name: 'Тайлан',
        value: 'report'
      }
    ]
    if (this.stateName === 'root.content.page.edit') this.load()
  }

  clear() {
    this.page = {
      name: '',
      content: '',
      name_en: '',
      content_en: '',
      sidebar: null
    }
  }

  create() {
    this.pageService.create(this.page).then(page => {
      this.page = page
      this.$state.go('root.content.page.list');
    })
  }

  update() {
    this.pageService.update(this.page).then(page => {
      this.page = page
      this.toastr.success('Таны хүсэлт амжилттай!');
    }, () => {
      this.toastr.error('Таны хүсэлт амжилтгүй!');
    })
  }

  remove() {
    this.modalService.danger('Та уг үүнийг устгахдаа итгэлтэй байна уу', true).then(() => {
      this.pageService.remove({id: this.$state.params.id}).then(() => {
        this.toastr.success('Таны хүсэлт амжилттай!');
        this.$state.go('root.content.page.list');
      }, () => {
        this.toastr.error('Таны хүсэлт амжилтгүй!');
      })
    })
  }

  load() {
    this.pageService.get({id: this.$state.params.id}).then(page => {
      this.page = page
    }, () => {
      this.toastr.error('Таны хүсэлт амжилтгүй!');
    })
  }


}
