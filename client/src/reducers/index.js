import { combineReducers } from 'redux';
import global from './global';
import news from './news';

const rootReducer = combineReducers({
  global,
  news
});

export default rootReducer;
