export class RegionService {
  constructor (server) {
    'ngInject';
    this.server = server
  }

  list () {
    return this.server.get('/project_region')
  }

  update ({id, name, name_en}) {
    return this.server.put(`/project_region/${id}`, {name, name_en})
  }

  create ({name, name_en}) {
    return this.server.post('/project_region', {name, name_en})
  }

  delete (id) {
    return this.server.delete(`/project_region/${id}`)
  }

  order ({ids}) {
    return this.server.put('/project_region', {ids})
  }
}
