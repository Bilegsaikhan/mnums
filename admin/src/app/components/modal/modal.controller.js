/**
 * Created by enxtur on 5/12/17.
 */
export class ModalController {
  constructor ($scope, $uibModalInstance, data) {
    'ngInject'
    $scope.data = data
    $scope.accept = (data) => {
      if ($scope.data.confirm) {
        $uibModalInstance.close(data)
      }
    }
  }
}
