export class LawsService {
  constructor (server) {
    'ngInject';
    this.server = server
  }

  list (obj) {
    let str = Object.keys(obj).map(function(key){
      return encodeURIComponent(key) + '=' + encodeURIComponent(obj[key]);
    }).join('&');
    return this.server.get(`/law/list?${str}`)
  }
  get ({id}) {
    return this.server.get(`/law/edit/${id}`)
  }
  update (data) {
    return this.server.put(`/law/${data.id}`, data)
  }
  remove (data) {
    return this.server.delete(`/law/${data.id}`)
  }
  create (data) {
    return this.server.post(`/law`, data)
  }
}
