/**
 * Created by enxtur on 5/5/17.
 */

import { Menu } from '../../../model'
import Err from '../../../modules/err'
export default async (req, res) => {
  const {id} = req.params
  let menu = await Menu.findById(id)
  if (!menu) return res.callback(Err.notfound())
  await menu.destroy()
  res.callback()
}
