import React, { Component } from 'react';
// import { Link } from 'react-router';
import { connect } from 'react-redux';
import { Helmet } from "react-helmet";
import './Home.css';
import Slider from '../../components/slider';
import ItemDescCarousel from '../../components/itemDescCarousel';
// import NewsListItem from '../../components/newsListItem';
import Banner from '../../components/banner';
import { loadReportHomeFilter } from '../../actions/report';
import { loadNewsHomeFilter } from '../../actions/news';
import language from "../../constants/language";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentIndex: 0,
      tabIndex: 'audit',
      tabProjectIndex: 1,
      tabNewsIndex: 1,
      reportLoading: false,
      newsLoading: true,
      projectLoading: false,
    };
  }

  componentDidMount() {
    this.maybeFetchData();
  }

    maybeFetchData = () => {
      // const {loadTopNews, loadTopProjects, loadTopReport} = this.props;
      // loadTopProjects('&status=ongoing');
      // loadTopNews(this.props.newTabs[0].id);
      // loadTopReport('audit');
    }

    chnageReportTab = (type) => {
      if (type === this.state.tabIndex) return false;
      this.setState({ reportLoading: true });
      return this.props.loadTopReport(type).then(() => {
        this.setState({ reportLoading: false });
        this.setState({ tabIndex: type });
      });
    }

    chnageProjectTab = (index) => {
      if (index === this.state.tabProjectIndex) return false;
      let status = index === 1 ? '&status=ongoing' : '&status=finished';
      this.setState({ projectLoading: true });
      return this.props.loadTopProjects(status).then(() => {
        this.setState({ projectLoading: false });
        this.setState({ tabProjectIndex: index });
      });
    }

    chnageNewsTab = (id) => {
      if (id === this.state.tabNewsIndex) return false;
      return this.props.loadTopNews(id).then(() => {
        this.setState({ newsLoading: false });
        this.setState({ tabNewsIndex: id });
      });
    }

    render() {
      const {
        timelines,
        mainSlide,
        // homeTops,
        lang,
      } = this.props;
      return (
        <div>
          <Helmet>
            <title>{language.homepage[lang]}</title>
          </Helmet>
          <div className="homepage">
            <div className={'container'}>
              <div className="gridlayout parbase section">
                <div className="row">
                  <div className="gridlayout parbase section">
                    <div className="col-md-12 col-sm-12 col-lg-12 col-xs-12">
                      <div className=" ">
                        <div className="grid_12_par_0 parsys">
                          <div
                            className="remove-padding-grid"
                          >
                            <Slider slides={mainSlide} />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="clearfix" />
                </div>
                <div className="row">
                  <div className="col-md-12 col-sm-12 col-lg-12 col-xs-12">
                    <ItemDescCarousel items={timelines} />
                  </div>
                </div>
                <div className="clearfix" />
                <div className="row">
                  <div className="col-md-12 col-sm-12 col-xs-12 col-lg-12 ">
                    <div className=" ">
                      <div className="grid_10_par_1 parsys">
                        <div
                          className="remove-padding-grid"
                        >
                          <div className="gridlayout parbase section">
                            <div className="row">
                              <div className="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                <hr className="hr-top" />
                                <div className=" ">
                                  <div className="grid_12_par_0 parsys">
                                    <div className="remove-padding-grid">
                                      <div className="gridlayout parbase section">
                                        <div className="row homeFeturedLists">

                                          {/* {
                                            homeTops.map(({ category, articles }) => (
                                              <div className="col-md-4 col-lg-4 col-xs-12 col-sm-12" key={`htops_${category.id}`}>
                                                <div className="">
                                                  <div
                                                    className="grid_7_par_0 parsys"
                                                  >
                                                    <div
                                                      className="remove-padding-grid"
                                                    >
                                                      <div
                                                        className="n02v5_highlight_list section"
                                                      >
                                                        <div className="">
                                                          <h6 className="pull-left">
                                                            <Link
                                                              to="/news?cat_id=3&offset=0"
                                                            >{`${category.name} »`}
                                                            </Link>
                                                          </h6>
                                                          <div
                                                            className="clearfix"
                                                          />
                                                          <div
                                                            className=" clearfix"
                                                          >
                                                            <div
                                                              className="tab-content"
                                                            >
                                                              <div
                                                                className="brd tab-pane tabcontent0 active"
                                                              >
                                                                <div
                                                                  className="tab-0 parsys"
                                                                >
                                                                  <div
                                                                    className="gridlayout parbase section"
                                                                  >
                                                                    <div
                                                                      className="row "
                                                                    >
                                                                      <div
                                                                        className="col-md-12"
                                                                      >
                                                                        <div
                                                                          className="hftNews"
                                                                        >
                                                                          <div
                                                                            className="grid_12_par_0 parsys"
                                                                          >
                                                                            <div
                                                                              className="remove-padding-grid"
                                                                            >
                                                                              <div
                                                                                className={`c14v1_static_content section}`}
                                                                              >
                                                                                <NewsListItem
                                                                                  items={articles}
                                                                                />
                                                                                <div
                                                                                  className="clearfix"
                                                                                />
                                                                              </div>
                                                                            </div>
                                                                          </div>
                                                                        </div>
                                                                      </div>
                                                                    </div>
                                                                    <div
                                                                      className="clearfix"
                                                                    />
                                                                  </div>
                                                                </div>
                                                              </div>
                                                            </div>
                                                          </div>
                                                        </div>
                                                        <div
                                                          className="clearfix"
                                                        />
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            ))
                                          } */}

                                        </div>
                                        <div className="clearfix" />
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div className="clearfix" />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="clearfix" />
              </div>
            </div>
            <Banner />
            { lang !== 'en' &&
            <div className="gridlayout parbase section">
              <div className="container">
                <div className="row ">
                  <div className="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                    {/* <Partner/> */}
                  </div>
                </div>
                <div className="clearfix" />
              </div>
            </div>
            }


          </div>
        </div>
      );
    }
}


const mapStateToProps = (state) => {
  const { core, report, news, config, home, basic } = state;

  let cat2 = basic.cats && basic.cats.slice(0, 2);

  return {
    core,
    homeTops: basic.tops,
    newTabs: cat2,
    topNews: home.topNews,
    topProjects: home.topProjects,
    topReports: home.topReports,
    lang: config.language,
    report: report.homeData,
    timelines: basic.timelines,
    mainSlide: core.mainSlide,
    topProject: core.topProject,
    marquee: news.marquee,
    bestProjects: core.bestProjects,
    latestProjects: core.bestProjects,
  };
};

const mapDispatch = {
  // loadTopProjects,
  // loadTopNews,
  // loadTopReport,
  loadReportHomeFilter,
  loadNewsHomeFilter,
};

export default connect(mapStateToProps, mapDispatch)(Home);

