export class ProjectService {
  constructor (server) {
    'ngInject';
    this.server = server
  }

  list (obj) {
    let str = Object.keys(obj).map(function(key){
      return encodeURIComponent(key) + '=' + encodeURIComponent(obj[key]);
    }).join('&');
    return this.server.get(`/project/list?${str}`)
  }
  get ({id}) {
    return this.server.get(`/project/edit/${id}`)
  }
  update (data) {
    return this.server.put(`/project/${data.id}`, data)
  }
  remove (data) {
    return this.server.delete(`/project/${data.id}`)
  }
  create (data) {
    return this.server.post(`/project`, data)
  }


}
