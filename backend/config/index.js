'use strict'
/**
 * Created with MacVim
 * User: enxtur
 * Date: 2016/10/19
 */
const env = process.env.NODE_ENV || 'development'
const PORT = process.env.PORT || 5000
const configs = {
  development: {
    APP_HOST: 'http://127.0.0.1:5001',
    APP_HTTP_PORT: PORT,
    APP_HTTPS_PORT: 443,
    REDIS_HOST: '127.0.0.1',
    REDIS_PORT: 6379,
    MYSQL_HOST: '127.0.0.1',
    MYSQL_PORT: 3306,
    MYSQL_DB: 'mnums',
    MYSQL_USER: 'root',
    MYSQL_PASS: 'root',
    MYSQLDUMP: '/usr/local/mysql/bin/mysqldump',
    BACKUP_DIR: '/Users/enxtur/Desktop',
    SESSION_KEY: 'dbm-sid',
    REDIS_SESSION_PREFIX: 'dbm:session:',
    ELASTIC_HOST: 'localhost:9200',
    RABBITMQ_HOST: 'localhost'
  },
  production: {
    APP_HOST: 'http://127.0.0.1:5001',
    APP_HTTP_PORT: PORT,
    APP_HTTPS_PORT: 443,
    REDIS_HOST: '127.0.0.1',
    REDIS_PORT: 6379,
    MYSQL_HOST: '127.0.0.1',
    MYSQL_PORT: 3306,
    MYSQL_DB: 'mnums',
    MYSQL_USER: 'delgee',
    MYSQL_PASS: 'pass',
    MYSQLDUMP: 'mysqldump',
    BACKUP_DIR: '/home/ubuntu/dbmbackup',
    SESSION_KEY: 'mnums-sid',
    REDIS_SESSION_PREFIX: 'mnums:session:',
    ELASTIC_HOST: 'localhost:9200',
    RABBITMQ_HOST: 'localhost'
  }
}
const config = configs[env]
if (!config) throw new Error(`bad NODE_ENV: ${env}`)
module.exports = config

