/**
 * Created by enxtur on 5/5/17.
 */

import { Project } from '../../../model'
import Err from '../../../modules/err'
export default async (req, res) => {
  const {id} = req.params
  let project = await Project.findById(id)
  if (!project) return res.callback(Err.notfound())
  res.callback(project)
}
