export function config ($logProvider, toastrConfig, uiSelectConfig, $httpProvider) {
  'ngInject';
  // Enable log
  $logProvider.debugEnabled(true);
  uiSelectConfig.theme = 'bootstrap';
  // Set options third-party lib
  toastrConfig.allowHtml = true;
  toastrConfig.timeOut = 3000;
  toastrConfig.positionClass = 'toast-top-right';
  toastrConfig.preventDuplicates = false;
  toastrConfig.progressBar = true;
  $httpProvider.defaults.withCredentials = true;
}
