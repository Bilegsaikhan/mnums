'use strict'
import { ProductImage } from '../../../model'
import Err from '../../../modules/err'

export default async function updateProductImage (req, res) {
  req.checkParams('id', 'bad_id').isInt()
  const err = req.validationErrors()
  if (err) return res.callback(err)
  const {id} = req.params

  if (req.body.is_main && req.body.is_main !== false) {
    req.body.is_main = (req.body.is_main === true || req.body.is_main === 'true')
  }

  let cat = await ProductImage.findById(id)
  if (!cat) return res.callback(Err.notfound())
  cat = await cat.updateAttributes(req.body)
  res.callback(cat)
}
