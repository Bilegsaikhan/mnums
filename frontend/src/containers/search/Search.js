import React, {Component} from 'react';
import {Link} from 'react-router'
import {connect} from 'react-redux'
import ReactPaginate from 'react-paginate';
import TextTruncate from 'react-text-truncate';
import {browserHistory} from 'react-router'
import './Search.css';
import {loadSearchResult} from '../../actions/search'
import language from "../../constants/language"

class Search extends Component {

    constructor(props) {
        super(props);

        this.state = {
            data: [],
            offset: 0
        }
    }

    componentDidMount() {
        this.maybeFetchData();
    }

    componentWillUpdate(nextProps) {
        if (nextProps.location.search !== this.props.location.search) this.maybeFetchData(nextProps);
    }

    maybeFetchData(props) {
        const {loadSearchResult, location: {search}} = props || this.props;
        loadSearchResult({params: search});
    }

    handlePageClick = (data) => {
        const {location: {query, pathname}} = this.props
        let search = {...query, offset: data.selected};
        browserHistory.push({
            pathname: pathname,
            query: search
        })
    };


    render() {

        const {result, total, location: {query}, isFetching, lang} = this.props

        let pageCount = total / 10
        let currentPage = query && parseInt(query.offset || 0, 10)
        return (
            <div>
                <div className="g01v1-this-page-in">
                    <div className="container">
                        <div className="row">
                            <div className="col-xs-5 col-sm-6">
                                <div className="breadcrumb_wbr wb_breadcrumb">
                                    <div className="g04v1-text">
                                        <ol className="breadcrumb">
                                            <li><Link to={`/`}>{language.homepage[lang]}</Link></li>
                                            <li>{language.searchResult[lang]}</li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="container">
                    <div className="titlepar parsys">
                        <div className="title section">
                            <div className="row">
                                <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div className="c01v1-page-title">{language.searchResult[lang]}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="par parsys">
                        <div className="gridlayout parbase section">
                            <div className="row">
                                <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div className="">
                                        <div className="row">

                                            <div className="col-xs-12 mrg-b-10">
                                                <div className="row">
                                                    <div className="col-xs-6 pad-t-10">
                                                        <label className="h7">
                                                            {lang === 'en' ?
                                                                `${total} results`
                                                                :
                                                                `Нийт ${total} илэрц байна.`
                                                            }

                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-12 col-sm-12 col-xs-12">
                                                <hr className="listing-hr"/>
                                                <div className={`projectLoading ${isFetching ? 'showLoading' : ''}`}>
                                                    <div className="showbox">
                                                        <div className="loader">
                                                            <svg className="circular" viewBox="25 25 50 50">
                                                                <circle className="path" cx="50" cy="50" r="20"
                                                                        fill="none" strokeWidth="2"
                                                                        strokeMiterlimit="10"/>
                                                            </svg>
                                                        </div>
                                                        {language.loading[lang]}...
                                                    </div>
                                                </div>
                                                <div className={`n07v4 dbmNewsList ${isFetching ? 'showLoading' : ''}`}>
                                                    <ul>

                                                        {
                                                            result.map(item => {
                                                                if (!item.title) return null
                                                                return (
                                                                    <li key={`newsItem_${item.type}${item.id}`}>
                                                                        <div id="n07v4-content">
                                                                            <div className="n07v4-title">
                                                                                <Link to={item.link}>
                                                                                    {item.title}
                                                                                </Link>
                                                                            </div>
                                                                            <div className="n07v4-blurb">
                                                                                { item.content &&
                                                                                    <TextTruncate
                                                                                        line={2}
                                                                                        truncateText="…"
                                                                                        text={item.content.replace(/<\/?[^>]+(>|$)/g, "")}
                                                                                        textTruncateChild={<Link
                                                                                            to={item.link}>{language.seeMore[lang]}</Link>}
                                                                                    />
                                                                                }
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                )
                                                            })
                                                        }
                                                    </ul>

                                                    { !result.length &&
                                                        <div className="search-result-not-found">
                                                            {language.noResult[lang]}
                                                        </div>
                                                    }
                                                </div>

                                                { total > 10 &&
                                                <ReactPaginate previousLabel={<i className="fa fa-arrow-left"/>}
                                                               nextLabel={<i className="fa fa-arrow-right"/>}
                                                               breakLabel={false}
                                                               breakClassName={"break-me"}
                                                               pageCount={pageCount}
                                                               forcePage={currentPage}
                                                               marginPagesDisplayed={5}
                                                               pageRangeDisplayed={5}
                                                               onPageChange={this.handlePageClick}
                                                               containerClassName={"pagination right"}
                                                               subContainerClassName={"pages pagination"}
                                                               activeClassName={"active"}/>
                                                }
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="clearfix"/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


const mapStateToProps = (state) => {
    const {core, search, basic, config} = state

    return {
        lang: config.language,
        basic,
        core,
        result: search.result,
        total: search.total,
        isFetching: search.isFetching
    }
}

export default connect(mapStateToProps, {
    loadSearchResult
})(Search)
