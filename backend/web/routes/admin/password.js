/**
 * Created by enxtur on 5/5/17.
 */

import { Admin } from '../../../model'
import Err from '../../../modules/err'
export default async (req, res) => {
  req.checkBody('password', 'bad_password').len(1, 255)
  const err = req.validationErrors()
  if (err) return res.callback(err)
  const {id} = req.params
  const {password} = req.body
  let admin = await Admin.findById(id)
  if (!admin) return res.callback(Err.notfound())
  await admin.updatePassword(password)
  res.callback()
}
