import React, {Component} from 'react';
import './Legislation.css';
import {connect} from "react-redux";
import language from "../../../constants/language"
import {loadLaws} from "../../../actions/laws";

class Legislation extends Component {

    componentDidMount() {
        this.maybeFetchData();
    }

    maybeFetchData() {
        const {loadLaws} = this.props;
        loadLaws('law')
    }

    render() {
        const {lang, data} = this.props;
        return (
            <div className="row dbmLaws">
                <div className="col-md-12 col-sm-12 col-lg-12 col-xs-12">
                    <div className="padding-right-20 center-right-border">
                        <div className="row dbm-page-content">
                            <div className="col-md-12">
                                <h4 className="text-uppercase mrg-b-15">{language.laws[lang]}</h4>

                                <div className="reportsThumbList">
                                    <ul className="row">
                                        {data && data.list.map(item => {
                                            return (
                                                <li
                                                    className="col-md-4"
                                                    key={`rtl_${item.id}`}
                                                >
                                                    <div className="reportsListItem book">
                                                        <a
                                                            target="_blank"
                                                            href={item.pdf}
                                                        >
                                                            <img
                                                                src={`/assets/images/${lang === 'en' ? 'logo-en.png' : 'main-logo.png'}`}
                                                                alt=""
                                                            />
                                                            <div className="fullname">
                                                                {item.name}
                                                            </div>
                                                        </a>
                                                    </div>
                                                </li>
                                            );
                                        })}
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    const {config, laws} = state;
    return {
        lang: config.language,
        data: laws.law
    };
};

const mapDispatch = {
    loadLaws
};

export default connect(mapStateToProps, mapDispatch)(Legislation);
