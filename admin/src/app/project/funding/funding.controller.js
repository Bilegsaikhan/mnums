export class ProjectFundingController {
  constructor ($log, fundingService, modalService) {
    'ngInject'
    this.log = $log.log
    this.service = fundingService
    this.modalService = modalService
    this.count = 0
    this.reload()
  }

  reload () {
    this.service.list().then(items => {
      this.clear()
      this.items = items
      this.count = items.length
    })

  }

  clear () {
    this.item = {
      id: '',
      name: '',
      name_en: ''
    }
  }

  edit (item) {
    this.item = Object.assign({}, item)
  }

  canSubmit () {
    return this.itemForm.$invalid
  }

  update () {
    return this.service.update(this.item)
  }

  remove () {
    this.modalService.danger('Та уг ангилалыг устгахдаа итгэлтэй байна уу', true).then(() => {
      this.service.delete(this.item.id).then(() => {
        this.reload()
      })
    })
  }

  create () {
    return this.service.create(this.item)
  }

  submit () {
    (this.item.id ? this.update() : this.create()).then(() => {
      this.reload()
    })
  }

  dragEnd () {
    const ids = this.items.map(item => item.id)
    return this.service.order({ids})
  }
}
