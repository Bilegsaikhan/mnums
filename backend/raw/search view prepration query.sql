-- select * from article where match(title,`desc`) against('МОНГОЛ');
CREATE VIEW search as
select 'article' as `type`, id, 		title,			   title_en, 				 content,                    content_en from article as ar
UNION
select    'page' as `type`, id, name as title, 	name_en as title_en, 				 content, 					 content_en from page 	 as pa
UNION
select 'project' as `type`, id, name as title, 	name_en as title_en, introduction as content, introduction_en as content_en from project as pr;
DROP VIEW search;
select * from search where title like '%МОНГОЛ%' OR content like '%МОНГОЛ%';
