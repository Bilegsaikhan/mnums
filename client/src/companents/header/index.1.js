import React, { Component } from 'react';
import { IoIosSearch } from 'react-icons/io';
import { Link } from 'react-router-dom';
import './style.scss';
import logo from '../../assets/logo.jpg';

export default class Header extends Component {
  render() {
    return (
      <header className="App-header">
        <div className="container d-flex">
          <Link id="logo" to="/">
            <img
              src={logo}
              width="100"
              alt="Т.Шагдарсүрэнгийн Нэрэмжит Анагаах Ухааны Хүрээлэн"
            />
          </Link>
          <div className="site-menu d-flex flex-column flex-grow-1 ml-3">
            <div className="sub-menu">
              <div className="nav-scroller py-1">
                <nav className="nav d-flex justify-content-between">
                  <a className="p-2 text-muted" href="#">
                    (+976) 70287809
                  </a>
                  <a className="p-2 text-muted" href="#">
                    Хамтрагч
                  </a>
                  <a className="p-2 text-muted" href="#">
                    Тусламж
                  </a>
                </nav>
              </div>
            </div>
            <div className="main-menu py-1 d-flex flex-column flex-md-row align-items-center">
              <div className="nav-scroller flex-grow-1">
                <nav className="nav d-flex justify-content-between">
                  <Link className="p-2" to="aboutus">
                    Бидний тухай
                  </Link>
                  <Link className="p-2" to="/event">
                    Бидний үйл ажиллагаа
                  </Link>
                  <Link className="p-2" to="shilendans">
                    Шилэн Данс
                  </Link>
                  <Link className="p-2" to="news">
                    Мэдээ мэдээлэл
                  </Link>
                  <Link className="p-2" to="contactus">
                    Холбоо барих
                  </Link>
                </nav>
              </div>
              <a className="px-2 ml-3" href="#">
                <IoIosSearch size="40px" />
              </a>
            </div>
          </div>
        </div>
      </header>
    );
  }
}
