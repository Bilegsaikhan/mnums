'use strict';

angular.module('furnitureAdminApp').directive(

  'ngFileSelect',

  ['$parse', '$timeout',

    function($parse, $timeout) {
      return function(scope, element, attr) {
        var fn = $parse(attr['ngFileSelect']);

        var listener = function(event) {
          fn(scope, {
            $files: event.target.files,
            $event: event
          });
        }

        element.bind('change', listener);
        element.bind('click', function() {
          this.value = null;
        });
      }
    }
  ])
