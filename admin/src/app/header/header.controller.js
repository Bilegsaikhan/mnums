export class HeaderController {
  constructor ($state, userService, modalService) {
    'ngInject';
    this.awesomeThings = [];
    this.$state = $state
    this.userService = userService
    this.modalService = modalService
  }

  logout () {
    this.modalService.danger('Та системээс гарахдаа итгэлтэй байна уу', true).then(() => {
      this.userService.logout().then(() => {
        this.$state.go('public.login')
      })
    })
  }
}
