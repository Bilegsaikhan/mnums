import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import './style.scss';

export class Page extends Component {
  static propTypes = {};

  render() {
    const { pageData } = this.props;

    return (
      <main role="main" className="container">
        <div className="row">
          <div className="col-md-8 blog-main">
            <div className="blog-post">
              <h2 className="blog-post-title">{pageData.title}</h2>
              <hr />
              <div
                dangerouslySetInnerHTML={{
                  __html: pageData.body
                }}
              />
            </div>
          </div>

          <aside className="col-md-4 blog-sidebar">
            <div className="p-3 mb-3 d-flex flex-column bg-light rounded">
              <Link to="/page/1">Захирлын мэндчилгээ »</Link>
              <Link to="/page/2">Танилцуулга »</Link>
              <Link to="/page/3">Эрхэм зорилго »</Link>
              <Link to="/page/4">Үйл ажиллагааны үндсэн чиг үүрэг »</Link>
              <Link to="/page/5">Бахархалт хүмүүс »</Link>
              <Link to="/page/6">Түүхэн замнал »</Link>
              <Link to="/page/7">АУХ-ийн эрдэмтэн, судлаачид »</Link>
            </div>
          </aside>
        </div>
      </main>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  const {
    global: { pageData }
  } = state;
  const {
    match: {
      params: { id }
    }
  } = ownProps;
  return {
    pageData: pageData[id] || {}
  };
};

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Page);
