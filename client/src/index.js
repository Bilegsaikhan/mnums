import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
// import "bootstrap/scss/bootstrap.scss";
import 'react-id-swiper/src/styles/scss/swiper.scss';
import './custom.scss';
import './index.scss';
// import "./kava.scss";
import App from './containers/App';
import configureStore from './store';
import * as serviceWorker from './serviceWorker';

const store = configureStore();

ReactDOM.render(
  <Router>
    <App store={store} />
  </Router>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
