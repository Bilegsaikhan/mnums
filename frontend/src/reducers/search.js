import {
    LOAD_SEARCH_RESULT, LOAD_SEARCH_RESULT_SUCCESS, LOAD_SEARCH_RESULT_ERROR,
} from "../constants";

const defaultState = {
    result: [],
    total: 0
};

// reducer
export default function searchResult(state = defaultState, action) {
    switch (action.type) {

        case LOAD_SEARCH_RESULT: {
            return {
                ...state,
                isFetching: true,
                error: null,
            };
        }
        case LOAD_SEARCH_RESULT_SUCCESS: {
            const { payload } = action
            return {
                ...state,
                result: payload.list,
                total: payload.total,
                isFetching: false,
            };
        }
        case LOAD_SEARCH_RESULT_ERROR: {
            return {
                ...state,
                error: action.error,
                isFetching: false,
            };
        }

        default: {
            return state;
        }
    }
}