export function UploadImageButtonDirective () {
  'ngInject';
  let directive = {
    restrict: 'EAC',
    templateUrl: 'app/components/uploadImage/uploadImage.html',
    replace: true,
    scope: {
      uploadDone: '=',
      remove: '=',
      uploadData: '=',
      scope: '=',
      ngModel: '='
    },
    controller: UploadImageController,
    controllerAs: 'vm',
    bindToController: true
  };
  return directive;
}

class UploadImageController {
  constructor ($log, server) {
    'ngInject';
    this.$log = $log.log
    this.server = server
  }

  uploadFiles (file/*, errFiles*/) {
    if (file) {
      this.server.uploadImage(file).then(res => {
        this.uploadDone.apply(this.scope, [res.url])
      })
    }
  }
}
