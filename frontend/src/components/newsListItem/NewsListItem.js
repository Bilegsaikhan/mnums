import React, { Component } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router";
import "./NewsListItem.css";
import TextTruncate from "react-text-truncate";
import moment from 'moment'

class NewsListItem extends Component {
  static propTypes = {
    items: PropTypes.array
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { items } = this.props;

    return (
      <section>
        <div className="c14v1-body c14v1-body-text flipboard-keep newsli">
          <div className="n02v5-body">
            <ul className="clearfix">
              {items.map((item, index) => {
                return (
                  <li className="media" key={`limc_${index}`}>
                    <div className="n02v5-text media-body">
                      <div className="hammer">{moment(item.created_at).format("YYYY.MM.DD")}</div>
                      <h5>
                        <Link to={`/news/${item.id}`}>
                          <TextTruncate
                            line={2}
                            truncateText="…"
                            text={item.title}
                          />
                        </Link>
                      </h5>
                    </div>
                  </li>
                );
              })}
            </ul>
          </div>
        </div>
      </section>
    );
  }
}

export default NewsListItem;
