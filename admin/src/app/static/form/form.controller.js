export class StaticFormController {
  constructor($log, staticService, $state, toastr, modalService) {
    'ngInject';
    this.$log = $log.log
    this.pageService = staticService
    this.toastr = toastr
    this.$state = $state
    this.stateName = $state.current.name
    this.modalService = modalService
    this.clear()
    this.sidebarData = [
      {
        name: 'Үйл явдал',
        value: 'event'
      }, {
        name: 'Төсөл хөтөлбөр',
        value: 'project'
      }, {
        name: 'Тайлан',
        value: 'report'
      }
    ]
    this.load()
  }

  clear() {
    this.page = {
      data: '',
      data_en: ''
    }
  }

  update() {
    this.pageService.update(this.page).then(page => {
      this.page = page
      this.toastr.success('Таны хүсэлт амжилттай!');
    }, () => {
      this.toastr.error('Таны хүсэлт амжилтгүй!');
    })
  }

  load() {
    this.pageService.get(this.$state.params.id).then(page => {
      this.page = page
    }, () => {
      this.toastr.error('Таны хүсэлт амжилтгүй!');
    })
  }


}
