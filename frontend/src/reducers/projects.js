import {
    LOAD_PROJECT_LIST, LOAD_PROJECT_LIST_SUCCESS, LOAD_PROJECT_LIST_ERROR,
    LOAD_PROJECT_ITEM, LOAD_PROJECT_ITEM_SUCCESS, LOAD_PROJECT_ITEM_ERROR
} from "../constants";

const defaultState = {
    data: [],
    detail: {}
};

// reducer
export default function projects(state = defaultState, action) {
    switch (action.type) {

        case LOAD_PROJECT_ITEM: {
            return {
                ...state,
                isItemFetching: true,
                error: null,
            };
        }
        case LOAD_PROJECT_ITEM_SUCCESS: {
            const { payload } = action
            return {
                ...state,
                detail: payload,
                isItemFetching: false,
            };
        }
        case LOAD_PROJECT_ITEM_ERROR: {
            return {
                ...state,
                error: action.error,
                isItemFetching: false,
            };
        }

        case LOAD_PROJECT_LIST: {
            return {
                ...state,
                isFetching: true,
                error: null,
            };
        }
        case LOAD_PROJECT_LIST_SUCCESS: {
            const { payload } = action
            return {
                ...state,
                data: payload.list,
                total: payload.total,
                isFetching: false,
            };
        }
        case LOAD_PROJECT_LIST_ERROR: {
            return {
                ...state,
                error: action.error,
                isFetching: false,
            };
        }
        
        case 'LOAD_PROJECT': {
            return {
                ...state,
                detail: state.indexedData[action.payload.id]
            }
        }
        case 'LOAD_PROJECT_FILTER': {
            return {
                ...state,
            }
        }
        case 'CLEAR_FILTER': {
            return {
                ...state
            }
        }
        default: {
            return state;
        }
    }
}