'use strict'
import { ProductCat, ProductSubCat } from '../../../model'

export default async function getCats(req, res) {
  const cats = await ProductCat.findAll({ order: '`order` ASC', include: [{ model: ProductSubCat, as: 'productSubCats' }] })
  res.callback(cats)
}
