'use strict'
import { ProductCat } from '../../../model'
import Err from '../../../modules/err'

export default async function updateCat (req, res) {
  req.checkParams('id', 'bad_id').isInt()
  req.checkBody('name', 'bad_name').len(1, 255)
  req.checkBody('name_en', 'bad_name_en').len(1, 255)
  const err = req.validationErrors()
  if (err) return res.callback(err)
  const {id} = req.params
  const {name, name_en} = req.body
  let cat = await ProductCat.findById(id)
  if (!cat) return res.callback(Err.notfound())
  cat = await cat.updateAttributes({name, name_en})
  res.callback(cat)
}
