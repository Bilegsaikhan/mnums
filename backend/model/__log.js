'use strict'
// import inc from './include'
const tableName = 'log'
const ignorePaths = ['/api/lang']
module.exports = function (sequelize, DataTypes) {
  return sequelize.define(tableName, {
    user_id: DataTypes.INTEGER,
    method: DataTypes.STRING,
    path: DataTypes.STRING,
    data: DataTypes.STRING
  }, {
    timestamps: true,
    classMethods: {
      add ({user_id, method, path, data}) {
        if (method === 'GET') return Promise.resolve(false)
        if (ignorePaths.indexOf(path) > -1) return Promise.resolve(false)
        return this.create({user_id, method, path, data}).then(() => true)
      },
      list ({offset = 0, limit = 10}) {
        // const include = inc(sequelize.models)
        const {admin: Admin} = sequelize.models
        return this.findAll({
          offset: offset * limit,
          limit: parseInt(limit),
          order: 'created_at DESC',
          include: [{
            model: Admin,
            as: 'admin',
            attributes: ['username']
          }]
        })
      }
    }
  })
}
