'use strict'
import { ArticleCopy as Article } from '../../../model'
import Err from '../../../modules/err'
import { format } from '../../../modules/formatter'

export default async (req, res, next) => {
  req.checkParams('id', 'bad_id').isInt()
  const err = req.validationErrors()
  if (err) return res.callback(err)
  const {id} = req.params
  let article = await Article.findById(id).then(o => o.get({plain: true}))
  if (!article) return res.callback(Err.notfound())
  // res.callback(article)
  // res.data = article
  res.callback(article)
  next()
}
