/**
 * Created by enxtur on 5/22/17.
 */
import { newsData } from '../raw/newsData'
import { Article } from '../model'
import moment from 'moment'
const debug = require('../modules/debug')(__dirname)
module.exports = () => {
  // newsData.forEach(article => {
  //   const date = moment(article.date, 'M-р сар. DD, YYYY').toDate()
  //
  //   debug(date)
  // })
  Article.bulkCreate(newsData.map(({title, text, type, date}, index) => {
    return {
      created_at: moment(date, 'M-р сар. DD, YYYY').toDate(),
      updated_at: moment(date, 'M-р сар. DD, YYYY').toDate(),
      cat_id: index <= 10 ? 1 : 2,
      title,
      desc: String(text).replace(/<[^>]+>/gm, '').substring(0, 255),
      content: text
    }
  })).then(() => {
    debug('success')
  }).catch((err) => {
    debug(err)
  })
}

