/**
 * Created by enxtur on 5/23/17.
 */
export function wrap (fn) {
  return (...args) => {
    const maybePromise = fn(...args)
    if (maybePromise && typeof maybePromise.catch === 'function') {
      maybePromise.catch(args[2])
    }
  }
}
