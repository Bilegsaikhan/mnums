import React, {Component, PropTypes} from 'react';
import {Link} from 'react-router'
import './ListItem.css';
import TextTruncate from 'react-text-truncate';

class ListItem extends Component {

    static propTypes = {
        items: PropTypes.array
    }

    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        const {items} = this.props

        return (
            <ul>
                {
                    items.map((item, index) => {
                        return (
                            <li key={`limc_${index}`}>
                                <Link to={`/news/${item.id}`}>
                                    <TextTruncate
                                        line={2}
                                        truncateText="…"
                                        text={item.title}
                                    />
                                </Link>
                            </li>
                        )
                    })
                }
            </ul>
        );
    }
}

export default ListItem
