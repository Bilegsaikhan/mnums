export class PdfController {
  constructor ($log, pdfService, modalService) {
    'ngInject'
    this.$log = $log.log
    this.pdfService = pdfService
    this.modalService = modalService
    this.count = 0
    this.reload()
  }

  reload () {
    this.pdfService.cats().then(res => {
      this.clear()
      this.pdfs = res.list
      this.count = res.total
    })

  }

  clear () {
    this.pdf = {
      id: '',
      name: '',
      thumb: '',
      url: ''
    }
  }

  edit (cat) {
    this.pdf = Object.assign({}, cat)
  }

  canSubmit () {
    return this.pdfForm.$invalid
  }

  update () {
    return this.pdfService.update(this.pdf)
  }

  remove () {
    this.modalService.danger('Та уг ангилалыг устгахдаа итгэлтэй байна уу', true).then(() => {
      this.pdfService.delete(this.pdf.id).then(() => {
        this.reload()
      })
    })
  }

  create () {
    return this.pdfService.create(this.pdf)
  }

  submit () {
    (this.pdf.id ? this.update() : this.create()).then(() => {
      this.reload()
    })
  }

  uploadMnPdfDone (url, thumb) {
    this.$log('upload mn pdf done')
    this.pdf.url = url
    this.pdf.thumb = thumb

  }

}
