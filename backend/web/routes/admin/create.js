/**
 * Created by enxtur on 5/5/17.
 */

import { Admin } from '../../../model'
import Err from '../../../modules/err'
export default async (req, res) => {
  req.checkBody('username', 'bad_username').len(1, 255)
  req.checkBody('password', 'bad_password').len(1, 255)
  const err = req.validationErrors()
  if (err) return res.callback(err)
  const {username, password} = req.body
  try {
    const admin = await Admin.create({username, password})
    res.callback(admin)
  } catch (err) {
    res.callback(Err.sequelize(err))
  }
}
