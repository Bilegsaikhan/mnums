import React, {Component} from 'react';
import './Social.css';
import {connect} from "react-redux";
import language from "../../constants/language"
import _find from "lodash/find";

class Social extends Component {
    render() {
        const {
            lang,
            facebook,
            youtube,
            twitter,
            linkedin
        } = this.props
        return (
            <div className="s03v2_stay_connected section">
                <div id="s03v2" className="">
                    <h6 className="mrg-b-15">{language.social[lang]}</h6>
                    <div className="s03v2-icon" style={{paddingBottom: '34px', paddingTop: '10px'}}>
                        <ul>
                            <li>
                                <a href={facebook && facebook.data} target="_blank" className="social-icon-img">
                                    <img src="/assets/images/svg/icons-facebook.svg" title="Image" alt="facebook"/>
                                </a>
                                <div className="s03v2-text">
                                    <a href={facebook && facebook.data} target="_blank"> <span>Facebook</span></a>
                                </div>
                            </li>
                            <li>
                                <a href={twitter && twitter.data} className="social-icon-img" target="_blank">
                                    <img src="/assets/images/svg/icons-twitter.svg" title="Image" alt="twitter"/>
                                </a>
                                <div className="s03v2-text">
                                    <a href={twitter && twitter.data} target="_blank"> <span>Twitter</span></a>
                                </div>
                            </li>
                            <li>
                                <a href={linkedin && linkedin.data} className="social-icon-img" target="_blank">
                                    <img src={`${process.env.PUBLIC_URL}/assets/images/svg/linkedln.svg`} height={25} title="linkedln" alt="linkedln"/>
                                </a>
                                <div className="s03v2-text">
                                    <a href={linkedin && linkedin.data} target="_blank"> <span>LinkedIn</span></a>
                                </div>
                            </li>
                            <li>
                                <a href={youtube && youtube.data} className="social-icon-img" target="_blank">
                                    <img src="/assets/images/svg/icons-youtube.svg" title="Image" alt="youtube"/>
                                </a>
                                <div className="s03v2-text">
                                    <a href={youtube && youtube.data} target="_blank"> <span>Youtube</span></a>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div className="clearfix"/>
                </div>
                <div className="clearfix"/>
            </div>
        );
    }
}


const mapStateToProps = (state) => {
    const {config, basic} = state;
    const facebook = _find(basic.stores, { key: 'facebook' });
    const youtube = _find(basic.stores, { key: 'youtube' });
    const twitter = _find(basic.stores, { key: 'twitter' });
    const linkedin = _find(basic.stores, { key: 'linkedin' });

    return {
        lang: config.language,
        facebook: facebook,
        youtube: youtube,
        twitter: twitter,
        linkedin: linkedin
    };
};

const mapDispatch = {};

export default connect(mapStateToProps, mapDispatch)(Social);

