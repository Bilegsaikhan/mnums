'use strict'
import { Page } from '../../../model'
import Err from '../../../modules/err'

export default async function updatePage (req, res) {
  req.checkBody('name', 'bad_name').len(1, 255)
  req.checkBody('name_en', 'bad_name_en').len(1, 255)
  req.checkBody('content', 'bad_content')
  req.checkBody('content_en', 'bad_content_en')
  if (req.body.sidebar) {
    req.checkBody('sidebar', 'bad_sidebar').isIn(['event', 'project', 'report'])
  }
  const err = req.validationErrors()
  if (err) return res.callback(err)
  const {id} = req.params
  const {name, name_en, content, content_en, sidebar} = req.body
  let page = await Page.findById(id)
  if (!page) return res.callback(Err.notfound())
  page = await page.updateAttributes({name, name_en, content, content_en, sidebar})
  res.callback(page)
}
