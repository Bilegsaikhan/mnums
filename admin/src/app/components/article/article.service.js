  // ?offset=0&limit=2&cat_id=1&string=mon
export class ArticleService {
  constructor (server) {
    'ngInject';
    this.server = server
  }

  list (obj) {
    let str = Object.keys(obj).map(function(key){
      return encodeURIComponent(key) + '=' + encodeURIComponent(obj[key]);
    }).join('&');
    return this.server.get(`/article/list?${str}`)
  }
  get ({id}) {
    return this.server.get(`/article/edit/${id}`)
  }
  update (data) {
    return this.server.put(`/article/${data.id}`, data)
  }
  remove (data) {
    return this.server.delete(`/article/${data.id}`)
  }
  create (data) {
    return this.server.post(`/article`, data)
  }
}
