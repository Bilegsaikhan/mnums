'use strict'
import conf from './index'
import session from 'express-session'
import connectRedis from 'connect-redis'

const RedisStore = connectRedis(session)
const store = new RedisStore({host: conf.REDIS_HOST, port: conf.REDIS_PORT, prefix: conf.REDIS_SESSION_PREFIX})
const maxAge = 30 * 24 * 60 * 60 * 1000
export default {
  name: 'ubtrade-session',
  cookie: {maxAge},
  secret: 'panasonic',
  saveUninitialized: true,
  resave: true,
  store,
  key: conf.SESSION_KEY
}
