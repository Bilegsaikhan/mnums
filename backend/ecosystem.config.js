module.exports = {
  apps: [{
    name: 'mnums',
    script: '/root/mnums/backend/index.js',
    interpreter: 'node@8.9.0',
    exec_mode: 'fork',
    args: ['--color'],
    env: {
      NODE_ENV: 'production',
      DEBUG: 'mnums:*',
      DEBUG_COLORS: true,
      PORT: 5001
    }
  }]
}
