'use strict'
import { Job as Model } from '../../../model'
import Err from '../../../modules/err'

export default ({router, passport, checkLoggedIn, multipartMiddleware, wrap, checkRootAuth}) => {
  router.route('/job')
    .post(checkLoggedIn, wrap(async (req, res) => {
      const instance = await Model.create({
        position: req.body.position,
        department: req.body.department
      })
      res.callback(instance)
    }))
    .get(wrap(async (req, res) => {
      const list = await Model.findAll({order: '`order` ASC'})
      res.callback(list)
    }))
  router.route('/job/order')
    .put(wrap(async (req, res) => {
      if (!req.body.ids || !Array.isArray(req.body.ids)) return res.callback(Err.badRequest('bad ids'))
      const ids = req.body.ids
      await Model.updateOrder(ids)
      res.callback()
    }))
  router.route('/job/:id')
    .delete(wrap(async (req, res) => {
      const instance = await Model.findById(req.params.id)
      if (!instance) return res.callback(Err.notfound())
      await instance.destroy()
      res.callback()
    }))
    .get(checkLoggedIn, wrap(async (req, res) => {
      const instance = await Model.findById(req.params.id)
      if (!instance) return res.callback(Err.notfound())
      res.callback(instance)
    }))
    .put(wrap(async (req, res) => {
      const instance = await Model.findById(req.params.id)
      if (!instance) return res.callback(Err.notfound())
      const updatedInstance = await instance.updateAttributes({
        position: req.body.position,
        department: req.body.department
      })
      res.callback(updatedInstance)
    }))
}
