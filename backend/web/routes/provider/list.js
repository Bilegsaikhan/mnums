'use strict'
import { Provider } from '../../../model'

export default async function getProviders (req, res) {
  const cats = await Provider.findAll({order: '`order` ASC'})
  res.callback(cats)
}
