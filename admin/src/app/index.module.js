/* global malarkey:false, moment:false */
import { config } from './index.config';
import { routerConfig } from './index.route';
import { runBlock } from './index.run';
import { PublicController } from '../app/public/public.controller';
import { LoginController } from '../app/login/login.controller';
import { ModalController } from '../app/components/modal/modal.controller';
import { RootController } from './root/root.controller';
import { ContentController } from './content/content.controller';
import { HeaderController } from './header/header.controller';
import { FooterController } from './footer/footer.controller';
import { SidebarController } from './sidebar/sidebar.controller';
import { MenuController } from './menu/menu.controller';
import { HomeController } from './home/home.controller';
import { AboutController } from './about/about.controller';

import { ArticleController } from './article/article.controller';
import { ArticleListController } from './article/list/list.controller';
import { ArticleFormController } from './article/form/form.controller';
import { ArticleCatController } from './article/cat/cat.controller';

import { ProjectController } from './project/project.controller';
import { ProjectListController } from './project/list/list.controller';
import { ProjectFormController } from './project/form/form.controller';
import { ProjectSectorController } from './project/sector/sector.controller';
import { ProjectRegionController } from './project/region/region.controller';
import { ProjectFundingController } from './project/funding/funding.controller';

import { UserController } from './user/user.controller';
import { UserFormController } from './user/form/form.controller';
import { UserListController } from './user/list/list.controller';

import { ReportController } from './report/report.controller';
import { ReportFormController } from './report/form/form.controller';
import { ReportListController } from './report/list/list.controller';

import { LawsController } from './laws/laws.controller';
import { LawsFormController } from './laws/form/form.controller';
import { LawsListController } from './laws/list/list.controller';

import { LogsController } from './logs/logs.controller';
import { LogsListController } from './logs/list/list.controller';

import { PageController } from './page/page.controller';
import { PageFormController } from './page/form/form.controller';
import { ListPageController } from './page/list/list.controller';

import { BannerController } from './banner/banner.controller';

import { StaticController } from './static/static.controller';
import { StaticFormController } from './static/form/form.controller';
import { StaticListController } from './static/list/list.controller';

import { SlideController } from './slide/slide.controller';
import { SlideFormController } from './slide/form/form.controller';
import { ListSlideController } from './slide/list/list.controller';

import { TimelineController } from './timeline/timeline.controller';
import { TimelineFormController } from './timeline/form/form.controller';
import { ListTimelineController } from './timeline/list/list.controller';

import { PdfController } from './pdf/pdf.controller';
import { fileBrowserController } from './fileBrowser/fileBrowser.controller';
import { BackUpController } from './backup/backup';

import { GithubContributorService } from '../app/components/githubContributor/githubContributor.service';
import { ServerService } from '../app/components/server/server.service';
import { UserService } from '../app/components/user/user.service';
import { ReportService } from '../app/components/report/report.service';
import { LawsService } from './components/laws/laws.service';
import { LogsService } from './components/logs/logs.service';
import { SliderService } from '../app/components/slider/slider.service';
import { MilestoneService } from '../app/components/milestone/milestone.service';
import { CatService } from '../app/components/cat/cat.service';
import { SectorService } from '../app/components/project/sector.service';
import { RegionService } from '../app/components/project/region.service';
import { FundingService } from '../app/components/project/funding.service';
import { MenuService } from '../app/components/menu/menu.service';
import { PageService } from '../app/components/page/page.service';
import { StaticService } from './components/static/static.service';
import { ArticleService } from '../app/components/article/article.service';
import { ProjectService } from '../app/components/project/project.service';
import { PdfService } from '../app/components/pdf/pdf.service';
import { ModalService } from '../app/components/modal/modal.service';
import { WebDevTecService } from '../app/components/webDevTec/webDevTec.service';
import { NavbarDirective } from '../app/components/navbar/navbar.directive';
import { MalarkeyDirective } from '../app/components/malarkey/malarkey.directive';
import { PaginateDirective } from '../app/components/paginate/paginate';
import { UploadPdfDirective } from '../app/components/uploadPdf/uploadPdf.directive';
import { UploadImageDirective } from '../app/components/uploadImage/uploadImage.directive';
import { UploadImageButtonDirective } from '../app/components/uploadImageButton/uploadImage.directive';

import { htmlToPlaintext } from '../app/components/filters/htmlToText';

angular.module('dbmAdmin', [
  'ngFileUpload',
  'ckeditor',
  'dndLists',
  'ngAnimate',
  'ui.router',
  'ui.bootstrap',
  'toastr',
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngTouch',
  'ngAria',
  'ngMaterial',
  'ngclipboard',
  'textAngular',
  'angular-loading-bar',
  'ui.select',
  'angular-redactor'
])

  .constant('malarkey', malarkey)
  .constant('moment', moment)
  .config(config)
  .config(routerConfig)
  .run(runBlock)
  .service('githubContributor', GithubContributorService)
  .service('webDevTec', WebDevTecService)
  .service('modalService', ModalService)
  .service('userService', UserService)
  .service('sliderService', SliderService)
  .service('server', ServerService)
  .service('catService', CatService)
  .service('menuService', MenuService)
  .service('pageService', PageService)
  .service('staticService', StaticService)
  .service('articleService', ArticleService)
  .service('projectService', ProjectService)
  .service('sectorService', SectorService)
  .service('regionService', RegionService)
  .service('fundingService', FundingService)
  .service('milestoneService', MilestoneService)
  .service('reportService', ReportService)
  .service('lawsService', LawsService)
  .service('pdfService', PdfService)
  .service('logsService', LogsService)

  .controller('PublicController', PublicController)
  .controller('LoginController', LoginController)
  .controller('ModalController', ModalController)
  .controller('RootController', RootController)
  .controller('ContentController', ContentController)
  .controller('HeaderController', HeaderController)
  .controller('FooterController', FooterController)
  .controller('SidebarController', SidebarController)
  .controller('MenuController', MenuController)
  .controller('HomeController', HomeController)
  .controller('AboutController', AboutController)

  .controller('UserController', UserController)
  .controller('UserFormController', UserFormController)
  .controller('UserListController', UserListController)

  .controller('LawsController', LawsController)
  .controller('LawsListController', LawsListController)
  .controller('LawsFormController', LawsFormController)

  .controller('LogsController', LogsController)
  .controller('LogsListController', LogsListController)

  .controller('ReportController', ReportController)
  .controller('ReportListController', ReportListController)
  .controller('ReportFormController', ReportFormController)

  .controller('PageController', PageController)
  .controller('PageFormController', PageFormController)
  .controller('ListPageController', ListPageController)

  .controller('BannerController', BannerController)

  .controller('StaticController', StaticController)
  .controller('StaticFormController', StaticFormController)
  .controller('StaticListController', StaticListController)

  .controller('SlideController', SlideController)
  .controller('SlideFormController', SlideFormController)
  .controller('ListSlideController', ListSlideController)

  .controller('TimelineController', TimelineController)
  .controller('TimelineFormController', TimelineFormController)
  .controller('ListTimelineController', ListTimelineController)

  .controller('ArticleController', ArticleController)
  .controller('ArticleListController', ArticleListController)
  .controller('ArticleFormController', ArticleFormController)
  .controller('ArticleCatController', ArticleCatController)

  .controller('ProjectController', ProjectController)
  .controller('ProjectListController', ProjectListController)
  .controller('ProjectFormController', ProjectFormController)
  .controller('ProjectSectorController', ProjectSectorController)
  .controller('ProjectRegionController', ProjectRegionController)
  .controller('ProjectFundingController', ProjectFundingController)

  .controller('PdfController', PdfController)
  .controller('fileBrowserController', fileBrowserController)
  .controller('BackUpController', BackUpController)

  .directive('acmeNavbar', NavbarDirective)
  .directive('acmeMalarkey', MalarkeyDirective)
  .directive('paginate', PaginateDirective)
  .directive('ngUploadPdf', UploadPdfDirective)
  .directive('ngUploadImage', UploadImageDirective)
  .directive('ngUploadImageButton', UploadImageButtonDirective)

  .filter('htmlToPlaintext', htmlToPlaintext)
