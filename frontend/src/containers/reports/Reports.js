import React, {Component} from "react";
import {connect} from "react-redux";
import "./Reports.css";
import language from "../../constants/language"
import {Link} from 'react-router'
import {loadReportList} from '../../actions/report';

class Reports extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false
        };
    }

    componentDidMount() {
        this.maybeFetchData()
    }

    componentWillUpdate(nextProps) {
        let {location} = nextProps

        if (location.search !== this.props.location.search) {
            this.maybeFetchData(nextProps)
        }
    }

    maybeFetchData(nextProps) {
        const {loadReportList, location: {search}} = nextProps || this.props;
        loadReportList({params: search});
    }


    searchReport() {
    }

    render() {
        const {reports, lang, location: {query}} = this.props;
        return (
            <div>
                <div className="g01v1-this-page-in">
                    <div className="container">
                        <div className="row">
                            <div className="col-xs-5 col-sm-6">
                                <div className="breadcrumb_wbr wb_breadcrumb">
                                    <div className="g04v1-text">
                                        <ol className="breadcrumb">
                                            <li><Link to={`/`}>{language.homepage[lang]}</Link></li>
                                            <li><a>{language.reports[lang]}</a></li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="container">
                    <div className="titlepar parsys">
                        <div className="title section">
                            <div className="row">
                                <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div className="c01v1-page-title">{language.reports[lang]}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="par parsys">
                        <div className="gridlayout parbase section">
                            <div className="row">
                                <div className="col-lg-3 col-md-3 col-sm-3 col-xs-12 hidden-xs">
                                    <div className="panel panel-default left-column refine-by-nav hidden-xs hidden-sm">
                                        <div className="panel-heading header">
                                            <div className="direction">{language.category[lang]} :</div>
                                        </div>
                                        <div className="panel-body">
                                            <ul>
                                                <li className="h7">{language.type[lang]}</li>
                                                <li>
                                                    <div className="sub-link text-uppercase">
                                                        <Link className={`active`} to={{
                                                            pathname: 'reports',
                                                            query: {...query, type: 'audit'}
                                                        }}>{language.audited[lang]}</Link>
                                                        {query.type === 'audit' &&
                                                        <Link to={{
                                                            pathname: 'reports',
                                                            query: {...query, type: undefined}
                                                        }}
                                                              className="glyphicon glyphicon-remove-circle mrg-l-5 delete-item">&nbsp;
                                                        </Link>
                                                        }
                                                    </div>
                                                </li>
                                                <li>
                                                    <div className="sub-link text-uppercase">
                                                        <Link className={`active`} to={{
                                                            pathname: 'reports',
                                                            query: {...query, type: 'year'}
                                                        }}>{language.annual[lang]}</Link>
                                                        {query.type === 'year' &&
                                                        <Link to={{
                                                            pathname: 'reports',
                                                            query: {...query, type: undefined}
                                                        }}
                                                              className="glyphicon glyphicon-remove-circle mrg-l-5 delete-item">&nbsp;
                                                        </Link>
                                                        }
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                    <div className="center-padding-left center-left-border">
                                        <div className="row">
                                            <div className="col-md-12 col-sm-12 col-lg-12 col-xs-12">
                                                <div className="">
                                                    <div className="row dbm-page-content">
                                                        <div className="col-md-12">
                                                            <h4 className="text-uppercase mrg-b-15">
                                                                {language.reports[lang]}
                                                            </h4>
                                                            <div className="reportsThumbList">
                                                                <ul className="row">
                                                                    {reports.map(item => {
                                                                        return (
                                                                            <li
                                                                                className="col-md-4"
                                                                                key={`rtl_${item.id}`}
                                                                            >
                                                                                <div className="reportsListItem book">
                                                                                    <a
                                                                                        target="_blank"
                                                                                        href={item.pdf}
                                                                                    >
                                                                                        <div className="reportYear">
                                                                                            {item.year}
                                                                                        </div>
                                                                                        <img
                                                                                            src={`/assets/images/${lang === 'en' ? 'logo-en.png' : 'main-logo.png'}`}
                                                                                            alt=""
                                                                                        />
                                                                                        <div className="fullname">
                                                                                            {item.name}
                                                                                        </div>
                                                                                    </a>
                                                                                </div>
                                                                            </li>
                                                                        );
                                                                    })}
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="clearfix"/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    const {core, report, config} = state;

    return {
        core,
        lang: config.language,
        reports: report.data
    }
};

export default connect(mapStateToProps, {
    loadReportList
})(Reports);
