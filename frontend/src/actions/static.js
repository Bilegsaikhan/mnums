import {LOAD_STATIC, LOAD_STATIC_SUCCESS, LOAD_STATIC_ERROR} from "../constants";

export function loadStatic(key) {
    return async (dispatch, getState, {fetch}) => {
        // eslint-disable-next-line no-param-reassign
        const payload = {};

        // console.log('key: ', key)
        // console.log('key: ', qs.stringify({key}))

        dispatch({
            type: LOAD_STATIC,
            payload
        });

        try {
            let response = await fetch(`store/${key}`, {
                method: "GET",
                credentials: 'include'
            });

            let data = await response.json();
            if (!response.ok) {
                const message = data.message || data;
                if (data) throw message;
            }
            dispatch({
                type: LOAD_STATIC_SUCCESS,
                payload: data
            });
            return data;
        } catch (error) {
            const message = error.message || error;
            dispatch({
                type: LOAD_STATIC_ERROR,
                error: error.message
            });
            throw new Error(message);
        }
    };
}
