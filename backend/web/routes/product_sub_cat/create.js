'use strict'
import { ProductSubCat } from '../../../model'
import Err from '../../../modules/err'

export default async function createSubCat (req, res) {
  req.checkBody('name', 'bad_name').len(1, 255)
  req.checkBody('name_en', 'bad_name_en').len(1, 255)
  req.checkBody('product_cat_id', 'bad_product_cat_id').len(1, 255)
  const err = req.validationErrors()
  if (err) return res.callback(err)
  try {
    const cat = await ProductSubCat.create({
      name: req.body.name,
      name_en: req.body.name_en,
      product_cat_id: req.body.product_cat_id
    })
    res.callback(cat)
  } catch (err) {
    res.callback(Err.sequelize(err))
  }
}
