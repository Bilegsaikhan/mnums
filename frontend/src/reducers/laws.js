import {LOAD_LAWS, LOAD_LAWS_SUCCESS, LOAD_LAWS_ERROR} from "../constants";

const defaultState = {};

// reducer
export default function laws(state = defaultState, action) {
    switch (action.type) {
        case LOAD_LAWS: {
            return { ...state };
        }
        case LOAD_LAWS_SUCCESS: {
            const {payload, key} = action
            return {
                ...state,
                [key]: payload
            };
        }

        case LOAD_LAWS_ERROR: {
            return { ...state };
        }


        default: {
            return state;
        }
    }
}
