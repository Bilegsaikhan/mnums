export function runBlock ($log, $rootScope, $state, userService) {
  'ngInject';
  // $log.debug('runBlock end');
  const stateChangeHandler = $rootScope.$on('$stateChangeError', (event, toState, toParams, fromState, fromData, rejection) => {
    $log.log('toState', toState)
    event.preventDefault()
    $log.log('Rejection', rejection)
    if (rejection === 'logged') {
      return $state.go('root.content.home')
    } else if (rejection === 'login') {
      return $state.go('public.login')
    } else if (rejection === 'ACCESS_DENIED') {
      return $state.go('root.content.home')
    }
  })
  $rootScope.$on('$destroy', stateChangeHandler)
  $rootScope.profile = userService.profile
}
