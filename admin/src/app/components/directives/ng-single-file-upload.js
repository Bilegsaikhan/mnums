'use strict';

angular.module('furnitureAdminApp').directive(

  'ngSingleFileUpload',

  ['$timeout', 'HelperService', 'UserService', '$upload', 'Configure', 'ErrorHandler',

    function($timeout, helperService, userService, $upload, config, errorHandler) {
      return {
        restrict: 'EA',
        scope: {
          file: '=ngModel',
          data: '=data',
          loading: '=loading'
        },
        require: 'ngModel',
        templateUrl: 'views/partials/ng-single-file-upload.html',
        link: function(scope, element, attrs) {
          scope.helpers = helperService;

          $('#fileDraggableElement').bind({
            dragover: function(event) {
              if (event != null)
                event.preventDefault();

              $(this).addClass('draggable');
              event.originalEvent.dataTransfer.effectAllowed = 'copy';

              return false;
            },
            dragenter: function(event) {
              if (event != null)
                event.preventDefault();

              $(this).removeClass('draggable');
              event.originalEvent.dataTransfer.effectAllowed = 'copy';

              return false;
            },
            dragleave: function(event) {
              if (event != null)
                event.preventDefault();

              $(this).removeClass('draggable');
              event.originalEvent.dataTransfer.effectAllowed = 'copy';

              return false;
            },
            drop: function(event) {
              if (event != null)
                event.preventDefault();

              $(this).removeClass('draggable');
              scope.handleFile(event.originalEvent.dataTransfer.files);

              return false;
            }
          });

          attrs.name ? scope.label = attrs.name : scope.label = 'Зураг';

          scope.selectFile = function() {
            $timeout(function() {
              element.find('#fileSelectElement').click();
            });
          }

          var validMimeTypes = attrs.acceptFileType;
          var validFileSize = attrs.maxFileSize;

          scope.handleFile = function(files) {
            if (!files || !files.length) return;

            var myFile = files[0];

            scope.$apply(function() {
              helperService.toaster('loading', true);
            });

            var name, reader, size, type;
            reader = new FileReader();

            reader.onload = function(event) {
              name = myFile.name;
              type = myFile.type;
              size = myFile.size;

              if (checkSizeFn(validFileSize, size) && isTypeValidFn(validMimeTypes, type)) {
                scope.$apply(function() {

                  var data = {
                    type: type,
                    size: size,
                    result: helperService.isImage(type) ? event.target.result : null,
                    progress: 0,
                    status: null,
                    loading: true
                  };

                  scope.file = data;
                  scope.loading = true;

                  helperService.toaster('loading', false);

                  var tempLink = config._APP_HOST + ':' + config._APP_PORT + '/rest/blog/image_upload';

                  if (attrs.link)
                    tempLink = config._APP_HOST + ':' + config._APP_PORT + attrs.link;

                  $upload.upload({
                    url: tempLink,
                    file: myFile
                  }).progress(function(event) {
                    var progressPercentage = parseInt(100.0 * event.loaded / event.total);
                    data.progress = progressPercentage >= 100 ? null : progressPercentage;
                  }).error(function(response, status, headers, config) {
                    data.loading = false;
                    data.status = 'error';

                    data.id = null;
                  }).success(function(response, status, headers, config) {
                    data.loading = false;
                    data.status = 'success';

                    data.link = response.data.link ? response.data.link : null;
                    data.pdf = response.data.pdf ? response.data.pdf : null;
                  });
                })
              }
            }
            reader.readAsDataURL(myFile);
          }
          function checkSizeFn(maxFileSize, size) {
            var _ref;
            if (((_ref = maxFileSize) === (void 0) || _ref === '') || (size / 1024) / 1024 < maxFileSize) {
              return true;
            } else {
              scope.bad_image = true;
              errorHandler.toast('error', 'Файлын хэмжээ хамгийн ихдээ [' + maxFileSize + 'MB].');
              helperService.toaster('error', 'Файлын хэмжээ хамгийн ихдээ [' + maxFileSize + 'MB].');
              console.log('checkSizeFn:', maxFileSize)
              return false;
            }
          }

          function isTypeValidFn(validMimeTypes, type) {
            if (validMimeTypes === '****' && _.indexOf([].concat(helperService.mimes.gif, helperService.mimes.jpeg, helperService.mimes.jpg, helperService.mimes.jpe, helperService.mimes.png, helperService.mimes.mp4, helperService.mimes.mp3, helperService.mimes.docx, helperService.mimes.xlsx, helperService.mimes.pptx, helperService.mimes.xls, helperService.mimes.ppt, helperService.mimes.doc, helperService.mimes.pdf, helperService.mimes.zip, helperService.mimes.rar), type) != -1) {
              return true;
            }

            if ((validMimeTypes === (void 0) || validMimeTypes === '') || type && validMimeTypes.indexOf(type) > -1) {
              return true;
            } else {
              scope.bad_image = true;
              errorHandler.toast('error', 'Файлын төрөл зөвхөн ' + validMimeTypes + '.');
              helperService.toaster('error', 'Файлын төрөл зөвхөн ' + validMimeTypes + '.');
              console.log('isTypeValidFn:', validMimeTypes);
              return false;
            }
          }
        }
      }
    }
  ])
