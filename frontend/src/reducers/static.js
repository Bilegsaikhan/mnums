import {LOAD_STATIC, LOAD_STATIC_SUCCESS, LOAD_STATIC_ERROR} from "../constants";

const defaultState = {};

// reducer
export default function staticReducer(state = defaultState, action) {
    switch (action.type) {
        case LOAD_STATIC: {
            return { ...state };
        }
        case LOAD_STATIC_SUCCESS: {
            const {payload} = action
            return {
                ...state,
                [payload.key]: payload
            };
        }

        case LOAD_STATIC_ERROR: {
            return { ...state };
        }


        default: {
            return state;
        }
    }
}
