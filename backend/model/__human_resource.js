'use strict'
const tableName = 'human_resource'
module.exports = function (sequelize, DataTypes) {
  return sequelize.define(tableName, {
    image: DataTypes.STRING,
    mn: DataTypes.BOOLEAN,
    en: DataTypes.BOOLEAN,
    name: DataTypes.STRING,
    name_en: DataTypes.STRING,
    desc: DataTypes.STRING,
    desc_en: DataTypes.STRING,
    content: DataTypes.STRING,
    content_en: DataTypes.STRING,
    link: DataTypes.STRING,
    order: DataTypes.INTEGER,
    type: DataTypes.ENUM('uz', 'za', 'sa', 'ubs')
  }, {
      classMethods: {
        async updateOrder(ids) {
          const idsJoined = ids.join()
          const query1 = 'set @ordering=0;'
          const query2 = `update ${tableName} set \`order\`=(@ordering:=@ordering+10) order by field(id,${idsJoined});`
          await sequelize.query(query1, { type: sequelize.QueryTypes.UPDATE })
          await sequelize.query(query2, { type: sequelize.QueryTypes.UPDATE })
        }
      }
    })
}
