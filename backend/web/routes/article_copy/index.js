'use strict'
const debug = require('../../../modules/debug')(__filename)
import create from './create'
import update from './update'
import get from './get'
import edit from './edit'
import del from './delete'
import list from './list'
import all from './all'
export default ({router, passport, checkLoggedIn, multipartMiddleware, wrap, checkRootAuth}) => {
  router.route('/article_copy')
    .get(wrap(all)) /* frontend */
    .post(checkLoggedIn, wrap(create))
  router.route('/article_copy/list')
    .get(wrap(list))
  router.route('/article_copy/edit/:id')
    .get(wrap(edit))
  router.route('/article_copy/:id')
    .put(checkLoggedIn, wrap(update))
    .get(wrap(get)) /* frontend */
    .delete(checkLoggedIn, wrap(del))
}
