'use strict'
import { ProductImage } from '../../../model'

export default async function getProductImages (req, res) {
  const cats = await ProductImage.findAll()
  res.callback(cats)
}
