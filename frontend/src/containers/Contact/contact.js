import React, {Component} from 'react';
import GoogleMap from '../../components/googleMap'
import './Contact.css';
import language from "../../constants/language"
import {connect} from "react-redux";
import {loadStatic} from "../../actions/static";

class Contact extends Component {

    componentDidMount() {
        this.maybeFetchData();
    }

    maybeFetchData() {
        const {loadStatic} = this.props;
        loadStatic('address')
        loadStatic('application')
        loadStatic('contact')
        // loadStatic(['address', 'application', 'contact'])
    }

    render() {
        const {lang, staticReducer} = this.props
        return (
            <div>
                <div className="container">
                    <br/>
                    <div className="title section">
                        <div className="row">
                            <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h6 className="c01v1-page-title">{language.contactus[lang]}</h6>
                            </div>
                        </div>
                    </div>
                </div>
                <GoogleMap/>
                <div className="container">
                    <div className="titlepar parsys">
                        <div className="gridlayout parbase section">
                            <div className="row">
                                <div className="col-md-12">
                                    <div>
                                        <div className="grid_12_par_0 parsys">
                                            <div className="remove-padding-grid">
                                                <br/>
                                                <div className="tabctrl section">
                                                    <div className="c17v4">
                                                        <div className="tab-content">
                                                            <div className="brd tab-pane tabcontent3 active">
                                                                <div className="tab-3 parsys">
                                                                    <div className="gridlayout parbase section">
                                                                        <div className="row">
                                                                            <div
                                                                                className="col-md-6 col-sm-12 col-lg-6 col-xs-12">
                                                                                <div className="center-right-border">
                                                                                    <div
                                                                                        className="grid_6_par_0 parsys">
                                                                                        <div
                                                                                            className="remove-padding-grid">
                                                                                            <div
                                                                                                className="c14v1_static_content section">
                                                                                                <section >
                                                                                                    <div
                                                                                                        className="c14v1-body c14v1-body-text flipboard-keep">
                                                                                                        <h6>
                                                                                                            <b>{language.address[lang]}:</b>
                                                                                                        </h6>

                                                                                                        <div
                                                                                                            dangerouslySetInnerHTML={{
                                                                                                                __html: staticReducer.address && staticReducer.address.data
                                                                                                            }}
                                                                                                        />
                                                                                                    </div>
                                                                                                </section>
                                                                                                <div
                                                                                                    className="clearfix"/>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                className="col-md-6 col-sm-12 col-lg-6 col-xs-12">
                                                                                <div>
                                                                                    <div
                                                                                        className="grid_6_par_1 parsys">
                                                                                        <div
                                                                                            className="remove-padding-grid">
                                                                                            <div
                                                                                                className="c14v1_static_content section">
                                                                                                <section >
                                                                                                    <div
                                                                                                        className="c14v1-body c14v1-body-text flipboard-keep ">
                                                                                                        <h6>
                                                                                                            <b>{language.clerk[lang]}:</b>
                                                                                                        </h6>

                                                                                                        <div
                                                                                                            dangerouslySetInnerHTML={{
                                                                                                                __html: staticReducer.application && staticReducer.application.data
                                                                                                            }}
                                                                                                        />
                                                                                                    </div>
                                                                                                </section>
                                                                                                <div
                                                                                                    className="clearfix"/>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="clearfix"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="clearfix"/>
                        </div>
                    </div>
                    <div className="par parsys"/>
                </div>
            </div>
        );
    }
}


const mapStateToProps = state => {
    const {config, staticReducer} = state;
    return {
        lang: config.language,
        staticReducer: staticReducer
    };
};

const mapDispatch = {
    loadStatic
};

export default connect(mapStateToProps, mapDispatch)(Contact);
