import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './style.scss';
import { FaFacebookF, FaTwitter, FaYoutube, FaInstagram } from 'react-icons/fa';
import logo from '../../assets/logo.png';

export default class Footer extends Component {
  render() {
    return (
      <footer className="site-footer border-top">
        <div className="container">
          <div className="row py-3">
            <div className="col-12 col-md mt-3">
              <Link id="logo" to="/">
                <img
                  src={logo}
                  width="80"
                  alt="Т.Шагдарсүрэнгийн Нэрэмжит Анагаах Ухааны Хүрээлэн"
                />
              </Link>
              <div className="d-block mt-3 text-muted address">
                Т.Шагдарсүрэнгийн Нэрэмжит Анагаах Ухааны Хүрээлэн.
              </div>
            </div>
            <div className="col-6 col-md">
              <h5 className="section-title">Сошиал холбоос</h5>
              <ul className="list-inline mb-0">
                <li className="list-inline-item">
                  <a className="u-icon rounded-circle" href="#">
                    <FaFacebookF />
                  </a>
                </li>
                <li className="list-inline-item">
                  <a className="u-icon rounded-circle" href="#">
                    <FaTwitter />
                  </a>
                </li>
                <li className="list-inline-item">
                  <a className="u-icon rounded-circle" href="#">
                    <FaYoutube />
                  </a>
                </li>
                <li className="list-inline-item">
                  <a className="u-icon rounded-circle" href="#">
                    <FaInstagram />
                  </a>
                </li>
              </ul>
            </div>
            <div className="col-6 col-md">
              <h5 className="section-title">Холбоо барих</h5>
              <div className="mb-3">
                <address className="mb-0">
                  <span className="d-block u-text-light mb-1">
                    +(976) 70287809
                  </span>
                  <span className="d-block u-text-light mb-1">
                    contact@imsm.mn
                  </span>
                  <span className="d-block u-text-light mb-1 address">
                    Монгол улс, Улаанбаатар хот 16081, Баянгол дүүрэг, Ард
                    Аюушийн гудамж-1, Улсын Гуравдугаар Төв Эмнэлэг
                  </span>
                </address>
              </div>
            </div>
          </div>
          <div className="d-block u-text-light copyright">
            Т.Шагдарсүрэнгийн Нэрэмжит Анагаах Ухааны Хүрээлэн | Бүх эрх хуулиар
            хамгаалагдсан © 2018
          </div>
        </div>
      </footer>
    );
  }
}
