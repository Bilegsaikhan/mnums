'use strict'
import { Product, ProductCat, ProductImage, ProductSubCat, Provider } from '../../../model'
import { format } from '../../../modules/formatter'
export default [
  async (req, res, next) => {
    req.checkQuery('limit', 'bad_limit').optional().isInt()
    req.checkQuery('offset', 'bad_offset').optional().isInt()
    req.checkQuery('product_cat_id', 'bad_product_cat_id').optional().isInt()
    req.checkQuery('provider_id', 'bad_provider_id').optional().isInt()
    req.checkQuery('product_sub_cat_id', 'bad_product_sub_cat_id').optional().isInt()
    req.checkQuery('string', 'bad_string').optional().len(1, 255)
    const err = req.validationErrors()
    if (err) return res.callback(err)
    const { limit, offset, product_cat_id, provider_id, product_sub_cat_id, string } = req.query
    const lang = req.session.lang || 'mn'

    try {
      const count = await Product.toCount({ lang, product_cat_id, product_sub_cat_id, string })
      if (count === 0) {
        return res.callback({
          total: 0,
          list: []
        })
      }

      const products = await Product.all({
        lang,
        offset,
        limit,
        product_cat_id,
        product_sub_cat_id,
        provider_id,
        string,
        include: [{
          model: ProductCat, as: 'productCat'
        }, {
          model: ProductImage, as: 'productSubCat'
        }, {
          model: ProductSubCat, as: 'images'
        }, {
          model: Provider, as: 'provider'
        }]
      }).then((list) => list.map(o => o.get({ plain: true })))

      res.data = {
        total: count,
        list: products
      }
      next()
    } catch (error) {
      next(error);
    }
  },
  format({
    list: [{
      name: true,
      desc: true,
      content: true,
    }]
  })
]
