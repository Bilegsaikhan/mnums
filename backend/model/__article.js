'use strict'
import inc from './include'
module.exports = function (sequelize, DataTypes) {

  return sequelize.define('article', {
    cat_id: DataTypes.INTEGER,
    title: DataTypes.STRING,
    desc: DataTypes.TEXT,
    content: DataTypes.TEXT,
    picture: DataTypes.STRING,
  }, {
    timestamps: true,
    classMethods: {
      toCount ({cat_id: catId, string}) {
        let where = []
        const attrs = []
        if (catId) {
          where.push('cat_id = ?')
          attrs.push(catId)
        }
        if (string) {
          where.push('(title like ? OR `desc` like ? OR content like ?)')
          const wildCardedString = `%${string}%`
          attrs.push(wildCardedString)
          attrs.push(wildCardedString)
          attrs.push(wildCardedString)
        }
        return this.count({
          where: where.length > 0 ? [where.join(' AND '), ...attrs] : undefined
        })
      },
      list ({offset = 0, limit = 10, cat_id: catId, string}) {
        const include = inc(sequelize.models)
        let where = []
        const attrs = []
        if (catId) {
          where.push('cat_id = ?')
          attrs.push(catId)
        }
        if (string) {
          where.push('(title like ? OR `desc` like ? OR content like ?)')
          const wildCardedString = `%${string}%`
          attrs.push(wildCardedString)
          attrs.push(wildCardedString)
          attrs.push(wildCardedString)
        }
        return this.findAll({
          offset: offset * limit,
          limit: parseInt(limit),
          where: where.length > 0 ? [where.join(' AND '), ...attrs] : undefined,
          order: 'created_at DESC',
          include: include({
            cat: true
          })
        })
      },
      all ({offset = 0, limit = 10, cat_id: catId, string}) {
        const include = inc(sequelize.models)
        let where = []
        const attrs = []
        if (catId) {
          where.push('cat_id = ?')
          attrs.push(catId)
        }
        if (string) {
          where.push('(title like ? OR `desc` like ? OR content like ?)')
          const wildCardedString = `%${string}%`
          attrs.push(wildCardedString)
          attrs.push(wildCardedString)
          attrs.push(wildCardedString)
        }
        return this.findAll({
          offset: offset * limit,
          limit: parseInt(limit),
          where: where.length > 0 ? [where.join(' AND '), ...attrs] : undefined,
          order: 'created_at DESC',
          include: include({
            cat: true
          })
        })
      }
    }
  })
}

