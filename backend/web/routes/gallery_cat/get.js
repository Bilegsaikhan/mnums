'use strict'
import { GalleryCat, GalleryItem } from '../../../model'
import Err from '../../../modules/err'

export default async function getCat (req, res) {
  req.checkParams('id', 'bad_id').isInt()
  const err = req.validationErrors()
  if (err) return res.callback(err)
  const {id} = req.params
  let cat = await GalleryCat.find({where: {id: id}, include: [{model: GalleryItem, as: 'images'}]})
  if (!cat) return res.callback(Err.notfound())
  res.callback(cat)
}
