import React, { Component } from 'react';
import { IoIosSearch, IoIosCopy } from 'react-icons/io';
import './style.scss';

export default class Loading extends Component {
  render() {
    return (
      <div className="searching-gif">
        <IoIosCopy className="doc" size="11em" />
        <IoIosSearch className="magnify" size="8em" />
      </div>
    );
  }
}
