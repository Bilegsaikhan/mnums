'use strict'
// import inc from './include'
const tableName = 'report'
module.exports = function (sequelize, DataTypes) {
  return sequelize.define(tableName, {
    mn: DataTypes.BOOLEAN,
    en: DataTypes.BOOLEAN,
    type: DataTypes.ENUM('year', 'audit'),
    year: DataTypes.STRING,
    name: DataTypes.STRING,
    name_en: DataTypes.STRING,
    pdf: DataTypes.STRING,
    pdf_en: DataTypes.STRING
  }, {
    timestamps: true,
    classMethods: {
      toCount ({lang, type, year, string}) {
        const where = []
        const attrs = []
        if (lang === 'mn') {
          where.push('mn is true')
        } else if (lang === 'en') {
          where.push('en is true')
        }
        if (year) {
          where.push('year = ?')
          attrs.push(year)
        }
        if (type) {
          where.push('type = ?')
          attrs.push(type)
        }
        if (string) {
          where.push('(name like ? OR name_en like ?)')
          attrs.push(`%${string}%`)
          attrs.push(`%${string}%`)
        }
        return this.count({
          where: where.length > 0 ? [where.join(' AND '), ...attrs] : undefined,
          order: 'created_at DESC'
        })
      },
      list ({offset = 0, limit = 10, lang, type, year, string}) {
        // const include = inc(sequelize.models)
        const where = []
        const attrs = []
        if (lang === 'mn') {
          where.push('mn is true')
        } else if (lang === 'en') {
          where.push('en is true')
        }
        if (year) {
          where.push('year = ?')
          attrs.push(year)
        }
        if (type) {
          where.push('type = ?')
          attrs.push(type)
        }
        if (string) {
          where.push('(name like ? OR name_en like ?)')
          attrs.push(`%${string}%`)
          attrs.push(`%${string}%`)
        }
        return this.findAll({
          where: where.length > 0 ? [where.join(' AND '), ...attrs] : undefined,
          offset: offset * limit,
          limit: parseInt(limit),
          order: 'created_at DESC',
          // include: include({
          //   pdf: true,
          //   pdf_en: true
          // })
        })
      },
      get (id) {
        // const include = inc(sequelize.models)
        return this.find({
          where: {id},
          // include: include({
          //   pdf: true,
          //   pdf_en: true
          // })
        })
      }
    }
  })
}
