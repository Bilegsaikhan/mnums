export class MenuService {
  constructor (server) {
    'ngInject';
    this.server = server
    this.pathPrefix = '/menu'
  }

  list () {
    return this.server.get(`${this.pathPrefix}/list`)
  }

  order ({ids}) {
    return this.server.put(`${this.pathPrefix}`, {ids})
  }

  edit (id) {
    return this.server.get(`${this.pathPrefix}/edit/${id}`)
  }

  update (data) {
    return this.server.put(`${this.pathPrefix}/${data.id}`, data)
  }

  create (data) {
    return this.server.post(`${this.pathPrefix}`, data)
  }

  delete (id) {
    return this.server.delete(`${this.pathPrefix}/${id}`)
  }
}
