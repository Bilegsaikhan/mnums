export class BannerController {
  constructor ($log, staticService, toastr) {
    'ngInject'
    this.log = $log.log
    this.banner = {
      key: 'finance_bundle'
    }
    this.staticService = staticService
    this.toastr = toastr
    this.load()
  }

  uploadDoneMn (image) {
    this.banner.data = image
  }

  uploadDoneEn (image) {
    this.banner.data_en = image
  }
  
  update() {
    this.staticService.update(this.banner).then(() => {
        this.toastr.success('Таны хүсэлт амжилттай!');
      }, () => {
        this.toastr.error('Таны хүсэлт амжилтгүй!');
      })
  }
  
  load() {
    this.staticService.get('finance_bundle').then(page => {
      this.banner = page
    }, () => {
      this.toastr.error('Таны хүсэлт амжилтгүй!');
    })
  }

}
