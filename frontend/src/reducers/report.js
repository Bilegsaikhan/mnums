import {
    LOAD_REPORT_LIST, LOAD_REPORT_LIST_SUCCESS, LOAD_REPORT_LIST_ERROR
} from "../constants";

const defaultState = {
    data: [],
    total: 0
};

// reducer
export default function report(state = defaultState, action) {
    switch (action.type) {

        case 'LOAD_REPORT_HOME_FILTER': {
            return {
                ...state
            }
        }

        case 'LOAD_REPORT_FILTER': {
            return {
                ...state
            }
        }

        case LOAD_REPORT_LIST: {
            return {
                ...state,
                isFetching: true,
                error: null,
            };
        }
        case LOAD_REPORT_LIST_SUCCESS: {
            const { payload } = action
            return {
                ...state,
                data: payload.list,
                total: payload.total,
                isFetching: false,
            };
        }
        case LOAD_REPORT_LIST_ERROR: {
            return {
                ...state,
                error: action.error,
                isFetching: false,
            };
        }

        default: {
            return state;
        }
    }
}