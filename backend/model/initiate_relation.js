/**
 * Created by enxtur on 3/20/17.
 */
const relations = require('./relations')
module.exports = function (models, shouldCamelize) {
  relations.forEach(function (obj) {
    obj.relations.forEach(function (relationInfo2) {
      let relationInfo = relationInfo2
      let name = shouldCamelize ? camelize(obj.name) : obj.name
      let belongsTo = shouldCamelize ? camelize(relationInfo.belongsTo) : relationInfo.belongsTo
      if (relationInfo.many) {
        models[name].belongsTo(models[belongsTo], {
          foreignKey: relationInfo.foreignKey,
          as: relationInfo.as
        })
        models[belongsTo].hasMany(models[name], {
          foreignKey: relationInfo.foreignKey,
          as: relationInfo.backas
        })
      } else {
        models[name].belongsTo(models[belongsTo], {
          foreignKey: relationInfo.foreignKey,
          as: relationInfo.as
        })
        models[belongsTo].hasOne(models[name], {
          foreignKey: relationInfo.foreignKey,
          as: relationInfo.backas
        })
      }
    })
  })
}
function camelize (string) {
  return string.split('_').map(function (word) {
    return word.charAt(0).toUpperCase() + word.slice(1)
  }).join('')
}
