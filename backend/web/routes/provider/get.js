'use strict'
import { Provider } from '../../../model'
import Err from '../../../modules/err'

export default async function getProvider (req, res) {
  req.checkParams('id', 'bad_id').isInt()
  const err = req.validationErrors()
  if (err) return res.callback(err)
  const {id} = req.params
  let cat = await Provider.findById(id)
  if (!cat) return res.callback(Err.notfound())
  res.callback(cat)
}
