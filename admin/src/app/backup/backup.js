export class BackUpController {
  constructor ($log, staticService, modalService, toastr) {
    'ngInject'
    this.$log = $log.log
    this.service = staticService
    this.toastr = toastr
    this.modalService = modalService
    this.backupDate = {}
    this.load()
  }

  load() {
    this.service.singleGet('backup').then(res => {
      this.backupDate = res
    })
  }

  backup() {
    this.modalService.danger('Та энд дарж датабаазаа архив хийхдээ итгэлтэй байна уу', true).then(() => {
      this.service.backup().then(() => {
        this.toastr.success('Таны хүсэлт амжилттай!');
        this.load()
      }, () => {
        this.toastr.error('Таны хүсэлт амжилтгүй!');
      })
    })
  }

}
