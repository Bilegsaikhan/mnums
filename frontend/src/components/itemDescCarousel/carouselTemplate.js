import React, { Component } from "react";
import PropTypes from "prop-types";
import OwlCarousel from "react-owl-carousel2";

class CarouselTemplate extends Component {
  static propTypes = {
    items: PropTypes.array
  };

  shouldComponentUpdate() {
    return false;
  }

  render() {
    let { items, itemDescCarousel, onChangeCarousel } = this.props;
    const options = {
      items: 1,
      nav: false,
      dots: false,
      startPosition: items && items.length - 1,
      mouseDrag: false,
      touchDrag: false
    };

    return (
      <OwlCarousel
        ref={node => {
          itemDescCarousel(node);
        }}
        options={options}
        events={{ onChanged: onChangeCarousel }}
      >
        { items &&
          items.map((item, index) => {
          return (
            <div className="no6v1IndepthItem" key={`imc_${index}`}>
              <div className="n06v1 ">
                <div className="n06v1-text">
                  <h3>{item.date}</h3>
                  {item.content}
                </div>
              </div>
            </div>
          );
        })
        }
      </OwlCarousel>
    );
  }
}

export default CarouselTemplate;
