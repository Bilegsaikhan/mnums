import React, { Component } from 'react';
import { connect } from 'react-redux';

export class News extends Component {
  static propTypes = {};

  render() {
    const { data } = this.props;
    return (
      <div className="container">
        {data.map(item => (
          <div>{item.title}</div>
        ))}
      </div>
    );
  }
}

const mapStateToProps = state => {
  const {
    news: { newsData }
  } = state;
  return {
    data: newsData || []
  };
};

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(News);
