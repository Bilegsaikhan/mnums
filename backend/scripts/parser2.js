let request = require('request');
let cheerio = require('cheerio');
let fall = require('async-waterfall')
let fs = require('fs')
let uuid = require('uuid');

let uuidv1 = uuid.v1;

const pageString = 'page/'
const baseUrl = 'http://www.uyanbest.mn/product/wayne-12-volt-transfer-pump/'
let urls = [];

const between = (string) => {
  const i = string.indexOf('url(');
  const j = string.indexOf(')');
  return string.substring(i + 4, j)
}

// console.log(between('background-image: url("http://www.uyanbest.mn/wp-content/uploads/2017/01/27-12.jpg"); width: 415px; margin-right: 10px;'));

const download = (uri, filename, callback) => {
  request.head(uri, (err, res, body) => {
    request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
  })
}

const init = () => {
  let z = 0;
  fs.readFile('cc.json', (err, data) => {
    if (err) return console.log('error while opening json');
    fs.writeFile('dd.json', data, (err) => {
      if (err) return console.log('error while writing json')

      let jsonData = JSON.parse(data);
      let tasks = [];
  
  
      jsonData.forEach((item) => {
        if (item.images && item.images.length) {
          let images = [];

          item.images.forEach((image, index) => {
            if (image.indexOf('uyanbest') !== -1) {
              let task = (callback) => {
                console.log('~~~~~~~~~~started', z)
                try {
                  let name = uuidv1();
                  let suffix = image.split('.').pop();
                  let filename = `../../uploads/${name}.${suffix}`;
                  download(image, filename, (err) => {
                    if (err) throw err;
                    fs.readFile('dd.json', (err, ccData) => {
                      if (err) throw err;
                      let ccJsonData = JSON.parse(ccData);
                      ccJsonData.forEach((ccItem) => {
                        if (ccItem.images && ccItem.images.length) {
                          ccItem.images.forEach((ccImage, idx) => {
                            if (ccImage === image) {
                              ccItem.images[idx] = `/product_images/${name}.${suffix}`;
                            }
                          })
                        }
                      })
                      fs.writeFile('dd.json', JSON.stringify(ccJsonData, null, 2), (err) => {
                        if (err) throw err;
                        console.log('~~~~~~~~~~~~~~~~#####done', z)
                        z++;
                        callback()
                      })
                    })
                  })
                } catch (error) {
                  callback(error) 
                }
              }
              tasks.push(task);
            }
          })
        }  
      })

      fall(tasks, (err) => {
        if (err) {
          console.log('error occured', err)
        }
        console.log('done!!!!')
      })
    })
  })
}

init();
