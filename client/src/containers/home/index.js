import React, { Component } from 'react';
import { connect } from 'react-redux';
import Swiper from 'react-id-swiper';
import TextTruncate from 'react-text-truncate';
import { Link } from 'react-router-dom';
import './style.scss';

import bg1 from '../../assets/slider/5.jpg';
import spacer from '../../assets/spacer.gif';

import part1 from '../../assets/partners/1.png';
import part2 from '../../assets/partners/2.png';
import part3 from '../../assets/partners/3.png';

export class Home extends Component {
  static propTypes = {};
  params = {
    pagination: {
      el: '.swiper-pagination',
      type: 'bullets',
      clickable: false
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev'
    },
    spaceBetween: 30,
    autoHeight: true
  };

  partnersParams = {
    slidesPerView: 'auto',
    freeMode: true,
    pagination: {
      el: '.swiper-pagination',
      type: 'bullets',
      clickable: true
    },
    // navigation: false,
    spaceBetween: 30,
    autoHeight: true
  };
  render() {
    const { newsData } = this.props;
    return (
      <div className="home-wrapper">
        <div className="container pb-3 main-banner">
          <h2 className="section-title" title="Онцлох">
            Онцлох
          </h2>
          <Swiper {...this.params}>
            {newsData.map(item => (
              <div
                key={`mb_${item.id}`}
                className="row align-items-center banner-item"
              >
                <div className="col-md-8">
                  <div className="d-flex justify-content-center banner-img">
                    <img
                      style={{ backgroundImage: `url(${bg1})` }}
                      src={spacer}
                      alt=""
                    />
                  </div>
                </div>
                <div className="col-md-4">
                  <h5>{item.title}</h5>
                  <Link to={`/read/${item.id}`}>
                    <button className="btn btn-sm btn-primary">
                      Дэлгэрэнгүй
                    </button>
                  </Link>
                </div>
              </div>
            ))}
          </Swiper>
        </div>

        <div className="section-news py-3">
          <div className="container">
            <h2
              className="section-title d-flex flex-column flex-md-row align-items-end"
              title="Мэдээ мэдээлэл"
            >
              <span className="my-0 mr-md-auto">Мэдээ мэдээлэл</span>
              <Link
                to="/news"
                className="text-muted st-link my-2 my-md-0 mr-md-3"
              >
                бүх мэдээлэл »
              </Link>
            </h2>
            <div className="row">
              {newsData.map(item => (
                <div key={`tn_${item.id}`} className="col-md-4">
                  <div className="card flex-md-row mb-4 shadow-sm h-md-250">
                    <div className="card-body d-flex flex-column align-items-start">
                      <h3 className="mb-3">
                        <div className="date-item">
                          <span className="year">2018</span>
                          <span className="day">12</span>
                          <span className="month">6 сар</span>
                        </div>
                        <Link
                          className="news-title text-dark"
                          to={`/read/${item.id}`}
                        >
                          {item.title}
                        </Link>
                      </h3>
                      <TextTruncate
                        line={4}
                        truncateText="…"
                        text={item.body
                          .replace(/<\/?[^>]+(>|$)/g, '')
                          .replace(/&nbsp;|&ldquo;|&rdquo;/g, ' ')}
                        textTruncateChild={
                          <Link to={`/read/${item.id}`}>Дэлгэрэнгүй</Link>
                        }
                      />
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
        {/* <div className="partners py-3">
          <h2 className="section-title" title="Төсөл хөтөлбөрүүд">
            Төсөл хөтөлбөрүүд
          </h2>
        </div> */}
        <div className="partners py-3">
          <div className="container">
            <h2 className="section-title" title="Хамтрагч байгууллагууд">
              Хамтрагчид
            </h2>
            <div className="partner-logos">
              <Swiper {...this.partnersParams}>
                <a
                  className="logo-item"
                  href="https://www.mohs.mn/home"
                  target="_blank"
                  rel="noopener noreferrer"
                  title="Эрүүл мэндийн яам"
                >
                  <img src={part1} alt="" />
                </a>
                <a
                  className="logo-item"
                  href="https://mecss.gov.mn"
                  target="_blank"
                  rel="noopener noreferrer"
                  title="Боловсрол, соёл, шинжлэх ухаан, спортын яам"
                >
                  <img src={part2} alt="" />
                </a>
                <a
                  className="logo-item"
                  href="https://www.mnums.edu.mn/"
                  target="_blank"
                  rel="noopener noreferrer"
                  title="Анагаахын Шинжлэх Ухааны Үндэсний Их Сургууль"
                >
                  <img src={part3} alt="" />
                </a>
                <a
                  className="logo-item"
                  href="http://www.stf.mn/"
                  target="_blank"
                  rel="noopener noreferrer"
                  title="Шинжлэх ухаан технологийн сан"
                >
                  <img
                    src={`https://shilendans.gov.mn/bundles/ftp/images/d13a014c0a0265a73bd9461e64d66240.png`}
                    alt=""
                  />
                </a>
                <a
                  className="logo-item"
                  href="https://www.ac.mn/"
                  target="_blank"
                  rel="noopener noreferrer"
                  title="Шинжлэх ухааны академи"
                >
                  <img
                    src={`http://khureltogoot.mysa.mn/backend/image/original/rSsloqFRTf.png`}
                    alt=""
                  />
                </a>
              </Swiper>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  const {
    news: { newsData }
  } = state;
  return {
    newsData: newsData || []
  };
};

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
