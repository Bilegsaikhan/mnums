import { Article, Project, Menu } from '../../model'
const appId = '426142144425275'
const urlPrefix = 'http://dbm.mn'
const debug = require('../debug')(__dirname)
const title = 'モンゴルへようこそ'
const description = 'モンゴルへようこそ'
export default async (path) => {
  const meta = await generateMetaData(path)
  debug('meta', meta)
  return `
  <title>${meta.title}</title>
  <meta property="fb:app_id" content="${appId}" />
  <meta property="og:type"   content="article" />
  <meta property="og:url"    content="${meta.url}" />
  <meta property="og:title"  content="${meta.title}" />
  <meta property="og:description"  content="${meta.description}" />
  `
}

async function generateMetaData (path) {
  const [, module1, module2, module3] = path.split('/')
  debug(module1, module2, module3)
  let meta = null
  if (module1 === 'news' && parseInt(module2)) {
    debug('handling news detail meta')
    const id = parseInt(module2)
    meta = await generateArticleMetaData(id, path)
  } else if (module1 === 'project' && parseInt(module2)) {
    debug('handling project detail meta')
    const id = parseInt(module2)
    meta = await generateProjectMetaData(id, path)
  } else if (module1 || module2 || module3) {
    const module = module3 || module2 || module1
    debug('handling menu meta')
    meta = await generateMenuMetaData(module, path)
  }

  if (!meta) {
    meta = {
      url: `${urlPrefix}${path}`,
      title,
      description
    }
  }
  return meta
}
async function generateArticleMetaData (id, path) {
  const article = await Article.find({
    where: {id}
  })
  if (article) {
    const {title, desc: description} = article
    return {
      url: `${urlPrefix}/news/${id}`,
      title,
      description
    }
  }
  return null
}

async function generateProjectMetaData (id, path) {
  const project = await Project.find({
    where: {id}
  })
  if (project) {
    const {name: title, program: description} = project
    return {
      url: `${urlPrefix}/project/${id}`,
      title,
      description
    }
  }
  return null
}

async function generateMenuMetaData (module, path) {
  const menu = await Menu.find({
    where: {module}
  })
  if (menu) {
    const {name: title} = menu
    return {
      url: `${urlPrefix}${path}`,
      title,
      description
    }
  }
  return null
}
