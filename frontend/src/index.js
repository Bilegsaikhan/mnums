import React from "react";
import { render } from "react-dom";
import { browserHistory } from "react-router";
import { syncHistoryWithStore } from "react-router-redux";
import "bootstrap/dist/css/bootstrap.min.css";
import "normalize.css/normalize.css";
import "font-awesome/css/font-awesome.css";

import configureStore from "./store/configureStore";
import Root from "./containers/Root";
import "./variables.css";
import "./main.css";
// import './overwrite.css';
// import './overwrite1.css';
import "./index.css";
// import "./icon.css";

const store = configureStore();
const history = syncHistoryWithStore(browserHistory, store);

render(
  <Root store={store} history={history} />,
  document.getElementById("root")
);
