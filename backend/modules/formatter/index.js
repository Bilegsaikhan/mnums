const debug = require('../debug')(__dirname)
export function format (schema) {
  return (req, res) => {
    debug('starting format ..')
    const data = res.data
    const lang = req.session.lang || 'mn'
    const formatter = new Formatter(lang)
    const result = formatter.format(data, schema)
    res.callback(result)
  }
}
export class Formatter {
  constructor (lang = 'mn') {
    this.lang = lang
  }

  format (data, schema) {
    // // debug('format started with lang: ' + this.lang)
    // let data = null
    // if (Array.isArray(_data)) {
    //   // debug('array plaining')
    //   data = _data.map(obj => {
    //     // debug('iterating for plaining')
    //     if (typeof obj.get === 'function') {
    //       // debug('has get function')
    //       return obj.get({plain: true})
    //     } else {
    //       // debug('has not get function')
    //       return obj
    //     }
    //   })
    // } else if (typeof _data.get === 'function') {
    //   // debug('object plaining')
    //   data = _data.get({plain: true})
    // } else {
    //   // debug('already plain')
    //   data = _data
    // }
    // // // debug(data)
    this.format2(data, schema)
    return data
  }

  format2 (data, schema) {
    if (Array.isArray(data)) {
      this.formatArray(data, schema)
    } else {
      this.formatObject(data, schema)
    }
  }

  formatArray (array, schema) {
    array.forEach(obj => {
      this.formatObject(obj, schema[0])
    })
  }

  formatObject (object, schema) {
    for (let key in schema) {
      if (typeof schema[key] === 'object') {
        this.format2(object[key], schema[key])
      } else {
        this.formatField(object, key)
      }
    }
  }

  formatField (object, field) {
    const enField = `${field}_en`
    if (this.lang === 'en') {
      // debug(`swaping ${enField} to ${field}`)
      object[field] = object[enField]
      delete object[enField]
    } else if (this.lang === 'mn') {
      // debug(`deleting field: ${enField}`)
      delete delete object[enField]
    }
  }
}
