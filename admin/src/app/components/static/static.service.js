export class StaticService {
  constructor (server) {
    'ngInject';
    this.server = server
  }

  backup () {
    return this.server.get(`/backup`)
  }
  getHome () {
    return this.server.get(`/stat`)
  }
  getLogs () {
    return this.server.get(`/log`)
  }
  singleGet (key) {
    return this.server.get(`/store/${key}`)
  }
  get (key) {
    return this.server.get(`/store/edit/${key}`)
  }
  update (data) {
    return this.server.put(`/store/${data.key}`, data)
  }
}
