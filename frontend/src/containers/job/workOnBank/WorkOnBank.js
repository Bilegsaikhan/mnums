import React, {Component} from 'react';
import {connect} from "react-redux";
import language from "../../../constants/language"
import {loadStatic} from "../../../actions/static";
import './WorkOnBank.css';


class WorkOnBank extends Component {

    componentDidMount() {
        this.maybeFetchData();
    }

    maybeFetchData() {
        const {loadStatic} = this.props;
        loadStatic('job_dbm')
    }

    render() {
        const {lang, staticReducer} = this.props;
        return (
            <div className="row">
                <div className="col-md-12 col-sm-12 col-lg-12 col-xs-12">
                    <div className="">
                        <div className="row dbm-page-content">
                            <div className="col-md-12">
                                <h4 className="text-uppercase mrg-b-15">{language.workAtDBM[lang]}</h4>
                                <div
                                    dangerouslySetInnerHTML={{
                                        __html: staticReducer.job_dbm && staticReducer.job_dbm.data
                                    }}
                                />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-md-4 col-sm-12 col-lg-4 col-xs-12">
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    const {config, staticReducer} = state;
    return {
        lang: config.language,
        staticReducer: staticReducer
    };
};

const mapDispatch = {
    loadStatic
};

export default connect(mapStateToProps, mapDispatch)(WorkOnBank);